package cc.vv.party.quartz

import org.quartz.CronScheduleBuilder.cronSchedule
import org.quartz.JobBuilder
import org.quartz.JobKey
import org.springframework.stereotype.Component
import org.springframework.scheduling.quartz.SchedulerFactoryBean
import org.springframework.beans.factory.annotation.Autowired
import org.quartz.SchedulerException
import org.quartz.TriggerBuilder
import org.slf4j.LoggerFactory
import javax.annotation.PostConstruct


/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@Component
class QuartzScheduler {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    lateinit var schedulerFactoryBean: SchedulerFactoryBean

    private val logger by lazy { LoggerFactory.getLogger(QuartzScheduler::class.java) }

    @PostConstruct
    @Throws(SchedulerException::class)
    fun init() {
        scheduleJobs()
    }

    private fun scheduleJobs() {
        logger.info("=======任务初始化========");
        val scheduler = schedulerFactoryBean.scheduler;

        val checkCodeScanJob = JobBuilder.newJob(CheckCodeScanner::class.java)
            .withDescription("delete expired code")
            .withIdentity("job-check-code-scanner", "check-code")
            .build()

        val projectWarningScannerJob = JobBuilder.newJob(ProjectWarningScanner::class.java)
            .withDescription("update project state")
            .withIdentity("job-project-warning-state-scanner", "warning-project")
            .build()

        val projectCompleteScannerJob = JobBuilder.newJob(ProjectCompleteScanner::class.java)
                .withDescription("update project state")
                .withIdentity("job-project-complete-state-scanner", "complete-project")
                .build()

        //每间隔5分钟执行一次
        val checkCodeJobTrigger = TriggerBuilder.newTrigger()
            .forJob(checkCodeScanJob)
            .withSchedule(cronSchedule("0 /5 * * * ?"))
            .build()

        //每天晚上23整执行一次
        val projectWarningJobTrigger = TriggerBuilder.newTrigger()
            .forJob(projectWarningScannerJob)
            .withSchedule(cronSchedule("0 0 23 * * ?"))
            .build()

        //每天晚上01整执行一次
        val projectCompleteJobTrigger = TriggerBuilder.newTrigger()
                .forJob(projectCompleteScannerJob)
                .withSchedule(cronSchedule("0 0 01 * * ?"))
                .build()

        try {
            if (!scheduler.checkExists(JobKey.jobKey("job-check-code-scanner", "check-code"))) {
                scheduler.scheduleJob(checkCodeScanJob, checkCodeJobTrigger)
            }
            if (!scheduler.checkExists(JobKey.jobKey("job-project-warning-state-scanner", "warning-project"))) {
                scheduler.scheduleJob(projectWarningScannerJob, projectWarningJobTrigger)
            }
            if (!scheduler.checkExists(JobKey.jobKey("job-project-complete-state-scanner", "complete-project"))) {
                scheduler.scheduleJob(projectCompleteScannerJob, projectCompleteJobTrigger)
            }
            scheduler.start()
            logger.info("=======任务初始化完成========")
        } catch (e: SchedulerException) {
            e.printStackTrace()
        }
    }
}