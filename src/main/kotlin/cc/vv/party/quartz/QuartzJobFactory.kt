package cc.vv.party.quartz

import org.quartz.spi.TriggerFiredBundle
import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.scheduling.quartz.SpringBeanJobFactory
import org.springframework.stereotype.Component

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@Component
class QuartzJobFactory : SpringBeanJobFactory(), ApplicationContextAware {

    private var factory: AutowireCapableBeanFactory? = null

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        factory = applicationContext.autowireCapableBeanFactory
    }

    override fun createJobInstance(bundle: TriggerFiredBundle): Any {
        val job = super.createJobInstance(bundle)
        factory?.autowireBean(job)
        return job
    }
}