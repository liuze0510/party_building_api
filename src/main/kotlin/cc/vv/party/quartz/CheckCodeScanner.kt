package cc.vv.party.quartz

import cc.vv.party.beans.model.CheckCode
import cc.vv.party.service.CheckCodeService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.date.before
import commonx.core.date.dateFormat
import commonx.core.date.dateString
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.PersistJobDataAfterExecution
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 * **/
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
class CheckCodeScanner : Job {

    @Autowired
    lateinit var checkCodeService: CheckCodeService

    private val logger by lazy { LoggerFactory.getLogger(CheckCodeScanner::class.java) }

    @Transactional
    override fun execute(context: JobExecutionContext?) {
        val tenMinBefore = Date().before(Calendar.MINUTE, 10)
        logger.error(tenMinBefore.dateString())
        checkCodeService.delete(EntityWrapper<CheckCode>().le("create_time", tenMinBefore))
    }

}