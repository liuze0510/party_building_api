package cc.vv.party.quartz

import cc.vv.party.mapper.ProjectMapper
import cc.vv.party.mapper.VolunteerProjectMapper
import com.alibaba.fastjson.JSON
import org.apache.commons.collections.CollectionUtils
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.PersistJobDataAfterExecution
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * 项目预警
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
class ProjectWarningScanner : Job {

    @Autowired
    lateinit var projectMapper: ProjectMapper

    private val logger by lazy { LoggerFactory.getLogger(CheckCodeScanner::class.java) }

    override fun execute(context: JobExecutionContext?) {
        logger.debug("开始执行项目预警检测")

        var idList = projectMapper.selectWarningProjectList()
        if(CollectionUtils.isNotEmpty(idList)){
            logger.debug("需预警项目编号：" + idList.joinToString(","))

            projectMapper.batchUpdateProject(2, idList)
        }

        logger.debug("结束执行项目预警检测")
    }

}