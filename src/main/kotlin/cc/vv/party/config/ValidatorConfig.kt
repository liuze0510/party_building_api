package cc.vv.party.config

import org.hibernate.validator.HibernateValidator
import org.springframework.context.annotation.Bean
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor
import javax.validation.Validation
import javax.validation.Validator

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/9
 * @description
 **/
class ValidatorConfig {

    @Bean("validator")
    fun configValidator(): Validator {
        return Validation.byProvider(HibernateValidator::class.java)
                .configure()
                .failFast(true)
                .buildValidatorFactory()
                .validator
    }

    @Bean
    fun methodValidationPostProcessor(): MethodValidationPostProcessor {
        val postProcessor = MethodValidationPostProcessor()
        /**设置validator模式为快速失败返回*/
        postProcessor.setValidator(configValidator())
        return postProcessor
    }
}