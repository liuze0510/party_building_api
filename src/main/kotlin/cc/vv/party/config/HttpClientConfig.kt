package cc.vv.party.config

import cc.vv.party.processor.http.RemoteService
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description
 **/

class HttpClientConfig(
    @Value("\${yanan.pmc-base-url}") val baseUrl: String
) {

    private val retrofit by lazy { buildHttpClient() }

    private fun buildHttpClient(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


    @Bean("remoteService")
    fun createRemoteService(): RemoteService {
        return retrofit.create(RemoteService::class.java)
    }
}