package cc.vv.party.config.annotation

import cc.vv.party.config.ValidatorConfig
import org.springframework.context.annotation.Import

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/9
 * @description
 **/
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(ValidatorConfig::class)
@MustBeDocumented
annotation class ImportValidator