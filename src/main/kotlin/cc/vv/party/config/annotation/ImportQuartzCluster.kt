package cc.vv.party.config.annotation

import cc.vv.party.config.QuartzClusterConfig
import org.springframework.context.annotation.Import

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(QuartzClusterConfig::class)
@MustBeDocumented
annotation class ImportQuartzCluster