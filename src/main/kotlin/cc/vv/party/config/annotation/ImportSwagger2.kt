package cc.vv.party.config.annotation

import cc.vv.party.config.SwaggerConfig
import org.springframework.context.annotation.Import
import springfox.documentation.swagger2.annotations.EnableSwagger2


/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/8
 * @description
 **/
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@EnableSwagger2
@Import(SwaggerConfig::class)
@MustBeDocumented
annotation class ImportSwagger2