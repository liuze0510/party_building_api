package cc.vv.party.config.annotation

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/9
 * @description
 **/
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class NoArg