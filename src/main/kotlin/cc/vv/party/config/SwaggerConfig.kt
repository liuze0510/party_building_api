package cc.vv.party.config

import io.swagger.annotations.ApiOperation
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.ParameterBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.schema.ModelRef
import springfox.documentation.service.Parameter
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import java.util.*
import springfox.documentation.service.ApiInfo



/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/8
 * @description
 **/
@Import(BeanValidatorPluginsConfiguration::class)
open class SwaggerConfig {

    /**
     * 添加请求头
     * @return
     */
    private fun setHeaderToken(): List<Parameter> {
        val tokenPar = ParameterBuilder()
        val pars = ArrayList<Parameter>()
        tokenPar.name("X-Auth-Token")
            .description("token")
            .modelRef(ModelRef("string"))
            .parameterType("header")
            .required(false).build()
        pars.add(tokenPar.build())
        return pars
    }

    @Bean
    fun appApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation::class.java))
                .paths(PathSelectors.any())
                .paths(PathSelectors.ant("/api/**")).build().groupName("APP接口文档V1.0").pathMapping("/")
                .apiInfo(apiInfo("APP宝塔党建接口文档V1.0", "文档中可以查询及测试接口调用参数和结果", "1.0")).globalOperationParameters(setHeaderToken())
    }

    @Bean
    fun webApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation::class.java))
                .paths(PathSelectors.any())
                .paths(PathSelectors.ant("/web/**")).build().groupName("WEB接口文档V1.0").pathMapping("/")
                .apiInfo(apiInfo("WEB宝塔党建接口文档V1.0", "文档中可以查询及测试接口调用参数和结果", "1.0")).globalOperationParameters(setHeaderToken())
    }

    @Bean
    fun pcApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation::class.java))
                .paths(PathSelectors.any())
                .paths(PathSelectors.ant("/pc/**")).build().groupName("PC接口文档V1.0").pathMapping("/")
                .apiInfo(apiInfo("PC宝塔党建接口文档V1.0", "文档中可以查询及测试接口调用参数和结果", "1.0")).globalOperationParameters(setHeaderToken())
    }

    private fun apiInfo(name: String, description: String, version: String): ApiInfo {
        return ApiInfoBuilder().title(name).description(description).version(version).build()
    }
}