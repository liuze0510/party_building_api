package cc.vv.party.config

import com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor
import com.baomidou.mybatisplus.plugins.PaginationInterceptor
import org.mybatis.spring.annotation.MapperScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
@MapperScan("cc.vv.party.mapper")
class MyBatisPlusConfig {

    @Bean
    fun paginationInterceptor(): PaginationInterceptor {
        return PaginationInterceptor()
    }

    @Bean
    fun optimisticLockerInterceptor(): OptimisticLockerInterceptor {
        return OptimisticLockerInterceptor()
    }
}