package cc.vv.party.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession
import org.springframework.session.web.http.HeaderHttpSessionIdResolver
import org.springframework.transaction.PlatformTransactionManager
import javax.sql.DataSource


/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-11
 * @description spring http session  配置, session 12个小时过期时间
 **/
@Configuration
@EnableJdbcHttpSession(maxInactiveIntervalInSeconds = 60 * 60 * 24)
class SpringSessionConfig {

    @Bean
    fun httpSessionIdResolver(): HeaderHttpSessionIdResolver {
        return HeaderHttpSessionIdResolver.xAuthToken()
    }

    @Bean
    fun transactionManager(dataSource: DataSource): PlatformTransactionManager {
        return DataSourceTransactionManager(dataSource)
    }

}