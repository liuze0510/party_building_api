package cc.vv.party.processor.http

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description 延安党建接口访问接口
 **/
interface RemoteService {

    /** 登录接口 */
    @POST("mobile/login")
    fun login(@Query("account") account: String, @Query("password") password: String): Call<MutableMap<String, Any?>>

    /** 读取远程TXT文件接口 */
    @GET
    @Headers("Content-Type:text/html")
    fun getThreeLessonContent(@Url url: String): Call<ResponseBody>
}