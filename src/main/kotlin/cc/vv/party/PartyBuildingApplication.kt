package cc.vv.party

import cc.vv.party.config.annotation.ImportHttpClient
import cc.vv.party.config.annotation.ImportQuartzCluster
import cc.vv.party.config.annotation.ImportSwagger2
import cc.vv.party.config.annotation.ImportValidator
import org.springframework.boot.autoconfigure.AutoConfigurationPackage
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-10
 * @description
 **/
@ImportSwagger2
@ImportValidator
@ImportHttpClient
@ImportQuartzCluster
@EnableAsync
@EnableTransactionManagement
@SpringBootApplication
class PartyBuildingApplication

fun main(args: Array<String>) {
    runApplication<PartyBuildingApplication>(*args)
}