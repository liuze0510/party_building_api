package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 项目信息查询条件参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "项目信息查询条件参数")
data class ProjectListParam (

    @ApiModelProperty(value = "项目名称", dataType = "String")
    var projectName: String? = null,

    @ApiModelProperty(value = "项目类型 0 标准项目 1 文本项目 2 数值项目 3 包装项目 4物料无关的项目", dataType = "String")
    var projectType: String? = null,

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "项目系统状态", dataType = "Integer")
    var state: Integer? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null


)