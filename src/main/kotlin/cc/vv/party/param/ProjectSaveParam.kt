package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 项目信息保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "项目信息保存参数")
data class ProjectSaveParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "项目名称", dataType = "String")
    var projectName: String? = null,

    @ApiModelProperty(value = "项目介绍", dataType = "String")
    var projectIntroduction: String? = null,

    @ApiModelProperty(value = "项目类型 0 标准项目 1 文本项目 2 数值项目 3 包装项目 4物料无关的项目", dataType = "String")
    var projectType: String? = null,

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间", dataType = "String")
    var endTime: Long? = null,

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "申报时间", dataType = "Long")
    var declarationTime: Long? = null,

    @ApiModelProperty(value = "可见范围 0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区", dataType = "String")
    var visibleRange: String? = null,

    @ApiModelProperty(value = "预警时间", dataType = "Long")
    var warningTime: Long? = null,

    @ApiModelProperty(value = "完成时间", dataType = "Long")
    var completeTime: Long? = null,

    @ApiModelProperty(value = "状态 0 未开始 1 已开始 2 已完成", dataType = "Integer")
    var state: Integer? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "审核不通过原因", dataType = "String")
    var checkReson: String? = null,

    @ApiModelProperty(value = "项目阶段信息", dataType = "List")
    var segmentationList: List<ProjectSegmentationEditParam>? = null

)