package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 三会一课编辑参数信息
 * @version 1.0.0
 * @author: zhangx
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "三会一课编辑参数信息")
data class ThreeMeetOneLessonEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "类型", dataType = "String")
    var type: String? = null,

    @ApiModelProperty(value = "主题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: String? = null,

    @ApiModelProperty(value = "会议地点", dataType = "String")
    var meetingPlace: String? = null,

    @ApiModelProperty(value = "缺席人员", dataType = "String")
    var absentee: String? = null,

    @ApiModelProperty(value = "应到人数", dataType = "Integer")
    var numberPeople: Integer? = null,

    @ApiModelProperty(value = "实到人数", dataType = "Integer")
    var actualNumber: Integer? = null,

    @ApiModelProperty(value = "视频地址", dataType = "String")
    var video: String? = null,

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null
)

