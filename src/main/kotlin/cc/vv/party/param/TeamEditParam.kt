package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 四支队伍编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "四支队伍编辑参数")
data class TeamEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "姓名", dataType = "String")
    var pname: String? = null,

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null,

    @ApiModelProperty(value = "性别", dataType = "Integer")
    var sex: Integer? = null,

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    var age: Integer? = null,

    @ApiModelProperty(value = "学历 0 初中及以下 1 中专 2 高中 3 大专 4 本科 5 研究生及以上", dataType = "String")
    var education: String? = null,

    @ApiModelProperty(value = "所在镇", dataType = "String")
    var town: String? = null,

    @ApiModelProperty(value = "所在村", dataType = "String")
    var village: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "队伍类型 0 第一书记 1 村两委班子 2 扶贫工作队 3 驻村干部", dataType = "String")
    var teamType: String? = null,

    @ApiModelProperty(value = "职务", dataType = "String")
    var plant: String? = null


)