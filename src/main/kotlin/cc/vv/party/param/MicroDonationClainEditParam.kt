package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 微捐赠认领编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "微捐赠认领编辑参数信息")
data class MicroDonationClainEditParam (

    @ApiModelProperty(value = "微捐助编号", dataType = "String")
    var microDonationId: String? = null,

    @ApiModelProperty(value = "认领人", dataType = "String")
    var person: String? = null,

    @ApiModelProperty(value = "认领人联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "认领时间", dataType = "String")
    var time: String? = null,

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null,

    @ApiModelProperty(value = "认领备注", dataType = "String")
    var strDesc: String? = null
)

