package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 志愿者信息列表参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "志愿者信息列表参数")
data class VolunteerUserListParam (

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "性别", dataType = "Integer")
    var sex: Integer? = null,

    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    @ApiModelProperty(value = "志愿类型 0 定时性 1 临时性", dataType = "String")
    var volunteerType: String? = null


)