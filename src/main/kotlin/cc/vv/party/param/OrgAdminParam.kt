package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description
 **/
@ApiModel(value = "组织机构管理员参数")
@Validated
class OrgAdminParam {

    @ApiModelProperty("管理员姓名")
    @NotEmpty(message = "管理员姓名不能为空")
    var name: String? = null

    @ApiModelProperty("登录账号")
    @NotEmpty(message = "管理员账号不能为空")
    var account: String? = null

    @ApiModelProperty("角色名称")
    @NotEmpty(message = "角色名称不能为空")
    var roleType: String? = null
}