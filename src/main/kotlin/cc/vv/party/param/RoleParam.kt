package cc.vv.party.param

import cc.vv.party.common.constants.enums.RoleType
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-19
 * @description
 **/
@ApiModel
class RoleParam {

    @NotEmpty(message = "请输入角色名称")
    @ApiModelProperty("角色名称")
    var roleName: String? = null

    @NotNull(message = "请选择角色类型")
    @ApiModelProperty("角色类型")
    var roleType: RoleType? = null

    @NotEmpty(message = "请输入角色描述")
    @ApiModelProperty("角色描述")
    var description: String? = null

    @NotNull(message = "请选择权限")
    @Size(min = 1, message = "请选择权限")
    @ApiModelProperty("权限列表")
    var permissionList: List<String>? = null
}