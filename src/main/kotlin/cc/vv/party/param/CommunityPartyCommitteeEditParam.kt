package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 社区大党委编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "社区大党委编辑参数")
data class CommunityPartyCommitteeEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "党委名称", dataType = "String")
    var partyCommitteeName: String? = null,

    @ApiModelProperty(value = "召集人", dataType = "String")
    var convener: String? = null,

    @ApiModelProperty(value = "牵头负责单位", dataType = "String")
    var responsibleUnit: String? = null,

    @ApiModelProperty(value = "负责人姓名", dataType = "String")
    var principalName: String? = null,

    @ApiModelProperty(value = "负责人账号", dataType = "String")
    var principalAccount: String? = null,

    @ApiModelProperty(value = "党委简介", dataType = "String")
    var partyCommitteeyIntroduction: String? = null,

    @ApiModelProperty(value = "成员编号集合", dataType = "String")
    var memberList: List<String>? = null,

    @ApiModelProperty(value = "管理员信息集合", dataType = "String")
    var adminList: List<PartyCommitteeAdminEditParam>? = null


)