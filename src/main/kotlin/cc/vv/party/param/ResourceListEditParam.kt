package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 资源清单保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "资源清单保存参数")
data class ResourceListEditParam (

    @ApiModelProperty(value = "编号")
    var id: String? = null,

    @ApiModelProperty(value = "单位编号")
    var unitId: String? = null,

    @ApiModelProperty(value = "资源品类")
    var resourceType: String? = null,

    @ApiModelProperty(value = "资源内容")
    var conetnt: String? = null,

    @ApiModelProperty(value = "数量")
    var num: Integer? = null,

    @ApiModelProperty(value = "有无酬劳 0 无 1有")
    var reward: String? = null,

    @ApiModelProperty(value = "金额")
    var amount: String? = null,

    @ApiModelProperty(value = "开始时间")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间")
    var endTime: Long? = null
)