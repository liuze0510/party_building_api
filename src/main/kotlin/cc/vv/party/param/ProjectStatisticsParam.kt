package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * WEB端项目统计参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "WEB端项目统计参数")
data class ProjectStatisticsParam (

    @ApiModelProperty(value = "开始时间", dataType = "Long")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间", dataType = "Long")
    var endTime: Long? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null
)