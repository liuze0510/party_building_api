package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * WEB端报道统计参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "WEB端报道统计参数")
data class ReportStatisticsParam (

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "报道类型 0 党员 1 单位", dataType = "Integer")
    var type: Integer? = null
)