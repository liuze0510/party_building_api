package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 扶贫行政村信息
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "扶贫行政村编辑参数")
data class PovertyAlleviationVillageEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "父编号", dataType = "String")
    var parentId: String? = null,

    @ApiModelProperty(value = "名称", dataType = "String")
    var villageName: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null,

    @ApiModelProperty(value = "人口数量", dataType = "Integer")
    var population: Integer? = null,

    @ApiModelProperty(value = "耕地面积", dataType = "Float")
    var cultivatedArea: Float? = null,

    @ApiModelProperty(value = "主导产业", dataType = "String")
    var leadingIndustry: String? = null,

    @ApiModelProperty(value = "贫困户数量", dataType = "Integer")
    var povertyAlleviationNum: Integer? = null,

    @ApiModelProperty(value = "党员数量", dataType = "Integer")
    var partyNum: Integer? = null,

    @ApiModelProperty(value = "是否脱贫 0 未脱贫 1 已脱贫", dataType = "Integer")
    var poverty: Integer? = null,

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null

)