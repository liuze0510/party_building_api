package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * APP端民主和组织生活会列表查询参数
 * @version 1.0.0
 * @author: zhangx
 * @date 2018/11/12 11:23
 * @description
 */
@ApiModel(value = "APP端民主和组织生活会列表查询参数")
data class DemocraticAndOrgLifeMeetingListParam (

    @ApiModelProperty(value = "党委Id", dataType = "String")
    var partyCommitteeId: String? = null,

    @ApiModelProperty(value = "党工委Id", dataType = "String")
    var partyWorkCommitteeId: String? = null,

    @ApiModelProperty(value = "党总支Id", dataType = "String")
    var partyTotalBranchId: String? = null,

    @ApiModelProperty(value = "党支部Id", dataType = "String")
    var partyBranchId: String? = null,

    @ApiModelProperty(value = "开始时间", dataType = "Long")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间", dataType = "Long")
    var endTime: Long? = null

//    @ApiModelProperty(value = "召开类型 0 未召开 1 已召开", dataType = "Int")
//    var type: Int = 1
)
