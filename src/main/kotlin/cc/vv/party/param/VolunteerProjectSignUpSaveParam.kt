package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 志愿项目报名保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "志愿项目报名保存参数")
data class VolunteerProjectSignUpSaveParam (

//    @ApiModelProperty(value = "编号", dataType = "String")
//    var id: String? = null,

    @ApiModelProperty(value = "志愿项目编号", dataType = "String")
    var volunteerProjectId: String? = null,

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "性别", dataType = "Integer")
    var sex: Integer? = null,

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branch: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null


)