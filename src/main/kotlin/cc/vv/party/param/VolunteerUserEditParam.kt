package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 志愿者信息保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "志愿者信息保存参数")
data class VolunteerUserEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null,

    @ApiModelProperty(value = "性别 0 男 1 女", dataType = "Integer")
    var sex: Integer? = null,

    @ApiModelProperty(value = "出生日期", dataType = "Long")
    var dateofbirth: Long? = null,

    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    @ApiModelProperty(value = "职业", dataType = "String")
    var career: String? = null,

    @ApiModelProperty(value = "个人特长", dataType = "String")
    var specialty: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "志愿类型 0 定时性 1 临时性", dataType = "String")
    var volunteerType: String? = null,

    @ApiModelProperty(value = "志愿时间", dataType = "Long")
    var volunteerTime: Long? = null,

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null


)