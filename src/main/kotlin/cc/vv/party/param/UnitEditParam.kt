package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 单位信息编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "单位信息编辑参数")
data class UnitEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "单位编号", dataType = "String")
    var unitId: String? = null,

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null,

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "党员人数", dataType = "Integer")
    var partyNum: Integer? = null,

    @ApiModelProperty(value = "单位地址", dataType = "String")
    var unitAddr: String? = null,

    @ApiModelProperty(value = "邮编", dataType = "String")
    var zipCode: String? = null,

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "报道时间", dataType = "Long")
    var reportTime: Long? = null,

    @ApiModelProperty(value = "单位资源集合", dataType = "List")
    var resourceList: List<ResourceListEditParam>? = null
//    客户更改需求，单位项目按照资源清单中数据来
//    @ApiModelProperty(value = "单位项目集合", dataType = "List")
//    var resourceList: List<UnitProjectEditParam>? = null



)