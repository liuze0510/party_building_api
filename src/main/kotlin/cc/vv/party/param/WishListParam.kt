package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 微心愿列表
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "微心愿列表参数")
data class WishListParam (


    /**
     * 所属街道
     */
    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    /**
     * 所属社区
     */
    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    /**
    * 所属网格
    */
    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    /**
     * 发布人
     */
    @ApiModelProperty(value = "发布人")
    var name: String? = null,

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null,

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    var addr: String? = null,

    /**
     * 认领状态 0 未认领 1 已认领
     */
    @ApiModelProperty(value = "认领状态 0 未认领 1 已认领")
    var claimState: Integer? = null,

    /**
     * 审核状态 0 通过 1 不通过
     */
    @ApiModelProperty(value = "审核状态 0 通过 1 不通过")
    var checkState: Integer? = null


)