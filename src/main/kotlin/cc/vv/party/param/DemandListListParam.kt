package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 需求清单列表搜索参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "需求清单列表搜索参数")
data class DemandListListParam (

    /**
     * 所属街道
     */
    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    /**
     * 所属社区
     */
    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    /**
     * 所属网格
     */
    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    /**
     * 需求主题
     */
    @ApiModelProperty(value = "需求主题")
    var title: String? = null,

    /**
     * 单位名称
     */
    @ApiModelProperty(value = "单位名称")
    var companyName: String? = null,

    /**
     * 负责人
     */
    @ApiModelProperty(value = "负责人")
    var principal: String? = null,

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null



)