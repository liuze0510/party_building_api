package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "Banner编辑参数")
class BannerParam {

    @ApiModelProperty("图片地址")
    var id: String? = null

    @ApiModelProperty("标题")
    var title: String? = null

    @ApiModelProperty("图片地址")
    var cover: String? = null

    @ApiModelProperty("分类，0：党群新闻 1 党建概况  2 宝塔头条  3街道头条  4 社区头条 5 网格头条 ")
    var category: Int? = null

    @ApiModelProperty("内容")
    var content: String? = null

    @ApiModelProperty("外部链接")
    var url: String? = null

}

