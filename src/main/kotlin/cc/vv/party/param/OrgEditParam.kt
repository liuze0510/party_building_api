package cc.vv.party.param

import cc.vv.party.common.constants.SysConsts
import com.baomidou.mybatisplus.annotations.TableField
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.springframework.validation.annotation.Validated
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-31
 * @description
 **/
@ApiModel
@Validated
class OrgEditParam {
    @ApiModelProperty(value = "组织机构id，添加时不用传,更新时需要")
    var id: String? = null

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @NotEmpty(message = "组织机构名称不能为空")
    var name: String? = null
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    @Pattern(regexp = SysConsts.REGEX_PHONE_MOBILE, message = "组织机构电话格式不正确")
    var mobile: String? = null

    @ApiModelProperty("单元中心户位置，只有党员中心户时，存在该字段值")
    var location: String? = null

    /**
     * 简介
     */
    @ApiModelProperty("简介")
    var introduction: String? = null

    @ApiModelProperty("管理员列表")
    @NotNull(message = "请添加管理员")
    @Valid
    var adminList: List<OrgAdminParam>? = null
}