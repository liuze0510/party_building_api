package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 联席会议管理员编辑参数信息
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "联席会议管理员编辑参数信息")
data class StreetJointMeetingAdminEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "会议编号", dataType = "String")
    var meetingId: String? = null,

    @ApiModelProperty(value = "管理员姓名", dataType = "String")
    var adminName: String? = null,

    @ApiModelProperty(value = "管理员账号", dataType = "String")
    var adminAccount: String? = null,

    @ApiModelProperty(value = "所属单位", dataType = "String")
    var companyName: String? = null

)