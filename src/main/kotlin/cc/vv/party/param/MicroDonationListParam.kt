package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 微捐赠列表
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "微捐赠列表参数")
data class MicroDonationListParam (

    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    @ApiModelProperty(value = "捐赠人")
    var microDonationName: String? = null,

    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null,

    @ApiModelProperty(value = "地址")
    var addr: String? = null,

    @ApiModelProperty(value = "认领状态 0 未认领 1 已认领")
    var claimState: Integer? = null,

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过")
    var checkState: Integer? = null


)