package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 四支队伍签到参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "四支队伍签到参数")
data class TeamSignInParam (

    @ApiModelProperty(value = "队伍编号", dataType = "String")
    var teamId: String? = null,

    @ApiModelProperty(value = "签到类型 0 签到 1 签退", dataType = "String")
    var signType: String? = null,

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null




)