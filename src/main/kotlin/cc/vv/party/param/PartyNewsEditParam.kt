package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 党群新闻编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "党群新闻编辑参数")
data class PartyNewsEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "发布人", dataType = "String")
    var publisher: String? = null,

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branch: String? = null,

    @ApiModelProperty(value = "缩略图", dataType = "String")
    var thumbnail: String? = null,

    @ApiModelProperty(value = "视频", dataType = "String")
    var video: String? = null,

    @ApiModelProperty(value = "新闻内容", dataType = "String")
    var conetnt: String? = null


)