package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: zhangx
 * @date 2018/11/14 9:58
 * @description
 */
@ApiModel("单位报到提交资源保存参数")
data class ResourceListSaveList(

    @ApiModelProperty(value = "单位资源集合", dataType = "List")
    var resourceList: ArrayList<ResourceListEditParam>? = null
)
