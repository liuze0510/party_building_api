package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel
class OrgIntroductionParam {

    @NotEmpty(message = "组织结构Id不能为空")
    @ApiModelProperty("组织机构id")
    var id: String? = null

    @NotEmpty(message = "简介不能为空")
    @ApiModelProperty("简介")
    var introduction: String? = null
}