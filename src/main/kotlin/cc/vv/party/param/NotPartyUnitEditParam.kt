package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 非公党建单位编辑参数信息
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "非公党建单位编辑参数")
data class NotPartyUnitEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null,

    @ApiModelProperty(value = "组织类型 0 非公企业 1 非公党组织", dataType = "String")
    var type: Integer? = null,

    @ApiModelProperty(value = "单位简介", dataType = "String")
    var introduction: String? = null

)