package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 单位评星晋阶编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "单位评星晋阶编辑参数信息")
data class UnitEvaluationStarEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "组织类型 0 非公党组织 1 社会党组织", dataType = "String")
    var orgType: String? = null,

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var companyName: String? = null,

    @ApiModelProperty(value = "得分", dataType = "Float")
    var score: Float? = null,

    @ApiModelProperty(value = "单位评星晋阶明细", dataType = "List")
    var itemList: List<RatingDetailsEditParam>? = null
)

