package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 街道联席会议编辑参数信息
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "街道联席会议编辑参数信息")
data class StreetjointMeetingEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "会议名称", dataType = "String")
    var meetingName: String? = null,

    @ApiModelProperty(value = "会议成员", dataType = "List")
    var meetingMemberList: List<String>? = null,

    @ApiModelProperty(value = "召集人", dataType = "String")
    var convener: String? = null,

    @ApiModelProperty(value = "牵头负责单位", dataType = "String")
    var responsibleUnit: String? = null,

    @ApiModelProperty(value = "负责人姓名", dataType = "String")
    var principalName: String? = null,

    @ApiModelProperty(value = "负责人账号", dataType = "String")
    var principalAccount: String? = null,

    @ApiModelProperty(value = "会议简介", dataType = "String")
    var meetingIntroduction: String? = null,

    @ApiModelProperty(value = "会议管理员", dataType = "List")
    var streetJointMeetingAdmin: List<StreetJointMeetingAdminEditParam>? = null

)