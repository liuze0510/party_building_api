package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 志愿项目保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "志愿项目保存参数")
data class VolunteerProjectEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "项目名称", dataType = "String")
    var projectName: String? = null,

    @ApiModelProperty(value = "举办时间", dataType = "Long")
    var holdingTime: Long? = null,

    @ApiModelProperty(value = "举办单位", dataType = "String")
    var holdingCompany: String? = null,

    @ApiModelProperty(value = "举办地点", dataType = "String")
    var venue: String? = null,

    @ApiModelProperty(value = "项目名额", dataType = "Integer")
    var projectQuota: Integer? = null,

    @ApiModelProperty(value = "项目介绍", dataType = "String")
    var projectIntroduction: String? = null


)