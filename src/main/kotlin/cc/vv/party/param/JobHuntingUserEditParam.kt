package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 求职信息保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "求职信息保存参数")
data class JobHuntingUserEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null,

    @ApiModelProperty(value = "性别", dataType = "Integer")
    var sex: Integer? = null,

    @ApiModelProperty(value = "出生日期", dataType = "Long")
    var dateofbirth: Long? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "工作年限", dataType = "Integer")
    var workYear: Integer? = null,

    @ApiModelProperty(value = "学历", dataType = "String")
    var education: String? = null,

    @ApiModelProperty(value = "求职意向", dataType = "String")
    var careerObjective: String? = null,

    @ApiModelProperty(value = "期望薪资", dataType = "String")
    var expectedSalary: String? = null,

    @ApiModelProperty(value = "技能标签", dataType = "String")
    var skillTag: String? = null,

    @ApiModelProperty(value = "工作经历", dataType = "String")
    var workExperience: String? = null,

    @ApiModelProperty(value = "可见范围0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区", dataType = "String")
    var visibleRange: String? = null


)