package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 党员报道信息列表参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "党员报道信息列表参数")
data class PartyReportListParam (


    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "报道网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null



)