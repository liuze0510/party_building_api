package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 用户查询参数
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-24
 * @description
 **/
@ApiModel(value = "用户查询参数")
class UserListParam {

    @ApiModelProperty("姓名")
    var name: String? = null

    @ApiModelProperty("电话")
    var mobile: String? = null

    @ApiModelProperty("身份证")
    var card: String? = null

    @ApiModelProperty("性别 0 男 1女")
    var sex: String? = null

    @ApiModelProperty("所属街道")
    var street: String? = null

    @ApiModelProperty("所属社区")
    var community: String? = null

    @ApiModelProperty("所属网格")
    var grid: String? = null

    @ApiModelProperty("年龄起始")
    var ageStart: String? = null

    @ApiModelProperty("年龄结束")
    var ageEnd: String? = null
}