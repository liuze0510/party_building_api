package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 示范典型编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "示范典型编辑参数信息")
data class DemonstrationTypicalEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "类型 0 农村党组织  1 社区党组织  2 学校党组织  3 机关党组织 4 非公党组织  5 社会组织党组织 6 国家企事业单位党组织", dataType = "Integer")
    var type: Integer? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null,

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null
)

