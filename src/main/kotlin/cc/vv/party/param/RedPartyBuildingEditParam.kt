package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 红领党建编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "红领党建编辑参数信息")
data class RedPartyBuildingEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "类型: 0 品牌简介 1 六大工程 2 组织引领 3 党员引领 4 文化引领 5 标准引领", dataType = "Integer")
    var type: Integer? = null,

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null,

    @ApiModelProperty(value = "内容", dataType = "String")
    var conetnt: String? = null
)

