package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 政策文件编辑参数信息
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "政策文件编辑参数")
data class PolicyDocEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "来源", dataType = "String")
    var source: String? = null,

    @ApiModelProperty(value = "发布时间", dataType = "Long")
    var releaseTime: Long? = null,

    @ApiModelProperty(value = "发布内容", dataType = "String")
    var releaseContent: String? = null

)