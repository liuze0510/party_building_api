package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 单位信息编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "单位信息编辑参数")
data class UnitListParam (

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null,

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "报道类型 0 已报到 1 未报到", dataType = "Integer")
    var reportType: Integer? = null,

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null



)