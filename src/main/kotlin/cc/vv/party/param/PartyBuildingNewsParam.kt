package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel
class PartyBuildingNewsParam {

    @ApiModelProperty("头条Id")
    var id: String? = null

    @NotEmpty(message = "标题不能为空")
    @ApiModelProperty("标题")
    var title: String? = null

    @NotEmpty(message = "封面不能为空")
    @ApiModelProperty("封面")
    var cover: String? = null

    @NotEmpty(message = "消息来源不能为空")
    @ApiModelProperty("消息来源")
    var source: String? = null

    @NotEmpty(message = "内容不能为空")
    @ApiModelProperty("内容")
    var content: String? = null

    @NotEmpty(message = "头条类型不能为空")
    @ApiModelProperty("头条类型")
    var type: String? = null
}