package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 资源清单列表搜索参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "资源清单列表搜索参数")
data class ResourceListListParam (

    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    @ApiModelProperty(value = "所属单位")
    var companyName: String? = null

)