package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 需求清单保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "需求清单保存参数")
data class DemandListEditParam (

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    var id: String? = null,

    /**
     * 所属街道
     */
    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    /**
     * 所属社区
     */
    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    /**
    * 所属网格
    */
    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    /**
     * 单位编号
     */
    @ApiModelProperty(value = "单位编号")
    var unitId: String? = null,

    /**
     * 需求主题
     */
    @ApiModelProperty(value = "需求主题")
    var title: String? = null,

    /**
     * 需求内容
     */
    @ApiModelProperty(value = "需求内容")
    var conetnt: String? = null,

    /**
     * 负责人
     */
    @ApiModelProperty(value = "负责人")
    var principal: String? = null,

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null,

    /**
     * 有无酬劳 0 无 1有
     */
    @ApiModelProperty(value = "有无酬劳 0 无 1有")
    var reward: Integer? = null,

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    var amount: String? = null

)