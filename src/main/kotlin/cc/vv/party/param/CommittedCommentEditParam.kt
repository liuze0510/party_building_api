package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 承诺评价编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "承诺评价编辑参数")
data class CommittedCommentEditParam (

    /**
     * 承诺编号
     */
    @ApiModelProperty("id")
    var committedId: String? = null,
    /**
     * 评论人姓名
     */
    @ApiModelProperty("评论人姓名")
    var commentName: String? = null,
    /**
     * 评论人头像
     */
    @ApiModelProperty("评论人头像")
    var faceUrl: String? = null,
    /**
     * 评论内容
     */
    @ApiModelProperty("评论内容")
    var content: String? = null



)