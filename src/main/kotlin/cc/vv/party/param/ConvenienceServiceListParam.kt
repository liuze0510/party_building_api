package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 便民服务列表搜索参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "便民服务列表搜索参数")
data class ConvenienceServiceListParam (

    /**
     * 所属街道
     */
    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    /**
     * 所属社区
     */
    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    /**
    * 所属网格
    */
    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    /**
     * 服务类型
     */
    @ApiModelProperty(value = "服务类型")
    var type: String? = null,

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    var title: String? = null,

    /**
     * 编辑时间
     */
    @ApiModelProperty(value = "编辑时间")
    var editTime: Long? = null



)