package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 支部活动编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "支部活动编辑参数信息")
data class BranchActivityEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "活动类型", dataType = "String")
    var type: String? = null,

    @ApiModelProperty(value = "活动主题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "活动时间", dataType = "String")
    var activityTime: String? = null,

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityPlace: String? = null,

    @ApiModelProperty(value = "缺席人员", dataType = "String")
    var absentee: String? = null,

    @ApiModelProperty(value = "应到人数", dataType = "Integer")
    var numberPeople: Integer? = null,

    @ApiModelProperty(value = "实到人数", dataType = "Integer")
    var actualNumber: Integer? = null,

    @ApiModelProperty(value = "视频地址", dataType = "String")
    var video: String? = null,

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null
)

