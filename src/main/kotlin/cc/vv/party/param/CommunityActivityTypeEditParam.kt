package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 社区活动类型编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "社区活动类型编辑参数")
data class CommunityActivityTypeEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "类型名称", dataType = "String")
    var typeName: String? = null,

    @ApiModelProperty(value = "类型描述", dataType = "String")
    var typeDesc: String? = null


)