package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 用户编辑参数
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-24
 * @description
 **/
@ApiModel(value = "用户编辑参数")
class UserEditParam {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty("姓名")
    var name: String? = null

    @ApiModelProperty("电话")
    var mobile: String? = null

    @ApiModelProperty("头像")
    var faceUrl: String? = null

    @ApiModelProperty("身份证")
    var card: String? = null

    @ApiModelProperty("是否党员 0 时 1 不是")
    var party: Integer? = null

    @ApiModelProperty("性别 0 男 1 女")
    var sex: Integer? = null

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    var age: Integer? = null

    @ApiModelProperty("出生日期")
    var dateofbirth: Long? = null

    @ApiModelProperty("民族 0 汉族 1 少数民族")
    var ethnic: Integer? = null

    @ApiModelProperty("职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  8 其他职业")
    var career: String? = null

    @ApiModelProperty("文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上")
    var educationalLevel: Integer? = null

    @ApiModelProperty("学历 0 初中 1 职中 3 高中 3 高职  4 大专 5 本科 6 研究生 7 硕士 8 博士")
    var education: Integer? = null

    @ApiModelProperty("职称")
    var jobTitle: String? = null

    @ApiModelProperty("身份 0 学生 1 农民 2 工人 3  干部（含聘干）")
    var identity: Integer? = null

    @ApiModelProperty("工作单位")
    var workCompany: String? = null

    @ApiModelProperty("所属街道")
    var street: String? = null

    @ApiModelProperty("所属社区")
    var community: String? = null

    @ApiModelProperty("所属网格")
    var grid: String? = null

    @ApiModelProperty("是否报道 0 已报到 1 未报到")
    var reported: Integer? = null

//    @ApiModelProperty("报道时间")
//    var reportTime: Long? = null
}