package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 社区大党委活动编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "社区大党委活动编辑参数")
data class PartyActivityEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "社区党委编号", dataType = "String")
    var partyId: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null,

    @ApiModelProperty(value = "举办方", dataType = "String")
    var organizer: String? = null,

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityLocation: String? = null,

    @ApiModelProperty(value = "活动时间", dataType = "Long")
    var activityTime: Long? = null,

    @ApiModelProperty(value = "活动内容", dataType = "String")
    var activityDesc: String? = null


)