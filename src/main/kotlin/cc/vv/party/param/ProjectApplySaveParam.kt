package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 *
 *
 * @author: zhangx
 * @date 2018/12/18 16:48
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "项目延期申请编辑参数")
data class ProjectApplySaveParam (

   @ApiModelProperty(value = "参数对象", dataType = "List")
   var param: List<ProjectApplyEditParam>? = null

)