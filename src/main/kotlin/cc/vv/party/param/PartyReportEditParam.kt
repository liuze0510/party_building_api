package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 党员报道信息编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "党员报道信息编辑参数")
data class PartyReportEditParam(

    @ApiModelProperty(value = "延安党建中的UserId", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "报道网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "职业特点", dataType = "String")
    var professionalCharacteristics: String? = null,

    @ApiModelProperty(value = "工作时间", dataType = "Long")
    var workTime: Long? = null,

    @ApiModelProperty(value = "特长爱好", dataType = "String")
    var specialHobby: String? = null,

    @ApiModelProperty(value = "个人承诺", dataType = "String")
    var personalCommitment: String? = null,

    @ApiModelProperty(value = "是否愿意成为志愿者 0 愿意 1 不愿意", dataType = "Integer")
    var volunteer: Integer? = null


)