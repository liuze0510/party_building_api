package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 社会组织动态编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "社会组织动态编辑参数")
data class SocietyOrgDynamicEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "来源", dataType = "String")
    var source: String? = null,

    @ApiModelProperty(value = "社区编号", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "发布时间", dataType = "Long")
    var releaseTime: Long? = null,

    @ApiModelProperty(value = "发布组织编号", dataType = "String")
    var orgId: String? = null,

    @ApiModelProperty(value = "发布组织", dataType = "String")
    var releaseOrg: String? = null,

    @ApiModelProperty(value = "发布内容", dataType = "String")
    var releaseContent: String? = null


)