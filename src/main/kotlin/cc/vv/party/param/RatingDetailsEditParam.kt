package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty

/**
 * 单位评星晋阶明细编辑参数信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "单位评星晋阶明细编辑参数信息")
data class RatingDetailsEditParam (

    @ApiModelProperty(value = "明细项", dataType = "String")
    var itemId: String? = null,

    @ApiModelProperty(value = "得分", dataType = "Float")
    var score: Float? = null
)

