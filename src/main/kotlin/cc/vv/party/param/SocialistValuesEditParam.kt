package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 社会主义核心价值观编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "社会主义核心价值观编辑参数")
data class SocialistValuesEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "类型 0 文明 1 和谐 2 敬业  3 友善 4 诚信", dataType = "Integer")
    var type: Integer? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "视频", dataType = "String")
    var video: String? = null,

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null,

    @ApiModelProperty(value = "内容", dataType = "String")
    var conetnt: String? = null,

    @ApiModelProperty(value = "来源", dataType = "String")
    var source: String? = null


)