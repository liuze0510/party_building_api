package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 脱贫行政村活动编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "脱贫行政村活动编辑参数")
data class PovertyAlleviationVillageActivityEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "区", dataType = "String")
    var area: String? = null,

    @ApiModelProperty(value = "镇", dataType = "String")
    var town: String? = null,

    @ApiModelProperty(value = "村", dataType = "String")
    var village: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null,

    @ApiModelProperty(value = "举办方", dataType = "String")
    var organizer: String? = null,

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityLocation: String? = null,

    @ApiModelProperty(value = "活动时间", dataType = "Long")
    var activityTime: Long? = null,

    @ApiModelProperty(value = "活动内容", dataType = "String")
    var activityDesc: String? = null


)