package cc.vv.party.param

import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.OrgType
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description
 **/
@ApiModel(value = "组织机构参数")
class OrgParam {

    @ApiModelProperty
    @NotEmpty(message = "缺少上级Id")
    var parentId: String? = null

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @NotEmpty(message = "组织机构名称不能为空")
    var name: String? = null
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    @Pattern(regexp = SysConsts.REGEX_PHONE_MOBILE, message = "组织机构电话格式不正确")
    var mobile: String? = null
    /**
     * 类型 0 区 1 街道 2 社区 3 网格 4 单位 5 社会组织 6 楼道党小组 7 普通党员 8 单元中心户
     */
    @ApiModelProperty("类型")
    @NotNull(message = "组织机构类型不能为空")
    var type: OrgType? = null
    /**
     * 路径
     */
    @ApiModelProperty("路径", hidden = true)
    var path: String? = null

    @ApiModelProperty("单元中心户位置，只有党员中心户时，存在该字段值")
    var location: String? = null

    /**
     * 简介
     */
    @ApiModelProperty("简介")
    var introduction: String? = null

    @ApiModelProperty("管理员列表")
    @NotNull(message = "请添加管理员")
    @Valid
    var adminList: List<OrgAdminParam>? = null
}