package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 用户统计参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "用户统计参数")
data class UserStatisticsParam (

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null,

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null,

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null,

    @ApiModelProperty(value = "人员类型 0 所有 1 党员 2 非党员", dataType = "Integer")
    var type: Integer? = null
)