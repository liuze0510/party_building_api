package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 保存微捐赠
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "微捐赠参数")
data class MicroDonationSaveParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "捐赠人", dataType = "String")
    var microDonationName: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null,

    @ApiModelProperty(value = "捐赠内容", dataType = "String")
    var microDonationConetnt: String? = null,

    @ApiModelProperty(value = "开始时间", dataType = "Long")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间", dataType = "Long")
    var endTime: Long? = null,

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "Integer")
    var checkState: Integer? = null,

    @ApiModelProperty(value = "审核不通过原因", dataType = "String")
    var checkReson: String? = null,

    @ApiModelProperty(hidden = true)
    var createUser: String? = null


)