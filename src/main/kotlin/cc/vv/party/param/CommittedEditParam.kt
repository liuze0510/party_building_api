package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 承诺编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "承诺编辑参数")
data class CommittedEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "承诺类型0 单位承诺 1 党员承诺", dataType = "Integer")
    var committedType: Integer? = null,

    @ApiModelProperty(value = "承诺单位名称，当承诺类型为单位是才需要填写", dataType = "String")
    var committedUnitName: String? = null,

    @ApiModelProperty(value = "关联编号， 单位承诺时，传入单位编号； 党员承诺时，传入党员编号", dataType = "Integer")
    var targetId: String? = null,

    @ApiModelProperty(value = "承诺内容", dataType = "String")
    var content: String? = null,

    @ApiModelProperty(value = "完成时间", dataType = "Long")
    var completeTime: Long? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null



)