package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 留言回复保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "留言回复保存参数")
data class MessageBoardReplyParam (

    /**
     * 留言编号
     */
    @ApiModelProperty(value = "留言编号")
    var messageBoardId: String? = null,

    /**
    * 回复单位
    */
    @ApiModelProperty(value = "回复单位")
    var content: String? = null,

    /**
     * 回复内容
     */
    @ApiModelProperty(value = "回复内容")
    var name: String? = null



)