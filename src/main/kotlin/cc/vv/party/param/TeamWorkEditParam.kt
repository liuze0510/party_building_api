package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 四支队伍工作编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "四支队伍工作编辑参数")
data class TeamWorkEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "队伍编号", dataType = "String")
    var teamId: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null,

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null


)