package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 项目延期、取消申请编辑参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "项目延期、取消申请编辑参数")
data class ProjectApplyEditParam (

    /** 项目编号 */
    @ApiModelProperty(value = "项目编号", dataType = "String")
    var proId: String? = null,

    /** 阶段编号 */
    @ApiModelProperty(value = "阶段编号", dataType = "String")
    var segId: String? = null,

    /** 申请类型 0 项目 1 阶段 */
    @ApiModelProperty(value = "申请类型 0 项目 1 阶段", dataType = "Int")
    var type: Integer? = null,

    /** 预警时间 */
    @ApiModelProperty(value = "预警时间" , dataType = "Long")
    var warningTime: Long? = null,

    /** 完成时间 */
    @ApiModelProperty(value = "完成时间", dataType = "Long")
    var completeTime: Long? = null,

    /** 申请理由 */
    @ApiModelProperty(value = "申请理由", dataType = "String")
    var reason: String? = null


)