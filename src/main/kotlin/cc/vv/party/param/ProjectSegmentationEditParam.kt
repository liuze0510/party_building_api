package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 项目阶段信息保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "项目阶段信息保存参数")
data class ProjectSegmentationEditParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "项目编号", dataType = "String")
    var projectId: String? = null,

    @ApiModelProperty(value = "阶段", dataType = "String")
    var stage: String? = null,

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null,

    @ApiModelProperty(value = "预警时间", dataType = "Long")
    var warningTime: Long? = null,

    @ApiModelProperty(value = "完成时间", dataType = "Long")
    var completeTime: Long? = null,

    @ApiModelProperty(value = "状态 0 未开始 1 已开始 2 已完成", dataType = "Integer")
    var state: Integer? = null

)