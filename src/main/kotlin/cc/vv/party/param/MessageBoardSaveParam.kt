package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 留言保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "留言保存参数")
data class MessageBoardSaveParam (


    /**
     * 留言人姓名
     */
    @ApiModelProperty(value = "留言人姓名")
    var name: String? = null,

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null,

    /**
     * 党组织名称
     */
    @ApiModelProperty(value = "党组织名称")
    var title: String? = null,

    /**
     * 党组织名称
     */
    @ApiModelProperty(value = "党组织名称")
    var conetnt: String? = null,

    /**
     * 审核状态 0 通过 1 不通过
     */
    @ApiModelProperty(value = "审核状态 0 通过 1 不通过")
    var checkState: Integer? = null


)