package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 微心愿保存
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "微心愿保存参数")
data class WishSaveParam (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "发布人", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null,

    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null,

    @ApiModelProperty(value = "心愿内容", dataType = "String")
    var wishConetnt: String? = null,

    @ApiModelProperty(value = "开始时间", dataType = "Long")
    var startTime: Long? = null,

    @ApiModelProperty(value = "结束时间", dataType = "Long")
    var endTime: Long? = null,

    @ApiModelProperty(value = "认领状态 0 未认领 1 已认领", dataType = "Integer")
    var claimState: Integer? = null,

    @ApiModelProperty(value = "认领人", dataType = "String")
    var clainPerson: String? = null,

    @ApiModelProperty(value = "认领人联系方式", dataType = "String")
    var clainMobile: String? = null,

    @ApiModelProperty(value = "认领时间", dataType = "Long")
    var clainTime: Long? = null,

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null,

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "Integer")
    var checkState: Integer? = null,

    @ApiModelProperty(value = "审核不通过原因", dataType = "String")
    var checkReson: String? = null,

    @ApiModelProperty(value = "微心愿认领备注", dataType = "String")
    var strDesc: String? = null,

    @ApiModelProperty(hidden = true)
    var createUser: String? = null


)