package cc.vv.party.param

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 留言列表
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "留言列表参数")
data class MessageBoardListParam (

    /**
     * 所属街道
     */
    @ApiModelProperty(value = "所属街道")
    var street: String? = null,

    /**
     * 所属社区
     */
    @ApiModelProperty(value = "所属社区")
    var community: String? = null,

    /**
    * 所属网格
    */
    @ApiModelProperty(value = "所属网格")
    var grid: String? = null,

    /**
     * 留言人姓名
     */
    @ApiModelProperty(value = "留言人姓名")
    var name: String? = null,

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null,

    /**
     * 党组织名称
     */
    @ApiModelProperty(value = "党组织名称")
    var branch: String? = null,

    /**
     * 审核状态 0 通过 1 不通过
     */
    @ApiModelProperty(value = "审核状态 0 通过 1 不通过")
    var checkState: Integer? = null


)