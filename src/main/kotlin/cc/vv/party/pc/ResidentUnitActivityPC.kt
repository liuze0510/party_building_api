package cc.vv.party.api;


import cc.vv.party.beans.vo.ResidentUnitActivityVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ResidentUnitActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 驻区单位活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Api(value = "驻区单位活动", tags = ["驻区单位活动网页端API"], description = "驻区单位活动相关接口")
@RestController
@RequestMapping("/pc/resident-unit-activity")
class ResidentUnitActivityPC : BaseController(){

    @Autowired
    lateinit var residentUnitActivityService: ResidentUnitActivityService

    @ApiOperation(value = "获取信息", notes = "获取信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ResidentUnitActivityVO> {
        var vo = residentUnitActivityService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "unitId", value = "单位", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) unitId: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ResidentUnitActivityVO>> {
        var page = residentUnitActivityService.listPage(street, community, unitId, title, size, page)
        return success(page)
    }


}
