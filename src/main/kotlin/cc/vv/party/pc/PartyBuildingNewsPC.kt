package cc.vv.party.pc;


import cc.vv.party.beans.model.PartyBuildingNews
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.PartyBuildingNewsVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.PartyBuildingNewsParam
import cc.vv.party.service.PartyBuildingNewsService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.*
import org.hibernate.validator.constraints.Range
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.constraints.Max
import javax.validation.constraints.NotEmpty

/**
 * <p>
 * 党建要闻 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/pc/party-building-news")
@Api(value = "党建要闻", tags = ["党建要闻网页端API"], description = "党建要闻网页端API-网页端党建要闻相关接口")
class PartyBuildingNewsPC : BaseController() {

    @Autowired
    lateinit var partyBuildingNewsService: PartyBuildingNewsService

    @ApiOperation("获取党建要闻列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "当前页", name = "page", required = true),
            ApiImplicitParam(value = "页大小", name = "pageSize", required = true),
            ApiImplicitParam(
                value = "头条类型, 0 宝塔头条 1 街道头条 2 社区头条 3 网格头条",
                name = "type",
                allowableValues = "range[0, 3]",
                required = true
            ),
            ApiImplicitParam(value = "街道", name = "street", required = false),
            ApiImplicitParam(value = "社区", name = "community", required = false),
            ApiImplicitParam(value = "网格", name = "grid", required = false),
            ApiImplicitParam(value = "发布时间", name = "publishTime", required = false),
            ApiImplicitParam(value = "标题", name = "title", required = false)
        ]
    )
    @GetMapping("/list")
    fun list(
        @RequestParam(required = false) street: String?,
        @RequestParam(required = false) community: String?, @RequestParam(required = false) grid: String?,
        @RequestParam(required = false) title: String?, @RequestParam(required = false) publishTime: String?,
        @Range(
            message = "类型不正确",
            max = 3,
            min = 0
        ) @RequestParam type: Int, @RequestParam page: Int,
        @Range(max = 50, min = 10, message = "分页大小范围为[10,50]") @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<PartyBuildingNewsVO>> {
        return success(partyBuildingNewsService.list(type, page, pageSize, title, publishTime, street, community, grid))
    }

    @ApiOperation("党建要闻详情")
    @ApiImplicitParam(value = "新闻Id", name = "id")
    @GetMapping("/detail/{id}")
    fun detail(@PathVariable id: String): ResponseEntityWrapper<PartyBuildingNews> {
        var entity = partyBuildingNewsService.selectById(id)
        entity.browseNum = Integer(entity.browseNum!!.toInt().plus(1))
        return success(entity)
    }

}
