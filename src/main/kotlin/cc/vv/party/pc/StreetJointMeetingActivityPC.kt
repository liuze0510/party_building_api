package cc.vv.party.api;


import cc.vv.party.beans.vo.StreetJointMeetingActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.StreetJointMeetingActivityEditParam
import cc.vv.party.service.StreetJointMeetingActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 街道联席会议活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
@Api(value = "街道联席会议活动", tags = ["街道联席会议活动网页端API"], description = "网页端街道联席会议活动相关接口")
@RestController
@RequestMapping("/pc/street-joint-meeting-activity")
class StreetJointMeetingActivityPC : BaseController(){

    @Autowired
    lateinit var streetJointMeetingActivityService: StreetJointMeetingActivityService


    @ApiOperation(value = "获取街道联席会议活动", notes = "获取街道联席会议活动")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<StreetJointMeetingActivityVO> {
        var vo = streetJointMeetingActivityService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取街道联席会议活动列表", notes = "获取街道联席会议活动列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<StreetJointMeetingActivityVO>> {
        var page = streetJointMeetingActivityService.listPage(null, null, null, null, size, page)
        return success(page)
    }


}
