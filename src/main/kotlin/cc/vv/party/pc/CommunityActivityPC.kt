package cc.vv.party.pc;


import cc.vv.party.beans.vo.CommunityActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.CommunityActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区活动", tags = ["社区活动网页端API"], description = "网页端社区活动相关接口")
@RestController
@RequestMapping("/pc/community-activity")
class CommunityActivityPC : BaseController() {

    @Autowired
    lateinit var communityActivityService: CommunityActivityService

    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityActivityVO> {
        var vo = communityActivityService.info(id)
        return success(vo)
    }

    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "time", value = "活动时间", required = false)),
                (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) type: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommunityActivityVO>> {
        var page = communityActivityService.listPage(null , null, time, type, title, size, page)
        return success(page)
    }

}
