package cc.vv.party.pc

import cc.vv.party.beans.model.LoginController
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.TerminalType
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.PartyReportService
import cc.vv.party.service.UserService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.collections.CollectionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.session.jdbc.JdbcOperationsSessionRepository
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.Pattern
import kotlin.streams.toList

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-30
 * @description
 **/
@Api(value = "用户", tags = ["用户网页端API"], description = "网页端用户相关接口")
@RestController
@RequestMapping("/pc/user")
class UserPC : BaseController() {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var repository: JdbcOperationsSessionRepository

    @ApiOperation("网页端用户登录")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "账号", name = "account"),
            ApiImplicitParam(value = "密码", name = "password")
        ]
    )
    @PostMapping("/login")
    fun login(
        @Pattern(
            regexp = SysConsts.REGEX_MOBILE,
            message = "账号格式不正确"
        ) @RequestParam account: String, @RequestParam password: String
    ): ResponseEntityWrapper<MutableMap<String, Any?>> {
        val result = userService.appUserLogin(account, password)
        val session = getSessionIfAbsent()
        result["token"] = session.id
        session.setAttribute(SysConsts.SESSION_KEY_USER_INFO, result)
        session.setAttribute(SysConsts.SESSION_KEY_USER_ID, result["userId"])
        session.setAttribute(SysConsts.SESSION_KEY_DEVICE, TerminalType.WEB)
        session.setAttribute(SysConsts.SESSION_KEY_USER_NAME, result["userName"])
        session.setAttribute(SysConsts.SESSION_KEY_USER_ACCOUNT, result["userAccount"])
        session.setAttribute(SysConsts.SESSION_KEY_USER_ROLE, result["userRole"])
        session.setAttribute(SysConsts.SESSION_KEY_YANAN_ROLE_ID, result["yananRole"])
        session.setAttribute(SysConsts.SESSION_KEY_APP_USER_ORG, result["userOrgList"])
        session.setAttribute(SysConsts.SESSION_KEY_LOCAL_USER_INFO, result["reportedPartyMemberInfo"])
        session.setAttribute(SysConsts.SESSION_KEY_CURRENT_BRANCH, result["currentBranchInfo"])
        return success(result)
    }

    @ApiOperation("注销")
    @PostMapping("/logout")
    fun logout(): ResponseEntityWrapper<*> {
        val session = getSession()
        session.invalidate()
        repository.deleteById(session.id)
        LoginController().delete(EntityWrapper<LoginController>().where("token = {0}", session.id))
        return success(null)
    }
}