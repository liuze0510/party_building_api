package cc.vv.party.pc

import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.vo.BannerVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-08
 * @description
 **/
@RestController
@RequestMapping("/pc/banner")
@Api(value = "网页端Banner接口", tags = ["Banner网页端API"], description = "Banner网页端API-网页端Banner相关接口")
class BannerPC : BaseController() {

    @Autowired
    lateinit var bannerService: BannerService

    @ApiOperation("获取Banner列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "当前页", name = "page"),
            ApiImplicitParam(value = "页大小", name = "pageSize"),
            ApiImplicitParam(value = "分类，0：党群新闻 1 党建概况  2 宝塔头条  3街道头条  4 社区头条 5 网格头条 ", name = "category")
        ]
    )
    @GetMapping("/list")
    fun list(@RequestParam page: Int, @RequestParam pageSize: Int, @RequestParam category: Int): ResponseEntityWrapper<PageWrapper<BannerVO>> {
        return success(
            bannerService.selectPage(
                Page(page, pageSize),
                EntityWrapper<Banner>().eq("category", category).orderBy("create_time", false)
            ).wrapper()
        )
    }

    @ApiOperation("Banner详情")
    @ApiImplicitParam(value = "banner id")
    @GetMapping("/detail/{id}")
    fun detail(@PathVariable id: String): ResponseEntityWrapper<BannerVO> {
        return success(bannerService.selectById(id).transfer())
    }
}
