package cc.vv.party.pc;


import cc.vv.party.beans.vo.MicroDonationVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.MicroDonationListParam
import cc.vv.party.service.MicroDonationService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 微捐赠 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "微捐助", tags = ["微捐赠网页端API"], description = "微捐赠网页端API-网页端微捐助相关接口")
@RestController
@RequestMapping("/pc/micro-donation")
class MicroDonationPC : BaseController(){

    @Autowired
    lateinit var microDonationService: MicroDonationService



    @ApiOperation(value = "获取捐赠信息", notes = "获取捐赠信息")
    @ApiImplicitParam(name = "id", value = "捐赠编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<MicroDonationVO>{
        var vo = microDonationService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取微捐赠列表", notes = "获取微捐赠列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MicroDonationVO>> {
        var page = microDonationService.listPage(MicroDonationListParam(), size, page)
        return success(page)
    }



}



