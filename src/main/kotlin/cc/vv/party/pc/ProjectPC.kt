package cc.vv.party.pc;


import cc.vv.party.beans.vo.ProjectVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ProjectService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 项目信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "项目信息", tags = ["项目信息网页端API"], description = "网页端项目信息相关接口")
@RestController
@RequestMapping("/pc/project")
class ProjectPC : BaseController() {

    @Autowired
    lateinit var projectService: ProjectService

    @ApiOperation(value = "获取项目信息", notes = "获取项目信息")
    @ApiImplicitParam(name = "id", value = "项目编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ProjectVO> {
        var vo = projectService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取项目信息列表", notes = "获取项目信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true)),
                (ApiImplicitParam(name = "type", value = "类型 0 未审核 1 已审核", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}/{type}")
    fun listPage(@PathVariable size: Int,
                 @PathVariable page: Int,
                 @PathVariable type: Int): ResponseEntityWrapper<PageWrapper<ProjectVO>> {
        var page = projectService.pcListPage(size, page, type)
        return success(page)
    }

}
