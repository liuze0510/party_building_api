package cc.vv.party.pc

import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ThreeMeetOneLessonService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-09
 * @description
 **/
@Api(value = "三会一课", tags = ["三会一课信网页端API"], description = "网页端三会一课信息相关接口")
@RestController
@RequestMapping("/pc/three-meet-one-lesson")
class ThreeLessonPC : BaseController() {

    @Autowired
    lateinit var threeMeetOneLessonService: ThreeMeetOneLessonService

    @ApiOperation(value = "获取三会一课信息", notes = "获取三会一课信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ThreeMeetOneLessonVO> {
        val vo = threeMeetOneLessonService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取三会一课信息列表", notes = "获取三会一课信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = ["/list/{page}/{pageSize}"])
    fun listPage(
        @PathVariable pageSize: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<ThreeMeetOneLessonVO>> {
        return success(threeMeetOneLessonService.pcListPage(page = page, pageSize = pageSize))
    }

}
