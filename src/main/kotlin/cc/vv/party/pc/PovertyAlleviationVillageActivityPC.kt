package cc.vv.party.api;


import cc.vv.party.beans.vo.PovertyAlleviationVillageActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.PovertyAlleviationVillageActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 脱贫行政村活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Api(value = "脱贫行政村活动", tags = ["脱贫行政村活动网页端API"], description = "脱贫行政村活动相关接口")
@RestController
@RequestMapping("/pc/poverty-alleviation-village-activity")
class PovertyAlleviationVillageActivityPC : BaseController(){

    @Autowired
    lateinit var povertyAlleviationVillageActivityService: PovertyAlleviationVillageActivityService

    @ApiOperation(value = "获取信息", notes = "获取信息")
    @ApiImplicitParam(name = "id", value = "岗位编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PovertyAlleviationVillageActivityVO> {
        var vo = povertyAlleviationVillageActivityService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "town", value = "镇", required = false)),
                (ApiImplicitParam(name = "village", value = "村", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) town: String?,
                 @RequestParam(required = false) village: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PovertyAlleviationVillageActivityVO>> {
        var page = povertyAlleviationVillageActivityService.listPage(town, village, title, size, page)
        return success(page)
    }

}
