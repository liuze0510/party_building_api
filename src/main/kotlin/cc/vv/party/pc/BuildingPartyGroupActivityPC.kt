package cc.vv.party.api;


import cc.vv.party.beans.vo.BuildingPartyGroupActivityVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.BuildingPartyGroupActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 楼栋党小组活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Api(value = "楼栋党小组活动", tags = ["楼栋党小组活动网页端API"], description = "楼栋党小组活动相关接口")
@RestController
@RequestMapping("/pc/building-party-group-activity")
class BuildingPartyGroupActivityPC : BaseController(){

    @Autowired
    lateinit var buildingPartyGroupActivityService: BuildingPartyGroupActivityService

    @ApiOperation(value = "获取信息", notes = "获取信息")
    @ApiImplicitParam(name = "id", value = "岗位编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<BuildingPartyGroupActivityVO> {
        var vo = buildingPartyGroupActivityService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "groupId", value = "楼栋党小组编号", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) groupId: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<BuildingPartyGroupActivityVO>> {
        var page = buildingPartyGroupActivityService.listPage(street, community, grid, groupId, title, size, page)
        return success(page)
    }

}
