package cc.vv.party.pc;


import cc.vv.party.beans.vo.MessageBoardVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.MessageBoardService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 留言板 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "留言板", tags = ["留言板网页端API"], description = "网页端留言板相关接口")
@RestController
@RequestMapping("/pc/message-board")
class MessageBoardPC : BaseController() {

    @Autowired
    lateinit var messageBoardService: MessageBoardService

    @ApiOperation(value = "获取留言板信息", notes = "获取留言板信息")
    @ApiImplicitParam(name = "id", value = "留言编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<MessageBoardVO> {
        var vo = messageBoardService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取留言板列表", notes = "获取留言板列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MessageBoardVO>> {
        var page = messageBoardService.pcListPage(size, page)
        return success(page)
    }

}
