package cc.vv.party.api;


import cc.vv.party.beans.vo.*
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.service.LearningFieldService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 学习园地 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "学习园地", tags = ["学习园地信息网页端API"], description = "学习园地信息相关接口")
@RestController
@RequestMapping("/pc/learning-field")
class LearningFieldPC : BaseController() {

    @Autowired
    lateinit var learningFieldService: LearningFieldService


    @ApiOperation(value = "获取微感悟信息列表", notes = "获取微感悟信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "branchType", value = "支部类型", required = true)),
            (ApiImplicitParam(name = "branchId", value = "支部编号", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/feel-list/{page}/{size}")
    fun findFeelListPage(
        @RequestParam branchType: String,
        @RequestParam branchId: String,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<FeelingVO>> {
        return success(learningFieldService.findFeelingPageList(branchType, branchId, page, size))
    }

    @ApiOperation(value = "获取党章党规列表", notes = "获取党章党规列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/party-constitution-list/{page}/{size}")
    fun findPartyConstitutionPageList(@PathVariable page: Int, @PathVariable size: Int): ResponseEntityWrapper<PageWrapper<PartyConstitutionVO>> {
        return success(learningFieldService.findPartyConstitutionPageList(page, size))
    }

    @ApiOperation(value = "获取讲话系列列表", notes = "获取讲话系列列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/series-speech-list/{page}/{size}")
    fun findSeriesSpeechPageList(@PathVariable page: Int, @PathVariable size: Int): ResponseEntityWrapper<PageWrapper<SeriesSpeechVO>> {
        return success(learningFieldService.findSeriesSpeechPageList(page, size))
    }

    @ApiOperation(value = "获取微百科科目集合", notes = "获取微百科科目集合")
    @PostMapping(value = "/subject-list")
    fun findSubjectList(): ResponseEntityWrapper<List<SubjectVO>> {
        return success(learningFieldService.findSubjectList())
    }

    @ApiOperation(value = "获取微百科列表", notes = "获取微百科列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "subjectId", value = "科目编号", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/open-class-list/{page}/{size}")
    fun findOpenClassList(@RequestParam subjectId: String, @PathVariable page: Int, @PathVariable size: Int): ResponseEntityWrapper<PageWrapper<OpenClassVO>> {
        return success(learningFieldService.findOpenClassList(subjectId, page, size))
    }


    @ApiOperation(value = "获取心得体会信息列表", notes = "获取心得体会信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "branchType", value = "支部类型", required = true)),
            (ApiImplicitParam(name = "branchId", value = "支部编号", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/experience-list/{page}/{size}")
    fun findExperiencePageList(
        @RequestParam branchType: String,
        @RequestParam branchId: String,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<ExperienceVO>> {
        return success(learningFieldService.findExperiencePageList(branchType, branchId, page, size))
    }

    @ApiOperation(value = "获取在线考试信息列表", notes = "获取在线考试信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/online-exam-list/{page}/{size}")
    fun findOnlineExamPageList(
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<OnlineExamVO>> {
        return success(learningFieldService.findOnlineExamPageList(page, size))
    }

    @ApiOperation(value = "获取在线考试试卷题目列表", notes = "获取在线考试试卷题目列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "blankId", value = "试题编号", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/question-list/{page}/{size}")
    fun findQuestionPageList(
        @RequestParam blankId: String,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<QuestionVO>> {
        return success(learningFieldService.findQuestionPageList(blankId, page, size))
    }

    @ApiOperation(value = "获取远程教育类型列表", notes = "远程教育类型列表 id 类型编号， type 类型名称")
    @PostMapping(value = "/education-type-list")
    fun findEducationTypeList(): ResponseEntityWrapper<List<Map<String, String>>> {
        return success(learningFieldService.findEducationTypeList())
    }

    @ApiOperation(value = "获取远程教育信息列表", notes = "获取远程教育信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "typeId", value = "类型编号", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @PostMapping(value = "/education-list/{page}/{size}")
    fun findEducationPageList(
        @RequestParam typeId: String,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<EducationVO>> {
        return success(learningFieldService.findEducationPageList(typeId, page, size))
    }

    @ApiOperation(value = "获取在线统计头部信息", notes = "获取在线统计头部信息")
    @PostMapping(value = "/experience-statistics-top")
    fun getExperienceStatisticsTop(): ResponseEntityWrapper<ExperienceStatisticsTopVO> {
        return success(ExperienceStatisticsTopVO())
    }

    @ApiOperation(value = "获取详情", notes = "获取详情")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "type", value = "类型 0 党规党章 1 讲话系列 2 微百科", required = true)),
            (ApiImplicitParam(name = "url", value = "地址", required = true))
        ]
    )
    @PostMapping(value = "/party-constitution-info")
    fun getMessageInfo(@PathVariable type: Int, @RequestParam url: String): ResponseEntityWrapper<String> {
        return success(learningFieldService.getMessageInfo(type, url))
    }

    /********  start 微感悟统计  *****************************************************************/
    @ApiOperation(value = "获取微感悟统计信息", notes = "获取微感悟统计信息")
    @GetMapping(value = "/feeling-statistics")
    fun getFeelingStatistics(): ResponseEntityWrapper<FeelingStatisticsVO> {
        return success(learningFieldService.getFeelingStatistics())
    }

    @ApiOperation(value = "获取微感悟数据趋势信息", notes = "获取微感悟数据趋势信息")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "startTime", value = "开始时间", required = false)),
            (ApiImplicitParam(name = "endTime", value = "结束时间", required = false))
        ]
    )
    @GetMapping(value = "/feeling-data-trend")
    fun getFeelingStatisticsDataTrend(@RequestParam startTime: Long?, @RequestParam endTime: Long?): ResponseEntityWrapper<FeelingStatisticsDataTrendVO> {
        return success(learningFieldService.getFeelingStatisticsDataTrend(startTime, endTime))
    }

    @ApiOperation(value = "获取微感悟数据详情信息", notes = "获取微感悟数据详情信息")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "time", value = "时间 格式:yyyy-MM", required = false)),
            (ApiImplicitParam(name = "branchId", value = "支部编号", required = false)),
            (ApiImplicitParam(name = "sumDesc", value = "按总数排序 0 降序 1 升序", required = true)),
            (ApiImplicitParam(name = "byDesc", value = "按通过数排序 0 降序 1 升序", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @GetMapping(value = "/feeling-data-detail/{page}/{size}")
    fun getByFeelingDataDetail(
        @RequestParam(required = false) time: String?,
        @RequestParam(required = false) branchId: String?,
        @RequestParam(defaultValue = "0") sumDesc: Int,
        @RequestParam(defaultValue = "0") byDesc: Int,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<FeelingDataDetailVO>> {
        return success(learningFieldService.getFeelingByDataDetail(time, branchId, sumDesc, byDesc, page, size))
    }
    /********  end   微感悟统计  *****************************************************************/

    /********  start 心得体会统计  *****************************************************************/
    @ApiOperation(value = "获取心得体会统计信息", notes = "获取心得体会统计信息")
    @GetMapping(value = "/experience-statistics")
    fun getExperienceStatistics(): ResponseEntityWrapper<ExperienceStatisticsVO> {
        return success(learningFieldService.getExperienceStatistics())
    }

    @ApiOperation(value = "获取心得体会数据趋势信息", notes = "获取心得体会数据趋势信息")
    @ApiImplicitParam(name = "date", value = "时间 格式:yyyy-MM", required = false)
    @GetMapping(value = "/experience-data-trend")
    fun getExperienceStatisticsDataTrend(@RequestParam date: Long?): ResponseEntityWrapper<ExperienceStatisticsDataTrendVO> {
        return success(learningFieldService.getExperienceStatisticsDataTrend(date))
    }

    @ApiOperation(value = "获取心得体会数据详情信息", notes = "获取心得体会数据详情信息")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "time", value = "时间 格式:yyyy-MM", required = false)),
            (ApiImplicitParam(name = "branchId", value = "支部编号", required = false)),
            (ApiImplicitParam(
                name = "branchType",
                value = "支部类型 PARTYCOMMITTEE: 党委; PARTYWORKCOMMITTEE: 党工委; PARTYTOTALBRANCH: 党总支; PARTY: 党支部",
                required = false
            )),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
        ]
    )
    @GetMapping(value = "/experience-data-detail/{page}/{size}")
    fun getByExperienceDataDetail(
        @RequestParam(required = false) time: String?,
        @RequestParam(required = false) branchId: String?,
        @RequestParam(required = false) branchType: String?,
        @PathVariable page: Int,
        @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<ExperienceDataDetailVO>> {
        return success(learningFieldService.getExperienceByDataDetail(time, branchId, branchType, page, size))
    }
    /********  end   心得体会统计  *****************************************************************/

    /********  start 考试统计  *****************************************************************/

    @ApiOperation(value = "考试统计头部数据", notes = "考试统计头部数据")
    @GetMapping("/exam-head-statistics")
    fun examTotalStatistics(): ResponseEntityWrapper<ExamTotalStatisticsVO> {
        val roleList = getCurrentUserRole()
//        if (roleList != null && roleList.size == 1 && roleList[0].type == RoleType.PARTY_MEMBER) {
//            throw BizException(StatusCode.FORBIDDEN)
//        }
        return success(learningFieldService.examTotalStatistics())
    }

    @ApiOperation(value = "考试统计趋势数据", notes = "考试统计趋势数据")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "type", value = "统计类型 0:全部，1，优秀人数 2，合格人数 3 不合格人数 4，优秀率 5，合格率，6，不合格率")),
            (ApiImplicitParam(name = "timeType", value = "时间类型 0：7天，1：15天，2：30天"))
        ]
    )
    @GetMapping("/exam-trend-statistics")
    fun examTrendStatistics(@RequestParam type: Int, @RequestParam timeType: Int): ResponseEntityWrapper<List<ExamTrendVO>> {
        val roleList = getCurrentUserRole()
//        if (roleList != null && roleList.size == 1 && roleList[0].type == RoleType.PARTY_MEMBER) {
//            throw BizException(StatusCode.FORBIDDEN)
//        }
        return success(learningFieldService.examTrendStatistics(type, timeType))
    }

    @ApiOperation(value = "获取在线考试题库信息列表", notes = "获取在线考试题库信息列表")
    @GetMapping(value = "/question-blank-list")
    fun findQuestionBlankList(): ResponseEntityWrapper<List<OnlineExamVO>> {
        return success(learningFieldService.findQuestionBlankList())
    }

    @ApiOperation(value = "获取在线考试数据详情信息", notes = "获取在线考试数据详情信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "blankId", value = "题库编号", required = false)),
                (ApiImplicitParam(name = "date", value = "时间 格式:yyyy-MM", required = false)),
                (ApiImplicitParam(name = "branchId", value = "支部编号", required = false)),
                (ApiImplicitParam(
                        name = "branchType",
                        value = "支部类型 PARTYCOMMITTEE: 党委; PARTYWORKCOMMITTEE: 党工委; PARTYTOTALBRANCH: 党总支;",
                        required = false
                )),
                (ApiImplicitParam(name = "sort", value = "排序 0 正序 1 倒序", required = false)),
                (ApiImplicitParam(name = "page", value = "页码", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true))
            ]
    )
    @GetMapping(value = "/online-exam-data-detail/{page}/{size}")
    fun findOnlineExamDataDetail(
            @RequestParam blankId: String,
            @RequestParam(required = false) date: String?,
            @RequestParam(required = false) branchId: String?,
            @RequestParam(required = false) branchType: String?,
            @RequestParam(defaultValue = "0") sort: Int,
            @PathVariable page: Int,
            @PathVariable size: Int
    ): ResponseEntityWrapper<PageWrapper<OnlineStudyDataDetailVO>> {
        return success(learningFieldService.findOnlineExamDataDetail(blankId, date, branchId, branchType, sort, page, size))
    }
    /********  end 考试统计  *****************************************************************/
}
