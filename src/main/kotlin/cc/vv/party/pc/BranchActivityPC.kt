package cc.vv.party.api;


import cc.vv.party.beans.vo.BranchActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.BranchActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 支部活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "支部活动", tags = ["支部活动信息网页端API"], description = "网页端支部活动信息相关接口")
@RestController
@RequestMapping("/pc/branch-activity")
class BranchActivityPC : BaseController() {

    @Autowired
    lateinit var branchActivityService: BranchActivityService


    @ApiOperation(value = "获取支部活动信息", notes = "获取支部活动信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<BranchActivityVO> {
        val vo = branchActivityService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取支部活动信息列表", notes = "获取支部活动信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{pageSize}")
    fun listPage(
        @PathVariable pageSize: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<BranchActivityVO>> {
        return success(branchActivityService.pcListPage(page, pageSize))
    }

}
