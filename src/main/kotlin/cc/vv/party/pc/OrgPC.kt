package cc.vv.party.pc;


import cc.vv.party.beans.model.Org
import cc.vv.party.beans.vo.NodeVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.OrgService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 组织机构 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Validated
@RestController
@Api(value = "组织机构", tags = ["组织机构网页端-API"], description = "组织机构网页端API-网页端组织机构相关接口")
@RequestMapping("/pc/org")
class OrgPC : BaseController() {

    @Autowired
    lateinit var orgService: OrgService


    @ApiOperation(value = "组织机构列表", notes = "组织机构列表")
    @GetMapping("/list")
    fun orgList(): ResponseEntityWrapper<OrgVO> {
        return success(orgService.getOrgList())
    }

    @ApiOperation(value = "组织机构详情", notes = "组织机构详情")
    @GetMapping("/detail")
    fun orgDetail(@RequestParam id: String): ResponseEntityWrapper<OrgVO> {
        return success(orgService.getOrgDetailById(id))
    }


    @ApiOperation(value = "获取宝塔区简介", notes = "获取宝塔区简介")
    @PostMapping("/bt-intro")
    fun getAreaIntro(): ResponseEntityWrapper<OrgVO> {
        val org = getOrg()
        return success(org.transfer())
    }

    @ApiOperation(value = "获取某一个类型的组织机构列表")
    @ApiImplicitParam(
        name = "type",
        value = "组织机构类型，可输入多个用','分割(0:社区，1:街道，2:社区，3:网格，4:单位，5:社会组织，6:楼栋党小组，7:党员，8:单元中心户，9:支部，10:街道联席会议，11:四支队伍，12:非公党建，13:社区大党委)"
    )
    @GetMapping("/org-list-by-type")
    fun orgListByType(@RequestParam type: List<Int>): ResponseEntityWrapper<List<OrgVO>> {
        return success(
            orgService.selectList(
                EntityWrapper<Org>().`in`(
                    "type",
                    type
                )
            ).transferEntries<OrgVO>() as List<OrgVO>
        )
    }

    /**
     * 获取数据库中默认的宝塔区组织机构
     */
    private fun getOrg(): Org {
        return orgService.selectById(SysConsts.BAOTA_AREA_ID)
    }

    @ApiOperation(value = "获取党组织机构列表", notes = "获取党组织机构列表")
    @ApiImplicitParams(
            value = [ApiImplicitParam(name = "type", value = "节点类型", required = false),
                ApiImplicitParam(name = "nodeId", value = "节点Id", required = false)]
    )
    @GetMapping("/party-node-list")
    fun partyNodeList(
            @RequestParam(required = false) type: String?,
            @RequestParam(required = false) nodeId: String?
    ): ResponseEntityWrapper<List<NodeVO>> {
        return success(orgService.findPartyNode(type, nodeId))
    }

    @ApiOperation(value = "获取党建简介", notes = "获取党建简介")
    @PostMapping("/get-party-introduction")
    fun getPartyIntroduction(): ResponseEntityWrapper<String> {
        var entity= orgService.selectById(SysConsts.BAOTA_AREA_ID)
        return success(entity.partyIntroduction)
    }
}
