package cc.vv.party.pc;


import cc.vv.party.beans.vo.CommunityPartyCommitteeVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.CommunityPartyCommitteeEditParam
import cc.vv.party.service.CommunityPartyCommitteeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区大党委  前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区大党委", tags = ["社区大党委网页端API"], description = "网页端社区大党委相关接口")
@RestController
@RequestMapping("/pc/community-party-committee")
class CommunityPartyCommitteePC : BaseController() {

    @Autowired
    lateinit var communityPartyCommitteeService: CommunityPartyCommitteeService

    @ApiOperation(value = "获取社区大党委信息", notes = "获取社区大党委信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityPartyCommitteeVO> {
        var vo = communityPartyCommitteeService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取社区大党委信息列表", notes = "获取社区大党委信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommunityPartyCommitteeVO>> {
        var page = communityPartyCommitteeService.listPage(null, null, null, size, page)
        return success(page)
    }
    
    
}
