package cc.vv.party.logger

import cc.vv.party.beans.model.Log
import cc.vv.party.service.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description
 **/
@Component
class AsyncRecorder {

    @Autowired
    lateinit var logService: LogService

    @Async
    fun writeLog(log: Log) {
        logService.insert(log)
    }


}