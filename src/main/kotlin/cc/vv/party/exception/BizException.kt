package cc.vv.party.exception

import cc.vv.party.common.constants.StatusCode

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-10
 * @description
 **/
class BizException(override val message: String?) : RuntimeException() {

    var statusCode = StatusCode.ERROR.statusCode

    constructor(statusCode: StatusCode) : this(statusCode.statusMessage) {
        this.statusCode = statusCode.statusCode
    }

    constructor(code: Int, message: String) : this(message) {
        this.statusCode = code
    }
}