package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 图片信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "图片信息")
class ImageVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "关联编号", dataType = "String")
    var associaId: String? = null

    @ApiModelProperty(value = "类型0 签到 1工作内容 2 三会一课 3 支部活动 4 扶贫行政村 5 民主生活会 6 组织生活会", dataType = "Integer")
    var type: Integer? = null

    @ApiModelProperty(value = "图片地址", dataType = "String")
    var url: String? = null

}
