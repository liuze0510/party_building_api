package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 心得体会数量统计信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "心得体会数量统计信息")
data class ExperienceStatisticsVO (

    @ApiModelProperty(value = "发布总数", dataType = "int")
    var allCount: Int? = null,

    @ApiModelProperty(value = "审核通过总数", dataType = "int")
    var allByCount: Int? = null,

    @ApiModelProperty(value = "月发布总数", dataType = "int")
    var monthAllCount: Int? = null,

    @ApiModelProperty(value = "月审核通过总数", dataType = "int")
    var monthByCount: Int? = null,

    @ApiModelProperty(value = "周发布总数", dataType = "int")
    var weekAllCount: Int? = null,

    @ApiModelProperty(value = "周审核通过总数", dataType = "int")
    var weekByCount: Int? = null,

    @ApiModelProperty(value = "日发布总数", dataType = "int")
    var dayAllCount: Int? = null,

    @ApiModelProperty(value = "日发布审核通过总数", dataType = "int")
    var dayByCount: Int? = null

)