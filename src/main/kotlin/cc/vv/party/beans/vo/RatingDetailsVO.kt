package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 单位评星晋阶明细信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "单位评星晋阶明细信息")
class RatingDetailsVO : BaseVo() {

    @ApiModelProperty(value = "明细项", dataType = "String")
    var itemId: String? = null

    @ApiModelProperty(value = "得分", dataType = "Float")
    var score: Float? = null

}
