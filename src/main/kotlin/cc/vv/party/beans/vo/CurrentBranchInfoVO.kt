package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-12
 * @description 延安党委、党工委、党总支、党支部 id
 **/
data class CurrentBranchInfoVO(
    /**
     * 党委Id
     */
    var partyCommitteeId: String?,
    var partyCommitteeName: String?,

    /**
     * 党工委Id
     */
    var partyWorkCommitteeId: String?,
    var partyWorkCommitteeName: String?,

    /**
     * 党总支Id
     */
    var partyTotalBranchId: String?,
    var partyTotalBranchName: String?,

    /**
     * 党支部Id
     */
    var partyBranchId: String?,
    var partyBranchName: String?,

    //部
    var orgMinistryId: String?,
    var orgMinistryName: String?

) : BaseVo()