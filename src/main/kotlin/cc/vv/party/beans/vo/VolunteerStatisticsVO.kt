package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 志愿统计
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "志愿统计信息")
class VolunteerStatisticsVO : BaseVo() {

    @ApiModelProperty(value = "男性数量 0", dataType = "Int")
    var maleNum: Int? = null

    @ApiModelProperty(value = "女性数量 1", dataType = "Int")
    var femaleNum: Int? = null

    @ApiModelProperty(value = "志愿者数量", dataType = "Int")
    var volunteerNum: Int? = null

    @ApiModelProperty(value = "项目数量", dataType = "Int")
    var proNum: Int? = null

    @ApiModelProperty(value = "志愿月数量集合", dataType = "json")
    var volunteer: LinkedHashMap<String, Int>? = null

    @ApiModelProperty(value = "志愿月数量集合", dataType = "json")
    var project: LinkedHashMap<String, Int>? = null
}