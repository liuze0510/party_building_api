package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-30
 * @description
 **/
@ApiModel
class ExamTrendVO : BaseVo() {

    @ApiModelProperty("日期")
    var dayTime: String? = null

    @ApiModelProperty("答题人数或者率")
    var answerUserCnt: String? = null
}