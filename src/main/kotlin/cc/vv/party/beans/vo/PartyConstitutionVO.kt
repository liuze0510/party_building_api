package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 党规党章信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "党规党章信息")
data class PartyConstitutionVO (

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "简介", dataType = "String")
    var snippetInfo: String? = null,

    @ApiModelProperty(value = "详情路径", dataType = "String")
    var url: String? = null


)