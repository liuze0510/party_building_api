package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 四支队伍工作信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "四支队伍工作信息")
class TeamWorkVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "队伍编号", dataType = "String")
    var teamId: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null

    @ApiModelProperty(value = "队伍信息", dataType = "TeamVO")
    var teamVO: TeamVO? = null

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createTime: Date? = null

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<String>? = null

}
