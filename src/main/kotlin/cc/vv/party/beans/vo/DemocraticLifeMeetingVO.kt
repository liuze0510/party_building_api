package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 民主生活会信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "民主生活会信息")
class DemocraticLifeMeetingVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "类型", dataType = "String")
    var type: String? = null

    @ApiModelProperty(value = "主题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null

    @ApiModelProperty(value = "浏览数", dataType = "Integer")
    var browseNum: Integer? = null

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Date? = null

    @ApiModelProperty(value = "地点", dataType = "String")
    var place: String? = null

    @ApiModelProperty(value = "缺席人员", dataType = "String")
    var absentee: String? = null

    @ApiModelProperty(value = "应到人数", dataType = "Integer")
    var numberPeople: Integer? = null

    @ApiModelProperty(value = "实到人数", dataType = "Integer")
    var actualNumber: Integer? = null

    @ApiModelProperty(value = "视频地址", dataType = "String")
    var video: String? = null

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgList: List<ImageVO>? = null

    @ApiModelProperty(value = "编辑时间", dataType = "List")
    var createTime: Date? = null

    @ApiModelProperty(value = "党委名称", dataType = "String")
    var partyCommitteeName: String? = null

    @ApiModelProperty(value = "党工委名称", dataType = "String")
    var partyWorkCommitteeName: String? = null

    @ApiModelProperty(value = "党总支名称", dataType = "String")
    var partyTotalBranchName: String? = null

    @ApiModelProperty(value = "党支部名称", dataType = "String")
    var partyBranchName: String? = null

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null

}
