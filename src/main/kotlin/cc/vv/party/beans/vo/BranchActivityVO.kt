package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 支部活动信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "支部活动信息")
class BranchActivityVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "活动类型", dataType = "String")
    var type: String? = null

    @ApiModelProperty(value = "活动主题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "可见范围", dataType = "String")
    var visibleScope: String? = null

    @ApiModelProperty(value = "所属区域", dataType = "String")
    var area: String? = null

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branchName: String? = null

    @ApiModelProperty(value = "活动时间", dataType = "String")
    var activityTime: Date? = null

    @ApiModelProperty(value = "阅读数", dataType = "String")
    var viewCount: Int = 0

    @ApiModelProperty(value = "点赞数", dataType = "String")
    var praiseCount: Int = 0

    @ApiModelProperty(value = "转发数", dataType = "String")
    var forwardCount: Int = 0

    @ApiModelProperty(value = "评论数", dataType = "String")
    var commentCount: Int = 0

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityPlace: String? = null

    @ApiModelProperty(value = "缺席人员", dataType = "String")
    var absentee: String? = null

    @ApiModelProperty(value = "应到人数", dataType = "Integer")
    var numberPeople: Integer? = null

    @ApiModelProperty(value = "实到人数", dataType = "Integer")
    var actualNumber: Integer? = null

    @ApiModelProperty(value = "图片集合", dataType = "List")
    var imgName: String? = null

    @ApiModelProperty(value = "视频名称", dataType = "String")
    var videoName: String? = null

    @ApiModelProperty(value = "视频真实名称，显示用", dataType = "String")
    var videoRealName: String? = null

    @ApiModelProperty("内容")
    var content: String? = null

    @ApiModelProperty("图片或视频存放目录")
    var folder: String? = null

    @ApiModelProperty("创建时间")
    var createTime: Date? = null

    @ApiModelProperty("是否推荐 0 已推荐 1 未推荐")
    var recommend: Integer? = null

    @ApiModelProperty("图片集合")
    var imageList: MutableList<String>? = null

    @ApiModelProperty("视频集合")
    var videoList: MutableList<String>? = null

    @ApiModelProperty("活动时间")
    var startTime: Date? = null

    @ApiModelProperty(value = "会议地点", dataType = "String")
    var meetingPlace: String? = null
}
