package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微百科信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微百科信息")
data class OpenClassVO (

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "支部名称", dataType = "String")
    var branchName: String? = null,

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    var createtime: Date? = null,

    @ApiModelProperty(value = "详情路径", dataType = "String")
    var url: String? = null


)