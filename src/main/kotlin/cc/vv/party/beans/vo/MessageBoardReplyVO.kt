package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 留言回复信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "留言回复信息")
class MessageBoardReplyVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "单位名称")
    var unit: String? = null

    @ApiModelProperty(value = "回复用户名称")
    var name: String? = null

    @ApiModelProperty(value = "回复用户头像")
    var faceUrl: String? = null

    @ApiModelProperty(value = "回复内容")
    var content: String? = null

    @ApiModelProperty(value = "回复时间", dataType = "String")
    var createTime: Date? = null


}
