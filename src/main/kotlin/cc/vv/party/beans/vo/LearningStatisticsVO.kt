package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-23
 * @description
 **/
@ApiModel
class LearningStatisticsVO : BaseVo() {

    @ApiModelProperty("编号")
    var id: String? = null

    @ApiModelProperty("名称")
    var name: String? = null

    @ApiModelProperty("开始时间")
    var beginTime: Date? = null

    @ApiModelProperty("结束时间")
    var endTime: Date? = null

    @ApiModelProperty("学习时长")
    var total: String? = null

    @ApiModelProperty("学习时长")
    var totalStr: String? = null

    @ApiModelProperty("节点类型")
    var type: String? = null
}