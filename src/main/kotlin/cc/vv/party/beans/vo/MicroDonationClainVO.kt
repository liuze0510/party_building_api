package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微捐赠认领信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微捐赠认领信息")
class MicroDonationClainVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "微捐助编号", dataType = "String")
    var microDonationId: String? = null

    @ApiModelProperty(value = "认领状态 0 未选中 1 已选中", dataType = "Integer")
    var seelected: Integer? = null

    @ApiModelProperty(value = "认领人", dataType = "String")
    var person: String? = null

    @ApiModelProperty(value = "认领人联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "认领时间", dataType = "String")
    var time: Date? = null

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null

    @ApiModelProperty(value = "认领备注", dataType = "String")
    var strDesc: String? = null

}
