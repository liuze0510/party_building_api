package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 项目阶段信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "项目阶段信息")
class ProjectSegmentationVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "项目编号", dataType = "String")
    var projectId: String? = null

    @ApiModelProperty(value = "阶段", dataType = "String")
    var stage: String? = null

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null

    @ApiModelProperty(value = "预警时间", dataType = "Long")
    var warningTime: Date? = null

    @ApiModelProperty(value = "完成时间", dataType = "Long")
    var completeTime: Date? = null

    @ApiModelProperty(value = "状态 0 未开始 1 已开始 2 已完成", dataType = "Integer")
    var state: Integer? = null

    @ApiModelProperty(value = "项目延期、取消申请信息", dataType = "List")
    var applyList: List<ProjectApplyVO>? = null

}
