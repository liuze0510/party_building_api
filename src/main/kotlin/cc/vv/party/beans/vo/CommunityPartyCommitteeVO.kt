package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 社区大党委信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社区大党委信息")
class CommunityPartyCommitteeVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "党委名称", dataType = "String")
    var partyCommitteeName: String? = null

    @ApiModelProperty(value = "召集人", dataType = "String")
    var convener: String? = null

    @ApiModelProperty(value = "牵头负责单位", dataType = "String")
    var responsibleUnit: String? = null

    @ApiModelProperty(value = "负责人姓名", dataType = "String")
    var principalName: String? = null

    @ApiModelProperty(value = "负责人账号", dataType = "String")
    var principalAccount: String? = null

    @ApiModelProperty(value = "党委简介", dataType = "String")
    var partyCommitteeyIntroduction: String? = null

    @ApiModelProperty(value = "党委管理员", dataType = "String")
    var adminList: List<PartyCommitteeAdminVO>? = null

    @ApiModelProperty(value = "党委组织成员", dataType = "List")
    var memberList: List<PartyOrgMemberVO>? = null


}
