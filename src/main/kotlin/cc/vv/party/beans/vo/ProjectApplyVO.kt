package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 项目延期、取消申请信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "项目延期、取消申请信息")
class ProjectApplyVO : BaseVo() {

    /** 编号 */
    @ApiModelProperty(value = "编号")
    var id: String? = null

    /** 项目编号 */
    @ApiModelProperty(value = "项目编号")
    var proId: String? = null

    /** 阶段编号 */
    @ApiModelProperty(value = "阶段编号")
    var segId: String? = null

    /** 申请类型 0 项目 1 阶段 */
    @ApiModelProperty(value = "申请类型 0 项目 1 阶段")
    var type: Integer? = null

    /** 预警时间 */
    @ApiModelProperty(value = "预警时间")
    var warningTime: Date? = null

    /** 完成时间 */
    @ApiModelProperty(value = "完成时间")
    var completeTime: Date? = null

    /** 申请理由 */
    @ApiModelProperty(value = "申请理由")
    var reason: String? = null

    /** 创建用户 */
    @ApiModelProperty(value = "创建用户")
    var createUser: String? = null

    @ApiModelProperty(value = "创建时间")
    var createTime: Date? = null

}
