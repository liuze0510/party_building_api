package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 志愿项目信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "志愿项目信息")
class VolunteerProjectVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "项目名称", dataType = "String")
    var projectName: String? = null

    @ApiModelProperty(value = "举办时间", dataType = "String")
    var holdingTime: Date? = null

    @ApiModelProperty(value = "举办单位", dataType = "String")
    var holdingCompany: String? = null

    @ApiModelProperty(value = "举办地点", dataType = "String")
    var venue: String? = null

    @ApiModelProperty(value = "项目名额", dataType = "Integer")
    var projectQuota: Integer? = null

    @ApiModelProperty(value = "报名人数", dataType = "Integer")
    var signupPeople: Integer? = null

    @ApiModelProperty(value = "项目介绍", dataType = "String")
    var projectIntroduction: String? = null

    @ApiModelProperty(value = "项目报名用户", dataType = "String")
    var volunteerProjectSignUpList: List<VolunteerProjectSignUpVO>? = null
}
