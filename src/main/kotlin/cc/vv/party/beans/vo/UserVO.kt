package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "用户信息")
class UserVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "电话", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null

    @ApiModelProperty(value = "身份证", dataType = "String")
    var card: String? = null

    @ApiModelProperty(value = "是否党员 0 是 1 不是", dataType = "Integer")
    var party: Integer? = null

    @ApiModelProperty(value = "性别 0 男 1 女", dataType = "Integer")
    var sex: Integer? = null

    @ApiModelProperty(value = "年龄", dataType = "Integer")
    var age: Integer? = null

    @ApiModelProperty(value = "出生日期", dataType = "String")
    var dateofbirth: Date? = null

    @ApiModelProperty(value = "民族 0 汉族 1 少数民族", dataType = "Integer")
    var ethnic: Integer? = null

    @ApiModelProperty(
        value = "职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  8 其他职业",
        dataType = "String"
    )
    var career: String? = null

    @ApiModelProperty(value = "文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上", dataType = "String")
    var educationalLevel: String? = null

    @ApiModelProperty(value = "学历 0 初中 1 职中 3 高中 3 高职  4 大专 5 本科 6 研究生 7 硕士 8 博士", dataType = "String")
    var education: String? = null

    @ApiModelProperty(value = "职称", dataType = "String")
    var jobTitle: String? = null

    @ApiModelProperty(value = "身份 0 学生 1 农民 2 工人 3  干部（汗聘干）", dataType = "String")
    var identity: String? = null

    @ApiModelProperty(value = "工作单位", dataType = "String")
    var workCompany: String? = null

    @ApiModelProperty(value = "所属区", dataType = "String")
    var area: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "是否报道 0 已报到 1 未报到", dataType = "String")
    var reported: String? = null

    @ApiModelProperty(value = "报道时间", dataType = "String")
    var reportTime: Date? = null

    @ApiModelProperty(value = "用户账号")
    var userAccount: String? = null
}
