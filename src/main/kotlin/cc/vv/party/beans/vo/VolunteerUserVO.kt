package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 志愿者信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "志愿者信息")
class VolunteerUserVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null

    @ApiModelProperty(value = "性别 0 男 1 女", dataType = "Int")
    var sex: Integer? = null

    @ApiModelProperty(value = "年龄", dataType = "Int")
    var age: Integer? = null

    @ApiModelProperty(value = "出生日期", dataType = "String")
    var dateofbirth: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "职业", dataType = "String")
    var career: String? = null

    @ApiModelProperty(value = "个人特长", dataType = "String")
    var specialty: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "志愿类型 0 定时性 1 临时性", dataType = "String")
    var volunteerType: String? = null

    @ApiModelProperty(value = "志愿时间", dataType = "String")
    var volunteerTime: Date? = null

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null


}
