package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel
class BannerVO : BaseVo() {

    @ApiModelProperty("Banner主键ID")
    var id: String? = null

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    var title: String? = null
    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    var cover: String? = null
    /**
     * 编辑时间
     */
    @ApiModelProperty("编辑时间")
    var editTime: Date? = null


    /**
     * 分类，0：党群新闻， 1： 党建要闻，2：工作活动，3：
     */
    @ApiModelProperty("分类")
    var category: Int? = null

    /**
     * 内容
     */
    @ApiModelProperty("内容")
    var content: String? = null

    /**
     * 外部链接
     */
    @ApiModelProperty("外部链接")
    var url: String? = null

}