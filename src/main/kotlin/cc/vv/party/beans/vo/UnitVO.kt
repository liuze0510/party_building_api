package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 单位信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "单位信息")
class UnitVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "单位编号", dataType = "String")
    var unitId: String? = null

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "党员人数", dataType = "Integer")
    var partyNum: Integer? = null

    @ApiModelProperty(value = "单位地址", dataType = "String")
    var unitAddr: String? = null

    @ApiModelProperty(value = "邮编", dataType = "String")
    var zipCode: String? = null

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "报道时间", dataType = "String")
    var reportTime: Date? = null

    @ApiModelProperty("是否报道，0 已报道，1 未报道")
    var reported: Integer? = null

    @ApiModelProperty(value = "单位可提供的资源列表")
    var resourceList: List<ResourceListVO>? = null

}
