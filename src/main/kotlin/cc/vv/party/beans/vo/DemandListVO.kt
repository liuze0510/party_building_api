package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 需求清单信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "需求清单信息")
class DemandListVO : BaseVo() {

    @ApiModelProperty(value = "编号")
    var id: String? = null

    @ApiModelProperty(value = "所属区")
    var area: String? = null

    @ApiModelProperty(value = "所属街道")
    var street: String? = null

    @ApiModelProperty(value = "所属社区")
    var community: String? = null

    @ApiModelProperty(value = "所属网格")
    var grid: String? = null

    @ApiModelProperty(value = "单位编号")
    var unitId: String? = null

    @ApiModelProperty(value = "单位名称")
    var unitName: String? = null

    @ApiModelProperty(value = "需求主题")
    var title: String? = null

    @ApiModelProperty(value = "需求内容")
    var conetnt: String? = null

    @ApiModelProperty(value = "负责人")
    var principal: String? = null

    @ApiModelProperty(value = "联系方式")
    var mobile: String? = null

    @ApiModelProperty(value = "有无酬劳 0 无 1有")
    var reward: Integer? = null

    @ApiModelProperty(value = "金额")
    var amount: String? = null

    @ApiModelProperty(value = "创建时间")
    var createTime: Date? = null

}
