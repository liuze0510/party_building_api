package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@ApiModel
class PendingDocVO : BaseVo() {

    companion object {
        val TYPE_TASKASSIGNED = "TASKASSIGNED"
        val TYPE_SUPERVISE = "SUPERVISE"
        val TYPE_THROUGHTRAIN = "THROUGHTRAIN"

        val IS_COMPLETE_YES = "Y"
        val IS_COMPLETE_NO = "N"

        val IS_READED_YES = "Y"
        val IS_READED_NO = "N"
    }

    @ApiModelProperty("编号id")
    var id: String? = null

    @ApiModelProperty("类型")
    var type: String? = null

    @ApiModelProperty("标题")
    var title: String? = null

    @ApiModelProperty("内容")
    var content: String? = null

    @ApiModelProperty("图片路径")
    var imgPaths: String? = null

    @ApiModelProperty("图片名称")
    var imageNames: List<String>? = null

    @ApiModelProperty("创建时间")
    var createTime: Date? = null

    @ApiModelProperty("是否已阅")
    var isReaded: String? = null

    @ApiModelProperty("是否完成")
    var complete = IS_COMPLETE_NO

    @ApiModelProperty("发起人名字")
    var inspecName: String? = null//发起人 名字

    @ApiModelProperty("发起人所在的支部")
    var inspecBranchName: String? = null//发起人 所在的支部

    @ApiModelProperty("发起人所在所在的组织部")
    var inspecOrgName: String? = null//发起人 所在的组织部

    @ApiModelProperty("职务")
    var inspecorgDuties: String? = null//职务

    @ApiModelProperty("审核/监督人 名字")
    var verifyName: String? = null//审核/监督人 名字

    @ApiModelProperty("审核/监督 支部")
    var verifyBranch: String? = null//审核/监督 支部

}