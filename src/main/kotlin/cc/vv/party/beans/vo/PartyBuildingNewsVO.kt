package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel
class PartyBuildingNewsVO : BaseVo() {

    @ApiModelProperty("id")
    var id: String? = null

    @ApiModelProperty(value = "区域", dataType = "String")
    var region: String? = null

    /**
     * 封面
     */
    @ApiModelProperty("封面")
    var cover: String? = null
    /**
     * 浏览数
     */
    @ApiModelProperty("浏览数")
    var browseNum: String? = null
    /**
     * 所属街道
     */
    @ApiModelProperty("所属街道")
    var street: String? = null
    /**
     * 所属社区
     */
    @ApiModelProperty("所属社区")
    var community: String? = null
    /**
     * 所属网格
     */
    @ApiModelProperty("所属网格")
    var grid: String? = null
    /**
     * 标题
     */
    @ApiModelProperty("标题")
    var title: String? = null
    /**
     * 内容
     */
    @ApiModelProperty("内容")
    var content: String? = null
    /**
     * 来源
     */
    @ApiModelProperty("来源")
    var source: String? = null
    /**
     * 发布人
     */
    @ApiModelProperty("发布人")
    var publisher: String? = null
    /**
     * 发布时间
     */
    @ApiModelProperty("发布时间")
    var releaseTime: Date? = null
}