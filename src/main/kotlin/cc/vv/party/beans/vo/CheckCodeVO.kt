package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@ApiModel
class CheckCodeVO : BaseVo() {
    @ApiModelProperty("编号")
    var id: String? = null

    @ApiModelProperty("验证码图片字节数组")
    var code: ByteArray? = null
}