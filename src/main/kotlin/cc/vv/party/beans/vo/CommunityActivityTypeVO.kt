package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 社区活动类型信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社区活动类型信息")
class CommunityActivityTypeVO: BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "类型名称", dataType = "String")
    var typeName: String? = null

    @ApiModelProperty(value = "类型描述", dataType = "String")
    var typeDesc: String? = null


}
