package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 社会组织动态信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社会组织动态信息")
class SocietyOrgDynamicVO: BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "来源", dataType = "String")
    var source: String? = null

    @ApiModelProperty(value = "发布时间", dataType = "String")
    var releaseTime: Date? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所在社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "发布组织编号", dataType = "String")
    var orgId: String? = null

    @ApiModelProperty(value = "发布组织", dataType = "String")
    var releaseOrg: String? = null

    @ApiModelProperty(value = "发布内容", dataType = "String")
    var releaseContent: String? = null

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createTime: Date? = null

}
