package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 区用户树
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "区用户树")
class AreauserVO : BaseVo() {

    @ApiModelProperty("编号")
    var id: Integer? = null

    @ApiModelProperty("区")
    var county: String? = null

    @ApiModelProperty("街道")
    var town: String? = null

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "级别", hidden = true)
    var level: Integer? = null

    @ApiModelProperty("子节点")
    var child: List<AreauserVO>? = null

}