package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 社会主义核心价值观信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社会主义核心价值观信息")
class SocialistValuesVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "类型 0 文明 1 和谐 2 敬业  3 友善 4 诚信", dataType = "Integer")
    var type: Integer? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "视频", dataType = "String")
    var video: String? = null

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null

    @ApiModelProperty(value = "浏览数", dataType = "Integer")
    var browseNum: Integer? = null

    @ApiModelProperty(value = "内容", dataType = "String")
    var conetnt: String? = null

    @ApiModelProperty(value = "来源", dataType = "String")
    var source: String? = null

    @ApiModelProperty(value = "发布时间", dataType = "String")
    var createTime: Date? = null

}
