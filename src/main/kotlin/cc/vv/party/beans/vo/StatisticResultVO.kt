package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.Api
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-24
 * @description
 **/
@ApiModel
class StatisticResultVO : BaseVo() {

    @ApiModelProperty("社区名称")
    var communityName: String? = null

    @ApiModelProperty("总数")
    var totalCount: String? = null

    @ApiModelProperty("已报道数量")
    var reportedCount: String? = null
}