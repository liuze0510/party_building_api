package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-29
 * @description
 **/
@ApiModel
class ExamTotalStatisticsVO : BaseVo() {

    @ApiModelProperty("本周新增答题用户")
    var weekUserCnt: String? = null

    @ApiModelProperty("本周新增答题数量")
    var weekExamQuestionCnt: String? = null

    @ApiModelProperty("平均得分")
    var averageScore: String? = null

    @ApiModelProperty("累计答题用户")
    var totalAnswerUserCnt: String? = null

    @ApiModelProperty("本月新增用户")
    var monthIncrementUserCnt: String? = null

    @ApiModelProperty("累计答题数")
    var totalAnswerQuestionCnt: String? = null

    @ApiModelProperty("本月新增答题数量")
    var monthExamQuestionCnt: String? = null

    @ApiModelProperty("平均优秀率")
    var averageExcellentRate: String? = null

    @ApiModelProperty("优秀率上升")
    var monthExcellentIncrementCnt: String? = null

    @ApiModelProperty("平均合格率")
    var averageQualifiedRate: String? = null

    @ApiModelProperty("合格率上升")
    var monthQualifiedIncrementCnt: String? = null

    @ApiModelProperty("累计平均得分")
    var totalAverageScore: String? = null

    @ApiModelProperty("累计得分上升率")
    var totalAverageScoreIncrementCnt: String? = null
}