package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import cc.vv.party.common.constants.enums.RoleType
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description
 **/
@ApiModel(value = "角色信息")
class RoleVO : BaseVo() {

    @ApiModelProperty("角色Id")
    var id: String? = null

    @ApiModelProperty("角色名称")
    var name: String? = null

    @ApiModelProperty("角色类型")
    var type: RoleType? = null

    @ApiModelProperty("角色描述")
    var description: String? = null

    @ApiModelProperty("权限列表")
    var permissionList: List<PermissionVO>? = null

    companion object {
        @JvmStatic
        fun default(): RoleVO {
            val default = RoleVO()
            default.type = RoleType.PARTY_MEMBER
            default.name = "党员"
            default.description = "党员"
            return default
        }

    }
}