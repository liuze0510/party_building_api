package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 社会组织信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社会组织信息")
class SocietyOrgVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "组织名称", dataType = "String")
    var orgName: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所在社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "简介", dataType = "String")
    var introduction: String? = null

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createTime: Date? = null


}
