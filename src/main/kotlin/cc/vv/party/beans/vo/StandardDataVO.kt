package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 标准数据信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "标准数据信息")
class StandardDataVO : BaseVo() {

    @ApiModelProperty("红旗党支部")
    var score1: Integer? = null

    @ApiModelProperty("91-95分")
    var score2: Integer? = null

    @ApiModelProperty("80-90分")
    var score3: Integer? = null

    @ApiModelProperty("80分以下")
    var score4: Integer? = null

}