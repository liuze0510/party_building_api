package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 党群新闻
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "党群新闻信息")
class PartyNewsVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "发布人", dataType = "String")
    var publisher: String? = null

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branch: String? = null

    @ApiModelProperty(value = "缩略图", dataType = "String")
    var thumbnail: String? = null

    @ApiModelProperty(value = "视频", dataType = "String")
    var video: String? = null

    @ApiModelProperty(value = "新闻内容", dataType = "String")
    var conetnt: String? = null

    @ApiModelProperty(value = "发布时间", dataType = "String")
    var createTime: Date? = null


}
