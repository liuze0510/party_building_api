package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 心得体会信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "心得体会信息", description = "其中图片显示方案：path + imgName[0]即可")
data class ExperienceVO (

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null,

    @ApiModelProperty(value = "图片集合中图片路径", dataType = "String")
    var imgPath: String? = null,

    @ApiModelProperty(value = "图片集合 以‘，’分隔的集合", dataType = "String")
    var imgName: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "图片匹配路径", dataType = "String")
    var path: String? = null,

    @ApiModelProperty(value = "用户名称", dataType = "String")
    var userName: String? = null,

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null,

    @ApiModelProperty(value = "支部名称", dataType = "String")
    var branchName: String? = null


)