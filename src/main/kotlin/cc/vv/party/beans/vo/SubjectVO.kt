package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微百科科目信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微百科科目信息")
data class SubjectVO (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "图片", dataType = "String")
    var img: String? = null


)