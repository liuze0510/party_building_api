package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 心得体会数据详情
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "心得体会数据详情")
data class ExperienceDataDetailVO (

    @ApiModelProperty(value = "姓名", dataType = "String")
    var userName: String? = null,

    @ApiModelProperty(value = "性别", dataType = "String")
    var sex: String? = null,

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branchName: String? = null,

    @ApiModelProperty(value = "是否书记", dataType = "String")
    var shuji: String? = null,

    @ApiModelProperty(value = "发布总数", dataType = "Integer")
    var quanbu: Integer? = null,

    @ApiModelProperty(value = "通过总数", dataType = "Integer")
    var tongguo: Integer? = null


)