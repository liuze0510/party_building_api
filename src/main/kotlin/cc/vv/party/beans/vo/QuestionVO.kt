package cc.vv.party.beans.vo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 在线考试试题详情
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "在线考试试题详情")
data class QuestionVO (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "试题编号", dataType = "Integer")
    var num: Integer? = null,

    @ApiModelProperty(value = "问题", dataType = "String")
    var questions: String? = null,

    @ApiModelProperty(value = "选项1", dataType = "String")
    var optionA: String? = null,

    @ApiModelProperty(value = "选项2", dataType = "String")
    var optionB: String? = null,

    @ApiModelProperty(value = "选项3", dataType = "String")
    var optionC: String? = null,

    @ApiModelProperty(value = "选项4", dataType = "String")
    var optionD: String? = null,

    @ApiModelProperty(value = "答案", dataType = "String")
    var answer: String? = null

)