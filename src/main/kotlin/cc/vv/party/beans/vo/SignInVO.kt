package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 签到信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "签到信息")
class SignInVO : BaseVo() {

    @ApiModelProperty(value = "队伍信息", dataType = "TeamVO")
    var teamVO: TeamVO? = null

    @ApiModelProperty(value = "队伍签到信息", dataType = "TeamVO")
    var teamSignInList: List<TeamSignInVO>? = null

    @ApiModelProperty(value = "时间", dataType = "String")
    var time: String? = null

    @ApiModelProperty(value = "系统当前时间", dataType = "String")
    var systemCurrentTime: String = DateUtil.format(Date(), DatePattern.NORM_DATE_PATTERN)

}
