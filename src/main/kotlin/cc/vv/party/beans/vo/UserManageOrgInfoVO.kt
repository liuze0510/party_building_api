package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-12-06
 * @description 管理的组织机构vo
 **/
class UserManageOrgInfoVO: BaseVo() {
    //区ID
    var areaId: String? = null
    //街道ID
    var streetId: String? = null
    //社区ID
    var communityId: String? = null
    //网格ID
    var gridId: String? = null
    //单位ID
    var unitId: String? = null
    //街道联席会议ID
    var streetMeetingId: String? = null
    //社区大党委ID
    var communityCommitteeId: String? = null
    //村ID
    var villageId: String? = null
    //乡镇ID
    var townId: String? = null
}