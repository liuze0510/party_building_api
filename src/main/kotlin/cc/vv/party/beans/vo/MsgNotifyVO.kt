package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@ApiModel
class MsgNotifyVO : BaseVo() {

    @ApiModelProperty("编号")
    var id: String? = null

    @ApiModelProperty("标题")
    var title: String? = null

    @ApiModelProperty("通知的党员")
    var deliverMember: String? = null

    @ApiModelProperty("通知地区ID 全员可见")
    var regionIds: String? = null

    @ApiModelProperty("区域名称")
    var regionNames: String? = null

    @ApiModelProperty("通知类型")
    var notifyType: String? = null

    @ApiModelProperty("接收党员IDs")
    var receivePartyMemberIds: String? = null//

    @ApiModelProperty("接收组织部IDS")
    var receiveOrgIds: String? = null//

    @ApiModelProperty("隶属区域")
    var belongRegion: String? = null//

    @ApiModelProperty("时间戳文件夹路径")
    var timeStampFolder: String? = null //

    @ApiModelProperty("生成时间")
    var createTime: Date? = null //

    @ApiModelProperty("内容html")
    var html: String? = null
}