package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 非公党建单位
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "非公党建单位信息")
class NotPartyUnitVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null

    @ApiModelProperty(value = "组织类型 0 非公企业 1 非公党组织", dataType = "String")
    var type: Integer? = null

    @ApiModelProperty(value = "单位简介", dataType = "String")
    var introduction: String? = null

}
