package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 党员报道信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "党员报道信息")
class PartyReportVO : BaseVo() {

    @ApiModelProperty(value = "延安党建中的UserId", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null

    @ApiModelProperty(value = "报道街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "报道社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "报道网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "职业特点", dataType = "String")
    var professionalCharacteristics: String? = null

    @ApiModelProperty(value = "工作时间", dataType = "String")
    var workTime: Date? = null

    @ApiModelProperty(value = "特长爱好", dataType = "String")
    var specialHobby: String? = null

    @ApiModelProperty(value = "个人承诺", dataType = "String")
    var personalCommitment: String? = null

    @ApiModelProperty(value = "是否愿意成为志愿者 0 愿意 1 不愿意", dataType = "Integer")
    var volunteer: Integer? = null

    @ApiModelProperty(value = "报道时间", dataType = "String")
    var createTime: Date? = null


}
