package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微感悟统计信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微感悟统计信息")
data class FeelingStatisticsVO (

    @ApiModelProperty(value = "统计微感悟总数", dataType = "Integer")
    var allCount: Integer? = null,

    @ApiModelProperty(value = "本月新增", dataType = "Integer")
    var monthAdd: Integer? = null,

    @ApiModelProperty(value = "审核通过总数", dataType = "Integer")
    var sumCount: Integer? = null,

    @ApiModelProperty(value = "本月审核通过新增", dataType = "Integer")
    var monthAddCount: Integer? = null,

    @ApiModelProperty(value = "本周发布总数", dataType = "Integer")
    var workReleaseCount: Integer? = null,

    @ApiModelProperty(value = "本周审核通过新增", dataType = "Integer")
    var workAddCount: Integer? = null,

    @ApiModelProperty(value = "实时统计", dataType = "List")
    var newFeelingList: List<FeelingVO>? = null

)