package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 社民主、组织生活会类型信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "民主、组织生活会类型信息")
class LifeMeetingTypeVO: BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "类型", dataType = "Integer")
    var type: Integer? = null

    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null


}
