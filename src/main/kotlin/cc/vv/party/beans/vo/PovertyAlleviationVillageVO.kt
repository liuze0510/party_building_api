package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 扶贫行政村信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "扶贫行政村信息")
class PovertyAlleviationVillageVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "父编号", dataType = "String")
    var parentId: String? = null

    @ApiModelProperty(value = "名称", dataType = "String")
    var villageName: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null

    @ApiModelProperty(value = "人口数量", dataType = "Integer")
    var population: Integer? = null

    @ApiModelProperty(value = "耕地面积", dataType = "Float")
    var cultivatedArea: Float? = null

    @ApiModelProperty(value = "主导产业", dataType = "String")
    var leadingIndustry: String? = null

    @ApiModelProperty(value = "贫困户数量", dataType = "Integer")
    var povertyAlleviationNum: Integer? = null

    @ApiModelProperty(value = "党员数量", dataType = "Integer")
    var partyNum: Integer? = null

    @ApiModelProperty(value = "是否脱贫 0 未脱贫 1 已脱贫", dataType = "Integer")
    var poverty: Integer? = null

    @ApiModelProperty(value = "资源路径", dataType = "String")
    var path: String? = null

    @ApiModelProperty(value = "子集", dataType = "String")
    var child: List<PovertyAlleviationVillageVO>? = null

    @ApiModelProperty(value = "图片集合", dataType = "String")
    var imgList: List<String>? = null

    @ApiModelProperty(value = "第一书记信息", dataType = "TeamVO")
    var secretary: TeamVO? = null

    @ApiModelProperty(value = "扶贫工作队", dataType = "List")
    var taskForceList: List<TeamVO>? = null

    @ApiModelProperty(value = "驻村干部", dataType = "List")
    var villageCadreList: List<TeamVO>? = null

    @ApiModelProperty(value = "获取村两委班子", dataType = "List")
    var twoCommittee: List<TeamVO>? = null
}
