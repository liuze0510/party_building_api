package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import com.baomidou.mybatisplus.annotations.TableField
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 留言板信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "留言板信息")
class MessageBoardVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "所属区", dataType = "String")
    var area: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "留言人姓名", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "用户头像")
    var faceUrl: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "标题")
    var title: String? = null

    @ApiModelProperty(value = "党组织名称")
    var branch: String? = null

    @ApiModelProperty(value = "内容")
    var conetnt: String? = null

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "int")
    var checkState: Integer? = null

    @ApiModelProperty(value = "回复数量", dataType = "int")
    var count: Int? = null

    @ApiModelProperty(value = "发布时间", dataType = "String")
    var createTime: Date? = null
}
