package cc.vv.party.beans.vo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 心得体会统计头部信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "心得体会统计头部信息")
data class ExperienceStatisticsTopVO (


    @ApiModelProperty(value = "名称", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "图片", dataType = "String")
    var img: String? = null


)