package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * 岗位、求职者统计
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "岗位求职者统计信息")
class PostJobHuntingUserStatisticsVO : BaseVo() {

    @ApiModelProperty(value = "岗位月数量集合", dataType = "Int")
    var postNum: Int = 0

    @ApiModelProperty(value = "求职者月数量集合", dataType = "Int")
    var jobHuntingUserNum: Int = 0

    @ApiModelProperty(value = "岗位月数量集合", dataType = "json")
    var post: LinkedHashMap<String, Int>? = null

    @ApiModelProperty(value = "求职者月数量集合", dataType = "json")
    var jobHuntingUser: LinkedHashMap<String, Int>? = null
}