package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 街道联席会议信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "街道联席会议信息")
class StreetJointMeetingVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "会议名称", dataType = "String")
    var meetingName: String? = null

    @ApiModelProperty(value = "会议成员", dataType = "List")
    var meetingMemberList: List<StreetJointMeetingMemberVO>? = null

    @ApiModelProperty(value = "召集人", dataType = "String")
    var convener: String? = null

    @ApiModelProperty(value = "牵头负责单位", dataType = "String")
    var responsibleUnit: String? = null

    @ApiModelProperty(value = "负责人姓名", dataType = "String")
    var principalName: String? = null

    @ApiModelProperty(value = "负责人账号", dataType = "String")
    var principalAccount: String? = null

    @ApiModelProperty(value = "会议简介", dataType = "String")
    var meetingIntroduction: String? = null

    @ApiModelProperty(value = "会议管理员", dataType = "List")
    var streetJointMeetingAdmin: List<StreetJointMeetingAdminVO>? = null

}
