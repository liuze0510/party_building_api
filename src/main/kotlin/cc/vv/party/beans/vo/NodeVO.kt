package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-14
 * @description
 **/
class NodeVO : BaseVo() {
    var id: String? = null
    var name: String? = null
    var type: String? = null
}