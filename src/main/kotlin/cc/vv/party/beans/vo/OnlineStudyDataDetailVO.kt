package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * APP 实时在线信息统计
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "APP实时在线信息统计")
data class OnlineStudyDataDetailVO (

    @ApiModelProperty(value = "支部名称", dataType = "String")
    var name: String? = null,

    @ApiModelProperty(value = "总人员数量", dataType = "String")
    var sumMember: String? = null,

    @ApiModelProperty(value = "优秀人员数量", dataType = "String")
    var excellentMember: String? = null,

    @ApiModelProperty(value = "优秀人员比重", dataType = "String")
    var excellentMemberProportion: String? = null,

    @ApiModelProperty(value = "合格人数", dataType = "String")
    var qualifiedMember: String? = null,

    @ApiModelProperty(value = "合格人数比重", dataType = "String")
    var qualifiedMemberProportion: String? = null,

    @ApiModelProperty(value = "不合格人数", dataType = "String")
    var notQualifiedMember: String? = null,

    @ApiModelProperty(value = "不合格人数比重", dataType = "String")
    var notQualifiedMemberProportion: String? = null,

    @ApiModelProperty(value = "平均分", dataType = "String")
    var averageScore: String? = null






)