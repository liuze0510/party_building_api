package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 资源清单信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "资源清单信息")
class ResourceListVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "所属区", dataType = "String")
    var area: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "单位编号")
    var unitId: String? = null

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "资源品类", dataType = "String")
    var resourceType: String? = null

    @ApiModelProperty(value = "资源内容", dataType = "String")
    var conetnt: String? = null

    @ApiModelProperty(value = "数量", dataType = "Integer")
    var num: Integer? = null

    @ApiModelProperty(value = "有无酬劳 0 无 1有", dataType = "Integer")
    var reward: String? = null

    @ApiModelProperty(value = "金额", dataType = "String")
    var amount: String? = null

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Date? = null

    @ApiModelProperty(value = "结束时间", dataType = "String")
    var endTime: Date? = null

}
