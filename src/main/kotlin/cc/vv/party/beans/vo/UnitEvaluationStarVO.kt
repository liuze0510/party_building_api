package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 单位评星晋阶信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "单位评星晋阶信息")
class UnitEvaluationStarVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "组织类型", dataType = "String")
    var orgType: String? = null

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var companyName: String? = null

    @ApiModelProperty(value = "得分", dataType = "Float")
    var score: Float? = null

    @ApiModelProperty(value = "单位评星晋阶明细信息", dataType = "List")
    var itemList: List<RatingDetailsVO>? = null

}
