package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 驻区单位活动信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "驻区单位活动信息")
class ResidentUnitActivityVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "区域", dataType = "String")
    var region: String? = null

    @ApiModelProperty(value = "街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "单位", dataType = "String")
    var unitId: String? = null

    @ApiModelProperty(value = "单位名称", dataType = "String")
    var unitName: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null

    @ApiModelProperty(value = "举办方", dataType = "String")
    var organizer: String? = null

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityLocation: String? = null

    @ApiModelProperty(value = "活动时间", dataType = "String")
    var activityTime: Date? = null

    @ApiModelProperty(value = "活动内容", dataType = "String")
    var activityDesc: String? = null


}
