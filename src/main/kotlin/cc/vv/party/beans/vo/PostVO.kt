package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 岗位信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "岗位信息")
class PostVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "岗位名称", dataType = "String")
    var postName: String? = null

    @ApiModelProperty(value = "岗位来源", dataType = "String")
    var postSource: String? = null

    @ApiModelProperty(value = "招聘人数", dataType = "Integer")
    var jobNum: Integer? = null

    @ApiModelProperty(value = "工作年限", dataType = "Integer")
    var workYear: Integer? = null

    @ApiModelProperty(value = "学历 0 初中及以下 1 中专 2 高中 3 大专 4 本科 5 研究生及以上", dataType = "String")
    var education: String? = null

    @ApiModelProperty(value = "薪资", dataType = "String")
    var salary: String? = null

    @ApiModelProperty(value = "工作地址", dataType = "String")
    var workAddr: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "职位描述", dataType = "String")
    var postDescription: String? = null

    @ApiModelProperty(value = "技能要求", dataType = "String")
    var skillClaim: String? = null

    @ApiModelProperty(value = "公司简介", dataType = "String")
    var companyIntroduction: String? = null

    @ApiModelProperty(value = "匹配人员", dataType = "Integer")
    var matchMember: Integer? = null

    @ApiModelProperty(value = "可见范围 0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区", dataType = "String")
    var visibleRange: String? = null

    @ApiModelProperty(value = "发布时间", dataType = "String")
    var createTime: Date? = null

}
