package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 志愿项目报名信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "志愿项目报名信息")
class VolunteerProjectSignUpVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "志愿项目编号", dataType = "String")
    var volunteerProjectId: String? = null

    @ApiModelProperty(value = "姓名", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "性别", dataType = "Integer")
    var sex: Integer? = null

    @ApiModelProperty(value = "所属支部", dataType = "String")
    var branch: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null


}
