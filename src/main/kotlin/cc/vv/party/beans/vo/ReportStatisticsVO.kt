package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * WEB端报道统计信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "WEB端报道统计信息")
class ReportStatisticsVO : BaseVo() {


    @ApiModelProperty(value = "社区名称", dataType = "String")
    var name: String? = null

    @ApiModelProperty(value = "报道数量", dataType = "String")
    var num: Int? = null

}
