package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 街道联席会议成员信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "街道联席会议成员信息")
class StreetJointMeetingMemberVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "成员编号", dataType = "String")
    var memberId: String? = null

    @ApiModelProperty(value = "成员名称", dataType = "String")
    var memberName: String? = null

    @ApiModelProperty(value = "会议编号", dataType = "String")
    var meetingId: String? = null

}
