package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 街道联席会议活动信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "街道联席会议活动信息")
class StreetJointMeetingActivityVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "区域", dataType = "String")
    var region: String? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "会议编号", dataType = "String")
    var meetingId: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null

    @ApiModelProperty(value = "浏览数", dataType = "Integer")
    var browseNum: Integer? = null

    @ApiModelProperty(value = "举办方", dataType = "String")
    var organizer: String? = null

    @ApiModelProperty(value = "活动地点", dataType = "String")
    var activityLocation: String? = null

    @ApiModelProperty(value = "活动时间", dataType = "String")
    var activityTime: Date? = null

    @ApiModelProperty(value = "活动内容", dataType = "String")
    var activityDesc: String? = null


}
