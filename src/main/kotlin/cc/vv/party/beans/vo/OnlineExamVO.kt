package cc.vv.party.beans.vo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 在线考试信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "在线考试信息")
data class OnlineExamVO (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "试题数量", dataType = "Integer")
    var count: Integer? = null,

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createtime: Date? = null


)