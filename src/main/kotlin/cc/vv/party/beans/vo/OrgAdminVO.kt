package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import com.baomidou.mybatisplus.annotations.TableField
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-16
 * @description
 **/
@ApiModel
class OrgAdminVO : BaseVo() {
    /**
     * 组织机构编号
     */
    @ApiModelProperty("组织机构编号")
    var orgId: String? = null
    /**
     * 用户登录账号
     */
    @ApiModelProperty("用户登录账号")
    var userAccount: String? = null
    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    var userName: String? = null

    /**
     * 头像
     */
    @ApiModelProperty("头像")
    var userPhoto: String? = null

    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    var userMobile: String? = null

    /**
     * 角色类型
     */
    @ApiModelProperty("角色类型")
    var roleType:String? = null

}