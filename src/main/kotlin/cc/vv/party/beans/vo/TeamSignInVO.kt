package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 四支队伍签到信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "四支队伍签到信息")
class TeamSignInVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "队伍编号", dataType = "String")
    var teamId: String? = null

    @ApiModelProperty(value = "签到类型 0 签到 1 签退", dataType = "String")
    var signType: Integer? = null

    @ApiModelProperty(value = "签到时间", dataType = "String")
    var signTime: Date? = null

    @ApiModelProperty(value = "图片路径", dataType = "TeamVO")
    var imageList: List<String>? = null

}
