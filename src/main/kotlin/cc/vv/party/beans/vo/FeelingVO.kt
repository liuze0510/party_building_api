package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微感悟信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微感悟信息")
data class FeelingVO (

    @ApiModelProperty(value = "内容", dataType = "String")
    var content: String? = null,

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    var createTime: Date? = null,

    @ApiModelProperty(value = "用户名称", dataType = "String")
    var userName: String? = null,

    @ApiModelProperty(value = "头像", dataType = "String")
    var faceUrl: String? = null,

    @ApiModelProperty(value = "支部名称", dataType = "String")
    var branchName: String? = null


)