package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * APP 实时在线信息统计
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "APP实时在线信息统计")
data class AppRealTimeStatisticsVO (

    @ApiModelProperty(value = "日期", dataType = "String")
    var date: String? = null,

    @ApiModelProperty(value = "性别", dataType = "Int")
    var pcCount: Int? = null,

    @ApiModelProperty(value = "所属支部", dataType = "Int")
    var appCount: Int? = null


)