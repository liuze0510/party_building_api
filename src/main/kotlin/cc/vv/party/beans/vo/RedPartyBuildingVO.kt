package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 红领党建信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "红领党建信息")
class RedPartyBuildingVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null

    @ApiModelProperty(value = "类型: 0 品牌简介 1 六大工程 2 组织引领 3 党员引领 4 文化引领 5 标准引领", dataType = "Integer")
    var type: Integer? = null

    @ApiModelProperty(value = "封面", dataType = "String")
    var cover: String? = null

    @ApiModelProperty(value = "内容", dataType = "String")
    var conetnt: String? = null

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createTime: Date? = null

}
