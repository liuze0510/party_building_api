package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-19
 * @description
 **/
@ApiModel
class RoleTypeVO : BaseVo() {

    @ApiModelProperty("角色类型值")
    var value: String? = null

    @ApiModelProperty("角色类型名称")
    var name: String? = null
}