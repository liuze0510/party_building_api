package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 便民服务保存参数
 * @author: zhangx
 * @date 2018/10/12 15:08
 * @version 1.0.0
 * @description
 **/
@ApiModel(value = "便民服务信息")
class ConvenienceServiceVO : BaseVo() {

    @ApiModelProperty(value = "编号")
    var id: String? = null

    @ApiModelProperty(value = "所属区")
    var area: String? = null

    @ApiModelProperty(value = "所属街道")
    var street: String? = null

    @ApiModelProperty(value = "所属社区")
    var community: String? = null

    @ApiModelProperty(value = "所属网格")
    var grid: String? = null

    @ApiModelProperty(value = "服务类型")
    var type: String? = null

    @ApiModelProperty(value = "服务类型名称")
    var typeName: String? = null

    @ApiModelProperty(value = "标题")
    var title: String? = null

    @ApiModelProperty(value = "内容")
    var conetnt: String? = null

    @ApiModelProperty(value = "编辑时间")
    var editTime: Date? = null

    @ApiModelProperty(value = "创建时间")
    var createTime: Date? = null

}