package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 承诺评论信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "承诺评论信息")
class CommittedCommentVO : BaseVo() {

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "承诺编号", dataType = "String")
    var committedId: String? = null

    @ApiModelProperty(value = "评论人编号", dataType = "String")
    var commentId: String? = null

    @ApiModelProperty(value = "评论人姓名", dataType = "String")
    var commentName: String? = null

    @ApiModelProperty(value = "评论人头像", dataType = "String")
    var faceUrl: String? = null

    @ApiModelProperty(value = "评论内容", dataType = "String")
    var content: String? = null

    @ApiModelProperty(value = "评论时间", dataType = "String")
    var commentTime: Date? = null


}
