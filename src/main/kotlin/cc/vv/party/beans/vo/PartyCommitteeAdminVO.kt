package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 社区大党委管理员信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社区大党委管理员信息")
class PartyCommitteeAdminVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "党委编号", dataType = "String")
    var partyCommitteeyId: String? = null

    @ApiModelProperty(value = "管理员姓名", dataType = "String")
    var adminName: String? = null

    @ApiModelProperty(value = "管理员账号", dataType = "String")
    var adminAccount: String? = null

    @ApiModelProperty(value = "所属单位", dataType = "String")
    var companyName: String? = null

}
