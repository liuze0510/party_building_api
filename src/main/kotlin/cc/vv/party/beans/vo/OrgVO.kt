package cc.vv.party.beans.vo

import cc.vv.party.beans.model.OrgAdmin
import cc.vv.party.common.base.BaseVo
import cc.vv.party.common.constants.enums.OrgType
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description
 **/
@ApiModel(value = "组织机构信息")
class OrgVO : BaseVo() {

    /**
     * 组织结构id
     */
    @ApiModelProperty("组织结构Id")
    var id: String? = null
    /**
     * 名称
     */
    @ApiModelProperty("名称")
    var name: String? = null
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    var mobile: String? = null
    /**
     * 类型 0 区 1 街道 2 社区 3 网格 4 单位 5 社会组织 6 楼道党小组 7 普通党员 8 单元中心户
     */
    @ApiModelProperty("类型")
    var type: OrgType? = null
    /**
     * 路径
     */
    @ApiModelProperty("路径", hidden = true)
    var path: String? = null

    @ApiModelProperty("单元中心户位置，只有党员中心户时，存在该字段值")
    var location: String? = null

    /**
     * 父节点Id
     */
    @ApiModelProperty("父节点Id")
    var parentId: String? = null
    /**
     * 简介
     */
    @ApiModelProperty("简介")
    var introduction: String? = null

    /**
     * 子节点列表
     */
    @ApiModelProperty("子节点列表")
    var children: MutableList<OrgVO>? = null

    @ApiModelProperty("管理员列表")
    var adminList: MutableList<OrgAdminVO>? = null
}