package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 承诺信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "承诺信息")
class CommittedVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "承诺类型0 单位承诺 1 党员承诺", dataType = "Integer")
    var committedType: Integer? = null

    @ApiModelProperty(value = "承诺单位名称，当承诺类型为单位是才需要填写", dataType = "String")
    var committedUnitName: String? = null

    @ApiModelProperty(value = "承诺内容", dataType = "String")
    var content: String? = null

    @ApiModelProperty(value = "完成时间", dataType = "String")
    var completeTime: Date? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "可见范围", dataType = "String")
    var visibleRange: String? = null

    @ApiModelProperty("评论列表")
    var commentList: List<CommittedCommentVO>? = null

    @ApiModelProperty("目标id，如果是党员承诺，则为用户ID，如果是单位，则为单位Id")
    var targetId: String? = null

    @ApiModelProperty("党员名称")
    var userName: String? = null

    @ApiModelProperty("发布人编号")
    var createUser: String? = null
}
