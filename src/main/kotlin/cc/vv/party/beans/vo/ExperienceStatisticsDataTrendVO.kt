package cc.vv.party.beans.vo;

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 心得体会数据趋势信息
 * </p>
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "心得体会数据趋势信息")
data class ExperienceStatisticsDataTrendVO (

    @ApiModelProperty(value = "通过", dataType = "Map")
    var tongguo: LinkedHashMap<String, Integer>? = null,

    @ApiModelProperty(value = "发布", dataType = "Map")
    var zongshu: LinkedHashMap<String, Integer>? = null



)