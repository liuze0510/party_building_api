package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * <p>
 * 社区大党委与组织成员关系信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "社区大党委与组织成员关系信息")
class PartyOrgMemberVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "党委编号", dataType = "String")
    var partyCommitteeyId: String? = null

    @ApiModelProperty(value = "组织成员编号", dataType = "String")
    var orgMemberId: String? = null

    @ApiModelProperty(value = "组织成员名称", dataType = "String")
    var MemberName: String? = null



}
