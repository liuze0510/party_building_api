package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import com.baomidou.mybatisplus.annotations.TableField
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel
class LogVO : BaseVo() {

    /**
     * 操作内容
     */
    @ApiModelProperty("操作内容")
    var operateContent: String? = null
    /**
     * 操作人账号
     */
    @ApiModelProperty("操作人账号")
    var operateAccount: String? = null

    /**
     * 操作人名称
     */
    @ApiModelProperty("操作人名称")
    var operateName: String? = null
    /**
     * 操作人角色名称
     */
    @ApiModelProperty("操作人角色名称")
    var operateRole: String? = null

    /**
     * ip地址
     */
    @ApiModelProperty("ip地址")
    var remoteIp: String? = null

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    var createTime: Date? = null
}