package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微捐赠
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微捐赠信息")
class MicroDonationVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "捐赠人", dataType = "String")
    var microDonationName: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null

    @ApiModelProperty(value = "捐赠内容", dataType = "String")
    var microDonationConetnt: String? = null

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Date? = null

    @ApiModelProperty(value = "结束时间", dataType = "String")
    var endTime: Date? = null

    @ApiModelProperty(value = "认领状态 0 未认领 1 已认领", dataType = "int")
    var claimState: Integer? = null

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "int")
    var checkState: Integer? = null

    @ApiModelProperty(value = "认领人", dataType = "String")
    var clainPerson: String? = null

    @ApiModelProperty(value = "认领人联系方式", dataType = "String")
    var clainMobile: String? = null

    @ApiModelProperty(value = "认领时间", dataType = "String")
    var clainTime: Date? = null

    @ApiModelProperty(value = "备注", dataType = "String")
    var remarks: String? = null

    @ApiModelProperty(value = "审核不通过原因", dataType = "String")
    var checkReson: String? = null

    @ApiModelProperty(value = "认领信息集合", dataType = "List")
    var clainList: List<MicroDonationClainVO>? = null

    @ApiModelProperty(value = "是否选中 0 认领失败 1 认领成功", dataType = "Integer")
    var seelected: Integer? = null

}
