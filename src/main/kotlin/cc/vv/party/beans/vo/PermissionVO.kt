package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-19
 * @description
 **/
@ApiModel
class PermissionVO : BaseVo() {

    @ApiModelProperty("权限id")
    var id: String? = null

    @ApiModelProperty("权限名称")
    var name: String? = null

    @ApiModelProperty("父节点Id")
    var parentId: String? = null

    @ApiModelProperty("子节点列表")
    var children: MutableList<PermissionVO>? = null
}