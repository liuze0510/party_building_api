package cc.vv.party.beans.vo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * 远程教育信息
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-17
 * @description
 **/
@ApiModel(value = "远程教育信息")
data class EducationVO (

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null,

    @ApiModelProperty(value = "标题", dataType = "String")
    var title: String? = null,

    @ApiModelProperty(value = "创建时间", dataType = "String")
    var createtime: Date? = null,

    @ApiModelProperty(value = "点赞数", dataType = "Integer")
    var praiseCount: Integer? = null,

    @ApiModelProperty(value = "访问数", dataType = "Integer")
    var degreeCount: Integer? = null,

    @ApiModelProperty(value = "图片地市", dataType = "String")
    var imgname: String? = null,

    @ApiModelProperty(value = "视频地址", dataType = "String")
    var videonames: String? = null,

    @ApiModelProperty(value = "简介", dataType = "String")
    var snippetInfo: String? = null




)