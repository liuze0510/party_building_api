package cc.vv.party.beans.vo

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-23
 * @description 社区VO
 **/
@ApiModel
class CommunityVO : BaseVo() {

    @ApiModelProperty("社区Id")
    var communityId: String? = null

    @ApiModelProperty("党委名称")
    var title: String? = null

    @ApiModelProperty("负责人姓名")
    var name: String? = null

    @ApiModelProperty("负责人电话")
    var mobile: String? = null

    @ApiModelProperty("社区大党委简介")
    var committeeIntro: String? = null

    @ApiModelProperty("网格列表")
    var gridList: MutableList<OrgVO>? = null

    @ApiModelProperty("单位列表")
    var unitList: MutableList<UnitVO>? = null

    @ApiModelProperty("社会组织列表")
    var societyOrgList: MutableList<SocietyOrgVO>? = null

    @ApiModelProperty("社会组织动态列表")
    var societyOrgDynamicList: MutableList<SocietyOrgDynamicVO>? = null

}
