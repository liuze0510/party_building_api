package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 微心愿
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "微心愿信息")
class WishVO : BaseVo() {

    /**
     * 主键id
     */
    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    /**
     * 发布人
     */
    @ApiModelProperty(value = "发布人", dataType = "String")
    var name: String? = null

    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址", dataType = "String")
    var addr: String? = null

    /**
     * 心愿内容
     */
    @ApiModelProperty(value = "心愿内容", dataType = "String")
    var wishConetnt: String? = null

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Date? = null

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", dataType = "String")
    var endTime: Date? = null

    /**
     * 认领状态 0 未认领 1 已认领
     */
    @ApiModelProperty(value = "认领状态 0 未认领 1 已认领", dataType = "int")
    var claimState: Integer? = null

    /**
     * 认领人
     */
    @ApiModelProperty(value = "认领人")
    var clainPerson: String? = null

    /**
     * 认领人联系方式
     */
    @ApiModelProperty(value = "认领人联系方式")
    var clainMobile: String? = null

    /**
     * 认领时间
     */
    @ApiModelProperty(value = "认领时间")
    var clainTime: Date? = null

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    var remarks: String? = null

    @ApiModelProperty(value = "微心愿认领备注", dataType = "String")
    var strDesc: String? = null

    /**
     * 审核状态 0 通过 1 不通过
     */
    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "int")
    var checkState: Integer? = null

}
