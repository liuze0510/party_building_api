package cc.vv.party.beans.vo;

import cc.vv.party.common.base.BaseVo
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*

/**
 * <p>
 * 项目信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@ApiModel(value = "项目信息")
class ProjectVO : BaseVo(){

    @ApiModelProperty(value = "编号", dataType = "String")
    var id: String? = null

    @ApiModelProperty(value = "项目名称", dataType = "String")
    var projectName: String? = null

    @ApiModelProperty(value = "项目介绍", dataType = "String")
    var projectIntroduction: String? = null

    @ApiModelProperty(value = "项目类型 0 标准项目 1 文本项目 2 数值项目 3 包装项目 4物料无关的项目", dataType = "String")
    var projectType: String? = null

    @ApiModelProperty(value = "开始时间", dataType = "String")
    var startTime: Date? = null

    @ApiModelProperty(value = "结束时间", dataType = "String")
    var endTime: Date? = null

    @ApiModelProperty(value = "负责人", dataType = "String")
    var principal: String? = null

    @ApiModelProperty(value = "联系方式", dataType = "String")
    var mobile: String? = null

    @ApiModelProperty(value = "申报时间", dataType = "String")
    var declarationTime: Date? = null

    @ApiModelProperty(value = "可见范围0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区", dataType = "String")
    var visibleRange: String? = null

    @ApiModelProperty(value = "预警时间", dataType = "String")
    var warningTime: Date? = null

    @ApiModelProperty(value = "完成时间", dataType = "String")
    var completeTime: Date? = null

    @ApiModelProperty(value = "状态 0 未开始 1 已开始 2 已完成", dataType = "Integer")
    var state: Integer? = null

    @ApiModelProperty(value = "系统状态 0 正常 1 延期 2 预警  3 取消  4 完结", dataType = "Integer")
    var sysState: Integer? = null

    @ApiModelProperty(value = "所属街道", dataType = "String")
    var street: String? = null

    @ApiModelProperty(value = "所属社区", dataType = "String")
    var community: String? = null

    @ApiModelProperty(value = "所属网格", dataType = "String")
    var grid: String? = null

    @ApiModelProperty(value = "审核状态 0 通过 1 不通过", dataType = "String")
    var checkState: String? = null

    @ApiModelProperty(value = "审核不通过原因", dataType = "String")
    var checkReson: String? = null

    @ApiModelProperty(value = "项目阶段信息", dataType = "List")
    var segmentationList: List<ProjectSegmentationVO>? = null

    @ApiModelProperty(value = "项目延期、取消申请信息", dataType = "JSON")
    var applyInfo: ProjectApplyVO? = null

    @ApiModelProperty(value = "申请类型 0 正常 1 新增 2 延期 3 取消", dataType = "String")
    var applyType: String? = null

    @ApiModelProperty(value = "申请理由", dataType = "String")
    var applyReson: String? = null

    @ApiModelProperty(value = "创建人编号", dataType = "String")
    var createUser: String? = null

}
