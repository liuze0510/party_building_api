package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 街道联席会议
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_street_joint_meeting")
class StreetJointMeeting : BaseModel<StreetJointMeeting>() {

    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 会议名称
     */
    @TableField("meeting_name")
    var meetingName: String? = null
    /**
     * 会议成员
     */
    @TableField("meeting_member")
    var meetingMember: String? = null
    /**
     * 召集人
     */
    @TableField("convener")
    var convener: String? = null
    /**
     * 牵头负责单位
     */
    @TableField("responsible_unit")
    var responsibleUnit: String? = null
    /**
     * 负责人姓名
     */
    @TableField("principal_name")
    var principalName: String? = null
    /**
     * 负责人账号
     */
    @TableField("principal_account")
    var principalAccount: String? = null
    /**
     * 会议简介
     */
    @TableField("meeting_introduction")
    var meetingIntroduction: String? = null

    override fun toString(): String {
        return "StreetJointMeeting{" +
        "street=" + street +
        ", meetingName=" + meetingName +
        ", meetingMember=" + meetingMember +
        ", convener=" + convener +
        ", responsibleUnit=" + responsibleUnit +
        ", principalName=" + principalName +
        ", principalAccount=" + principalAccount +
        ", meetingIntroduction=" + meetingIntroduction +
        "}"
    }
}
