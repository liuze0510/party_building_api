package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 民主生活会
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@TableName("bt_democratic_life_meeting")
class DemocraticLifeMeeting : BaseModel<DemocraticLifeMeeting>() {

    /** 党委Id */
    @TableField("party_committee_id")
    var partyCommitteeId: String? = null

    /** 党工委Id */
    @TableField("party_work_committee_id")
    var partyWorkCommitteeId: String? = null

    /** 党总支Id */
    @TableField("party_total_branch_id")
    var partyTotalBranchId: String? = null

    /** 党支部Id */
    @TableField("party_branch_id")
    var partyBranchId: String? = null

    /** 党委名称 */
    @TableField("party_committee_name")
    var partyCommitteeName: String? = null

    /** 党工委名称 */
    @TableField("party_work_committee_name")
    var partyWorkCommitteeName: String? = null

    /** 党总支名称 */
    @TableField("party_total_branch_name")
    var partyTotalBranchName: String? = null

    /** 党支部名称 */
    @TableField("party_branch_name")
    var partyBranchName: String? = null

    @TableField("content")
    var content: String? = null

    /**
     * 类型
     */
    @TableField("type")
    var type: String? = null
    /**
     * 主题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("browseNum")
    var browseNum: Integer? = null
    /**
     * 开始时间
     */
    @TableField("start_time")
    var startTime: Date? = null
    /**
     * 地点
     */
    @TableField("place")
    var place: String? = null
    /**
     * 缺席人员
     */
    @TableField("absentee")
    var absentee: String? = null
    /**
     * 应到人数
     */
    @TableField("number_people")
    var numberPeople: Integer? = null
    /**
     * 实到人数
     */
    @TableField("actual_number")
    var actualNumber: Integer? = null
    /**
     * 视频地址
     */
    @TableField("video")
    var video: String? = null


    override fun toString(): String {
        return "DemocraticLifeMeeting{" +
        "type=" + type +
        ", title=" + title +
        ", startTime=" + startTime +
        ", place=" + place +
        ", absentee=" + absentee +
        ", numberPeople=" + numberPeople +
        ", actualNumber=" + actualNumber +
        ", video=" + video +
        "}"
    }
}
