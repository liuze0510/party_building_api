package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 图片信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_image")
class Image : BaseModel<Image>() {

    /**
     * 关联编号
     */
    @TableField("associa_id")
    var associaId: String? = null
    /**
     * 类型0 签到 1工作内容 2 三会一课 3 支部活动 4 扶贫行政村 5 民主生活会 6 组织生活会
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 图片地址
     */
    @TableField("url")
    var url: String? = null

    override fun toString(): String {
        return "Image{" +
        "associaId=" + associaId +
        ", type=" + type +
        ", url=" + url +
        "}"
    }
}
