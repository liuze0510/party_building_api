package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 驻区单位活动
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@TableName("bt_resident_unit_activity")
class ResidentUnitActivity : BaseModel<ResidentUnitActivity>() {

    /**
     * 街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 单位编号
     */
    @TableField("unit_id")
    var unitId: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null

    override fun toString(): String {
        return "ResidentUnitActivity{" +
        "street=" + street +
        ", community=" + community +
        ", unitId=" + unitId +
        ", cover=" + cover +
        ", title=" + title +
        ", organizer=" + organizer +
        ", activityLocation=" + activityLocation +
        ", activityTime=" + activityTime +
        ", activityDesc=" + activityDesc +
        "}"
    }
}
