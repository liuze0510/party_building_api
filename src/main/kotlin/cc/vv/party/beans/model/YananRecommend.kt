package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import java.util.*

/**
 * <p>
 * 延安推荐信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_yanan_recommend")
class YananRecommend : Model<YananRecommend>() {

    /** 编号 */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /** 关联编号 */
    @TableField("associa_id")
    var associaId: String? = null

    /**  类型 0 支部活动 1 三会一课 */
    @TableField("type")
    var type: Integer? = null

    /** 创建时间 */
    @TableField("create_time")
    var createTime: Date? = null


    override fun pkVal(): String? {
        return id
    }

    override fun toString(): String {
        return "YananRecommend(id=$id, associaId=$associaId, type=$type, createTime=$createTime)"
    }


}
