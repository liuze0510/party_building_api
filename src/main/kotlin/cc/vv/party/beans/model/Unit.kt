package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 单位信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_unit")
class Unit : BaseModel<Unit>() {

    /**
     * 单位编号
     */
    @TableField("unit_id")
    var unitId: String? = null
    /**
     * 单位名称
     */
    @TableField("unit_name")
    var unitName: String? = null
    /**
     * 负责人
     */
    @TableField("principal")
    var principal: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 党员人数
     */
    @TableField("party_num")
    var partyNum: Integer? = null
    /**
     * 单位地址
     */
    @TableField("unit_addr")
    var unitAddr: String? = null
    /**
     * 邮编
     */
    @TableField("zip_code")
    var zipCode: String? = null
    /**
     * 报道街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 报道社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 报道时间
     */
    @TableField("report_time")
    var reportTime: Date? = null

    /**
     * 是否报道 0 已报到 1 未报到
     */
    @TableField("reported")
    var reported: Integer? = null

    override fun toString(): String {
        return "Unit{" +
                "unitName=" + unitName +
                ", principal=" + principal +
                ", mobile=" + mobile +
                ", partyNum=" + partyNum +
                ", unitAddr=" + unitAddr +
                ", zipCode=" + zipCode +
                ", street=" + street +
                ", community=" + community +
                ", reportTime=" + reportTime +
                "}"
    }
}
