package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 非公党建单位
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_not_party_unit")
class NotPartyUnit : BaseModel<NotPartyUnit>() {

    /**
     * 单位名称
     */
    @TableField("unit_name")
    var unitName: String? = null
    /**
     * 单位简介
     */
    @TableField("introduction")
    var introduction: String? = null

    /**
     * 组织类型 0 非公企业 1 非公党组织
     */
    @TableField("type")
    var type: Integer? = null

    override fun toString(): String {
        return "NotPartyUnit{" +
        "unitName=" + unitName +
        ", introduction=" + introduction +
        "}"
    }
}
