package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 承诺信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_committed")
class Committed : BaseModel<Committed>() {

    /**
     * 目标Id，如果是单位承诺，则保存单位Id，如果是党员承诺，这保存userId
     */
    @TableField("target_id")
    var targetId: String? = null
    /**
     * 承诺类型0 单位承诺 1 党员承诺
     */
    @TableField("committed_type")
    var committedType: Integer? = null
    /**
     * 承诺单位名称，当承诺类型为单位是才需要填写
     */
    @TableField("committed_unit_name")
    var committedUnitName: String? = null
    /**
     * 承诺内容
     */
    @TableField("content")
    var content: String? = null
    /**
     * 完成时间
     */
    @TableField("complete_time")
    var completeTime: Date? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 可见范围
     */
    @TableField("visible_range")
    var visibleRange: String? = null

    override fun toString(): String {
        return "Committed{" +
                "committedType=" + committedType +
                ", committedUnitName=" + committedUnitName +
                ", content=" + content +
                ", completeTime=" + completeTime +
                ", mobile=" + mobile +
                ", visibleRange=" + visibleRange +
                "}"
    }
}
