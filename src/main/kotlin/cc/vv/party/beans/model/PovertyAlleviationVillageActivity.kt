package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 脱贫行政村活动
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@TableName("bt_poverty_alleviation_village_activity")
class PovertyAlleviationVillageActivity : BaseModel<PovertyAlleviationVillageActivity>() {

    /**
     * 区
     */
    @TableField("area")
    var area: String? = null

    /**
     * 镇
     */
    @TableField("town")
    var town: String? = null
    /**
     * 村
     */
    @TableField("village")
    var village: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null


    override fun toString(): String {
        return "PovertyAlleviationVillageActivity{" +
        "town=" + town +
        ", village=" + village +
        ", cover=" + cover +
        ", title=" + title +
        ", organizer=" + organizer +
        ", activityLocation=" + activityLocation +
        ", activityTime=" + activityTime +
        ", activityDesc=" + activityDesc +
        "}"
    }
}
