package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 单位评星晋阶明细
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_rating_details")
class RatingDetails : BaseModel<RatingDetails>() {

    /**
     * 单位评星晋阶编号 关联bt_unit_evaluation_star
     */
    @TableField("star_id")
    var starId: String? = null
    /**
     * 项
     */
    @TableField("item_id")
    var itemId: String? = null
    /**
     * 得分
     */
    @TableField("score")
    var score: Float? = null

    override fun toString(): String {
        return "RatingDetails{" +
        "starId=" + starId +
        ", itemId=" + itemId +
        ", score=" + score +
        "}"
    }
}
