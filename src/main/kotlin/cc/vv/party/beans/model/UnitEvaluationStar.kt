package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 单位评星晋阶
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_unit_evaluation_star")
class UnitEvaluationStar : BaseModel<UnitEvaluationStar>() {

    /**
     * 单位名称
     */
    @TableField("company_name")
    var companyName: String? = null
    /**
     * 组织类型  0 非公党组织 1 社会党组织
     */
    @TableField("org_type")
    var orgType: String? = null
    /**
     * 得分
     */
    @TableField("score")
    var score: Float? = null

    override fun toString(): String {
        return "UnitEvaluationStar{" +
        "companyName=" + companyName +
        ", orgType=" + orgType +
        ", score=" + score +
        "}"
    }
}
