package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * banner管理
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_banner")
class Banner : BaseModelWithLogicDelete<Banner>() {

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 图片
     */
    @TableField("cover")
    var cover: String? = null

    /**
     * 分类，0：党群新闻 1 党建概况  2 宝塔头条  3街道头条  4 社区头条 5 网格头条
     */
    @TableField("category")
    var category: Int? = null

    /**
     * 内容
     */
    @TableField("content")
    var content: String? = null

    /**
     * 外部链接
     */
    @TableField("url")
    var url: String? = null

    /**
     * 编辑时间
     */
    @TableField("edit_time")
    var editTime: Date? = null

    override fun toString(): String {
        return "Banner{" +
                "title=" + title +
                ", url=" + url +
                ", editTime=" + editTime +
                "}"
    }
}
