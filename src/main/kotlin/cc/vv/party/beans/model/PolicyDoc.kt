package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 政策文件信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_policy_doc")
class PolicyDoc : BaseModel<PolicyDoc>() {

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 来源
     */
    @TableField("source")
    var source: String? = null
    /**
     * 发布时间
     */
    @TableField("release_time")
    var releaseTime: Date? = null
    /**
     * 发布内容
     */
    @TableField("release_content")
    var releaseContent: String? = null

    override fun toString(): String {
        return "PolicyDoc{" +
        "title=" + title +
        ", source=" + source +
        ", releaseTime=" + releaseTime +
        ", releaseContent=" + releaseContent +
        "}"
    }
}
