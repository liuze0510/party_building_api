package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModelWithLogicDelete
import cc.vv.party.common.constants.enums.RoleType
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 角色
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@TableName("bt_role")
class Role : BaseModelWithLogicDelete<Role>() {

    /**
     * 角色名称
     */
    @TableField("name")
    var name: String? = null
    /**
     * 角色类型
     */
    @TableField("type")
    var type: RoleType? = null

    /**
     * 角色描述
     */
    @TableField("description")
    var description: String? = null

    override fun toString(): String {
        return "Role{" +
                "name=" + name +
                ", type=" + type +
                ", description=" + description +
                "}"
    }
}
