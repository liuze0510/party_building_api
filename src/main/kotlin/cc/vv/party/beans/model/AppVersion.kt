package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import java.util.*

/**
 * <p>
 * app版本管理
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("app_version")
class AppVersion : Model<AppVersion>() {

    /** 编号 */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /** 内容 */
    @TableField("content")
    var content: String? = null

    /** 版本 */
    @TableField("version_code")
    var version: String? = null

    /** 创建时间 */
    @TableField("create_time")
    var createTime: Date? = null


    override fun pkVal(): String? {
        return id
    }

    override fun toString(): String {
        return "AppVersion(id=$id, content=$content, version=$version, createTime=$createTime)"
    }


}
