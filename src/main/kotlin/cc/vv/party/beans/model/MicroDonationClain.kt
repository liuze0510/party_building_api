package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 微捐赠认领
 * </p>
 *
 * @author Gyb
 * @since 2018-11-05
 */
@TableName("bt_micro_donation_clain")
class MicroDonationClain : BaseModel<MicroDonationClain>() {

    /**
     * 微捐助编号
     */
    @TableField("micro_donation_id")
    var microDonationId: String? = null
    /**
     * 认领状态 0 未选中 1 已选中
     */
    @TableField("seelected")
    var seelected: Integer? = null
    /**
     * 认领人
     */
    @TableField("person")
    var person: String? = null
    /**
     * 认领人联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 认领时间
     */
    @TableField("time")
    var time: Date? = null
    /**
     * 备注
     */
    @TableField("remarks")
    var remarks: String? = null

    /**
     * 认领备注
     */
    @TableField("strDesc")
    var strDesc: String? = null



    override fun toString(): String {
        return "MicroDonationClain{" +
        "microDonationId=" + microDonationId +
        ", seelected=" + seelected +
        ", person=" + person +
        ", mobile=" + mobile +
        ", time=" + time +
        ", remarks=" + remarks +
        "}"
    }
}
