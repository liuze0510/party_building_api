package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 支部活动
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@TableName("bt_branch_activity")
class BranchActivity : BaseModel<BranchActivity>() {

    /**
     * 活动类型
     */
    @TableField("type")
    var type: String? = null
    /**
     * 活动主题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动地点
     */
    @TableField("activity_place")
    var activityPlace: String? = null
    /**
     * 缺席人员
     */
    @TableField("absentee")
    var absentee: String? = null
    /**
     * 应到人数
     */
    @TableField("number_people")
    var numberPeople: Integer? = null
    /**
     * 实到人数
     */
    @TableField("actual_number")
    var actualNumber: Integer? = null
    /**
     * 视频地址
     */
    @TableField("video")
    var video: String? = null


    override fun toString(): String {
        return "BranchActivity{" +
        "type=" + type +
        ", title=" + title +
        ", activityTime=" + activityTime +
        ", activityPlace=" + activityPlace +
        ", absentee=" + absentee +
        ", numberPeople=" + numberPeople +
        ", actualNumber=" + actualNumber +
        ", video=" + video +
        "}"
    }
}
