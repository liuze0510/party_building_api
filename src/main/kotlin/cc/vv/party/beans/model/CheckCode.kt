package cc.vv.party.beans.model

import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@TableName("bt_check_code")
class CheckCode : BaseModelWithLogicDelete<CheckCode>() {

    @TableField("code")
    var code: String? = null
}