package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 项目信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_project")
class Project : BaseModel<Project>() {

    /**
     * 项目名称
     */
    @TableField("project_name")
    var projectName: String? = null
    /**
     * 项目介绍
     */
    @TableField("project_introduction")
    var projectIntroduction: String? = null
    /**
     * 项目类型
     */
    @TableField("project_type")
    var projectType: String? = null

    /** 开始时间 */
    @TableField("start_time")
    var startTime: Date? = null

    /** 结束时间 */
    @TableField("end_time")
    var endTime: Date? = null

    /**
     * 负责人
     */
    @TableField("principal")
    var principal: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 申报时间
     */
    @TableField("declaration_time")
    var declarationTime: Date? = null
    /**
     * 可见范围 0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区
     */
    @TableField("visible_range")
    var visibleRange: String? = null
    /**
     * 预警时间
     */
    @TableField("warning_time")
    var warningTime: Date? = null
    /**
     * 完成时间
     */
    @TableField("complete_time")
    var completeTime: Date? = null
    /**
     * 状态 0 未开始 1 已开始 2 已完成
     */
    @TableField("state")
    var state: Integer? = null

    /**
     * 系统状态 0 正常 1 延期 2 预警  3 取消  4 完结
     */
    @TableField("sys_state")
    var sysState: Integer? = null

    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null

    /** 预警是否查看  0 正常 1 未查看 2 已查看 */
    @TableField("view")
    var view: Integer? = null

    /**
     * 审核状态 0 通过 1 不通过
     */
    @TableField("check_state")
    var checkState: Integer? = null
    /**
     * 审核不通过原因
     */
    @TableField("check_reson")
    var checkReson: String? = null

    /** 申请类型 0 正常 1 新增 2 延期 3 取消 */
    @TableField("apply_type")
    var applyType: Integer? = null

    /** 申请理由 */
    @TableField("apply_reson")
    var applyReson: String? = null


    override fun toString(): String {
        return "Project{" +
        "projectName=" + projectName +
        ", projectIntroduction=" + projectIntroduction +
        ", projectType=" + projectType +
        ", startTime=" + startTime +
                ", endTime=" + endTime +
        ", principal=" + principal +
        ", mobile=" + mobile +
        ", declarationTime=" + declarationTime +
        ", visibleRange=" + visibleRange +
        ", warningTime=" + warningTime +
        ", completeTime=" + completeTime +
        ", state=" + state +
        ", street=" + street +
        ", community=" + community +
        ", grid=" + grid +
        ", checkState=" + checkState +
        ", checkReson=" + checkReson +
        "}"
    }
}
