package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 三会一课
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@TableName("bt_three_meet_one_lesson")
class ThreeMeetOneLesson : BaseModel<ThreeMeetOneLesson>() {

    /**
     * 类型
     */
    @TableField("type")
    var type: String? = null
    /**
     * 主题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 开始时间
     */
    @TableField("start_time")
    var startTime: Date? = null
    /**
     * 会议地点
     */
    @TableField("meeting_place")
    var meetingPlace: String? = null
    /**
     * 缺席人员
     */
    @TableField("absentee")
    var absentee: String? = null
    /**
     * 应到人数
     */
    @TableField("number_people")
    var numberPeople: Integer? = null
    /**
     * 实到人数
     */
    @TableField("actual_number")
    var actualNumber: Integer? = null
    /**
     * 视频地址
     */
    @TableField("video")
    var video: String? = null


    override fun toString(): String {
        return "ThreeMeetOneLesson{" +
        "type=" + type +
        ", title=" + title +
        ", startTime=" + startTime +
        ", meetingPlace=" + meetingPlace +
        ", absentee=" + absentee +
        ", numberPeople=" + numberPeople +
        ", actualNumber=" + actualNumber +
        ", video=" + video +
        "}"
    }
}
