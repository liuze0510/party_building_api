package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 资源
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@TableName("bt_permission")
class Permission : BaseModelWithLogicDelete<Permission>() {

    /**
     * 角色名称
     */
    @TableField("name")
    var name: String? = null
    /**
     * 角色类型
     */
    @TableField("type")
    var type: String? = null
    /**
     * 父id
     */
    @TableField("parent_id")
    var parentId: String? = null

    /**
     * 资源路径
     */
    @TableField("path")
    var path: String? = null

    override fun toString(): String {
        return "Permission{" +
                "name=" + name +
                ", type=" + type +
                ", parentId=" + parentId +
                ", path=" + path +
                "}"
    }
}
