package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 角色资源
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@TableName("bt_role_permission")
class RolePermission : BaseModelWithLogicDelete<RolePermission>() {

    /**
     * 角色id
     */
    @TableField("role_id")
    var roleId: String? = null
    /**
     * 资源id
     */
    @TableField("permission_id")
    var permissionId: String? = null

    
    override fun toString(): String {
        return "RolePermission{" +
                "roleId=" + roleId +
                ", permissionId=" + permissionId +
                "}"
    }
}
