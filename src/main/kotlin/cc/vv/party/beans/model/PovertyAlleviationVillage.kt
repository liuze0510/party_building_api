package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 扶贫行政村信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_poverty_alleviation_village")
class PovertyAlleviationVillage : BaseModel<PovertyAlleviationVillage>() {

    /**
     * 父编号
     */
    @TableField("parent_id")
    var parentId: String? = null
    /**
     * 名称
     */
    @TableField("village_name")
    var villageName: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 地址
     */
    @TableField("addr")
    var addr: String? = null

    /**
     * 人口数量
     */
    @TableField("population")
    var population: Integer? = null
    /**
     * 耕地面积
     */
    @TableField("cultivated_area")
    var cultivatedArea: Float? = null
    /**
     * 主导产业
     */
    @TableField("leading_industry")
    var leadingIndustry: String? = null
    /**
     * 贫困户数量
     */
    @TableField("poverty_alleviation_num")
    var povertyAlleviationNum: Integer? = null
    /**
     * 党员数量
     */
    @TableField("party_num")
    var partyNum: Integer? = null
    /**
     * 是否脱贫 0 未脱贫 1 已脱贫
     */
    @TableField("poverty")
    var poverty: Integer? = null

    /**
     * 路径
     */
    @TableField("path")
    var path: String? = null

    override fun toString(): String {
        return "PovertyAlleviationVillage{" +
        "parentId=" + parentId +
        ", villageName=" + villageName +
        ", mobile=" + mobile +
        ", addr=" + addr +
        ", path=" + path +
        "}"
    }
}
