package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 留言板
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_message_board")
class MessageBoard : BaseModel<MessageBoard>() {

    /**
     * 所属区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null

    /**
     * 姓名
     */
    @TableField("name")
    var name: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 所属支部
     */
    @TableField("branch")
    var branch: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 内容
     */
    @TableField("conetnt")
    var conetnt: String? = null
    /**
     * 审核状态 0 通过 1 不通过
     */
    @TableField("check_state")
    var checkState: Integer? = null

    override fun toString(): String {
        return "MessageBoard{" +
                "area=" + area +
                ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
        "name=" + name +
        ", mobile=" + mobile +
        ", branch=" + branch +
        ", title=" + title +
        ", conetnt=" + conetnt +
        ", checkState=" + checkState +
        "}"
    }
}
