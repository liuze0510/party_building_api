package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType

/**
 * <p>
 * 街道联席会议成员
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_street_joint_meeting_member")
class StreetJointMeetingMember : Model<StreetJointMeetingMember>() {

    /** 编号 */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /** 会议编号 */
    @TableField("meeting_id")
    var meetingId: String? = null

    /** 成员编号 */
    @TableField("member_id")
    var memberId: String? = null


    override fun pkVal(): String? {
        return id
    }

    override fun toString(): String {
        return "StreetJointMeetingMember(id=$id, meetingId=$meetingId, memberId=$memberId)"
    }


}
