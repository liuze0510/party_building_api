package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 社区大党委与组织成员关系
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_party_committee_org_relation")
class PartyCommitteeOrgRelation : BaseModel<PartyCommitteeOrgRelation>() {

    /**
     * 党委编号
     */
    @TableField("party_committeey_id")
    var partyCommitteeyId: String? = null
    /**
     * 组织成员编号
     */
    @TableField("org_member_id")
    var orgMemberId: String? = null


    override fun toString(): String {
        return "PartyCommitteeOrgRelation{" +
        "partyCommitteeyId=" + partyCommitteeyId +
        ", orgMemberId=" + orgMemberId +
        "}"
    }
}
