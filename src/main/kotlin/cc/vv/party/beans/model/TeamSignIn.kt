package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 四支队伍信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_team_sign_in")
class TeamSignIn : BaseModel<TeamSignIn>() {

    /**
     * 队伍编号
     */
    @TableField("team_id")
    var teamId: String? = null
    /**
     * 签到类型 0 签到 1 签退
     */
    @TableField("sign_type")
    var signType: Integer? = null
    /**
     * 签到时间
     */
    @TableField("sign_time")
    var signTime: Date? = null

    override fun toString(): String {
        return "TeamSignIn{" +
        "teamId=" + teamId +
        ", signType=" + signType +
        ", signTime=" + signTime +
        "}"
    }
}
