package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.constants.enums.OrgType
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 组织机构
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_org")
class Org : BaseModel<Org>() {

    /**
     * 父编号
     */
    @TableField("p_id")
    var parentId: String? = null
    /**
     * 名称
     */
    @TableField("name")
    var name: String? = null
    /**
     * 电话
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 类型 0 区 1 街道 2 社区 3 网格 4 单位 5 社会组织 6 楼道党小组 7 普通党员 8 单元中心户
     */
    @TableField("type")
    var type: OrgType? = null
    /**
     * 路径
     */
    @TableField("path")
    var path: String? = null

    /** 单元中心户位置，只有党员中心户时，存在该字段值 */
    @TableField("location")
    var location: String? = null

    /**
     * 简介
     */
    @TableField("introduction")
    var introduction: String? = null

    /**
     * 党建简介
     */
    @TableField("party_introduction")
    var partyIntroduction: String? = null



    override fun toString(): String {
        return "Org{" +
                ", parentId=" + parentId +
                ", name=" + name +
                ", mobile=" + mobile +
                ", type=" + type +
                ", path=" + path +
                ", introduction=" + introduction +
                "}"
    }
}
