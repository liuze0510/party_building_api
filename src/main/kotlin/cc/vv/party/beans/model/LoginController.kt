package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType

/**
 * <p>
 * 单点控制管理
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("login_controller")
class LoginController : Model<LoginController>() {

    /** 编号 */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /** 内容 */
    @TableField("account")
    var account: String? = null

    /** token */
    @TableField("token")
    var token: String? = null


    override fun pkVal(): String? {
        return id
    }

    override fun toString(): String {
        return "LoginController(id=$id, account=$account, token=$token)"
    }


}
