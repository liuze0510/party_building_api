package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 *
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_user")
class User : BaseModelWithLogicDelete<User>() {

    /**
     * 姓名
     */
    @TableField("name")
    var name: String? = null
    /**
     * 电话
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 头像
     */
    @TableField("face_url")
    var faceUrl: String? = null
    /**
     * 身份证
     */
    @TableField("card")
    var card: String? = null
    /**
     * 是否党员 0 是 1 不是
     */
    @TableField("party")
    var party: Integer? = null
    /**
     * 性别 0 男 1 女
     */
    @TableField("sex")
    var sex: Integer? = null
    /**
     * 出生日期
     */
    @TableField("dateofbirth")
    var dateofbirth: Date? = null
    /**
     * 年龄
     */
    @TableField("age")
    var age: Integer? = null
    /**
     * 民族 0 汉族 1 少数民族
     */
    @TableField("ethnic")
    var ethnic: Integer? = null
    /**
     * 职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  8 其他职业
     */
    @TableField("career")
    var career: String? = null
    /**
     * 文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上
     */
    @TableField("educational_level")
    var educationalLevel: Integer? = null
    /**
     * 学历 0 初中 1 职中 3 高中 3 高职  4 大专 5 本科 6 研究生 7 硕士 8 博士
     */
    @TableField("education")
    var education: Integer? = null
    /**
     * 职称
     */
    @TableField("job_title")
    var jobTitle: String? = null
    /**
     * 身份 0 学生 1 农民 2 工人 3  干部（汗聘干）
     */
    @TableField("identity")
    var identity: Integer? = null
    /**
     * 工作单位
     */
    @TableField("work_company")
    var workCompany: String? = null
    /**
     * 所属区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 是否报道 0 已报到 1 未报到
     */
    @TableField("reported")
    var reported: Integer? = null
    /**
     * 报道时间
     */
    @TableField("report_time")
    var reportTime: Date? = null

    override fun toString(): String {
        return "User{" +
                "name=" + name +
                ", mobile=" + mobile +
                ", faceUrl=" + faceUrl +
                ", card=" + card +
                ", party=" + party +
                ", sex=" + sex +
                ", dateofbirth=" + dateofbirth +
                ", ethnic=" + ethnic +
                ", career=" + career +
                ", educationalLevel=" + educationalLevel +
                ", education=" + education +
                ", jobTitle=" + jobTitle +
                ", identity=" + identity +
                ", workCompany=" + workCompany +
                ", area=" + area +
                ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
                "}"
    }
}
