package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 项目阶段信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_project_segmentation")
class ProjectSegmentation : BaseModel<ProjectSegmentation>() {

    /**
     * 项目编号 关联bt_project
     */
    @TableField("project_id")
    var projectId: String? = null
    /**
     * 阶段
     */
    @TableField("stage")
    var stage: String? = null
    /**
     * 内容
     */
    @TableField("content")
    var content: String? = null
    /**
     * 预警时间
     */
    @TableField("warning_time")
    var warningTime: Date? = null
    /**
     * 完成时间
     */
    @TableField("complete_time")
    var completeTime: Date? = null
    /**
     * 状态 0 未开始 1 已开始 2 已完成
     */
    @TableField("state")
    var state: Integer? = null

    override fun toString(): String {
        return "ProjectSegmentation{" +
        "projectId=" + projectId +
        ", stage=" + stage +
        ", content=" + content +
        ", warningTime=" + warningTime +
        ", completeTime=" + completeTime +
        ", state=" + state +
        "}"
    }
}
