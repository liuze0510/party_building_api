package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 *
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_org_admin")
class OrgAdmin : BaseModelWithLogicDelete<OrgAdmin>() {

    /**
     * 组织机构编号
     */
    @TableField("org_id")
    var orgId: String? = null
    /**
     * 用户登录账号
     */
    @TableField("user_account")
    var userAccount: String? = null
    /**
     * 用户姓名
     */
    @TableField("user_name")
    var userName: String? = null

    override fun toString(): String {
        return "OrgAdmin{" +
                "orgId=" + orgId +
                ", userAccount=" + userAccount +
                ", userName=" + userName +
                "}"
    }
}
