package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 楼栋党小组活动
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@TableName("bt_building_party_group_activity")
class BuildingPartyGroupActivity : BaseModel<BuildingPartyGroupActivity>() {

    /**
     * 街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 楼栋党小组编号
     */
    @TableField("group_id")
    var groupId: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null


    override fun toString(): String {
        return "BuildingPartyGroupActivity{" +
        "street=" + street +
        ", community=" + community +
        ", grid=" + grid +
        ", groupId=" + groupId +
        ", cover=" + cover +
        ", title=" + title +
        ", organizer=" + organizer +
        ", activityLocation=" + activityLocation +
        ", activityTime=" + activityTime +
        ", activityDesc=" + activityDesc +
        "}"
    }
}
