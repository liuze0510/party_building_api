package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 承诺评论信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_committed_comment")
class CommittedComment : BaseModel<CommittedComment>() {

    /**
     * 承诺编号
     */
    @TableField("committed_id")
    var committedId: String? = null
    /**
     * 评论人编号
     */
    @TableField("comment_id")
    var commentId: String? = null
    /**
     * 评论人姓名
     */
    @TableField("comment_name")
    var commentName: String? = null
    /**
     * 评论人头像
     */
    @TableField("face_url")
    var faceUrl: String? = null
    /**
     * 评论内容
     */
    @TableField("content")
    var content: String? = null
    /**
     * 评论时间
     */
    @TableField("comment_time")
    var commentTime: Date? = null

    override fun toString(): String {
        return "CommittedComment{" +
        "committedId=" + committedId +
        ", commentId=" + commentId +
        ", commentName=" + commentName +
        ", faceUrl=" + faceUrl +
        ", content=" + content +
        ", commentTime=" + commentTime +
        "}"
    }
}
