package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 社区大党委活动
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@TableName("bt_party_activity")
class PartyActivity : BaseModel<PartyActivity>() {

    /**
     * 街道编号
     */
    @TableField("street")
    var street: String? = null
    /**
     * 社区编号
     */
    @TableField("community")
    var community: String? = null
    /**
     * 社区党委编号
     */
    @TableField("party_id")
    var partyId: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null

    override fun toString(): String {
        return "PartyActivity{" +
        "street=" + street +
        ", partyId=" + partyId +
        ", cover=" + cover +
        ", title=" + title +
        ", organizer=" + organizer +
        ", activityLocation=" + activityLocation +
        ", activityTime=" + activityTime +
        ", activityDesc=" + activityDesc +
        "}"
    }
}
