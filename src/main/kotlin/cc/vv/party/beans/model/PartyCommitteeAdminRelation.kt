package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 社区大党委管理员
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_party_committee_admin_relation")
class PartyCommitteeAdminRelation : BaseModel<PartyCommitteeAdminRelation>() {

    /**
     * 党委编号
     */
    @TableField("party_committeey_id")
    var partyCommitteeyId: String? = null
    /**
     * 管理员姓名
     */
    @TableField("admin_name")
    var adminName: String? = null
    /**
     * 管理员账号
     */
    @TableField("admin_account")
    var adminAccount: String? = null
    /**
     * 所属单位
     */
    @TableField("company_name")
    var companyName: String? = null

    override fun toString(): String {
        return "PartyCommitteeAdminRelation{" +
        "partyCommitteeyId=" + partyCommitteeyId +
        ", adminName=" + adminName +
        ", adminAccount=" + adminAccount +
        ", companyName=" + companyName +
        "}"
    }
}
