package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 社会主义核心价值观
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@TableName("bt_socialist_values")
class SocialistValues : BaseModel<SocialistValues>() {

    /**
     * 类型 0 文明 1 和谐 2 敬业  3 友善 4 诚信
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null

    /** 视频 */
    @TableField("video")
    var video: String? = null

    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null

    /**
     * 标题
     */
    @TableField("browseNum")
    var browseNum: Integer? = null

    /**
     * 内容
     */
    @TableField("conetnt")
    var conetnt: String? = null
    /**
     * 来源
     */
    @TableField("source")
    var source: String? = null
    /**
     * 发布人
     */
    @TableField("publisher")
    var publisher: String? = null
    /**
     * 发布时间
     */
    @TableField("release_time")
    var releaseTime: Date? = null


    override fun toString(): String {
        return "SocialistValues{" +
        "type=" + type +
        ", title=" + title +
        ", conetnt=" + conetnt +
        ", source=" + source +
        ", publisher=" + publisher +
        ", releaseTime=" + releaseTime +
        "}"
    }
}
