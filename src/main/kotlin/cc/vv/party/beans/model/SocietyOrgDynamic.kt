package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 社会组织动态
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_society_org_dynamic")
class SocietyOrgDynamic : BaseModel<SocietyOrgDynamic>() {

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 来源
     */
    @TableField("source")
    var source: String? = null
    /**
     * 发布时间
     */
    @TableField("release_time")
    var releaseTime: Date? = null
    /**
     * 发布组织
     */
    @TableField("org_id")
    var orgId: String? = null
    /**
     * 发布组织
     */
    @TableField("release_org")
    var releaseOrg: String? = null
    /**
     * 发布内容
     */
    @TableField("release_content")
    var releaseContent: String? = null

    /**
     * 社区Id，冗余字端，方便查询
     */
    @TableField("community_id")
    var communityId: String? = null

    /** 所属街道 */
    @TableField("street")
    var street: String? = null

    override fun toString(): String {
        return "SocietyOrgDynamic{" +
                "title=" + title +
                ", source=" + source +
                ", releaseTime=" + releaseTime +
                ", orgId=" + orgId +
                ", releaseOrg=" + releaseOrg +
                ", releaseContent=" + releaseContent +
                "}"
    }
}
