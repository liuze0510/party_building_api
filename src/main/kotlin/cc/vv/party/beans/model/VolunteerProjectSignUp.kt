package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 志愿项目报名信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_volunteer_project_sign_up")
class VolunteerProjectSignUp : BaseModel<VolunteerProjectSignUp>() {

    /**
     * 志愿项目编号
     */
    @TableField("volunteer_project_id")
    var volunteerProjectId: String? = null
    /**
     * 姓名
     */
    @TableField("name")
    var name: String? = null
    /**
     * 性别
     */
    @TableField("sex")
    var sex: Integer? = null
    /**
     * 所属支部
     */
    @TableField("branch")
    var branch: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null

    override fun toString(): String {
        return "VolunteerProjectSignUp{" +
        "volunteerProjectId=" + volunteerProjectId +
        ", name=" + name +
        ", sex=" + sex +
        ", branch=" + branch +
        ", mobile=" + mobile +
        "}"
    }
}
