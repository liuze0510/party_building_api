package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 岗位信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_post")
class Post : BaseModel<Post>() {

    /**
     * 岗位名称
     */
    @TableField("post_name")
    var postName: String? = null
    /**
     * 岗位来源
     */
    @TableField("post_source")
    var postSource: String? = null
    /**
     * 招聘人数
     */
    @TableField("job_num")
    var jobNum: Integer? = null
    /**
     * 工作年限
     */
    @TableField("work_year")
    var workYear: Integer? = null
    /**
     * 学历 0 初中及以下 1 中专 2 高中 3 大专 4 本科 5 研究生及以上
     */
    @TableField("education")
    var education: String? = null
    /**
     * 薪资
     */
    @TableField("salary")
    var salary: String? = null
    /**
     * 工作地址
     */
    @TableField("work_addr")
    var workAddr: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 职位描述
     */
    @TableField("post_description")
    var postDescription: String? = null
    /**
     * 技能要求
     */
    @TableField("skill_claim")
    var skillClaim: String? = null
    /**
     * 公司简介
     */
    @TableField("company_introduction")
    var companyIntroduction: String? = null
    /**
     * 可见范围 0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区
     */
    @TableField("visible_range")
    var visibleRange: String? = null

    override fun toString(): String {
        return "Post{" +
        "postName=" + postName +
        ", postSource=" + postSource +
        ", jobNum=" + jobNum +
        ", workYear=" + workYear +
        ", education=" + education +
        ", salary=" + salary +
        ", workAddr=" + workAddr +
        ", mobile=" + mobile +
        ", postDescription=" + postDescription +
        ", skillClaim=" + skillClaim +
        ", companyIntroduction=" + companyIntroduction +
        ", visibleRange=" + visibleRange +
        "}"
    }
}
