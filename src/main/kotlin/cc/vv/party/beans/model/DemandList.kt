package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 需求清单
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_demand_list")
class DemandList : BaseModel<DemandList>() {

    /**
     * 所属区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 单位编号
     */
    @TableField("unit_id")
    var unitId: String? = null
    /**
     * 需求主题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 需求内容
     */
    @TableField("conetnt")
    var conetnt: String? = null
    /**
     * 负责人
     */
    @TableField("principal")
    var principal: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 有无酬劳 0 无 1有
     */
    @TableField("reward")
    var reward: Integer? = null
    /**
     * 金额
     */
    @TableField("amount")
    var amount: String? = null

    override fun toString(): String {
        return "DemandList{" +
        "area=" + area +
        ", street=" + street +
        ", community=" + community +
        ", grid=" + grid +
        ", unitId=" + unitId +
        ", title=" + title +
        ", conetnt=" + conetnt +
        ", principal=" + principal +
        ", mobile=" + mobile +
        ", reward=" + reward +
        ", amount=" + amount +
        "}"
    }
}
