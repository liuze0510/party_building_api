package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 民主、组织生活会类型
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@TableName("bt_life_meeting_type")
class LifeMeetingType : BaseModel<LifeMeetingType>() {

    /**
     * 类型 0 民主生活会 1 组织生活会
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 名称
     */
    @TableField("name")
    var name: String? = null

    override fun toString(): String {
        return "LifeMeetingType{" +
        "type=" + type +
        ", name=" + name +
        "}"
    }
}
