package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 社区活动
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_community_activity")
class CommunityActivity : BaseModel<CommunityActivity>() {

    /**
     * 活动类型 关联bt_community_activity_type
     */
    @TableField("activity_type")
    var activityType: String? = null
    /**
     * 街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 社区
     */
    @TableField("community")
    var community: String? = null

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null

    override fun toString(): String {
        return "CommunityActivity{" +
                "activityType=" + activityType +
                ", title=" + title +
                ", organizer=" + organizer +
                ", activityLocation=" + activityLocation +
                ", activityTime=" + activityTime +
                ", activityDesc=" + activityDesc +
                "}"
    }
}
