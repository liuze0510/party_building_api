package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import java.util.*

/**
 * <p>
 * 项目延期、取消申请表
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_project_apply")
class ProjectApply : Model<ProjectApply>() {

    /** 编号 */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /** 项目编号 */
    @TableField("pro_id")
    var proId: String? = null

    /** 阶段编号 */
    @TableField("seg_id")
    var segId: String? = null

    /** 申请类型 0 项目 1 阶段 */
    @TableField("type")
    var type: Integer? = null

    /** 预警时间 */
    @TableField("warning_time")
    var warningTime: Date? = null

    /** 完成时间 */
    @TableField("complete_time")
    var completeTime: Date? = null

    /** 申请理由 */
    @TableField("reason")
    var reason: String? = null

    /** 创建用户 */
    @TableField("create_user")
    var createUser: String? = null

    /** 创建时间 */
    @TableField("create_time")
    var createTime: Date? = null

    override fun pkVal(): String? {
        return id
    }

    override fun toString(): String {
        return "ProjectApply(id=$id, proId=$proId, segId=$segId, type=$type, warningTime=$warningTime, completeTime=$completeTime, reason=$reason, createUser=$createUser, createTime=$createTime)"
    }


}
