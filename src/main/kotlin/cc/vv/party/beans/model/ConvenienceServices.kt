package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 便民服务
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_convenience_services")
class ConvenienceServices : BaseModel<ConvenienceServices>() {

    /**
     * 所属区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 类别 关联bt_convenience_service_type 中id
     */
    @TableField("type")
    var type: String? = null
    /**
     * 主题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 内容
     */
    @TableField("conetnt")
    var conetnt: String? = null

    /**
     * 编辑时间
     */
    @TableField("edit_time")
    var editTime: Date? = null

    override fun toString(): String {
        return "ConvenienceServices{" +
        "area=" + area +
        ", street=" + street +
        ", community=" + community +
        ", grid=" + grid +
        ", type=" + type +
        ", title=" + title +
        ", conetnt=" + conetnt +
        "}"
    }
}
