package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 红领党建
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@TableName("bt_red_party_building")
class RedPartyBuilding : BaseModel<RedPartyBuilding>() {

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 类型: 0 品牌简介 1 六大工程 2 组织引领 3 党员引领 4 文化引领 5 标准引领
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 内容
     */
    @TableField("conetnt")
    var conetnt: String? = null


    override fun toString(): String {
        return "RedPartyBuilding{" +
        "title=" + title +
        ", cover=" + cover +
        ", conetnt=" + conetnt +
        "}"
    }
}
