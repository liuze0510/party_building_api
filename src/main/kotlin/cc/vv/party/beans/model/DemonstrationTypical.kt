package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 示范典型
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@TableName("bt_demonstration_typical")
class DemonstrationTypical : BaseModel<DemonstrationTypical>() {

    /**
     * 类型 0 农村党组织  1 社区党组织  2 学校党组织  3 机关党组织 4 非公党组织  5 社会组织党组织 6 国家企事业单位党组织
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 标题
     */
    @TableField("browseNum")
    var browseNum: Integer? = null
    /**
     * 内容
     */
    @TableField("content")
    var content: String? = null


    override fun toString(): String {
        return "DemonstrationTypical{" +
        "type=" + type +
        ", title=" + title +
        ", cover=" + cover +
        ", content=" + content +
        "}"
    }
}
