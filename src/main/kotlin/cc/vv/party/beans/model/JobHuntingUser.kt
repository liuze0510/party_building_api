package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 求职信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_job_hunting_user")
class JobHuntingUser : BaseModel<JobHuntingUser>() {

    /**
     * 姓名
     */
    @TableField("name")
    var name: String? = null
    /**
     * 头像
     */
    @TableField("face_url")
    var faceUrl: String? = null
    /**
     * 性别
     */
    @TableField("sex")
    var sex: Integer? = null
    /**
     * 出生日期
     */
    @TableField("dateofbirth")
    var dateofbirth: Date? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 工作年限
     */
    @TableField("work_year")
    var workYear: Integer? = null
    /**
     * 学历
     */
    @TableField("education")
    var education: String? = null
    /**
     * 求职意向
     */
    @TableField("career_objective")
    var careerObjective: String? = null
    /**
     * 期望薪资
     */
    @TableField("expected_salary")
    var expectedSalary: String? = null
    /**
     * 技能标签
     */
    @TableField("skill_tag")
    var skillTag: String? = null
    /**
     * 工作经历
     */
    @TableField("work_experience")
    var workExperience: String? = null
    /**
     * 可见范围 0 本网格、1 本单位、2 本社区、3 本街道、4 宝塔区
     */
    @TableField("visible_range")
    var visibleRange: String? = null

    override fun toString(): String {
        return "JobHuntingUser{" +
        "name=" + name +
        ", faceUrl=" + faceUrl +
        ", sex=" + sex +
        ", dateofbirth=" + dateofbirth +
        ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
        ", mobile=" + mobile +
        ", workYear=" + workYear +
        ", education=" + education +
        ", careerObjective=" + careerObjective +
        ", expectedSalary=" + expectedSalary +
        ", skillTag=" + skillTag +
        ", workExperience=" + workExperience +
        ", visibleRange=" + visibleRange +
        "}"
    }
}
