package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import java.io.Serializable
import java.util.*

/**
 * <p>
 *
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
@TableName("pmc_person")
class PmcPerson : Model<PmcPerson>() {

    @TableId("ID")
    var id: String? = null
    @TableField("ACADEMICDEGREE")
    var academicdegree: String? = null
    @TableField("ADDRESS")
    var address: String? = null
    @TableField("AGE")
    var age: Integer? = null
    @TableField("BIRTHDAY")
    var birthday: Date? = null
    @TableField("CITY")
    var city: String? = null
    @TableField("COUNTRY")
    var country: String? = null
    @TableField("CREATETIME")
    var createtime: Date? = null
    @TableField("DELCAUSE")
    var delcause: String? = null
    @TableField("DELDEPICT")
    var deldepict: String? = null
    @TableField("DELETETIME")
    var deletetime: Date? = null
    @TableField("DIST")
    var dist: String? = null
    @TableField("education")
    var education: String? = null
    @TableField("EMAIL")
    var email: String? = null
    @TableField("HOMEPLACEADRESS")
    var homeplaceadress: String? = null
    @TableField("HOMEPLACECITY")
    var homeplacecity: String? = null
    @TableField("HOMEPLACEDIST")
    var homeplacedist: String? = null
    @TableField("HOMEPLACEPROVINCE")
    var homeplaceprovince: String? = null
    @TableField("PID")
    var pid: String? = null
    @TableField("ISUSE")
    var isuse: String? = null
    @TableField("NAME")
    var name: String? = null
    @TableField("NATION")
    var nation: String? = null
    @TableField("REMARK")
    var remark: String? = null
    @TableField("PHONE")
    var phone: String? = null
    @TableField("PHOTONAME")
    var photoname: String? = null
    @TableField("PROVINCE")
    var province: String? = null
    @TableField("SEX")
    var sex: String? = null
    @TableField("TIMESTAMPFOLDER")
    var timestampfolder: String? = null
    @TableField("ZIPCODE")
    var zipcode: String? = null


    override fun pkVal(): Serializable? {
        return id!!
    }

    override fun toString(): String {
        return "PmcPerson{" +
                "id=" + id +
                ", academicdegree=" + academicdegree +
                ", address=" + address +
                ", age=" + age +
                ", birthday=" + birthday +
                ", city=" + city +
                ", country=" + country +
                ", createtime=" + createtime +
                ", delcause=" + delcause +
                ", deldepict=" + deldepict +
                ", deletetime=" + deletetime +
                ", dist=" + dist +
                ", education=" + education +
                ", email=" + email +
                ", homeplaceadress=" + homeplaceadress +
                ", homeplacecity=" + homeplacecity +
                ", homeplacedist=" + homeplacedist +
                ", homeplaceprovince=" + homeplaceprovince +
                ", pid=" + pid +
                ", isuse=" + isuse +
                ", name=" + name +
                ", nation=" + nation +
                ", remark=" + remark +
                ", phone=" + phone +
                ", photoname=" + photoname +
                ", province=" + province +
                ", sex=" + sex +
                ", timestampfolder=" + timestampfolder +
                ", zipcode=" + zipcode +
                "}"
    }
}
