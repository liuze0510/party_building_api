package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 社会组织
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_society_org")
class SocietyOrg : BaseModel<SocietyOrg>() {

    /**
     * 组织名称
     */
    @TableField("org_name")
    var orgName: String? = null
    /**
     * 所在街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所在社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 负责人
     */
    @TableField("principal")
    var principal: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 简介
     */
    @TableField("introduction")
    var introduction: String? = null

    override fun toString(): String {
        return "SocietyOrg{" +
        "orgName=" + orgName +
        ", community=" + community +
        ", principal=" + principal +
        ", mobile=" + mobile +
        ", introduction=" + introduction +
        "}"
    }
}
