package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 志愿者信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_volunteer_user")
class VolunteerUser : BaseModel<VolunteerUser>() {

    /**
     * 姓名
     */
    @TableField("name")
    var name: String? = null
    /**
     * 头像
     */
    @TableField("face_url")
    var faceUrl: String? = null
    /**
     * 性别 0 男 1 女
     */
    @TableField("sex")
    var sex: Integer? = null
    /**
     * 年龄
     */
    @TableField("age")
    var age: Integer? = null
    /**
     * 出生日期
     */
    @TableField("dateofbirth")
    var dateofbirth: Date? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 职业
     */
    @TableField("career")
    var career: String? = null
    /**
     * 个人特长
     */
    @TableField("specialty")
    var specialty: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 志愿类型
     */
    @TableField("volunteer_type")
    var volunteerType: String? = null
    /**
     * 志愿时间
     */
    @TableField("volunteer_time")
    var volunteerTime: Date? = null
    /**
     * 备注
     */
    @TableField("remarks")
    var remarks: String? = null

    override fun toString(): String {
        return "VolunteerUser{" +
        "name=" + name +
        ", faceUrl=" + faceUrl +
        ", sex=" + sex +
        ", dateofbirth=" + dateofbirth +
                ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
        ", career=" + career +
        ", specialty=" + specialty +
        ", mobile=" + mobile +
        ", volunteerType=" + volunteerType +
        ", volunteerTime=" + volunteerTime +
        ", remarks=" + remarks +
        "}"
    }
}
