package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 党建概况
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_introduction")
class Introduction : BaseModel<Introduction>() {

    /**
     * 类别 0 宝塔简介 1.党建概括
     */
    @TableField("type")
    var type: String? = null
    /**
     * 内容
     */
    @TableField("conetnt")
    var content: String? = null

    override fun toString(): String {
        return "Introduction{" +
                "type=" + type +
                ", content=" + content +
                "}"
    }
}
