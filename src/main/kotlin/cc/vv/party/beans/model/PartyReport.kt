package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 党员报道信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_party_report")
class PartyReport : BaseModelWithLogicDelete<PartyReport>() {

    /**
     * 名称
     */
    @TableField("name")
    var name: String? = null
    /**
     * 报道街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 报道社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 报道网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 职业特点
     */
    @TableField("professional_characteristics")
    var professionalCharacteristics: String? = null
    /**
     * 工作时间
     */
    @TableField("work_time")
    var workTime: Date? = null
    /**
     * 特长爱好
     */
    @TableField("special_hobby")
    var specialHobby: String? = null
    /**
     * 个人承诺
     */
    @TableField("personal_commitment")
    var personalCommitment: String? = null
    /**
     * 是否愿意成为志愿者 0 愿意 1 不愿意
     */
    @TableField("volunteer")
    var volunteer: Integer? = null

    override fun toString(): String {
        return "PartyReport{" +
        "name=" + name +
        ", street=" + street +
        ", community=" + community +
        ", grid=" + grid +
        ", mobile=" + mobile +
        ", professionalCharacteristics=" + professionalCharacteristics +
        ", workTime=" + workTime +
        ", specialHobby=" + specialHobby +
        ", personalCommitment=" + personalCommitment +
        ", volunteer=" + volunteer +
        "}"
    }
}
