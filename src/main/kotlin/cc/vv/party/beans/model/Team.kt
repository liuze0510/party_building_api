package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 四支队伍信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_team")
class Team : BaseModel<Team>() {

    /**
     * 姓名
     */
    @TableField("pname")
    var pname: String? = null
    /**
     * 头像
     */
    @TableField("face_url")
    var faceUrl: String? = null
    /**
     * 性别
     */
    @TableField("sex")
    var sex: Integer? = null
    /**
     * 年龄
     */
    @TableField("age")
    var age: Integer? = null
    /**
     * 学历 0 初中及以下 1 中专 2 高中 3 大专 4 本科 5 研究生及以上
     */
    @TableField("education")
    var education: String? = null
    /**
     * 所在区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所在镇
     */
    @TableField("town")
    var town: String? = null
    /**
     * 所在村
     */
    @TableField("village")
    var village: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 队伍类型 0 第一书记 1 村两委班子 2 扶贫工作队 3 驻村干部
     */
    @TableField("team_type")
    var teamType: String? = null
    /**
     * 职务
     */
    @TableField("plant")
    var plant: String? = null

    override fun toString(): String {
        return "Team{" +
        "pname=" + pname +
        ", faceUrl=" + faceUrl +
        ", sex=" + sex +
        ", age=" + age +
        ", education=" + education +
        ", area=" + area +
        ", town=" + town +
        ", village=" + village +
        ", mobile=" + mobile +
        ", teamType=" + teamType +
        ", plant=" + plant +
        "}"
    }
}
