package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModelWithLogicDelete
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_log")
class Log : BaseModelWithLogicDelete<Log>() {

    /**
     * 操作内容
     */
    @TableField("operate_content")
    var operateContent: String? = null
    /**
     * 操作账号
     */
    @TableField("operate_account")
    var operateAccount: String? = null
    /**
     * 操作账号Id
     */
    @TableField("operate_id")
    var operateId: String? = null
    /**
     * 操作人名称
     */
    @TableField("operate_name")
    var operateName: String? = null
    /**
     * 操作人角色名称
     */
    @TableField("operate_role")
    var operateRole: String? = null

    /**
     * ip地址
     */
    @TableField("remote_ip")
    var remoteIp: String? = null

    override fun toString(): String {
        return "Log{" +
                "operateContent=" + operateContent +
                ", operateAccount=" + operateAccount +
                ", operateId=" + operateId +
                ", operateName=" + operateName +
                ", operateRole=" + operateRole +
                ", remoteIp=" + remoteIp +
                "}"
    }
}
