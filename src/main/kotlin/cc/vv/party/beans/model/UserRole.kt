package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import cc.vv.party.common.base.BaseModel;
import cc.vv.party.common.base.BaseModelWithLogicDelete

/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@TableName("bt_user_role")
class UserRole : BaseModelWithLogicDelete<UserRole>() {

    /**
     * 用户id
     */
    @TableField("user_account")
    var userAccount: String? = null
    /**
     * 角色id
     */
    @TableField("role_id")
    var roleId: String? = null


    override fun toString(): String {
        return "UserRole{" +
                "userId=" + userAccount +
                ", roleId=" + roleId +
                "}"
    }
}
