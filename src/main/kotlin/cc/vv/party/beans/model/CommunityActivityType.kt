package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 社区活动类型
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_community_activity_type")
class CommunityActivityType : BaseModel<CommunityActivityType>() {

    /**
     * 类型名称
     */
    @TableField("type_name")
    var typeName: String? = null
    /**
     * 类型描述
     */
    @TableField("type_desc")
    var typeDesc: String? = null

    override fun toString(): String {
        return "CommunityActivityType{" +
        "typeName=" + typeName +
        ", typeDesc=" + typeDesc +
        "}"
    }
}
