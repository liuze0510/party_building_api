package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 党建要闻
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_party_building_news")
class PartyBuildingNews : BaseModel<PartyBuildingNews>() {

    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null
    /**
     * 浏览数
     */
    @TableField("browseNum")
    var browseNum: Integer? = null
    /**
     * 类型 0 宝塔头条 1 街道头条 2 社区头条 3 网格头条
     */
    @TableField("type")
    var type: Integer? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 内容
     */
    @TableField("content")
    var content: String? = null
    /**
     * 来源
     */
    @TableField("source")
    var source: String? = null
    /**
     * 发布人
     */
    @TableField("publisher")
    var publisher: String? = null
    /**
     * 发布时间
     */
    @TableField("release_time")
    var releaseTime: Date? = null

    override fun toString(): String {
        return "PartyBuildingNews{" +
                "type=" + type +
                ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
                ", title=" + title +
                ", content=" + content +
                ", source=" + source +
                ", publisher=" + publisher +
                ", releaseTime=" + releaseTime +
                "}"
    }
}
