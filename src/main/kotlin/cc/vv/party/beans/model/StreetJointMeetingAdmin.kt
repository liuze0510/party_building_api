package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 联席会议管理员
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_street_joint_meeting_admin")
class StreetJointMeetingAdmin : BaseModel<StreetJointMeetingAdmin>() {

    /**
     * 会议编号
     */
    @TableField("meeting_id")
    var meetingId: String? = null
    /**
     * 管理员姓名
     */
    @TableField("admin_name")
    var adminName: String? = null
    /**
     * 管理员账号
     */
    @TableField("admin_account")
    var adminAccount: String? = null
    /**
     * 所属单位
     */
    @TableField("company_name")
    var companyName: String? = null

    override fun toString(): String {
        return "StreetJointMeetingAdmin{" +
        "meetingId=" + meetingId +
        ", adminName=" + adminName +
        ", adminAccount=" + adminAccount +
        ", companyName=" + companyName +
        "}"
    }
}
