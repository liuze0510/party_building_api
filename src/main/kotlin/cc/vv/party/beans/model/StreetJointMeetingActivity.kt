package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 街道联席会议活动
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
@TableName("bt_street_joint_meeting_activity")
class StreetJointMeetingActivity : BaseModel<StreetJointMeetingActivity>() {

    /**
     * 封面
     */
    @TableField("cover")
    var cover: String? = null

    /** 所属街道 */
    @TableField("street")
    var street: String? = null

    /** 所属会议 */
    @TableField("meeting_id")
    var meetingId: String? = null

    /**
     * 标题
     */
    @TableField("browseNum")
    var browseNum: Integer? = null

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null

    /**
     * 举办方
     */
    @TableField("organizer")
    var organizer: String? = null
    /**
     * 活动地点
     */
    @TableField("activity_location")
    var activityLocation: String? = null
    /**
     * 活动时间
     */
    @TableField("activity_time")
    var activityTime: Date? = null
    /**
     * 活动内容
     */
    @TableField("activity_desc")
    var activityDesc: String? = null

    override fun toString(): String {
        return "StreetJointMeetingActivity{" +
        "title=" + title +
        ", organizer=" + organizer +
        ", activityLocation=" + activityLocation +
        ", activityTime=" + activityTime +
        ", activityDesc=" + activityDesc +
        "}"
    }
}
