package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 便民服务类型
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_convenience_service_type")
class ConvenienceServiceType : BaseModel<ConvenienceServiceType>() {

    /**
     * 名称
     */
    @TableField("name")
    var name: String? = null

    override fun toString(): String {
        return "ConvenienceServiceType{" +
        "name=" + name +
        "}"
    }
}
