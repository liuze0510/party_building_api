package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 志愿项目信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_volunteer_project")
class VolunteerProject : BaseModel<VolunteerProject>() {

    /**
     * 项目名称
     */
    @TableField("project_name")
    var projectName: String? = null
    /**
     * 举办时间
     */
    @TableField("holding_time")
    var holdingTime: Date? = null
    /**
     * 举办单位
     */
    @TableField("holding_company")
    var holdingCompany: String? = null
    /**
     * 举办地点
     */
    @TableField("venue")
    var venue: String? = null
    /**
     * 项目名额
     */
    @TableField("project_quota")
    var projectQuota: Integer? = null

    /**
     * 报名人数
     */
    @TableField("signup_people")
    var signupPeople: Integer? = null

    /**
     * 项目介绍
     */
    @TableField("project_introduction")
    var projectIntroduction: String? = null

    override fun toString(): String {
        return "VolunteerProject{" +
        "projectName=" + projectName +
        ", holdingTime=" + holdingTime +
        ", holdingCompany=" + holdingCompany +
        ", venue=" + venue +
        ", projectQuota=" + projectQuota +
        ", projectIntroduction=" + projectIntroduction +
        "}"
    }
}
