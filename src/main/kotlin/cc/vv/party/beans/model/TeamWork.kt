package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 图片信息
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_team_work")
class TeamWork : BaseModel<TeamWork>() {

    /**
     * 队伍编号
     */
    @TableField("team_id")
    var teamId: String? = null
    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 内容
     */
    @TableField("content")
    var content: String? = null

    override fun toString(): String {
        return "TeamWork{" +
        "teamId=" + teamId +
        ", title=" + title +
        ", content=" + content +
        "}"
    }
}
