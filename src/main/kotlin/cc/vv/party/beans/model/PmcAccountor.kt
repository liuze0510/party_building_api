package cc.vv.party.beans.model;

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import java.io.Serializable

/**
 * <p>
 *
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
@TableName("pmc_accountor")
class PmcAccountor : Model<PmcAccountor>() {

    @TableId("ID")
    var id: String? = null
    @TableField("ACCOUNT")
    var account: String? = null
    @TableField("PASSWORD")
    var password: String? = null

    override fun pkVal(): Serializable? {
        return id
    }

    override fun toString(): String {
        return "PmcAccountor{" +
                "id=" + id +
                ", account=" + account +
                ", password=" + password +
                "}"
    }
}
