package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 社区大党委
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_community_party_committee")
class CommunityPartyCommittee : BaseModel<CommunityPartyCommittee>() {

    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 党委名称
     */
    @TableField("party_committee_name")
    var partyCommitteeName: String? = null
    /**
     * 召集人
     */
    @TableField("convener")
    var convener: String? = null
    /**
     * 牵头负责单位
     */
    @TableField("responsible_unit")
    var responsibleUnit: String? = null
    /**
     * 负责人姓名
     */
    @TableField("principal_name")
    var principalName: String? = null
    /**
     * 负责人账号
     */
    @TableField("principal_account")
    var principalAccount: String? = null
    /**
     * 党委简介
     */
    @TableField("party_committeey_introduction")
    var partyCommitteeyIntroduction: String? = null


    override fun toString(): String {
        return "CommunityPartyCommittee{" +
        "street=" + street +
        ", community=" + community +
        ", partyCommitteeName=" + partyCommitteeName +
        ", convener=" + convener +
        ", responsibleUnit=" + responsibleUnit +
        ", principalName=" + principalName +
        ", principalAccount=" + principalAccount +
        ", partyCommitteeyIntroduction=" + partyCommitteeyIntroduction +
        "}"
    }
}
