package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 微捐赠
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_micro_donation")
class MicroDonation : BaseModel<MicroDonation>() {

    /**
     * 所属区
     */
    @TableField("area")
    var area: String? = null
    /**
     * 所属街道
     */
    @TableField("street")
    var street: String? = null
    /**
     * 所属社区
     */
    @TableField("community")
    var community: String? = null
    /**
     * 所属网格
     */
    @TableField("grid")
    var grid: String? = null

    /**
     * 捐赠人
     */
    @TableField("micro_donation_name")
    var microDonationName: String? = null
    /**
     * 联系方式
     */
    @TableField("mobile")
    var mobile: String? = null
    /**
     * 地址
     */
    @TableField("addr")
    var addr: String? = null
    /**
     * 捐赠内容
     */
    @TableField("micro_donation_conetnt")
    var microDonationConetnt: String? = null
    /**
     * 开始时间
     */
    @TableField("start_time")
    var startTime: Date? = null
    /**
     * 结束时间
     */
    @TableField("end_time")
    var endTime: Date? = null
    /**
     * 认领状态 0 未认领 1 已认领
     */
    @TableField("claim_state")
    var claimState: Integer? = null

    /**
     * 审核状态 0 通过 1 不通过
     */
    @TableField("check_state")
    var checkState: Integer? = null

    /**
     * 认领人
     */
    @TableField("clain_person")
    var clainPerson: String? = null
    /**
     * 认领人联系方式
     */
    @TableField("clain_mobile")
    var clainMobile: String? = null
    /**
     * 备注
     */
    @TableField("remarks")
    var remarks: String? = null
    /**
     * 认领时间
     */
    @TableField("clain_time")
    var clainTime: Date? = null
    /**
     * 审核不通过原因
     */
    @TableField("check_reson")
    var checkReson: String? = null

    override fun toString(): String {
        return "MicroDonationSaveParam{" +
                "area=" + area +
                ", street=" + street +
                ", community=" + community +
                ", grid=" + grid +
        "microDonationName=" + microDonationName +
        ", mobile=" + mobile +
        ", addr=" + addr +
        ", microDonationConetnt=" + microDonationConetnt +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", claimState=" + claimState +
        ", checkState=" + checkState +
        "}"
    }
}
