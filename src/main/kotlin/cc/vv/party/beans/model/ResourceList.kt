package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 资源清单
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_resource_list")
class ResourceList : BaseModel<ResourceList>() {

    /**
     * 所属单位Id
     */
    @TableField("unit_id")
    var unitId: String? = null

    /**
     * 资源品类
     */
    @TableField("resource_type")
    var resourceType: String? = null
    /**
     * 资源内容
     */
    @TableField("conetnt")
    var conetnt: String? = null
    /**
     * 数量
     */
    @TableField("num")
    var num: Integer? = null
    /**
     * 有偿无偿 0 无 1有
     */
    @TableField("reward")
    var reward: String? = null
    /**
     * 金额
     */
    @TableField("amount")
    var amount: String? = null
    /**
     * 开始时间
     */
    @TableField("start_time")
    var startTime: Date? = null
    /**
     * 结束时间
     */
    @TableField("end_time")
    var endTime: Date? = null

    override fun toString(): String {
        return "ResourceList{" +
                ", resourceType=" + resourceType +
                ", conetnt=" + conetnt +
                ", num=" + num +
                ", reward=" + reward +
                ", amount=" + amount +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                "}"
    }
}
