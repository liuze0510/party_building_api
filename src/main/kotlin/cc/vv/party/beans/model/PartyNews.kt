package cc.vv.party.beans.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName

/**
 * <p>
 * 党群新闻
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@TableName("bt_party_news")
class PartyNews : BaseModel<PartyNews>() {

    /**
     * 标题
     */
    @TableField("title")
    var title: String? = null
    /**
     * 发布人
     */
    @TableField("publisher")
    var publisher: String? = null
    /**
     * 所属支部
     */
    @TableField("branch")
    var branch: String? = null
    /**
     * 缩略图
     */
    @TableField("thumbnail")
    var thumbnail: String? = null
    /**
     * 视频
     */
    @TableField("video")
    var video: String? = null
    /**
     * 新闻内容
     */
    @TableField("conetnt")
    var conetnt: String? = null

    override fun toString(): String {
        return "PartyNews{" +
        "title=" + title +
        ", publisher=" + publisher +
        ", branch=" + branch +
        ", thumbnail=" + thumbnail +
        ", video=" + video +
        ", conetnt=" + conetnt +
        "}"
    }
}
