package cc.vv.party.model;

import cc.vv.party.common.base.BaseModel
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableName
import java.util.*

/**
 * <p>
 * 留言回复
 * </p>
 *
 * @author Gyb
 * @since 2018-10-23
 */
@TableName("bt_message_board_reply")
class MessageBoardReply : BaseModel<MessageBoardReply>() {

    /**
     * 留言编号
     */
    @TableField("message_board_id")
    var messageBoardId: String? = null
    /**
     * 回复内容
     */
    @TableField("content")
    var content: String? = null
    /**
     * 回复单位
     */
    @TableField("unit")
    var unit: String? = null
    /**
     * 回复时间
     */
    @TableField("time")
    var time: Date? = null


    override fun toString(): String {
        return "MessageBoardReply{" +
        "messageBoardId=" + messageBoardId +
        ", content=" + content +
        ", unit=" + unit +
        ", time=" + time +
        "}"
    }
}
