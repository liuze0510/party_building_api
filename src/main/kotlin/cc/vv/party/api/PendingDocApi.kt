package cc.vv.party.api

import cc.vv.party.beans.vo.PendingDocVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.PendingDocService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@Api(value = "待办公文", tags = ["待办公文移动端API"], description = "待办公文相关接口")
@RestController
@RequestMapping("/api/pending-doc")
class PendingDocApi : BaseController() {

    @Autowired
    lateinit var pendingDocService: PendingDocService

    @OperateLog(content = "查看代办公文列表")
    @ApiOperation(value = "代办公文列表", notes = "代办公文列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @GetMapping("/list")
    fun pendingDocList(@RequestParam page: Int, @RequestParam pageSize: Int): ResponseEntityWrapper<PageWrapper<PendingDocVO>> {
        return success(
            pendingDocService.selectPendingDocList(
                page,
                pageSize,
                getYananRole(),
                getCurrentUserId(),
                getCurrentBranch()
            )
        )
    }


    @OperateLog(content = "查看代办公文详情")
    @ApiOperation(value = "代办公文详情", notes = "代办公文详情")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "id", value = "编号", required = true))
        ]
    )
    @GetMapping("/detail")
    fun pendingDocDetail(@RequestParam id: String): ResponseEntityWrapper<PendingDocVO> {
        return success(pendingDocService.details(id, getCurrentUserId()))
    }

}