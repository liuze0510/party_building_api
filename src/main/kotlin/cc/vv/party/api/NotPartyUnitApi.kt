package cc.vv.party.api;


import cc.vv.party.beans.vo.NotPartyUnitVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.NotPartyUnitService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 非公党建单位 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "非公党建单位API", tags = ["非公党建单位移动端API"], description = "非公党建单位移动端API-移动端非公党建单位相关接口")
@RestController
@RequestMapping("/api/not-party-unit")
class NotPartyUnitApi : BaseController(){

    @Autowired
    lateinit var notPartyUnitService: NotPartyUnitService


    @ApiOperation(value = "获取非公党建单位", notes = "获取非公党建单位")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<NotPartyUnitVO> {
        var vo = notPartyUnitService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取非公党建单位列表", notes = "获取非公党建单位列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<NotPartyUnitVO>> {
        var page = notPartyUnitService.listPage(null,null, size, page)
        return success(page)
    }

}
