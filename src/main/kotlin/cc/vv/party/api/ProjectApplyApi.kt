package cc.vv.party.api;


import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ProjectApplyEditParam
import cc.vv.party.param.ProjectApplySaveParam
import cc.vv.party.service.ProjectApplyService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * <p>
 * 项目延期申请 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/project-apply")
@Api(value = "客户端项目延期申请接口", tags = ["项目延期申请客户端API"], description = "项目延期申请客户端相关接口")
class ProjectApplyApi : BaseController() {

    @Autowired
    lateinit var projectApplyService: ProjectApplyService

    @OperateLog(content = "添加项目延期申请信息")
    @ApiOperation("添加项目延期申请信息")
    @PostMapping("/save")
    fun save(@RequestBody param: ProjectApplySaveParam): ResponseEntityWrapper<Boolean> {
        return success(projectApplyService.save(param.param!!, getCurrentUserId()))
    }
}
