package cc.vv.party.api;


import cc.vv.party.beans.vo.SignInVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.TeamSignInParam
import cc.vv.party.service.TeamSignInService
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * 四支队伍签到信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍签到信息API", tags = ["四支队伍签到信息移动端API"], description = "四支队伍签到信息移动端API-移动端四支队伍签到信息相关接口")
@RestController
@RequestMapping("/api/team-sign-in")
class TeamSignInApi : BaseController() {

    @Autowired
    lateinit var teamSignInService: TeamSignInService

    @ApiOperation(value = "保存队伍签到签到信息", notes = "保存队伍签到签到信息")
    @PostMapping("/sign-in")
    fun signIn(@RequestBody param: TeamSignInParam): ResponseEntityWrapper<Boolean> {
        var reslut = teamSignInService.signIn(param, getCurrentUserId())
        return success(reslut)
    }

    @ApiOperation(value = "获取四支队伍签到信息", notes = "获取四支队伍签到信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "teamId", value = "队伍编号", required = true)),
                (ApiImplicitParam(name = "time", value = "签到时间 yyyy-MM-dd格式", required = false))
            ]
    )
    @GetMapping("/info/{teamId}")
    fun info(@PathVariable teamId: String, @RequestParam time: String?): ResponseEntityWrapper<SignInVO> {
        val date = if(StringUtils.isNotBlank(time)){time} else {DateUtil.format(Date(), DatePattern.NORM_DATE_PATTERN)}
        var vo = teamSignInService.info(teamId, date!!)
        return success(vo)
    }

}
