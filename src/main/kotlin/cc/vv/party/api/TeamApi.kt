package cc.vv.party.api;


import cc.vv.party.beans.vo.SignInVO
import cc.vv.party.beans.vo.TeamVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.TeamService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 四支队伍信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍信息API", tags = ["四支队伍信息移动端API"], description = "四支队伍信息移动端API-移动端四支队伍信息相关接口")
@RestController
@RequestMapping("/api/team")
class TeamApi : BaseController() {

    @Autowired
    lateinit var teamService: TeamService

    @ApiOperation(value = "获取第一书记信息", notes = "获取第一书记信息")
    @ApiImplicitParam(name = "village", value = "所在村", required = true)
    @GetMapping("/secretary-message")
    fun getSecretaryMessage(@RequestParam(required = true) village: String): ResponseEntityWrapper<TeamVO>{
        var result = teamService.getSecretaryMessage(village)
        return success(result)
    }

    @ApiOperation(value = "获取四支队伍信息", notes = "获取四支队伍信息")
    @ApiImplicitParam(name = "id", value = "四支队伍编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<TeamVO> {
        var vo = teamService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取扶贫工作队信息", notes = "获取扶贫工作队信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "village", value = "所在村", required = true)),
                (ApiImplicitParam(name = "type", value = "队伍类型： 1 村两委班子 2 扶贫工作队 3 驻村干部", required = true))
            ]
    )
    @GetMapping("/village-team-list")
    fun findVillageTeamList(@RequestParam village: String, @RequestParam type: String): ResponseEntityWrapper<List<TeamVO>?>{
        var result = teamService.findVillageTeamList(village, type)
        return success(result)
    }


}
