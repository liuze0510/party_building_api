package cc.vv.party.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;

/**
 * <p>
 * 项目阶段信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/project-segmentation")
class ProjectSegmentationApi : BaseController()
