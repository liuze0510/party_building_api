package cc.vv.party.api;


import cc.vv.party.beans.vo.VolunteerUserVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.VolunteerUserEditParam
import cc.vv.party.param.VolunteerUserListParam
import cc.vv.party.service.VolunteerUserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿者信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿者API", tags = ["志愿者移动端API"], description = "志愿者移动端API-移动端志愿者相关接口")
@RestController
@RequestMapping("/api/volunteer-user")
class VolunteerUserApi : BaseController() {

    @Autowired
    lateinit var volunteerUserService: VolunteerUserService

    @ApiOperation(value = "编辑志愿者信息", notes = "编辑志愿者信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: VolunteerUserEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = volunteerUserService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取志愿者信息", notes = "获取志愿者信息")
    @ApiImplicitParam(name = "id", value = "志愿者编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<VolunteerUserVO> {
        var vo = volunteerUserService.getById(id)
        return success(vo)
    }

    @ApiOperation(value = "获取志愿者列表", notes = "获取志愿者列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: VolunteerUserListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<VolunteerUserVO>> {
        var page = volunteerUserService.listPage(param, size, page)
        return success(page)
    }

}
