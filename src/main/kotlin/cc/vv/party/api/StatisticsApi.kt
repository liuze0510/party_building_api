package cc.vv.party.api;


import cc.vv.party.beans.vo.*
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ProjectStatisticsParam
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UserStatisticsParam
import cc.vv.party.service.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 信息统计 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "信息统计", tags = ["信息统计移动端API"], description = "移动端信息统计相关接口")
@RestController
@RequestMapping("/api/statistics")
class StatisticsApi : BaseController() {

    @Autowired
    lateinit var postService: PostService

    @Autowired
    lateinit var projectService: ProjectService

    @Autowired
    lateinit var volunteerUserService: VolunteerUserService

    @Autowired
    lateinit var orgService: OrgService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var learningFieldService: LearningFieldService

    @Autowired
    lateinit var logService: LogService

    @OperateLog(content = "查看岗位、求职统计")
    @ApiOperation(value = "岗位、求职统计", notes = "岗位、求职统计")
    @ApiImplicitParam(name = "time", value = "时间 格式为：yyyy", required = false)
    @PostMapping(value = "/post-job-hunting-statistics")
    fun postJobHuntingStatistics(@RequestParam time: String?): ResponseEntityWrapper<PostJobHuntingUserStatisticsVO> {
        var result = postService.findStatistics(time)
        return success(result)
    }


    @OperateLog(content = "查看项目统计")
    @ApiOperation(value = "项目统计", notes = "项目统计（key 为项目状态类型，value为项目数量。key 取值范围与含义：系统状态 0 正常 1 延期 2 预警  3 取消  4 完结）")
    @PostMapping(value = "/project-statistics")
    fun projectStatistics(@RequestBody param: ProjectStatisticsParam): ResponseEntityWrapper<Map<String, Integer>> {
        var result = projectService.projectSysStateStatistics(param)
        return success(result)
    }


    @OperateLog(content = "查看志愿统计")
    @ApiOperation(value = "志愿统计", notes = "志愿统计")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "year", value = "年 如：2018", required = true),
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false),
                ApiImplicitParam(name = "grid", value = "网格", required = false)
            ]
    )
    @PostMapping(value = "/volunteer-statistics")
    fun volunteerStatistics(@RequestParam year: Int?,
                            @RequestParam street: String?, @RequestParam community: String?,
                            @RequestParam grid: String?): ResponseEntityWrapper<VolunteerStatisticsVO> {
        var result = volunteerUserService.findStatistics(year, street, community, grid)
        return success(result)
    }

    @OperateLog(content = "查看组织统计")
    @ApiOperation(value = "组织统计", notes = "组织统计, key 1 街道 2 社区 3 网格 6 楼道党小组 8 单元中心户")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "year", value = "年 如：2018", required = true),
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false),
                ApiImplicitParam(name = "grid", value = "网格", required = false)
            ]
    )
    @PostMapping(value = "/org-statistics")
    fun orgStatistics(@RequestParam year: Int,
                      @RequestParam(required = false) street: String?, @RequestParam(required = false) community: String?,
                      @RequestParam(required = false) grid: String?): ResponseEntityWrapper<Map<Int, Int>> {
        var result = orgService.findAppSatistics(year, street, community, grid)
        return success(result)
    }

    @OperateLog(content = "查看职业统计")
    @ApiOperation(value = "职业统计", notes = "职业统计(key为职业代码，value为职业对应的人数。职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  8其他职业)")
    @PostMapping(value = "/career-statistics")
    fun careerStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        var result = userService.careerStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看民族统计")
    @ApiOperation(value = "民族统计", notes = "民族统计(key为民族代码，value为该民族数量。民族 0 汉族 1 少数民族)")
    @PostMapping(value = "/ethnic-statistics")
    fun ethnicStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        var result = userService.ethnicStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看性别统计")
    @ApiOperation(value = "性别统计", notes = "性别统计(key为性别代码，value为该性别数量。性别 0 男 1 女)")
    @PostMapping(value = "/sex-statistics")
    fun sexStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        var result = userService.sexStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看文化程度统计")
    @ApiOperation(value = "文化程度统计", notes = "文化程度统计(key为文化程度代码，value为该文化程度数量。文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上)")
    @PostMapping(value = "/educational-level-statistics")
    fun educationalLevelStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        var result = userService.educationalLevelStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看年龄统计")
    @ApiOperation(value = "年龄统计", notes = "年龄统计(key为年龄段代码，value为该年龄段数量。年龄段 0： 0-6岁， 1:7-17岁，2： 18-35岁，3:36-45岁，4:46-59岁，5:60以上)")
    @PostMapping(value = "/age-statistics")
    fun ageStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        var result = userService.ageStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看党组织成员统计")
    @ApiOperation(value = "党组织成员统计",
            notes = "党组织成员统计(key为类型代码，value为该类型的数量。" +
                    "类型  COUNTRYSIDE： 农村党组织， " +
                    "OFFICE: 机关党组织，" +
                    "COMMUNITY： 社区党组织，" +
                    "ENTERPRISES: 企事业党组织，" +
                    "SCHOOL: 学校党组织，" +
                    "SOCIETY: 新社会组织党组织," +
                    "NONPUBLICOWNERSHIP: 非公有制企业党组织," +
                    "OTHER: 其它类型党组织)")
    @PostMapping(value = "/party-org-member-statistics")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "组织编号", dataType = "String", required = true),
                ApiImplicitParam(name = "type", value = "组织类型 rld(区); PARTYCOMMITTEE(党委); PARTYWORKCOMMITTEE(党工委);PARTYTOTALBRANCH(党总支);PARTY(党支部); 括号中的为类型说明", dataType = "String", required = true)
            ]
    )
    fun findPartyOrgMemberStatistics(@RequestParam id: String, @RequestParam type: String): ResponseEntityWrapper<Map<String, Int>> {
        var result = learningFieldService.findPartyOrgMemberStatistics(id, type)
        return success(result)
    }


    @OperateLog(content = "查看实时在线用户数量统计")
    @ApiOperation(value = "查看实时在线用户数量统计", notes = "查看实时在线用户数量统计")
    @ApiImplicitParam(name = "date", value = "开始时间 格式：yyyy-MM-dd", required = true)
    @PostMapping(value = "/app-real-time-statistics")
    fun findAppRealTimeStatistics(@RequestParam date: String): ResponseEntityWrapper<List<AppRealTimeStatisticsVO>> {
        return success(logService.findAppRealTimeStatistics(date))
    }

    @ApiOperation(value = "查看标准化区域树", notes = "查看标准化区域树")
    @GetMapping(value = "/area-tree")
    fun findAreaTree(): ResponseEntityWrapper<AreauserVO> {
        return success(learningFieldService.findAreaTree())
    }

    @OperateLog(content = "查看标准化数据")
    @ApiOperation(value = "查看标准化数据", notes = "查看标准化数据")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "areaUserId", value = "区域ID", required = true),
                ApiImplicitParam(name = "year", value = "年", required = false),
                ApiImplicitParam(name = "month", value = "月", required = false)
            ]
    )
    @GetMapping(value = "/standard-data")
    fun getStandardData(@RequestParam areaUserId: Int, @RequestParam(required = false) year: Integer?, @RequestParam(required = false) month: Integer?): ResponseEntityWrapper<StandardDataVO> {
        return success(learningFieldService.getStandardData(areaUserId, year, month))
    }



}

