package cc.vv.party.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;

/**
 * <p>
 * 微捐赠 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("//log")
class LogApi : BaseController()
