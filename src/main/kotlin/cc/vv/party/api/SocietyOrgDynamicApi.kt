package cc.vv.party.api;


import cc.vv.party.beans.vo.SocietyOrgDynamicVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.SocietyOrgDynamicService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社会组织动态 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社会组织动态API", tags = ["社会组织动态移动端API"], description = "社会组织动态移动端API-移动端社会组织动态相关接口")
@RestController
@RequestMapping("/api/society-org-dynamic")
class SocietyOrgDynamicApi : BaseController(){

    @Autowired
    lateinit var societyOrgDynamicService: SocietyOrgDynamicService

    @ApiOperation(value = "获取社会组织动态", notes = "获取社会组织动态")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<SocietyOrgDynamicVO> {
        var vo = societyOrgDynamicService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取社会组织动态列表", notes = "获取社会组织动态列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "time", value = "发布时间", required = false)),
                (ApiImplicitParam(name = "releaseOrg", value = "发布组织", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "type", value = "承诺类型 0 单位 1 党员", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) releaseOrg: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<SocietyOrgDynamicVO>> {
        var page = societyOrgDynamicService.listPage(time, releaseOrg, title, size, page)
        return success(page)
    }


}
