package cc.vv.party.api;


import cc.vv.party.beans.vo.PartyReportVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PartyReportListParam
import cc.vv.party.service.PartyReportService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 党员报道信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "党员报道", tags = ["党员报道信息移动端API"], description = "移动端党员报道信息相关接口")
@RestController
@RequestMapping("/api/party-report")
class PartyReportApi : BaseController() {

    @Autowired
    lateinit var partyReportService: PartyReportService

    @OperateLog(content = "查看党员报道详情")
    @ApiOperation(value = "获取党员报道信息", notes = "获取党员报道信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PartyReportVO> {
        var vo = partyReportService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看党员报道信息列表")
    @ApiOperation(value = "获取党员报道信息列表", notes = "获取党员报道信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(
        @RequestBody param: PartyReportListParam,
        @PathVariable size: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<PartyReportVO>> {
        var roleList = getCurrentUserRole()
        if (roleList.size == 1 && roleList[0].type == RoleType.PARTY_MEMBER) throw BizException(StatusCode.FORBIDDEN)
        return success(partyReportService.selectReportUserList(param, size, page))
    }


}
