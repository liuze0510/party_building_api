package cc.vv.party.api;


import cc.vv.party.beans.vo.JobHuntingUserVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.JobHuntingUserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 求职信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "求职API", tags = ["求职移动端API"], description = "求职移动端API-移动端求职相关接口")
@RestController
@RequestMapping("/api/job-hunting-user")
class JobHuntingUserApi : BaseController() {

    @Autowired
    lateinit var jobHuntingUserService: JobHuntingUserService

    @ApiOperation(value = "获取求职信息", notes = "获取求职信息")
    @ApiImplicitParam(name = "id", value = "求职编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<JobHuntingUserVO> {
        var vo = jobHuntingUserService.info(id)
        return success(vo)
    }

//    @ApiOperation(value = "获取求职列表", notes = "获取求职列表")
//    @ApiImplicitParams(
//            value = [
//                (ApiImplicitParam(name = "sex", value = "性别", required = false)),
//                (ApiImplicitParam(name = "education", value = "学历", required = false)),
//                (ApiImplicitParam(name = "workYear", value = "工作年限", required = false)),
//                (ApiImplicitParam(name = "name", value = "求职者姓名", required = false)),
//                (ApiImplicitParam(name = "mobile", value = "联系方式", required = false)),
//                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
//                (ApiImplicitParam(name = "page", value = "页码", required = true))
//            ]
//    )
//    @PostMapping(value = "/list/{page}/{size}")
//    fun listPage(@RequestParam(required = false) sex: Integer?,
//                 @RequestParam(required = false) education: String?,
//                 @RequestParam(required = false) workYear: Integer?,
//                 @RequestParam(required = false) name: String?,
//                 @RequestParam(required = false) mobile: String?,
//                 @PathVariable size: Int,
//                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<JobHuntingUserVO>> {
//        var page = jobHuntingUserService.listPage(sex, education, workYear, name, mobile, size, page)
//        return success(page)
//    }

    @ApiOperation(value = "获取求职列表", notes = "获取求职列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "key", value = "搜索关键字", required = false)),
                (ApiImplicitParam(name = "startTime", value = "开始时间", required = false)),
                (ApiImplicitParam(name = "endTime", value = "结束时间", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage( @RequestParam(required = false) street: String?,
                  @RequestParam(required = false) community: String?,
                  @RequestParam(required = false) grid: String?,
                  @RequestParam(required = false) key: String?,
                  @RequestParam(required = false) startTime: Long?,
                  @RequestParam(required = false) endTime: Long?,
                  @PathVariable size: Int,
                  @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<JobHuntingUserVO>> {
        var page = jobHuntingUserService.appListPage(street, community, grid, key, startTime, endTime, size, page, getPartyInfo())
        return success(page)
    }

}
