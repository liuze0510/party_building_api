package cc.vv.party.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.MicroDonationClainEditParam
import cc.vv.party.service.MicroDonationClainService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

/**
 * <p>
 * 微捐赠认领 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-05
 */
@Api(value = "微捐赠认领", tags = ["微捐赠认领移动端API"], description = "微捐赠认领移动端API-移动端微捐赠认领相关接口")
@RestController
@RequestMapping("/api/micro-donation-clain")
class MicroDonationClainApi : BaseController(){


    @Autowired
    lateinit var microDonationClainService: MicroDonationClainService

    @OperateLog(content = "添加微捐助信息")
    @ApiOperation(value = "添加微捐助信息", notes = "添加微捐助信息")
    @PostMapping("/save")
    fun save(@RequestBody param: MicroDonationClainEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = microDonationClainService.save(param, getCurrentUserId())
        return success(isAdd)
    }


}
