package cc.vv.party.api;


import cc.vv.party.beans.vo.DemonstrationTypicalVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.service.DemonstrationTypicalService
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 示范典型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@Api(value = "示范典型", tags = ["示范典型API"], description = "示范典型相关接口")
@RestController
@RequestMapping("/api/demonstration-typical")
class DemonstrationTypicalApi : BaseController() {

    @Autowired
    lateinit var demonstrationTypicalService: DemonstrationTypicalService


    @ApiOperation(value = "获取示范典型信息", notes = "获取示范典型信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemonstrationTypicalVO> {
        val entity = demonstrationTypicalService.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.browseNum = Integer(entity.browseNum!!.toInt().plus(1))
        demonstrationTypicalService.updateById(entity)
        return success(entity.transfer())
    }

    @ApiOperation(value = "获取示范典型信息列表", notes = "获取示范典型信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(
                name = "type",
                value = "类型 0 农村党组织  1 社区党组织  2 学校党组织  3 机关党组织 4 非公党组织  5 社会组织党组织 6 国家企事业单位党组织",
                required = true
            )),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(
        @RequestParam(required = false) type: Integer,
        @PathVariable size: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<DemonstrationTypicalVO>> {
        var page = demonstrationTypicalService.listPage(type, null, size, page)
        return success(page)
    }


}
