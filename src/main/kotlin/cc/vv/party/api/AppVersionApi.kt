package cc.vv.party.api;


import cc.vv.party.beans.model.AppVersion
import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.vo.BannerVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * App 版本 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/app-version")
@Api(value = "客户端App-Version接口", tags = ["App Version客户端API"], description = "App Version客户端相关接口")
class AppVersionApi : BaseController() {

    @OperateLog(content = "查看App Version最新版本")
    @ApiOperation("查看App Version最新版本")
    @GetMapping("/last-version")
    fun getLastVersion(): ResponseEntityWrapper<AppVersion> {
        return success(AppVersion().selectOne(EntityWrapper<AppVersion>().orderBy("create_time", false)))
    }
}
