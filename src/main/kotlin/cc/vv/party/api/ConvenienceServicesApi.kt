package cc.vv.party.api;


import cc.vv.party.beans.vo.ConvenienceServiceVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ConvenienceServicesService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 便民服务 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "便民服务API", tags = ["便民服务移动端API"], description = "便民服务移动端API-移动端便民服务相关接口")
@RestController
@RequestMapping("/api/convenience-services")
class ConvenienceServicesApi : BaseController() {

    @Autowired
    lateinit var convenienceServicesService: ConvenienceServicesService

    @ApiOperation(value = "获取便民服务信息", notes = "获取便民服务信息")
    @ApiImplicitParam(name = "id", value = "便民服务编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ConvenienceServiceVO> {
        var vo = convenienceServicesService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取便民服务列表", notes = "获取便民服务列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "type", value = "类型", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) type: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ConvenienceServiceVO>> {
        var result = convenienceServicesService.appListPage(street, community, grid, type, title, size, page)
        return success(result)
    }


}
