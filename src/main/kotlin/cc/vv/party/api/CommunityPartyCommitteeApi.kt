package cc.vv.party.api;


import cc.vv.party.beans.model.CommunityPartyCommittee
import cc.vv.party.beans.vo.CommunityPartyCommitteeVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.CommunityPartyCommitteeService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区大党委 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区大党委API", tags = ["社区大党委移动端API"], description = "社区大党委移动端API-移动端社区大党委相关接口")
@RestController
@RequestMapping("/api/community-party-committee")
class CommunityPartyCommitteeApi : BaseController() {


    @Autowired
    lateinit var communityPartyCommitteeService: CommunityPartyCommitteeService

    @OperateLog(content = "查看社区大党委详情")
    @ApiOperation(value = "获取社区大党委信息", notes = "获取社区大党委信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityPartyCommitteeVO> {
        var vo = communityPartyCommitteeService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区大党委详情")
    @ApiOperation(value = "根据社区编号获取社区大党委信息", notes = "根据社区编号获取社区大党委信息")
    @ApiImplicitParam(name = "communityId", value = "编号", required = true)
    @GetMapping("/community-party-committee-info")
    fun getInfo(@RequestParam communityId: String): ResponseEntityWrapper<CommunityPartyCommitteeVO> {
        var entity = communityPartyCommitteeService.selectOne(EntityWrapper<CommunityPartyCommittee>().where("community = {0}", communityId)) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return success(entity.transfer())
    }


}