package cc.vv.party.api;


import cc.vv.party.beans.vo.PolicyDocVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.PolicyDocService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 政策文件信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "政策文件信息API", tags = ["政策文件信息移动端API"], description = "政策文件信息移动端API-移动端政策文件信息相关接口")
@RestController
@RequestMapping("/api/policy-doc")
class PolicyDocApi : BaseController() {

    @Autowired
    lateinit var policyDocService: PolicyDocService

    @ApiOperation(value = "获取政策文件信息", notes = "获取政策文件信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PolicyDocVO> {
        var vo = policyDocService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取政策文件信息列表", notes = "获取政策文件信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PolicyDocVO>> {
        var page = policyDocService.listPage(title, null, size, page)
        return success(page)
    }

}
