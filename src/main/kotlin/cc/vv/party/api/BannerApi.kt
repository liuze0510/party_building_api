package cc.vv.party.api;


import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.vo.BannerVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * banner管理 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/banner")
@Api(value = "客户端Banner接口", tags = ["Banner客户端API"], description = "Banner客户端API-客户端Banner相关接口")
class BannerApi : BaseController() {

    @Autowired
    lateinit var bannerService: BannerService

    @OperateLog(content = "查看Banner列表")
    @ApiOperation("获取Banner列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "当前页", name = "page"),
            ApiImplicitParam(value = "页大小", name = "pageSize"),
            ApiImplicitParam(value = "分类，0：党群新闻 1 党建概况  2 宝塔头条  3街道头条  4 社区头条 5 网格头条 " , name = "category")
        ]
    )
    @GetMapping("/list")
    fun list(@RequestParam page: Int, @RequestParam pageSize: Int, @RequestParam category: Int): ResponseEntityWrapper<PageWrapper<BannerVO>> {
        return success(
            bannerService.selectPage(
                Page(page, pageSize),
                EntityWrapper<Banner>().eq("category", category).orderBy("create_time", false)
            ).wrapper()
        )
    }

    @OperateLog(content = "查看Banner详情")
    @ApiOperation("Banner详情")
    @ApiImplicitParam(value = "banner id")
    @GetMapping("/detail/{id}")
    fun detail(@PathVariable id: String): ResponseEntityWrapper<BannerVO> {
        return success(bannerService.selectById(id).transfer())
    }
}
