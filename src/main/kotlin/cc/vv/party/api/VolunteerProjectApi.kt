package cc.vv.party.api;


import cc.vv.party.beans.vo.VolunteerProjectVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.VolunteerProjectService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿项目信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿项目API", tags = ["志愿项目移动端API"], description = "志愿项目移动端API-移动端志愿项目相关接口")
@RestController
@RequestMapping("/api/volunteer-project")
class VolunteerProjectApi : BaseController() {

    @Autowired
    lateinit var volunteerProjectService: VolunteerProjectService

    @ApiOperation(value = "获取志愿项目信息", notes = "获取志愿项目信息")
    @ApiImplicitParam(name = "id", value = "志愿项目编号", required = true)
    @GetMapping("/info/{id}")
    fun appInfo(@PathVariable id: String): ResponseEntityWrapper<VolunteerProjectVO> {
        var vo = volunteerProjectService.appInfo(id)
        return success(vo)
    }

    @ApiOperation(value = "获取志愿项目列表", notes = "获取志愿项目列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "projectName", value = "项目名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun appListPage(@RequestParam(required = false) projectName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<VolunteerProjectVO>> {
        var page = volunteerProjectService.appListPage(projectName, size, page)
        return success(page)
    }


    @ApiOperation(value = "获取指定用户报名志愿项目列表", notes = "获取指定用户报名志愿项目列表")
    @PostMapping(value = "/user-volunteer-project-list")
    fun userVolunteerProjectList(): ResponseEntityWrapper<List<VolunteerProjectVO>> {
        var list = volunteerProjectService.userVolunteerProjectList(getCurrentUserId())
        return success(list)
    }

}
