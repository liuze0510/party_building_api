package cc.vv.party.api;


import cc.vv.party.beans.vo.VolunteerProjectSignUpVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.VolunteerProjectSignUpSaveParam
import cc.vv.party.service.VolunteerProjectSignUpService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿项目报名报名信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿项目报名API", tags = ["志愿项目报名移动端API"], description = "志愿项目报名移动端API-移动端志愿项目报名相关接口")
@RestController
@RequestMapping("/api/volunteer-project-sign-up")
class VolunteerProjectSignUpApi : BaseController() {

    @Autowired
    lateinit var volunteerProjectSignUpService: VolunteerProjectSignUpService

    @ApiOperation(value = "志愿项目报名信息", notes = "志愿项目报名信息")
    @PostMapping("/sign-up")
    fun signUp(@RequestBody param: VolunteerProjectSignUpSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = volunteerProjectSignUpService.save(param, getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取志愿项目报名用户信息", notes = "获取志愿项目报名用户信息")
    @GetMapping("/pro-user/{proId}")
    fun getProSignUpUserList(@PathVariable proId: String): ResponseEntityWrapper<List<VolunteerProjectSignUpVO>> {
        var proUserList = volunteerProjectSignUpService.getProSignUpUserList(proId)
        return success(proUserList)
    }

}
