package cc.vv.party.api;


import cc.vv.party.beans.model.Project
import cc.vv.party.beans.vo.ProjectVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ProjectListParam
import cc.vv.party.param.ProjectSaveParam
import cc.vv.party.service.ProjectService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 项目信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "项目信息API", tags = ["项目信息移动端API"], description = "项目信息移动端API-移动端项目信息相关接口")
@RestController
@RequestMapping("/api/project")
class ProjectApi : BaseController() {

    @Autowired
    lateinit var projectService: ProjectService

    @ApiOperation(value = "编辑项目信息", notes = "编辑项目信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: ProjectSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = projectService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "查看项目详情")
    @ApiOperation(value = "获取项目信息", notes = "获取项目信息")
    @ApiImplicitParam(name = "id", value = "项目编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ProjectVO> {
        var vo = projectService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "项目取消申请", notes = "项目延期、取消申请")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "项目编号", required = true),
                ApiImplicitParam(name = "type", value = "申请类型 2 延期 3 取消", required = true),
                ApiImplicitParam(name = "reason", value = "理由", required = true)
            ]
    )
    @PostMapping("/pro-apply")
    fun projectApply(@RequestParam id: String, @RequestParam type: Int, @RequestParam reason: String): ResponseEntityWrapper<Boolean> {
        var sysState: Int = if(type == 2){ 1 } else { 3 }
        projectService.updateProjectApply(id, type, reason, sysState)
        return success(true)
    }


    @ApiOperation(value = "获取项目信息列表", notes = "获取项目信息列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false),
                ApiImplicitParam(name = "grid", value = "网格", required = false),
                ApiImplicitParam(name = "name", value = "项目名称", required = false),
                ApiImplicitParam(name = "state", value = "状态 0 正常 1 延期 2 预警  3 取消  4 完结", required = true),
                ApiImplicitParam(name = "size", value = "每页显示数量", required = false),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) name: String?,
                 @RequestParam state: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ProjectVO>> {
        var page = projectService.appListPage(street, community, grid, name, state, size, page, getPartyInfo())
        return success(page)
    }


    @ApiOperation(value = "获取预警项目通知列表", notes = "获取预警项目通知列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false),
                ApiImplicitParam(name = "grid", value = "网格", required = false),
                ApiImplicitParam(name = "type", value = "预警是否查看类型 1 未查看 2 已查看", required = true),
                ApiImplicitParam(name = "size", value = "每页显示数量", required = false),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/warn-list/{page}/{size}")
    fun warnListPage(@RequestParam(required = false) street: String?,
                     @RequestParam(required = false) community: String?,
                     @RequestParam(required = false) grid: String?,
                     @RequestParam type: Int,
                     @PathVariable size: Int,
                     @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ProjectVO>> {
        var wrapper = EntityWrapper<Project>().where("view = {0}", type).and("sys_state = 2")
        if(StringUtils.isNotBlank(street)) wrapper.and("street = {0}", street)
        if(StringUtils.isNotBlank(community)) wrapper.and("community = {0}", community)
        if(StringUtils.isNotBlank(grid)) wrapper.and("grid = {0}", grid)
        wrapper.orderBy("create_time", false)
        var page = projectService.selectPage(Page(page, size), wrapper)
        return success(page.wrapper())
    }



}
