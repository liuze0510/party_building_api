package cc.vv.party.api;


import cc.vv.party.beans.vo.UnitEvaluationStarVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.UnitEvaluationStarService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 单位评星晋阶 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "单位评星晋阶API", tags = ["单位评星晋阶移动端API"], description = "单位评星晋阶移动端API-移动端单位评星晋阶相关接口")
@RestController
@RequestMapping("/api/unit-evaluation-star")
class UnitEvaluationStarApi : BaseController() {

    @Autowired
    lateinit var unitEvaluationStarService: UnitEvaluationStarService

    @OperateLog(content = "查看项目阶段详情")
    @ApiOperation(value = "获取详情信息", notes = "获取详情信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<UnitEvaluationStarVO> {
        var vo = unitEvaluationStarService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看项目阶段列表")
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "name", value = "单位名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) name: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<UnitEvaluationStarVO>> {
        var page = unitEvaluationStarService.listPage(name, size, page)
        return success(page)
    }



}
