package cc.vv.party.api;


import cc.vv.party.beans.vo.StreetJointMeetingVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.StreetJointMeetingService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 街道联席会议 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "街道联席会议API", tags = ["街道联席会议移动端API"], description = "街道联席会议移动端API-移动端街道联席会议相关接口")
@RestController
@RequestMapping("/api/street-joint-meeting")
class StreetJointMeetingApi : BaseController() {

    @Autowired
    lateinit var streetJointMeetingService: StreetJointMeetingService


    @ApiOperation(value = "获取街道联席会议信息", notes = "获取街道联席会议信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<StreetJointMeetingVO> {
        var vo = streetJointMeetingService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取街道联席会议信息列表", notes = "获取街道联席会议信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "street", value = "所属街道", required = false)),
            (ApiImplicitParam(name = "meetingName", value = "会议名称", required = false)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(
        @RequestParam(required = false) street: String?,
        @RequestParam(required = false) meetingName: String?,
        @PathVariable size: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<StreetJointMeetingVO>> {
        val streetId = if (street.isNullOrEmpty()) {
            getCurrentUserInfo().street
        } else {
            street
        }
        var page = streetJointMeetingService.listPage(streetId, meetingName, size, page)
        return success(page)
    }

}
