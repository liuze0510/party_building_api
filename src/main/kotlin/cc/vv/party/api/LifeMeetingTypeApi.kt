package cc.vv.party.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;

/**
 * <p>
 * 民主、组织生活会类型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@RestController
@RequestMapping("/web/life-meeting-type")
class LifeMeetingTypeApi : BaseController()
