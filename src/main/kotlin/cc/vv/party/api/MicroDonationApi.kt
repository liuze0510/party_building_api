package cc.vv.party.api;


import cc.vv.party.beans.vo.MicroDonationVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.MicroDonationListParam
import cc.vv.party.param.MicroDonationSaveParam
import cc.vv.party.service.MicroDonationService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 微捐赠 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "微捐助API", tags = ["微捐赠移动端API"], description = "微捐赠移动端API-移动端微捐助相关接口")
@RestController
@RequestMapping("/api/micro-donation")
class MicroDonationApi : BaseController() {

    @Autowired
    lateinit var microDonationService: MicroDonationService

    @ApiOperation(value = "保存捐赠信息", notes = "保存捐赠信息")
    @PostMapping("/save")
    fun save(@RequestBody param: MicroDonationSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = microDonationService.save(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }


    @ApiOperation(value = "获取捐赠信息", notes = "获取捐赠信息")
    @ApiImplicitParam(name = "id", value = "捐赠编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<MicroDonationVO>{
        var vo = microDonationService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取微捐赠列表", notes = "获取微捐赠列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: MicroDonationListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MicroDonationVO>> {
        //移动端获取审核通过的捐赠列表
        param.checkState = Integer(0)

        var page = microDonationService.listPage(param, size, page)
        return success(page)
    }

    @ApiOperation(value = "获取我认领的微捐赠列表", notes = "获取我认领的微捐赠列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/my-claim-list/{page}/{size}")
    fun claimListPage(@PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MicroDonationVO>> {
        var page = microDonationService.claimListPage(getCurrentUserAccount(), getCurrentUserId(), size, page)
        return success(page)
    }

}
