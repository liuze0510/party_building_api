package cc.vv.party.api;


import cc.vv.party.beans.vo.TeamWorkVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.TeamWorkEditParam
import cc.vv.party.service.TeamWorkService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 四支队伍工作信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍工作信息API", tags = ["四支队伍工作信息移动端API"], description = "四支队伍工作信息移动端API-移动端四支队伍工作信息相关接口")
@RestController
@RequestMapping("/api/team-work")
class TeamWorkApi : BaseController(){

    @Autowired
    lateinit var teamWorkService: TeamWorkService

    @ApiOperation(value = "获取四支队伍列表", notes = "获取四支队伍工作信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "teamId", value = "队伍编号", required = true)),
                (ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "Long", required = true)),
                (ApiImplicitParam(name = "endTime", value = "结束时间", dataType = "Long", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun appListPage(@RequestParam(required = false) teamId: String,
                 @RequestParam(required = false) startTime: Long,
                 @RequestParam(required = false) endTime: Long,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<TeamWorkVO>> {
        var page = teamWorkService.appListPage(teamId, startTime, endTime, size, page)
        return success(page)
    }

    @ApiOperation(value = "编辑四支队伍工作信息", notes = "编辑四支队伍工作信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: TeamWorkEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = teamWorkService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取四支队伍工作信息", notes = "获取四支队伍工作信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<TeamWorkVO> {
        var vo = teamWorkService.info(id)
        return success(vo)
    }

}
