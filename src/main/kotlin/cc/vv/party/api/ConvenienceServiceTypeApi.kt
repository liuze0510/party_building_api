package cc.vv.party.api;


import cc.vv.party.beans.vo.ConvenienceServiceTypeVO
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ConvenienceServiceTypeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping

/**
 * <p>
 * 便民服务类型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "便民服务类型API", tags = ["便民服务类型移动端API"], description = "便民服务类型移动端API-移动端便民服务类型相关接口")
@RestController
@RequestMapping("/api/convenience-service-type")
class ConvenienceServiceTypeApi : BaseController() {

    @Autowired
    lateinit var convenienceServiceTypeService: ConvenienceServiceTypeService

    @ApiOperation(value = "获取便民服务型集合", notes = "获取便民服务型集合")
    @PostMapping(value = "/list-all")
    fun listAll(): ResponseEntityWrapper<List<ConvenienceServiceTypeVO>> {
        var page = convenienceServiceTypeService.listAll()
        return success(page)
    }

}
