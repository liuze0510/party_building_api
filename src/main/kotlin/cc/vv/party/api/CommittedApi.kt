package cc.vv.party.api;


import cc.vv.party.beans.model.Committed
import cc.vv.party.beans.vo.CommittedVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommittedEditParam
import cc.vv.party.service.CommittedCommentService
import cc.vv.party.service.CommittedService
import com.baomidou.mybatisplus.plugins.Page
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 承诺信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/committed")
@Api(value = "党务管理-四双-党员/单位承诺", tags = ["党务管理-四双-党员/单位承诺API"], description = "党务管理-四双-党员/单位承诺相关接口")
class CommittedApi : BaseController() {

    @Autowired
    lateinit var committedService: CommittedService

    @Autowired
    lateinit var committedCommentService: CommittedCommentService

    @OperateLog(content = "编辑承诺信息")
    @ApiOperation(value = "编辑承诺信息", notes = "编辑承诺信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: CommittedEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = committedService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation("承诺详情")
    @ApiImplicitParam(value = "承诺id")
    @GetMapping("/detail")
    fun committedDetail(@RequestParam id: String): ResponseEntityWrapper<CommittedVO> {
        val committedVO = committedService.info(id) ?: throw BizException(StatusCode.ERROR.statusCode, "数据不存在")
        committedVO.commentList = committedCommentService.listPage(id, 1, 10).records
        return success(committedVO)
    }

    @ApiOperation(value = "查看党员最新承诺详情")
    @GetMapping("/user-last-committed")
    fun lastCommitted(): ResponseEntityWrapper<CommittedVO> {
        val committedVO = committedService.lastCommitted(getCurrentUserId())
        return success(committedVO)
    }

    @ApiOperation("党员承诺列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "组织机构id", name = "orgId", required = false),
            ApiImplicitParam(value = "是否有评论(0:已评论，1:未评论)", name = "havingComment", required = true),
            ApiImplicitParam(value = "当前页", name = "page", required = true),
            ApiImplicitParam(value = "页大小", name = "pageSize", required = true)
        ]
    )
    @GetMapping("/user-list")
    fun userCommittedList(
        @RequestParam(required = false) orgId: String?, @RequestParam havingComment: Int, @RequestParam page: Int, @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<CommittedVO>> {
        //默认展示当前用户所在的单位机构下党员承诺列表
        val id = if (orgId.isNullOrEmpty()) {
            getCurrentUserInfo().workCompany ?: throw BizException(StatusCode.USER_NOT_HAS_UNIT)
        } else {
            orgId
        }

        return success(committedService.listByOrg(id!!, SysConsts.CONST_1, page, pageSize, havingComment))
    }

    @ApiOperation("单位承诺列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "组织机构id", name = "orgId", required = false),
            ApiImplicitParam(value = "是否有评论(0:已评论，1:未评论)", name = "havingComment", required = true),
            ApiImplicitParam(value = "当前页", name = "page", required = true),
            ApiImplicitParam(value = "页大小", name = "pageSize", required = true)
        ]
    )
    @GetMapping("/unit-list")
    fun unitCommittedList(
        @RequestParam(required = false) orgId: String?, @RequestParam havingComment: Int, @RequestParam page: Int, @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<CommittedVO>> {
        //默认展示当前用户所在的单位的承诺列表
        val id = if (orgId.isNullOrEmpty()) {
            getCurrentUserInfo().workCompany ?: throw BizException(StatusCode.USER_NOT_HAS_UNIT)
        } else {
            orgId
        }

        return success(committedService.listByOrg(id!!, SysConsts.CONST_0, page, pageSize, havingComment))
    }

}
