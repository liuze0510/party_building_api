package cc.vv.party.api;


import cc.vv.party.beans.model.ResourceList
import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.beans.vo.UnitVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.ResourceListEditParam
import cc.vv.party.param.ResourceListSaveList
import cc.vv.party.param.UnitEditParam
import cc.vv.party.param.UnitListParam
import cc.vv.party.service.OrgService
import cc.vv.party.service.ResourceListService
import cc.vv.party.service.UnitService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transferEntries
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import net.bytebuddy.implementation.bytecode.Throw
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

/**
 * <p>
 * 单位信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/api/unit")
@Api(value = "党务管理-四双-单位报道", tags = ["党务管理-四双-单位报道API"], description = "党务管理-四双-单位报道相关接口")
class UnitApi : BaseController() {

    @Autowired
    lateinit var unitService: UnitService

    @Autowired
    lateinit var resourceListService: ResourceListService

    @Autowired
    lateinit var orgService: OrgService

    @ApiOperation("单位报道列表-管理员调用")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "area", value = "区", required = false),
            ApiImplicitParam(name = "street", value = "街道", required = false),
            ApiImplicitParam(name = "community", value = "社区", required = false),
            ApiImplicitParam(value = "当前页", name = "page"),
            ApiImplicitParam(value = "分页大小", name = "pageSize"),
            ApiImplicitParam(value = "类型(0:已报道，1:未报道)", name = "type")
        ]
    )
    @GetMapping("/reported/list")
    fun unitReportedList(
            @RequestParam(required = false) area: String?,
            @RequestParam(required = false) street: String?,
        @RequestParam(required = false) community: String?,
        @RequestParam(defaultValue = "0") type: Int, @RequestParam page: Int, @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<UnitVO>> {
        val roleList = getSession().getAttribute(SysConsts.SESSION_KEY_USER_ROLE) as List<RoleVO>
        if (roleList != null && roleList.isEmpty() && roleList[0].type == RoleType.PARTY_MEMBER) throw BizException(
            StatusCode.FORBIDDEN
        )
        val param = UnitListParam()
        param.street = street
        param.community = community
        param.reportType = Integer(type)
        return success(unitService.listPage(param, pageSize, page))
    }

    @ApiOperation("单位报道详情-管理员调用")
    @ApiImplicitParam(value = "单位Id", required = false, name = "unitId")
    @GetMapping("/reported/detail")
    fun reportedDetail(@RequestParam unitId: String?): ResponseEntityWrapper<UnitVO> {
        var id = unitId
        if (id.isNullOrEmpty()) {
            //根据当前账号，查询当前登录人员默认所在的单位
            val unit = unitService.selectOne(EntityWrapper<Unit>().eq("mobile", getCurrentUserAccount()))
            id = unit.id
        }
        val info = unitService.appInfo(id!!)
        val orgMap = orgService.getOrgListByIds(listOf(info.street!!, info.community!!)).associateBy { it.id }
        info.resourceList = resourceListService.selectList(EntityWrapper<ResourceList>().ge("unit_id", unitId))
            .transferEntries<ResourceListVO>() as List<ResourceListVO>
        info.community = orgMap[info.community]!!.name
        info.street = orgMap[info.street]!!.name
        return success(info)
    }


    @ApiOperation("单位报道-管理员调用")
    @PostMapping("/reported/submit")
    fun submitUnitInfo(@RequestBody param: UnitEditParam): ResponseEntityWrapper<Any> {
        var isAdd = unitService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }
}
