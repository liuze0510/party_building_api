package cc.vv.party.api;


import cc.vv.party.beans.model.CommittedComment
import cc.vv.party.beans.vo.CommittedCommentVO
import cc.vv.party.beans.vo.CommittedVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.CommittedCommentService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * 承诺评论信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Validated
@RestController
@RequestMapping("/api/committed-comment")
@Api(value = "党务管理-四双-党员/单位-评论", tags = ["党务管理-四双-党员/单位-评论API"], description = "党务管理-四双-党员/单位承诺-评论相关接口")
class CommittedCommentApi : BaseController() {

    @Autowired
    lateinit var committedCommentService: CommittedCommentService

    @ApiOperation("添加承诺评论详情")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "承诺id", name = "committedId"),
            ApiImplicitParam(value = "评论内容", name = "content")
        ]
    )
    @PostMapping("/add")
    fun committedDetail(@RequestParam committedId: String, @RequestBody content: String): ResponseEntityWrapper<Any> {
        val comment = CommittedComment()
        comment.commentTime = Date()
        comment.committedId = committedId
        comment.content = content
        val currentUserInfo = getCurrentUserInfo()
        comment.faceUrl = currentUserInfo.faceUrl
        comment.createUser = currentUserInfo.id
        comment.commentName = currentUserInfo.name
        comment.commentId = currentUserInfo.id
        return if (committedCommentService.insert(comment)) {
            success(null)
        } else {
            error(StatusCode.ERROR.statusCode, "添加评论失败")
        }
    }

    @OperateLog(content = "查看承诺评论列表")
    @ApiOperation(value = "获取承诺评论列表", notes = "获取承诺评论列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "committedId", value = "承诺编号", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam committedId: String,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommittedCommentVO>> {
        var page = committedCommentService.listPage(committedId, size, page)
        return success(page)
    }
}
