package cc.vv.party.api;


import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.ResourceListEditParam
import cc.vv.party.service.ResourceListService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 资源清单 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "资源清单API", tags = ["资源清单移动端API"], description = "资源清单移动端API-移动端资源清单相关接口")
@RestController
@RequestMapping("/api/resource-list")
class ResourceListApi : BaseController() {

    @Autowired
    lateinit var resourceListService: ResourceListService

    @ApiOperation(value = "编辑资源清单信息", notes = "编辑资源清单信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: ResourceListEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = resourceListService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取资源清单信息", notes = "获取资源清单信息")
    @ApiImplicitParam(name = "id", value = "资源清单编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ResourceListVO> {
        var vo = resourceListService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取资源清单列表", notes = "获取资源清单列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "startTime", value = "开始时间", required = false)),
                (ApiImplicitParam(name = "endTime", value = "截止时间", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun appListPage(
                    @RequestParam(required = false) street: String?,
                    @RequestParam(required = false) community: String?,
                    @RequestParam(required = false) title: String?,
                    @RequestParam(required = false) startTime: Long?,
                    @RequestParam(required = false) endTime: Long?,
                    @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ResourceListVO>> {
        var page = resourceListService.appListPage(street, community, title, startTime, endTime, size, page)
        return success(page)
    }
}
