package cc.vv.party.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;

/**
 * <p>
 * 脱贫行政村活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@RestController
@RequestMapping("/api/poverty-alleviation-village-activity")
class PovertyAlleviationVillageActivityApi : BaseController()
