package cc.vv.party.api;


import cc.vv.party.beans.vo.ThreeMeetOneLessonVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.ThreeMeetOneLessonService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 三会一课 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "三会一课API", tags = ["三会一课移动端API"], description = "三会一课移动端API-移动端三会一课相关接口")
@RestController
@RequestMapping("/api/three-meet-one-lesson")
class ThreeMeetOneLessonApi : BaseController() {

    @Autowired
    lateinit var threeMeetOneLessonService: ThreeMeetOneLessonService

    @ApiOperation(value = "获取三会一课信息", notes = "获取三会一课信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ThreeMeetOneLessonVO> {
        val vo = threeMeetOneLessonService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取三会一课信息列表", notes = "获取三会一课信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "branchId", value = "党支部Id", required = false)),
            (ApiImplicitParam(name = "branchType", value = "党支部类型", required = false)),
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{pageSize}")
    fun listPage(
        @RequestParam(required = false, defaultValue = SysConsts.REGION_ID) branchId: String,
        @RequestParam(required = false, defaultValue = SysConsts.BranchType.REGION_LEVEL_DISTRICT) branchType: String,
        @PathVariable pageSize: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<ThreeMeetOneLessonVO>> {
        return success(
            threeMeetOneLessonService.listPage(
                null,
                null,
                null,
                null,
                branchId = branchId,
                branchType = branchType,
                page = page,
                pageSize = pageSize
            )
        )
    }


}
