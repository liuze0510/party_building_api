package cc.vv.party.api;


import cc.vv.party.beans.vo.DemandListVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.DemandListEditParam
import cc.vv.party.param.DemandListListParam
import cc.vv.party.service.DemandListService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 需求清单 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "需求清单API", tags = ["需求清单移动端API"], description = "需求清单移动端API-移动端需求清单相关接口")
@RestController
@RequestMapping("/api/demand-list")
class DemandListApi : BaseController() {

    @Autowired
    lateinit var demandListService: DemandListService

    @ApiOperation(value = "编辑需求清单信息", notes = "编辑需求清单信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: DemandListEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = demandListService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取需求清单信息", notes = "获取需求清单信息")
    @ApiImplicitParam(name = "id", value = "需求清单编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemandListVO> {
        var vo = demandListService.info(id)
        return success(vo)
    }

//    @ApiOperation(value = "获取需求清单列表", notes = "获取需求清单列表")
//    @ApiImplicitParams(
//            value = [
//                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
//                (ApiImplicitParam(name = "page", value = "页码", required = true))
//            ]
//    )
//    @PostMapping(value = "/list/{page}/{size}")
//    fun listPage(@RequestBody param: DemandListListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<DemandListVO>> {
//        var page = demandListService.listPage(param, size, page)
//        return success(page)
//    }

    @ApiOperation(value = "获取需求清单列表", notes = "获取需求清单列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "key", value = "搜索关键字", required = false)),
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "startTime", value = "开始时间", required = false)),
                (ApiImplicitParam(name = "endTime", value = "结束时间", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) key: String?,
                 @RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) startTime: Long?,
                 @RequestParam(required = false) endTime: Long?, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<DemandListVO>> {
        var page = demandListService.appListPage(street, community, grid, key, startTime, endTime, size, page)
        return success(page)
    }

}
