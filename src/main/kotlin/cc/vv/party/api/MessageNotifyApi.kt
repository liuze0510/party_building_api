package cc.vv.party.api

import cc.vv.party.beans.vo.MsgNotifyVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.MsgNotifyService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@Api(value = "消息通知", tags = ["消息通知移动端API"], description = "消息通知相关接口")
@RestController
@RequestMapping("/api/msg-notify")
class MessageNotifyApi : BaseController() {

    @Autowired
    lateinit var msgNotifyService: MsgNotifyService

    @OperateLog(content = "查看消息通知列表")
    @ApiOperation(value = "消息通知列表", notes = "消息通知列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true)),
            (ApiImplicitParam(name = "type", value = "类型(0：今天，1：昨天，2：更多记录)", required = true))
        ]
    )
    @GetMapping("/list")
    fun pendingDocList(@RequestParam page: Int, @RequestParam pageSize: Int, @RequestParam type: Int): ResponseEntityWrapper<PageWrapper<MsgNotifyVO>> {
        return success(msgNotifyService.msgNotifyList(getCurrentUserId(), type, page, pageSize))
    }


    @OperateLog(content = "查看消息通知详情")
    @ApiOperation(value = "消息通知详情", notes = "消息通知详情")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "id", value = "编号", required = true))
        ]
    )
    @GetMapping("/detail")
    fun pendingDocDetail(@RequestParam id: String): ResponseEntityWrapper<MsgNotifyVO> {
        return success(msgNotifyService.msgNotifyDetail(id))
    }
}