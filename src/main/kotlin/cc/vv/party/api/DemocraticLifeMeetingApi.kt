package cc.vv.party.api;


import cc.vv.party.beans.vo.DemocraticLifeMeetingVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.DemocraticAndOrgLifeMeetingListParam
import cc.vv.party.service.DemocraticLifeMeetingService
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 民主生活会 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Api(value = "民主生活会", tags = ["民主生活会API"], description = "民主生活会相关接口")
@RestController
@RequestMapping("/api/democratic-life-meeting")
class DemocraticLifeMeetingApi : BaseController() {

    @Autowired
    lateinit var democraticLifeMeetingService: DemocraticLifeMeetingService

    @ApiOperation(value = "获取民主生活会信息", notes = "获取民主生活会信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemocraticLifeMeetingVO> {
        var entity = democraticLifeMeetingService.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.browseNum = Integer(entity.browseNum!!.toInt().plus(1))
        democraticLifeMeetingService.updateById(entity)
        return success(entity.transfer())
    }

    @OperateLog(content = "查看民主生活会信息列表")
    @ApiOperation(value = "获取民主生活会信息列表", notes = "获取民主生活会信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: DemocraticAndOrgLifeMeetingListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<DemocraticLifeMeetingVO>> {
        var page = democraticLifeMeetingService.appListPage(param, size, page)
        return success(page)
    }

}
