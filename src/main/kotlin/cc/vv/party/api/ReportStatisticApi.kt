package cc.vv.party.api

import cc.vv.party.beans.model.Org
import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.model.User
import cc.vv.party.beans.vo.StatisticResultVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.OrgService
import cc.vv.party.service.PartyReportService
import cc.vv.party.service.UnitService
import cc.vv.party.service.UserService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-24
 * @description
 **/
@RestController
@RequestMapping("/api/report/statistic")
@Api(value = "党务管理-四双-报道统计", tags = ["党务管理-四双-报道统计API"], description = "党务管理-四双-报道统计相关接口")
class ReportStatisticApi : BaseController() {

    @Autowired
    lateinit var unitService: UnitService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var orgService: OrgService

    @ApiOperation("单位统计")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false)
            ]
    )
    @GetMapping("/unit")
    fun unit(@RequestParam(required = false) street: String?,
             @RequestParam(required = false) community: String?): ResponseEntityWrapper<List<StatisticResultVO>> {
//        val streetId = if (street.isNullOrEmpty()) getCurrentUserInfo().street else street
        val orgMap = orgService.selectList(EntityWrapper<Org>()).associateBy { it.id }
        var wrapper = EntityWrapper<Unit>()
        if(StringUtils.isNotBlank(street)){
            wrapper.eq("street", street)
        }
        if(StringUtils.isNotBlank(community)){
            wrapper.eq("community", community)
        }

        val reportMap = unitService.selectList(wrapper).groupBy { it.community }
        val result = mutableListOf<StatisticResultVO>()
        for (entry in reportMap) {
            val vo = StatisticResultVO()
            vo.communityName = orgMap[entry.key]?.name
            vo.totalCount = entry.value.size.toString()
            vo.reportedCount =
                    entry.value.filterIndexed { _, unit -> unit.reported == SysConsts.REPORTED }.size.toString()
            result.add(vo)
        }
        return success(result)
    }

    @ApiOperation("党员报道统计")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "street", value = "街道", required = false),
                ApiImplicitParam(name = "community", value = "社区", required = false),
                ApiImplicitParam(name = "grid", value = "网格", required = false)
            ]
    )
    @GetMapping("/user")
    fun user(@RequestParam(required = false) street: String?,
             @RequestParam(required = false) community: String?,
             @RequestParam(required = false) grid: String?): ResponseEntityWrapper<List<StatisticResultVO>> {
//        val streetId = if (id.isNullOrEmpty()) getCurrentUserInfo().street else id
        val orgMap = orgService.selectList(EntityWrapper<Org>()).associateBy { it.id }

        var wrapper = EntityWrapper<User>()
        if(StringUtils.isNotBlank(street)){
            wrapper.eq("street", street)
        }
        if(StringUtils.isNotBlank(community)){
            wrapper.eq("community", community)
        }
        if(StringUtils.isNotBlank(grid)){
            wrapper.eq("grid", grid)
        }

        val reportMap =
            userService.selectList(wrapper).groupBy { it.community }
        val result = mutableListOf<StatisticResultVO>()
        for (entry in reportMap) {
            val vo = StatisticResultVO()
            vo.communityName = orgMap[entry.key]?.name
            vo.totalCount = entry.value.size.toString()
            vo.reportedCount =
                    entry.value.filterIndexed { _, user -> user.reported == SysConsts.REPORTED }.size.toString()
            result.add(vo)
        }
        return success(result)
    }
}