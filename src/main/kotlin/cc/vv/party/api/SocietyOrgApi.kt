package cc.vv.party.api;


import cc.vv.party.beans.vo.SocietyOrgVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.SocietyOrgService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社会组织 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社会组织API", tags = ["社会组织移动端API"], description = "社会组织移动端API-移动端社会组织相关接口")
@RestController
@RequestMapping("/api/society-org")
class SocietyOrgApi : BaseController() {

    @Autowired
    lateinit var societyOrgService: SocietyOrgService

    @ApiOperation(value = "获取社会组织", notes = "获取社会组织")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<SocietyOrgVO> {
        var vo = societyOrgService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取社会组织列表", notes = "获取社会组织列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "community", value = "所在社区", required = false)),
                (ApiImplicitParam(name = "orgName", value = "组织名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) community: String?,
                 @RequestParam(required = false) orgName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<SocietyOrgVO>> {
        var page = societyOrgService.listPage(community, orgName, size, page)
        return success(page)
    }

}
