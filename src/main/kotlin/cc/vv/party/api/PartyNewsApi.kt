package cc.vv.party.api;


import cc.vv.party.beans.vo.PartyNewsVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.service.PartyNewsService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 党群新闻 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "党群新闻API", tags = ["党群新闻移动端API"], description = "党群新闻移动端API-移动端党群新闻相关接口")
@RestController
@RequestMapping("/api/party-news")
class PartyNewsApi : BaseController() {

    @Autowired
    lateinit var partyNewsService: PartyNewsService

    @ApiOperation(value = "获取党群新闻", notes = "获取党群新闻")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PartyNewsVO> {
        var vo = partyNewsService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PartyNewsVO>> {
        var page = partyNewsService.listPage(title, size, page)
        return success(page)
    }


}
