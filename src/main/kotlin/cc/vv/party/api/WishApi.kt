package cc.vv.party.api;


import cc.vv.party.beans.vo.WishVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.WishListParam
import cc.vv.party.param.WishSaveParam
import cc.vv.party.service.WishService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 微心愿 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "微心愿API", tags = ["微心愿移动端API"], description = "微捐赠移动端API-移动端微心愿相关接口")
@RestController
@RequestMapping("/api/wish")
class WishApi : BaseController() {

    @Autowired
    lateinit var wishService: WishService

    @ApiOperation(value = "保存微心愿信息", notes = "保存、更新、认领微心愿信息，均用该接口")
    @PostMapping("/save")
    fun save(@RequestBody param: WishSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = wishService.save(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @ApiOperation(value = "获取微心愿信息", notes = "获取微心愿信息")
    @ApiImplicitParam(name = "id", value = "微心愿编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<WishVO>{
        var vo = wishService.info(id)
        return success(vo)
    }


    @ApiOperation(value = "获取微心愿列表", notes = "获取微心愿列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: WishListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<WishVO>> {
        param.checkState = Integer(0)
        var page = wishService.listPage(param, size, page)
        return success(page)
    }

    @ApiOperation(value = "我的认领的微心愿列表", notes = "我的认领的微心愿列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/my-claim-list/{page}/{size}")
    fun claimListPage(@PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<WishVO>> {
        var page = wishService.claimListPage(getCurrentUserAccount(), getCurrentUserId(), size, page)
        return success(page)
    }


}
