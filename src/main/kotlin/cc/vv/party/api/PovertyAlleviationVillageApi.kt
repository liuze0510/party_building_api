package cc.vv.party.api;


import cc.vv.party.beans.vo.PovertyAlleviationVillageVO
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.PovertyAlleviationVillageService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping

/**
 * <p>
 * 扶贫行政村信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "扶贫行政村信息API", tags = ["扶贫行政村信息移动端API"], description = "扶贫行政村信息移动端API-移动端扶贫行政村信息相关接口")
@RestController
@RequestMapping("/api/poverty-alleviation-village")
class PovertyAlleviationVillageApi : BaseController() {

    @Autowired
    lateinit var povertyAlleviationVillageService: PovertyAlleviationVillageService

    @ApiOperation(value = "获取扶贫行政村树", notes = "获取扶贫行政村树")
    @GetMapping("/tree")
    fun treeList(): ResponseEntityWrapper<List<PovertyAlleviationVillageVO>> {
        var tree = povertyAlleviationVillageService.treeList()
        return success(tree)
    }

    @ApiOperation(value = "获取扶贫行政村信息", notes = "获取扶贫行政村信息")
    @GetMapping("/message/{villageId}")
    fun message(villageId: String): ResponseEntityWrapper<PovertyAlleviationVillageVO> {
        var message = povertyAlleviationVillageService.message(villageId)
        return success(message)
    }

    @OperateLog("查看当前用户所在扶贫行政村信息")
    @ApiOperation(value = "获取当前用户所在扶贫行政村信息", notes = "获取当前用户所在扶贫行政村信息")
    @GetMapping("/current-user-village")
    fun getCurrentUserVillage(): ResponseEntityWrapper<PovertyAlleviationVillageVO> {
        var message = povertyAlleviationVillageService.getCurrentUserVillage(getCurrentUserAccount())
        return success(message)
    }

}
