package cc.vv.party.service;

import cc.vv.party.beans.model.CommunityPartyCommittee;
import cc.vv.party.beans.vo.CommunityPartyCommitteeVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.CommunityPartyCommitteeEditParam

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityPartyCommitteeService : BaseService<CommunityPartyCommittee> {


    fun edit(param: CommunityPartyCommitteeEditParam, userId: String): Boolean

    fun deleteCommunityPartyCommittee(id: String): Boolean

    fun info(id: String): CommunityPartyCommitteeVO

    /** 获取当前用户管理社区党委信息 */
    fun getCurrentUserManagerPartyCommittee(account: String): CommunityPartyCommitteeVO

    fun listPage(street: String?, community: String?, name: String?, size: Int, page: Int): PageWrapper<CommunityPartyCommitteeVO>

}
