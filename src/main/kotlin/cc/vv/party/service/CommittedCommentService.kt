package cc.vv.party.service;

import cc.vv.party.beans.model.CommittedComment;
import cc.vv.party.beans.vo.CommittedCommentVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper

/**
 * <p>
 * 承诺评论信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommittedCommentService : BaseService<CommittedComment> {

    fun listPage(committedId: String, size: Int, page: Int): PageWrapper<CommittedCommentVO>
    
}
