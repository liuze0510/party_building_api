package cc.vv.party.service;

import cc.vv.party.beans.model.VolunteerProjectSignUp;
import cc.vv.party.beans.vo.VolunteerProjectSignUpVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.param.VolunteerProjectSignUpSaveParam

/**
 * <p>
 * 志愿项目报名信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerProjectSignUpService : BaseService<VolunteerProjectSignUp>{

    fun save(param: VolunteerProjectSignUpSaveParam, userId: String): Boolean

    fun getProSignUpUserList(proId: String): List<VolunteerProjectSignUpVO>

}
