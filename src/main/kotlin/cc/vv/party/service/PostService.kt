package cc.vv.party.service;

import cc.vv.party.beans.model.Post;
import cc.vv.party.beans.vo.PostJobHuntingUserStatisticsVO
import cc.vv.party.beans.vo.PostVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PostEditParam

/**
 * <p>
 * 岗位信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PostService : BaseService<Post> {

    fun edit(param: PostEditParam, userId: String): Boolean

    fun info(id: String): PostVO

    fun findAppListPage(street: String?, community: String?, grid: String?, postName: String?, size: Int, page: Int, user: UserVO): PageWrapper<PostVO>

    fun listPage(street: String?, community: String?, grid: String?, time: Long?, postSource: String?, salary: String?, postName: String?, size: Int, page: Int): PageWrapper<PostVO>

    fun findStatistics(time: String?): PostJobHuntingUserStatisticsVO
    
    
}
