package cc.vv.party.service;

import cc.vv.party.beans.model.StreetJointMeetingAdmin;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 联席会议管理员 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface StreetJointMeetingAdminService : BaseService<StreetJointMeetingAdmin>
