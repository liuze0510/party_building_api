package cc.vv.party.service;

import cc.vv.party.beans.model.PartyNews;
import cc.vv.party.beans.vo.PartyNewsVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PartyNewsEditParam

/**
 * <p>
 * 党群新闻 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyNewsService : BaseService<PartyNews> {

    fun edit(param: PartyNewsEditParam, userId: String): Boolean

    fun info(id: String): PartyNewsVO

    fun listPage(title: String?, size: Int, page: Int): PageWrapper<PartyNewsVO>
}
