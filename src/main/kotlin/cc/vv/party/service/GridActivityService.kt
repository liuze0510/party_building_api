package cc.vv.party.service;

import cc.vv.party.beans.vo.GridActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.model.GridActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.GridActivityEditParam

/**
 * <p>
 * 网格活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface GridActivityService : BaseService<GridActivity> {

    fun edit(param: GridActivityEditParam, user: UserVO): Boolean

    fun info(id: String): GridActivityVO

    fun listPage(street: String?, community: String?, grid: String?, title: String?, size: Int, page: Int): PageWrapper<GridActivityVO>

}
