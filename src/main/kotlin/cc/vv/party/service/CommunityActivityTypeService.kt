package cc.vv.party.service;

import cc.vv.party.beans.model.CommunityActivityType;
import cc.vv.party.beans.vo.CommunityActivityTypeVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.CommunityActivityTypeEditParam

/**
 * <p>
 * 社区活动类型 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityActivityTypeService : BaseService<CommunityActivityType> {

    fun edit(param: CommunityActivityTypeEditParam, userId: String): Boolean

    fun info(id: String): CommunityActivityTypeVO

    fun listPage(typeName: String?, size: Int, page: Int): PageWrapper<CommunityActivityTypeVO>

}
