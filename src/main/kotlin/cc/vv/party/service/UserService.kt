package cc.vv.party.service;

import cc.vv.party.beans.model.User;
import cc.vv.party.beans.vo.LearningStatisticsVO
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UserEditParam
import cc.vv.party.param.UserListParam
import cc.vv.party.param.UserStatisticsParam

/**
 * <p>
 *  用户服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UserService : BaseService<User> {

    /**
     * 登录
     */
    fun login(account: String, password: String): MutableMap<String, Any?>

    /**
     * 手机端登录
     */
    fun appUserLogin(account: String, password: String): MutableMap<String, Any?>

    fun save(param: UserEditParam, userId: String): Boolean

    fun update(param: UserEditParam): Boolean

    fun info(id: String): UserVO

    fun listPage(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO>

    /**
     * 获取延安数据库中的党员信息列表
     */
    fun partyMemberListPage(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO>

    /**
     * 获取延安数据库中的党员信息详情
     */
    fun partyMemberInfo(id: String): UserVO

    /** 职业统计(key为职业代码，value为职业对应的人数。职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  其他职业 */
    fun careerStatistics(param: UserStatisticsParam): Map<String, Int>

    /** 民族统计(key为民族代码，value为该民族数量。民族 0 汉族 1 少数民族 */
    fun ethnicStatistics(param: UserStatisticsParam): Map<String, Int>

    /** 性别统计(key为性别代码，value为该性别数量。性别 0 男 1 女 */
    fun sexStatistics(param: UserStatisticsParam): Map<String, Int>

    /** 文化程度统计(key为文化程度代码，value为该文化程度数量。文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上) */
    fun educationalLevelStatistics(param: UserStatisticsParam): Map<String, Int>

    /** 年龄统计(key为年龄段代码，value为该年龄段数量。年龄段 0： 0-6岁， 1:7-17岁，2： 18-35岁，3:36-45岁，4:46-59岁，5:60以上 */
    fun ageStatistics(param: UserStatisticsParam): Map<String, Int>

    /** 党员报道统计 */
    fun reportStatistics(param: ReportStatisticsParam): List<ReportStatisticsVO>

    /**学习时长统计列表**/
    fun learningStatistics(
        branchType: String,
        branchId: String,
        page: Int,
        pageSize: Int
    ): PageWrapper<LearningStatisticsVO>

    /**学习时长统计详情**/
    fun learningStatisticsDetail(
        branchType: String,
        branchId: String,
        year: String,
        month: String?,
        startDay: String?, endDay: String?
    ): HashMap<String, Long>

    /**组织机构学习总时长**/
    fun learningTotalStatistics(
        branchType: String,
        branchId: String
    ): String
}
