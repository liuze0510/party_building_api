package cc.vv.party.service;

import cc.vv.party.beans.model.RatingDetails;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 单位评星晋阶明细 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface RatingDetailsService : BaseService<RatingDetails>
