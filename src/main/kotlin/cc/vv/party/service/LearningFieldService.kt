package cc.vv.party.service;

import cc.vv.party.beans.vo.*
import cc.vv.party.common.wrapper.PageWrapper

/**
 * <p>
 * 学习园地 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface LearningFieldService {

    /** 微感悟 **/
    fun findFeelingPageList(branchType: String, branchId: String, page: Int, size: Int): PageWrapper<FeelingVO>

    /** 党建党章 */
    fun findPartyConstitutionPageList(page: Int, size: Int): PageWrapper<PartyConstitutionVO>

    /** 讲话系列 */
    fun findSeriesSpeechPageList(page: Int, size: Int): PageWrapper<SeriesSpeechVO>

    /** 微百科科目列表 */
    fun findSubjectList(): List<SubjectVO>

    /** 微百科 */
    fun findOpenClassList(subjectId: String, page: Int, size: Int): PageWrapper<OpenClassVO>

    /** 心得体会 */
    fun findExperiencePageList(branchType: String, branchId: String, page: Int, size: Int): PageWrapper<ExperienceVO>

    /** 心得体会 */
    fun findOnlineExamPageList(page: Int, size: Int): PageWrapper<OnlineExamVO>

    /** 在线考试试卷题目列表 */
    fun findQuestionPageList(blankId: String, page: Int, size: Int): PageWrapper<QuestionVO>

    /** 获取远程教育类型 */
    fun findEducationTypeList(): List<Map<String, String>>

    /** 远程教育列表数量 */
    fun findEducationPageList(typeId: String, page: Int, size: Int): PageWrapper<EducationVO>

    /** 信息详情 */
    fun getMessageInfo(type: Int, url: String): String

    /** 党组织成员统计 */
    fun findPartyOrgMemberStatistics(id: String, type: String): Map<String, Int>


    /********  start 微感悟统计  *****************************************************************/
    fun getFeelingStatistics(): FeelingStatisticsVO

    fun getFeelingStatisticsDataTrend(startTime: Long?, endTime: Long?): FeelingStatisticsDataTrendVO

    fun getFeelingByDataDetail(
        time: String?,
        branchId: String?,
        sumDesc: Int,
        byDesc: Int,
        page: Int,
        size: Int
    ): PageWrapper<FeelingDataDetailVO>

    /********  end   微感悟统计  *****************************************************************/


    /********  start 心得体会统计  *****************************************************************/
    fun getExperienceStatistics(): ExperienceStatisticsVO

    fun getExperienceStatisticsDataTrend(date: Long?): ExperienceStatisticsDataTrendVO

    fun getExperienceByDataDetail(
        time: String?,
        branchId: String?,
        branchType: String?,
        page: Int,
        size: Int
    ): PageWrapper<ExperienceDataDetailVO>

    /********  end   心得体会统计  *****************************************************************/

    /********  start   在线考试统计  *****************************************************************/

    fun examTotalStatistics(): ExamTotalStatisticsVO

    fun examTrendStatistics(type: Int, timeType: Int): List<ExamTrendVO>

    fun findQuestionBlankList(): List<OnlineExamVO>

    /** 获取在线考试数据详情 */
    fun findOnlineExamDataDetail(blankId: String, date: String?, branchId: String?, branchType: String?, sort: Int, page: Int, size: Int): PageWrapper<OnlineStudyDataDetailVO>
    /********  end   在线考试统计  *****************************************************************/

    /********  start  app端 标准化数据  *****************************************************************/
    fun findAreaTree(): AreauserVO

    fun getStandardData(areaUserId: Int, year: Integer?, month: Integer?): StandardDataVO
    /********  end   app端 标准化数据  *****************************************************************/
}
