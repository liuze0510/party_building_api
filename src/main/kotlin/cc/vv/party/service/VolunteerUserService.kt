package cc.vv.party.service;

import cc.vv.party.beans.model.VolunteerUser
import cc.vv.party.beans.vo.VolunteerStatisticsVO
import cc.vv.party.beans.vo.VolunteerUserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.VolunteerUserEditParam
import cc.vv.party.param.VolunteerUserListParam

/**
 * <p>
 * 志愿者信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerUserService : BaseService<VolunteerUser> {

    fun edit(param: VolunteerUserEditParam, userId: String): Boolean

    fun getById(id: String): VolunteerUserVO

    fun info(id: String): VolunteerUserVO

    fun listPage(param: VolunteerUserListParam, size: Int, page: Int): PageWrapper<VolunteerUserVO>

    fun findStatistics(year: Int?, street: String?, community: String?, grid: String?): VolunteerStatisticsVO

}
