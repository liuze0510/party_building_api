package cc.vv.party.service;

import cc.vv.party.beans.vo.StreetJointMeetingActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.model.StreetJointMeetingActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.StreetJointMeetingActivityEditParam

/**
 * <p>
 * 街道联席会议活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
interface StreetJointMeetingActivityService : BaseService<StreetJointMeetingActivity> {

    fun edit(param: StreetJointMeetingActivityEditParam, user: UserVO): Boolean

    fun info(id: String): StreetJointMeetingActivityVO

    fun listPage(street: String?, time: Long?, type: String?, title: String?, size: Int, page: Int): PageWrapper<StreetJointMeetingActivityVO>

}
