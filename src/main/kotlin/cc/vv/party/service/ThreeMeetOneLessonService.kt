package cc.vv.party.service;

import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.model.ThreeMeetOneLesson;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ThreeMeetOneLessonEditParam
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 三会一课 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
interface ThreeMeetOneLessonService : BaseService<ThreeMeetOneLesson> {

    fun info(id: String): ThreeMeetOneLessonVO

    fun listPage(
        startDate: String?,
        endDate: String?,
        type: String?,
        title: String?,
        branchId: String?,
        branchType: String?,
        page: Int,
        pageSize: Int
    ): PageWrapper<ThreeMeetOneLessonVO>

    fun pcListPage(page: Int, pageSize: Int): PageWrapper<ThreeMeetOneLessonVO>
}
