package cc.vv.party.service;

import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.UnitVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UnitEditParam
import cc.vv.party.param.UnitListParam

/**
 * <p>
 * 单位信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UnitService : BaseService<Unit> {

    fun edit(param: UnitEditParam, user: UserVO, userId: String): Boolean

    fun appInfo(unitId: String): UnitVO

    fun managerInfo(id: String): UnitVO

    fun appListPage(type: Int, size: Int, page: Int): PageWrapper<UnitVO>

    fun listPage(param: UnitListParam, size: Int, page: Int): PageWrapper<UnitVO>

    /** 单位报道统计 */
    fun reportStatistics(param: ReportStatisticsParam): List<ReportStatisticsVO>

}
