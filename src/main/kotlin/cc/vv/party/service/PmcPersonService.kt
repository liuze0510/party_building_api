package cc.vv.party.service;

import cc.vv.party.beans.model.PmcPerson
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.UserListParam

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
interface PmcPersonService : BaseService<PmcPerson> {

    fun selectPartyMemberList(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO>

    fun selectPartyMemberById(id: String): UserVO?

    fun selectPartyMemberByIdCard(idCard: String): UserVO?

    fun selectPartyMemberByAccounts(accounts: List<String>): List<UserVO>
}
