package cc.vv.party.service;

import cc.vv.party.beans.model.Permission
import cc.vv.party.beans.model.Role
import cc.vv.party.beans.model.UserRole;
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface UserRoleService : BaseService<UserRole> {

    fun findUserPermission(userAccount: String): List<Permission>

    fun findUserRole(userAccount: String): List<RoleVO>?
}
