package cc.vv.party.service

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.PendingDocVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
interface PendingDocService : BaseService<PartyReport> {

    fun selectPendingDocList(
        page: Int,
        pageSize: Int,
        yananRole: String,
        currentUserId: String,
        branchVo: CurrentBranchInfoVO
    ): PageWrapper<PendingDocVO>

    fun details(id: String, currentUserId: String): PendingDocVO
}