package cc.vv.party.service;

import cc.vv.party.beans.vo.PartyActivityVO
import cc.vv.party.model.PartyActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PartyActivityEditParam

/**
 * <p>
 * 社区大党委活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface PartyActivityService : BaseService<PartyActivity> {

    fun edit(param: PartyActivityEditParam, userId: String): Boolean

    fun info(id: String): PartyActivityVO

    fun listPage(street: String?, partyId: String?, title: String?, size: Int, page: Int): PageWrapper<PartyActivityVO>
}
