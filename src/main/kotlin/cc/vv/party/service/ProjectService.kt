package cc.vv.party.service;

import cc.vv.party.beans.model.Project
import cc.vv.party.beans.vo.ProjectVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ProjectListParam
import cc.vv.party.param.ProjectSaveParam
import cc.vv.party.param.ProjectStatisticsParam

/**
 * <p>
 * 项目信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectService : BaseService<Project> {

    fun edit(param: ProjectSaveParam, user: UserVO, userId: String): Boolean

    fun deleteProject(id: String): Boolean

    fun info(id: String): ProjectVO

    fun appListPage(street: String?, community: String?, grid: String?, title: String?, state: String?, size: Int, page: Int, user: UserVO): PageWrapper<ProjectVO>

    fun pcListPage(size: Int, page: Int, type: Int): PageWrapper<ProjectVO>

    fun listPage(param: ProjectListParam, size: Int, page: Int, type: Int): PageWrapper<ProjectVO>

    fun findWarnListPage(param: ProjectListParam, size: Int, page: Int): PageWrapper<ProjectVO>

    fun projectSysStateStatistics(param: ProjectStatisticsParam): Map<String, Integer>

    fun updateProjectApply(id: String, type: Int, applyReson: String, sysState: Int)
    
}
