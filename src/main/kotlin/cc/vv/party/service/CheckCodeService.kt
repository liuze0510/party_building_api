package cc.vv.party.service

import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.model.CheckCode
import cc.vv.party.common.base.BaseService

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
interface CheckCodeService : BaseService<CheckCode>