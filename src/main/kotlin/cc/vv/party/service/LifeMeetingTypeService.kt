package cc.vv.party.service;

import cc.vv.party.beans.vo.LifeMeetingTypeVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.model.LifeMeetingType

/**
 * <p>
 * 民主、组织生活会类型 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
interface LifeMeetingTypeService : BaseService<LifeMeetingType> {

    fun edit(id: String?, type: Int, name: String, userId: String): Boolean

    fun info(id: String): LifeMeetingTypeVO

    fun listPage(type: Int, size: Int, page: Int): PageWrapper<LifeMeetingTypeVO>

    fun listAll(type: Int): List<LifeMeetingTypeVO>

}
