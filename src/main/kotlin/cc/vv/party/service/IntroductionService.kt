package cc.vv.party.service;

import cc.vv.party.beans.model.Introduction;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 党建概况 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface IntroductionService : BaseService<Introduction>
