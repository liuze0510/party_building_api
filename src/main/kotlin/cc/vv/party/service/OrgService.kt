package cc.vv.party.service;

import cc.vv.party.beans.model.Org
import cc.vv.party.beans.model.Role
import cc.vv.party.beans.vo.CommunityVO
import cc.vv.party.beans.vo.NodeVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.param.OrgEditParam
import cc.vv.party.param.OrgParam

/**
 * <p>
 * 组织机构 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface OrgService : BaseService<Org> {

    fun addOrg(param: OrgParam, currentUserId: String, currentAccount: String): OrgVO

    fun getOrgList(): OrgVO

    fun getManagedOrgList(currentAccount: String): OrgVO

    fun deleteOrg(orgId: String, currentAccount: String): OrgVO

    /**
     * 获取当前账号所管理的组织结构列表
     */
    fun findOrgByUserAccount(account: String): List<OrgVO>

    /**
     * 获取当前用户所管理的组织结构列表及子集合列表
     */
    fun findUserOrgAndChildren(account: String): List<OrgVO>

    /**
     * 获取社区下的组织结构信息
     */
    fun getCommunityAndChildrenById(communityId: String): CommunityVO

    fun getGridListByCommunityId(communityId: String): MutableList<OrgVO>

    fun getOrgListByIds(orgIds: List<String>): List<OrgVO>

    fun getOrgDetailById(orgId: String): OrgVO

    fun updateOrg(param: OrgEditParam, currentUserId: String, currentAccount: String): OrgVO

    fun findPartyNode(type: String?, nodeId: String?): List<NodeVO>

    fun findAppSatistics(year: Int, street: String?, community: String?, grid: String?): Map<Int, Int>
}
