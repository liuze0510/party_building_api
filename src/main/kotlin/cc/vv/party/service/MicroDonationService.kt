package cc.vv.party.service;

import cc.vv.party.beans.model.MicroDonation
import cc.vv.party.beans.vo.MicroDonationVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.MicroDonationListParam
import cc.vv.party.param.MicroDonationSaveParam

/**
 * <p>
 * 微捐赠 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface MicroDonationService : BaseService<MicroDonation> {

    fun save(param: MicroDonationSaveParam, user: UserVO, userId: String): Boolean

    fun edit(param: MicroDonationSaveParam): Boolean

    fun claimMicroDonation(id: String): Boolean

    fun checkMicroDonation(id: String, state: Int, reason: String?): Boolean

    fun deleteMicroDonation(id: String): Boolean

    fun info(id: String): MicroDonationVO

    fun claimListPage(mobile: String, userId: String, size: Int, page: Int): PageWrapper<MicroDonationVO>

    fun listPage(param: MicroDonationListParam, size: Int, page: Int): PageWrapper<MicroDonationVO>
}
