package cc.vv.party.service;

import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.OrgLiFeMeetingVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.model.OrgLiFeMeeting
import cc.vv.party.param.DemocraticAndOrgLifeMeetingListParam
import cc.vv.party.param.OrgLiFeMeetingEditParam

/**
 * <p>
 * 组织生活会 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface OrgLiFeMeetingService : BaseService<OrgLiFeMeeting> {

    fun edit(param: OrgLiFeMeetingEditParam, userId: String, branchInfo: CurrentBranchInfoVO): Boolean

    fun info(id: String): OrgLiFeMeetingVO

    fun appListPage(param: DemocraticAndOrgLifeMeetingListParam, size: Int, page: Int): PageWrapper<OrgLiFeMeetingVO>

    fun listPage(
        branchType: String?,
        branchId: String?,
        time: Long?,
        type: String?,
        title: String?,
        size: Int,
        page: Int,
        currentBranchInfoVO: CurrentBranchInfoVO
    ): PageWrapper<OrgLiFeMeetingVO>

}
