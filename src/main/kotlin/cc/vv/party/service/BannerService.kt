package cc.vv.party.service;

import cc.vv.party.beans.model.Banner;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * banner管理 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface BannerService : BaseService<Banner>
