package cc.vv.party.service;

import cc.vv.party.beans.model.SocietyOrgDynamic
import cc.vv.party.beans.vo.SocietyOrgDynamicVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.SocietyOrgDynamicEditParam

/**
 * <p>
 * 社会组织动态 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface SocietyOrgDynamicService : BaseService<SocietyOrgDynamic> {

    fun edit(param: SocietyOrgDynamicEditParam, userId: String): Boolean

    fun info(id: String): SocietyOrgDynamicVO

    fun listPage(time: Long?, releaseOrg: String?, title: String?, size: Int, page: Int): PageWrapper<SocietyOrgDynamicVO>

    fun listPageByCommunityId(communityId: String, size: Int, page: Int): PageWrapper<SocietyOrgDynamicVO>
}
