package cc.vv.party.service;

import cc.vv.party.beans.model.Log;
import cc.vv.party.beans.vo.AppRealTimeStatisticsVO
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 微捐赠 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface LogService : BaseService<Log> {


    fun findAppRealTimeStatistics(date: String): List<AppRealTimeStatisticsVO>

}
