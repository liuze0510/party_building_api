package cc.vv.party.service;

import cc.vv.party.beans.model.PartyCommitteeOrgRelation;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 社区大党委与组织成员关系 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyCommitteeOrgRelationService : BaseService<PartyCommitteeOrgRelation>
