package cc.vv.party.service;

import cc.vv.party.beans.model.ConvenienceServiceType;
import cc.vv.party.beans.vo.ConvenienceServiceTypeVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper

/**
 * <p>
 * 便民服务类型 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ConvenienceServiceTypeService : BaseService<ConvenienceServiceType> {

    fun edit(id: String?, name: String, userId: String): Boolean

    fun deleteConvenienceServiceType(id: String): Boolean

    fun info(id: String): ConvenienceServiceTypeVO

    fun listPage(size: Int, page: Int): PageWrapper<ConvenienceServiceTypeVO>

    fun listAll(): List<ConvenienceServiceTypeVO>
    
}
