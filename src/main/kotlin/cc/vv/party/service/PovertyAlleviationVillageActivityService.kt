package cc.vv.party.service;

import cc.vv.party.beans.vo.PovertyAlleviationVillageActivityVO
import cc.vv.party.model.PovertyAlleviationVillageActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PovertyAlleviationVillageActivityEditParam

/**
 * <p>
 * 脱贫行政村活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface PovertyAlleviationVillageActivityService : BaseService<PovertyAlleviationVillageActivity> {


    fun edit(param: PovertyAlleviationVillageActivityEditParam, userId: String): Boolean

    fun info(id: String): PovertyAlleviationVillageActivityVO

    fun listPage(town: String?, village: String?, title: String?, size: Int, page: Int): PageWrapper<PovertyAlleviationVillageActivityVO>

}
