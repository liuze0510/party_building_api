package cc.vv.party.service;

import cc.vv.party.beans.model.CommunityActivity;
import cc.vv.party.beans.vo.CommunityActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.CommunityActivityEditParam

/**
 * <p>
 * 社区活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityActivityService : BaseService<CommunityActivity> {

    fun edit(param: CommunityActivityEditParam, user: UserVO): Boolean

    fun info(id: String): CommunityActivityVO

    fun listByCommunityId(communityId: String, size: Int, page: Int): PageWrapper<CommunityActivityVO>

    fun listPage(street: String?, community: String?, time: Long?, type: String?, title: String?, size: Int, page: Int): PageWrapper<CommunityActivityVO>
}
