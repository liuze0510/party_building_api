package cc.vv.party.service;

import cc.vv.party.beans.vo.SocialistValuesVO
import cc.vv.party.model.SocialistValues;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.SocialistValuesEditParam

/**
 * <p>
 * 社会主义核心价值观 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
interface SocialistValuesService : BaseService<SocialistValues> {

    fun edit(param: SocialistValuesEditParam, userId: String): Boolean

    fun info(id: String): SocialistValuesVO

    fun listPage(time: Long?, type: Integer?, title: String?, size: Int, page: Int): PageWrapper<SocialistValuesVO>


}
