package cc.vv.party.service;

import cc.vv.party.model.MicroDonationClain;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.param.MicroDonationClainEditParam

/**
 * <p>
 * 微捐赠认领 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-05
 */
interface MicroDonationClainService : BaseService<MicroDonationClain> {


    fun save(param: MicroDonationClainEditParam, userId: String): Boolean

    fun clain(id: String): Boolean

}
