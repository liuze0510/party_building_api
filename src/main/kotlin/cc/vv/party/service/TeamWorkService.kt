package cc.vv.party.service;

import cc.vv.party.beans.model.TeamWork;
import cc.vv.party.beans.vo.TeamWorkVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.TeamWorkEditParam

/**
 * <p>
 * 图片信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface TeamWorkService : BaseService<TeamWork> {

    fun edit(param: TeamWorkEditParam, userId: String): Boolean

    fun info(id: String): TeamWorkVO

    fun listPage(teamId: String, title: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<TeamWorkVO>

    fun appListPage(teamId: String, startTime: Long, endTime: Long, size: Int, page: Int): PageWrapper<TeamWorkVO>

    
}
