package cc.vv.party.service;

import cc.vv.party.beans.vo.BuildingPartyGroupActivityVO
import cc.vv.party.model.BuildingPartyGroupActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.BuildingPartyGroupActivityEditParam

/**
 * <p>
 * 楼栋党小组活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface BuildingPartyGroupActivityService : BaseService<BuildingPartyGroupActivity> {

    fun edit(param: BuildingPartyGroupActivityEditParam, userId: String): Boolean

    fun info(id: String): BuildingPartyGroupActivityVO

    fun listPage(street: String?, community: String?, grid: String?, groupId: String?, title: String?, size: Int, page: Int): PageWrapper<BuildingPartyGroupActivityVO>

}
