package cc.vv.party.service;

import cc.vv.party.beans.model.VolunteerProject;
import cc.vv.party.beans.vo.VolunteerProjectVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.VolunteerProjectEditParam

/**
 * <p>
 * 志愿项目信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerProjectService : BaseService<VolunteerProject> {

    fun edit(param: VolunteerProjectEditParam, userId: String): Boolean

    fun appInfo(id: String): VolunteerProjectVO

    fun info(id: String): VolunteerProjectVO

    fun userVolunteerProjectList(userId: String): List<VolunteerProjectVO>

    fun appListPage(projectName: String?, size: Int, page: Int): PageWrapper<VolunteerProjectVO>

    fun listPage(holdingTime: Long?, holdingCompany: String?, projectName: String?, size: Int, page: Int): PageWrapper<VolunteerProjectVO>

}
