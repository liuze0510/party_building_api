package cc.vv.party.service;

import cc.vv.party.beans.model.PmcAccountor
import cc.vv.party.common.base.BaseService

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
interface PmcAccountorService : BaseService<PmcAccountor> {

    fun checkAccountExists(account: String, orgId: String?)

    fun checkPmcAccountExists(account: String)

    fun checkAccountListExists(accountList: List<String>)
}
