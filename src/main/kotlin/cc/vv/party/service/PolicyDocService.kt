package cc.vv.party.service;

import cc.vv.party.beans.model.PolicyDoc;
import cc.vv.party.beans.vo.PolicyDocVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PolicyDocEditParam

/**
 * <p>
 * 政策文件信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PolicyDocService : BaseService<PolicyDoc> {

    fun edit(param: PolicyDocEditParam, userId: String): Boolean

    fun info(id: String): PolicyDocVO

    fun listPage(title: String?, releaseTime: Long?, size: Int, page: Int): PageWrapper<PolicyDocVO>
    
}
