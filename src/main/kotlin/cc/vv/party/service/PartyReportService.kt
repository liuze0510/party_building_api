package cc.vv.party.service;

import cc.vv.party.beans.model.PartyReport;
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.PartyReportVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.PartyReportEditParam
import cc.vv.party.param.PartyReportListParam

/**
 * <p>
 * 党员报道信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyReportService : BaseService<PartyReport> {

    fun edit(param: PartyReportEditParam, user: UserVO, userId: String): Boolean

    fun info(id: String): PartyReportVO

    fun listPage(param: PartyReportListParam, size: Int, page: Int): PageWrapper<PartyReportVO>

    fun selectReportUserList(param: PartyReportListParam, size: Int, page: Int): PageWrapper<PartyReportVO>
}
