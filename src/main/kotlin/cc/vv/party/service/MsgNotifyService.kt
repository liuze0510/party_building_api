package cc.vv.party.service

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.MsgNotifyVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
interface MsgNotifyService : BaseService<PartyReport> {

    fun msgNotifyList(currentUserId: String, type: Int, page: Int, pageSize: Int): PageWrapper<MsgNotifyVO>

    fun msgNotifyDetail(id: String): MsgNotifyVO
}