package cc.vv.party.service;

import cc.vv.party.beans.model.ConvenienceServices;
import cc.vv.party.beans.vo.ConvenienceServiceVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ConvenienceServiceListParam
import cc.vv.party.param.ConvenienceServiceEditParam

/**
 * <p>
 * 便民服务 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ConvenienceServicesService : BaseService<ConvenienceServices> {


    fun edit(param: ConvenienceServiceEditParam, userId: String): Boolean

    fun deleteConvenienceService(id: String): Boolean

    fun info(id: String): ConvenienceServiceVO

    fun appListPage(street: String?, community: String?, grid: String?, type: String?, title: String?, size: Int, page: Int): PageWrapper<ConvenienceServiceVO>

    fun listPage(param: ConvenienceServiceListParam, size: Int, page: Int): PageWrapper<ConvenienceServiceVO>
    
}
