package cc.vv.party.service;

import cc.vv.party.beans.model.TeamSignIn
import cc.vv.party.beans.vo.SignInVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.param.TeamSignInParam

/**
 * <p>
 * 四支队伍信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface TeamSignInService : BaseService<TeamSignIn>{

    fun signIn(param: TeamSignInParam, userId: String): Boolean

    fun info(teamId: String, time: String): SignInVO

}
