package cc.vv.party.service;

import cc.vv.party.beans.model.OrgAdmin;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface OrgAdminService : BaseService<OrgAdmin>
