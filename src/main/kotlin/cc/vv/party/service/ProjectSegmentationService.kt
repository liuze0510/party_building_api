package cc.vv.party.service;

import cc.vv.party.beans.model.ProjectSegmentation;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 项目阶段信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectSegmentationService : BaseService<ProjectSegmentation>
