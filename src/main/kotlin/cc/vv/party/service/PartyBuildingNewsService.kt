package cc.vv.party.service;

import cc.vv.party.beans.model.PartyBuildingNews
import cc.vv.party.beans.vo.PartyBuildingNewsVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper

/**
 * <p>
 * 党建要闻 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyBuildingNewsService : BaseService<PartyBuildingNews> {

    fun list(
        type: Int,
        page: Int,
        pageSize: Int,
        title: String?,
        publishTime: String?,
        street: String?,
        community: String?,
        grid: String?
    ): PageWrapper<PartyBuildingNewsVO>
}
