package cc.vv.party.service;

import cc.vv.party.beans.model.Wish;
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.beans.vo.WishVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.WishListParam
import cc.vv.party.param.WishSaveParam

/**
 * <p>
 * 微心愿 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface WishService : BaseService<Wish> {

    fun save(param: WishSaveParam, user: UserVO, userId: String): Boolean

    fun checkWish(id: String, state: Int, reason: String?): Boolean

    fun deleteWish(id: String): Boolean

    fun info(id: String): WishVO

    fun claimListPage(mobile: String, userId: String, size: Int, page: Int): PageWrapper<WishVO>

    fun listPage(param: WishListParam, size: Int, page: Int): PageWrapper<WishVO>

}
