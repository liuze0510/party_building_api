package cc.vv.party.service;

import cc.vv.party.beans.vo.RedPartyBuildingVO
import cc.vv.party.model.RedPartyBuilding;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.RedPartyBuildingEditParam

/**
 * <p>
 * 红领党建 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface RedPartyBuildingService : BaseService<RedPartyBuilding> {

    fun edit(param: RedPartyBuildingEditParam, userId: String): Boolean

    fun info(id: String): RedPartyBuildingVO

    fun listPage(title: String?, type: Integer?, size: Int, page: Int): PageWrapper<RedPartyBuildingVO>

}
