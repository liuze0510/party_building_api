package cc.vv.party.service;

import cc.vv.party.beans.model.DemandList;
import cc.vv.party.beans.vo.DemandListVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.DemandListListParam
import cc.vv.party.param.DemandListEditParam

/**
 * <p>
 * 需求清单 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface DemandListService : BaseService<DemandList> {

    fun edit(param: DemandListEditParam, user: UserVO, userId: String): Boolean

    fun info(id: String): DemandListVO

    fun appListPage(street: String?, community: String?, grid: String?, key: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<DemandListVO>

    fun listPage(param: DemandListListParam, size: Int, page: Int): PageWrapper<DemandListVO>

}
