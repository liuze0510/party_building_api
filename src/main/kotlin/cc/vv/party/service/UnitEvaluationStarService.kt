package cc.vv.party.service;

import cc.vv.party.beans.model.UnitEvaluationStar;
import cc.vv.party.beans.vo.UnitEvaluationStarVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.UnitEvaluationStarEditParam

/**
 * <p>
 * 单位评星晋阶 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UnitEvaluationStarService : BaseService<UnitEvaluationStar> {


    fun edit(param: UnitEvaluationStarEditParam, userId: String): Boolean


    fun deleteUnitEvaluationStar(id: String): Boolean


    fun info(id: String): UnitEvaluationStarVO


    fun listPage(name: String?, size: Int, page: Int): PageWrapper<UnitEvaluationStarVO>
    
}
