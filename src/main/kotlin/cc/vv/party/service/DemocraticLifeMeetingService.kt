package cc.vv.party.service;

import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.DemocraticLifeMeetingVO
import cc.vv.party.model.DemocraticLifeMeeting;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.DemocraticAndOrgLifeMeetingListParam
import cc.vv.party.param.DemocraticLifeMeetingEditParam

/**
 * <p>
 * 民主生活会 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface DemocraticLifeMeetingService : BaseService<DemocraticLifeMeeting> {

    fun edit(param: DemocraticLifeMeetingEditParam, userId: String, branchInfo: CurrentBranchInfoVO): Boolean

    fun info(id: String): DemocraticLifeMeetingVO

    fun appListPage(
        param: DemocraticAndOrgLifeMeetingListParam,
        size: Int,
        page: Int
    ): PageWrapper<DemocraticLifeMeetingVO>

    fun listPage(
        branchType: String?,
        branchId: String?,
        time: Long?,
        type: String?,
        title: String?,
        size: Int,
        page: Int,
        currentBranchInfoVO: CurrentBranchInfoVO
    ): PageWrapper<DemocraticLifeMeetingVO>

}
