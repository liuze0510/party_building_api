package cc.vv.party.service;

import cc.vv.party.beans.model.PovertyAlleviationVillage;
import cc.vv.party.beans.vo.PovertyAlleviationVillageVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.param.PovertyAlleviationVillageEditParam

/**
 * <p>
 * 扶贫行政村信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PovertyAlleviationVillageService : BaseService<PovertyAlleviationVillage> {

    fun edit(param: PovertyAlleviationVillageEditParam, userId: String): Boolean

    fun deletePovertyAlleviationVillage(id: String): Boolean

    fun info(id: String): PovertyAlleviationVillageVO

    fun getCurrentUserVillage(account: String): PovertyAlleviationVillageVO

    fun message(villageId: String): PovertyAlleviationVillageVO

    fun child(parentId: String?): List<PovertyAlleviationVillageVO>

    fun treeList(): List<PovertyAlleviationVillageVO>

}
