package cc.vv.party.service;

import cc.vv.party.beans.model.RolePermission;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 角色资源 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface RolePermissionService : BaseService<RolePermission>
