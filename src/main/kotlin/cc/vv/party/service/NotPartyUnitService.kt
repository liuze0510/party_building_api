package cc.vv.party.service;

import cc.vv.party.beans.model.NotPartyUnit;
import cc.vv.party.beans.vo.NotPartyUnitVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.NotPartyUnitEditParam

/**
 * <p>
 * 非公党建单位 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface NotPartyUnitService : BaseService<NotPartyUnit> {

    fun edit(param: NotPartyUnitEditParam, userId: String): Boolean

    fun info(id: String): NotPartyUnitVO

    fun listPage(type: Integer?, unitName: String?, size: Int, page: Int): PageWrapper<NotPartyUnitVO>
    
}
