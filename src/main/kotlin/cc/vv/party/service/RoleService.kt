package cc.vv.party.service;

import cc.vv.party.beans.model.Role;
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.param.RoleParam

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface RoleService : BaseService<Role> {

    fun addRole(param: RoleParam, currentUser: String)

    fun deleteRole(id: String)

    fun detail(id: String): RoleVO

    fun updateRole(id: String, param: RoleParam, currentUser: String)
}
