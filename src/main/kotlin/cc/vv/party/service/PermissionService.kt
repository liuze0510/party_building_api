package cc.vv.party.service;

import cc.vv.party.beans.model.Permission;
import cc.vv.party.beans.vo.PermissionVO
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 资源 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface PermissionService : BaseService<Permission> {

    fun getPermissionListByRoleId(roleId: String): List<PermissionVO>
}
