package cc.vv.party.service;

import cc.vv.party.beans.model.MessageBoard
import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.MessageBoardVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.MessageBoardListParam
import cc.vv.party.param.MessageBoardSaveParam

/**
 * <p>
 * 留言板 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface MessageBoardService : BaseService<MessageBoard> {

    fun save(param: MessageBoardSaveParam, user: UserVO, branchInfo: CurrentBranchInfoVO): Boolean

    fun info(id: String): MessageBoardVO

    fun checkMessageBoard(id: String, state: Int): Boolean

    fun pcListPage(size: Int, page: Int): PageWrapper<MessageBoardVO>

    fun appListPage(size: Int, page: Int): PageWrapper<MessageBoardVO>

    fun listPage(listParam: MessageBoardListParam, size: Int, page: Int): PageWrapper<MessageBoardVO>

}
