package cc.vv.party.service.impl;

import cc.vv.party.beans.model.CommittedComment;
import cc.vv.party.beans.vo.CommittedCommentVO
import cc.vv.party.mapper.CommittedCommentMapper;
import cc.vv.party.service.CommittedCommentService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 承诺评论信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class CommittedCommentServiceImpl : BaseServiceImpl<CommittedCommentMapper, CommittedComment>(), CommittedCommentService {

    override fun listPage(committedId: String, size: Int, page: Int): PageWrapper<CommittedCommentVO> {
        var wrapper = EntityWrapper<CommittedComment>()
        wrapper.and("committed_id = {0}", committedId)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
