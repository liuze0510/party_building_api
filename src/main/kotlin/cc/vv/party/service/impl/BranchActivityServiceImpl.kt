package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.BranchActivityVO
import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.mapper.BranchActivityMapper
import cc.vv.party.model.BranchActivity
import cc.vv.party.processor.http.RemoteService
import cc.vv.party.service.BranchActivityService
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * <p>
 * 支部活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Service
open class BranchActivityServiceImpl : BaseServiceImpl<BranchActivityMapper, BranchActivity>(), BranchActivityService {

    @Autowired
    lateinit var remoteService: RemoteService

    override fun info(id: String): BranchActivityVO {
        val lessonsDetail = this.baseMapper.selectBranchActivityDetail(id)
        val call =
            remoteService.getThreeLessonContent("${SysConsts.BRANCH_ACTIVITY_CONTENT_URL}${lessonsDetail.folder}/$id.txt")
        val response = call.execute()
        if (response.isSuccessful) {
            lessonsDetail.content = response.body()?.string()
        }
        lessonsDetail.imageList = mutableListOf()
        lessonsDetail.videoList = mutableListOf()
        lessonsDetail.imgName?.split(",")?.forEach {
            if (it.isNotEmpty()) {
                lessonsDetail.imageList!!.add("http://dja.yadj.gov.cn/pmc/files/image/branchactivitie${lessonsDetail.folder}/${lessonsDetail.id}/$it")
            }
        }

        lessonsDetail.videoName?.split(",")?.forEach {
            if (it.isNotEmpty()) {
                lessonsDetail.videoList!!.add("http://dja.yadj.gov.cn/pmc/files/video/branchactivitie${lessonsDetail.folder}/${lessonsDetail.id}/$it")
            }
        }
        return lessonsDetail
    }

    override fun listPage(
        startDate: String?,
        endDate: String?,
        type: String?,
        title: String?,
        branchId: String?,
        branchType: String?,
        page: Int,
        pageSize: Int
    ): PageWrapper<BranchActivityVO> {
        val p = Page<BranchActivityVO>(page, pageSize)
        p.records = this.baseMapper.selectBranchActivityList(
            null,
            startDate, endDate, type,
            title, branchId, branchType, p
        ) as MutableList<BranchActivityVO>
        return p.wrapper()
    }

    override fun pcListPage(page: Int, pageSize: Int): PageWrapper<BranchActivityVO> {
        val p = Page<BranchActivityVO>(page, pageSize)
        p.records = this.baseMapper.selectBranchActivityList(
            "pc", null, null, null, null, null, null, p
        ) as MutableList<BranchActivityVO>

        p.records.map {
            val call =
                remoteService.getThreeLessonContent("${SysConsts.BRANCH_ACTIVITY_CONTENT_URL}${it.folder}/${it.id}.txt")
            val response = call.execute()
            val vo = it.transfer<BranchActivityVO>()
            if (response.isSuccessful) {
                vo.content = response.body()?.string()
            }
            vo
        }
        return p.transfer()
    }
}
