package cc.vv.party.service.impl;

import cc.vv.party.beans.model.SocietyOrg;
import cc.vv.party.beans.vo.SocietyOrgVO
import cc.vv.party.mapper.SocietyOrgMapper;
import cc.vv.party.service.SocietyOrgService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.SocietyOrgEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 社会组织 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class SocietyOrgServiceImpl : BaseServiceImpl<SocietyOrgMapper, SocietyOrg>(), SocietyOrgService {

    override fun edit(param: SocietyOrgEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<SocietyOrg>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.orgName)) entity.orgName = param.orgName
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(StringUtils.isNotBlank(param.principal)) entity.principal = param.principal
            if(StringUtils.isNotBlank(param.introduction)) entity.introduction = param.introduction
            return super.updateById(entity)
        }
    }

    override fun info(id: String): SocietyOrgVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(community: String?, orgName: String?, size: Int, page: Int): PageWrapper<SocietyOrgVO> {
        var pages = Page<SocietyOrgVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, community, orgName)
        return pages.transfer()
    }

}
