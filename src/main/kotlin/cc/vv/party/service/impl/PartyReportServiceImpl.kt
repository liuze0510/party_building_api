package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.PartyReportVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.PartyReportMapper
import cc.vv.party.param.PartyReportEditParam
import cc.vv.party.param.PartyReportListParam
import cc.vv.party.service.PartyReportService
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 党员报道信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PartyReportServiceImpl : BaseServiceImpl<PartyReportMapper, PartyReport>(), PartyReportService {

    override fun edit(param: PartyReportEditParam, user: UserVO, userId: String): Boolean {
        if (StringUtils.isBlank(param.id)) {
            var entity = param.transfer<PartyReport>()
            entity.street = user.street
            entity.community = user.community
            entity.grid = user.grid
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.name)) entity.name = param.name
            if (StringUtils.isNotBlank(param.street)) entity.street = param.street
            if (StringUtils.isNotBlank(param.community)) entity.community = param.community
            if (StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if (StringUtils.isNotBlank(param.professionalCharacteristics)) entity.professionalCharacteristics =
                    param.professionalCharacteristics
            if (StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if (param.workTime != null) entity.workTime = Date(param.workTime!!)
            if (StringUtils.isNotBlank(param.specialHobby)) entity.specialHobby = param.specialHobby
            if (StringUtils.isNotBlank(param.personalCommitment)) entity.personalCommitment = param.personalCommitment
            if (param.volunteer != null) entity.volunteer = param.volunteer
            return super.updateById(entity)
        }
    }

    override fun info(id: String): PartyReportVO {
//        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return baseMapper.selectInfo(id)
    }

    override fun listPage(param: PartyReportListParam, size: Int, page: Int): PageWrapper<PartyReportVO> {
//        var wrapper = EntityWrapper<PartyReport>()
//        if(StringUtils.isNotBlank(param.name)) wrapper.and("name = {0}", param.name)
//        if(StringUtils.isNotBlank(param.mobile)) wrapper.and("mobile = {0}", param.mobile)
//        if(StringUtils.isNotBlank(param.street)) wrapper.and("street = {0}", param.street)
//        if(StringUtils.isNotBlank(param.community)) wrapper.and("community = {0}", param.community)
//        if(StringUtils.isNotBlank(param.grid)) wrapper.and("grid = {0}", param.grid)
//        var pages = super.selectPage(Page(page, size), wrapper)
        var pages = Page<PartyReportVO>(page, size)
        pages.records = baseMapper.selectPageList(pages, param)
        return pages.wrapper()
    }

    override fun selectReportUserList(param: PartyReportListParam, size: Int, page: Int): PageWrapper<PartyReportVO> {
        var pages = Page<PartyReportVO>(page, size)
        pages.records = baseMapper.selectReportUserPageList(pages, param)
        return pages.wrapper()
    }
}
