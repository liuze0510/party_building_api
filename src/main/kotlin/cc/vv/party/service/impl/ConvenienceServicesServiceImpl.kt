package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ConvenienceServices
import cc.vv.party.beans.vo.ConvenienceServiceVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ConvenienceServicesMapper
import cc.vv.party.param.ConvenienceServiceListParam
import cc.vv.party.param.ConvenienceServiceEditParam
import cc.vv.party.service.ConvenienceServicesService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 便民服务 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ConvenienceServicesServiceImpl : BaseServiceImpl<ConvenienceServicesMapper, ConvenienceServices>(), ConvenienceServicesService {

    override fun edit(param: ConvenienceServiceEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<ConvenienceServices>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if(StringUtils.isNotBlank(param.type)) entity.type = param.type
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            entity.editTime = Date(System.currentTimeMillis())
            return super.updateById(entity)
        }
    }

    override fun deleteConvenienceService(id: String): Boolean {
        return super.deleteById(id)
    }

    override fun info(id: String): ConvenienceServiceVO {
        return baseMapper.getById(id)
    }

    override fun appListPage(street: String?, community: String?, grid: String?, type: String?, title: String?, size: Int, page: Int): PageWrapper<ConvenienceServiceVO> {
        var pages = Page<ConvenienceServiceVO>(page, size)
        pages.records = baseMapper.appListPage(pages, street, community, grid, type, title)
        return pages.transfer()
    }

    override fun listPage(param: ConvenienceServiceListParam, size: Int, page: Int): PageWrapper<ConvenienceServiceVO> {
        var wrapper = EntityWrapper<ConvenienceServices>()
        if(StringUtils.isNotBlank(param.street)) wrapper.and("street = {0}", param.street)
        if(StringUtils.isNotBlank(param.community)) wrapper.and("community = {0}", param.community)
        if(StringUtils.isNotBlank(param.grid)) wrapper.and("grid = {0}", param.grid)
        if(StringUtils.isNotBlank(param.type)) wrapper.and("type = {0}", param.type)
        if(StringUtils.isNotBlank(param.title)) wrapper.and("title = {0}", param.title)
        if(param.editTime != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(edit_time, '%Y-%m-%d')) = {0}", param.editTime!!/1000)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
