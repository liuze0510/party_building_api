package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PartyCommitteeAdminRelation;
import cc.vv.party.mapper.PartyCommitteeAdminRelationMapper;
import cc.vv.party.service.PartyCommitteeAdminRelationService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PartyCommitteeAdminRelationServiceImpl : BaseServiceImpl<PartyCommitteeAdminRelationMapper, PartyCommitteeAdminRelation>(), PartyCommitteeAdminRelationService {

}
