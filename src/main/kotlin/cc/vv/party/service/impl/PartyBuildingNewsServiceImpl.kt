package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PartyBuildingNews
import cc.vv.party.beans.vo.PartyBuildingNewsVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.mapper.PartyBuildingNewsMapper
import cc.vv.party.service.PartyBuildingNewsService
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.plugins.Page
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 党建要闻 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PartyBuildingNewsServiceImpl : BaseServiceImpl<PartyBuildingNewsMapper, PartyBuildingNews>(),
    PartyBuildingNewsService {

    override fun list(
        type: Int, page: Int, pageSize: Int, title: String?,
        time: String?,
        street: String?,
        community: String?,
        grid: String?
    ): PageWrapper<PartyBuildingNewsVO> {
        val p = Page<Any>(page, pageSize)
        val publishTime = if(StringUtils.isNotBlank(time)){DateUtil.format(Date(time!!.toLong()), DatePattern.NORM_DATE_FORMAT)} else {null}
        p.records = this.baseMapper.findListByType(p, type, street, community, grid, title, publishTime)
        return p.wrapper()
    }

}
