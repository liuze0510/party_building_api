package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.LifeMeetingTypeVO
import cc.vv.party.model.LifeMeetingType;
import cc.vv.party.mapper.LifeMeetingTypeMapper;
import cc.vv.party.service.LifeMeetingTypeService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民主、组织生活会类型 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@Service
open class LifeMeetingTypeServiceImpl : BaseServiceImpl<LifeMeetingTypeMapper, LifeMeetingType>(), LifeMeetingTypeService {


    override fun edit(id: String?, type: Int, name: String, userId: String): Boolean {
        if(StringUtils.isBlank(id)){
            var entity = LifeMeetingType()
            entity.name = name
            entity.type = Integer(type)
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            entity.type = Integer(type)
            if(StringUtils.isNotBlank(name)) entity.name = name
            return super.updateById(entity)
        }
    }

    override fun info(id: String): LifeMeetingTypeVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(type: Int, size: Int, page: Int): PageWrapper<LifeMeetingTypeVO> {
        var pages = super.selectPage(Page(page, size), EntityWrapper<LifeMeetingType>().where("type = {0}", type))
        return pages.wrapper()
    }

    override fun listAll(type: Int): List<LifeMeetingTypeVO> {
        return super.selectList(EntityWrapper<LifeMeetingType>().where("type = {0}", type)).transferEntries<LifeMeetingTypeVO>() as? ArrayList ?: emptyList()
    }

}
