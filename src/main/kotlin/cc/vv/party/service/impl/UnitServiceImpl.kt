package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ResourceList
import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.beans.vo.UnitVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.uuid
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ResourceListMapper
import cc.vv.party.mapper.UnitMapper
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UnitEditParam
import cc.vv.party.param.UnitListParam
import cc.vv.party.service.UnitService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * <p>
 * 单位信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class UnitServiceImpl : BaseServiceImpl<UnitMapper, Unit>(), UnitService {

    @Autowired
    lateinit var resourceListMapper: ResourceListMapper

    @Transactional
    override fun edit(param: UnitEditParam, user: UserVO, userId: String): Boolean {
        var entity: Unit?
        if (StringUtils.isBlank(param.id)) {
            val unit = super.selectOne(EntityWrapper<Unit>().eq("unit_id", param.unitId))
            if (unit != null) {
                throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "${unit.unitName}已经报道")
            }
            entity = param.transfer<Unit>()
            entity.id = uuid()
            entity.reported = Integer(0)
            entity.street = param.street
            entity.community = param.community
            entity.reportTime = Date(param.reportTime!!)
            entity.createUser = userId
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.unitName)) entity.unitName = param.unitName
            if (StringUtils.isNotBlank(param.principal)) entity.principal = param.principal
            if (StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if (param.partyNum != null) entity.partyNum = param.partyNum
            if (StringUtils.isNotBlank(param.unitAddr)) entity.unitAddr = param.unitAddr
            if (StringUtils.isNotBlank(param.zipCode)) entity.zipCode = param.zipCode
            if (param.reportTime != null) {
                entity.reportTime = Date(param.reportTime!!)
                entity.street = param.street
                entity.community = param.community
            }

            //删除旧的单位项目信息
            resourceListMapper.delete(EntityWrapper<ResourceList>().where("unit_id = {0}", entity.unitId))

            //更新单位信息
            super.updateById(entity)
        }

        if (CollectionUtils.isNotEmpty(param.resourceList)) {
            for (temp in param.resourceList!!) {
                var resourceList = temp.transfer<ResourceList>()
                resourceList.id = uuid()
                resourceList.unitId = entity.unitId
                resourceList.createUser = userId
                resourceListMapper.insert(resourceList)
            }
        }
        return true
    }

    override fun appInfo(unitId: String): UnitVO {
        var unit = super.selectOne(EntityWrapper<Unit>().where("unit_id = {0}", unitId)) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "单位信息不存在")

        var result = unit.transfer<UnitVO>()

        var resourceList = resourceListMapper.selectList(EntityWrapper<ResourceList>().where("unit_id = {0}", unit.id))
        if (CollectionUtils.isNotEmpty(resourceList)) {
            result.resourceList = resourceList.transferEntries<ResourceListVO>() as ArrayList<ResourceListVO>
        }
        return result
    }

    override fun managerInfo(id: String): UnitVO {
        var unit = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "单位信息不存在")

        var result = unit.transfer<UnitVO>()

        var resourceList = resourceListMapper.selectList(EntityWrapper<ResourceList>().where("unit_id = {0}", unit.unitId).orderBy("create_time", false))
        if (CollectionUtils.isNotEmpty(resourceList)) {
            result.resourceList = resourceList.transferEntries<ResourceListVO>() as ArrayList<ResourceListVO>
        }
        return result
    }

    override fun appListPage(type: Int, size: Int, page: Int): PageWrapper<UnitVO> {
        var wrapper = EntityWrapper<Unit>()
        if (type == 0) {
            wrapper.where("street is null").and("community is null").and("report_time is null")
        } else {
            wrapper.where("street is not null").and("community is not null").and("report_time is not null")
        }
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

    override fun listPage(param: UnitListParam, size: Int, page: Int): PageWrapper<UnitVO> {
        var pages = Page<UnitVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, param)
        return pages.transfer()
    }

    override fun reportStatistics(param: ReportStatisticsParam): List<ReportStatisticsVO> {
        return baseMapper.selectReportStatistics(param)
    }

}
