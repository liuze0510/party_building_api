package cc.vv.party.service.impl;

import cc.vv.party.beans.model.CommunityActivityType;
import cc.vv.party.beans.vo.CommunityActivityTypeVO
import cc.vv.party.mapper.CommunityActivityTypeMapper;
import cc.vv.party.service.CommunityActivityTypeService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.CommunityActivityTypeEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 社区活动类型 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class CommunityActivityTypeServiceImpl : BaseServiceImpl<CommunityActivityTypeMapper, CommunityActivityType>(), CommunityActivityTypeService {

    override fun edit(param: CommunityActivityTypeEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<CommunityActivityType>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.typeName)) entity.typeName = param.typeName
            if(StringUtils.isNotBlank(param.typeDesc)) entity.typeDesc = param.typeDesc
            return super.updateById(entity)
        }
    }

    override fun info(id: String): CommunityActivityTypeVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(typeName: String?, size: Int, page: Int): PageWrapper<CommunityActivityTypeVO> {
        var wrapper = EntityWrapper<CommunityActivityType>()
        if(StringUtils.isNotBlank(typeName)) wrapper.and("type_name = {0}", typeName)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
