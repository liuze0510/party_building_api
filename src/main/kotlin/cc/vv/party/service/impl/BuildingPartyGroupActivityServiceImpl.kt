package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.BuildingPartyGroupActivityVO
import cc.vv.party.model.BuildingPartyGroupActivity;
import cc.vv.party.mapper.BuildingPartyGroupActivityMapper;
import cc.vv.party.service.BuildingPartyGroupActivityService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.BuildingPartyGroupActivityEditParam
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 楼栋党小组活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Service
open class BuildingPartyGroupActivityServiceImpl : BaseServiceImpl<BuildingPartyGroupActivityMapper, BuildingPartyGroupActivity>(), BuildingPartyGroupActivityService {

    override fun edit(param: BuildingPartyGroupActivityEditParam, userId: String): Boolean {
        var entity: BuildingPartyGroupActivity
        if(StringUtils.isBlank(param.id)){
            entity = param.transfer()
            entity.createUser = userId
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if(StringUtils.isNotBlank(param.groupId)) entity.groupId = param.groupId
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.organizer)) entity.organizer = param.organizer
            if(StringUtils.isNotBlank(param.activityLocation)) entity.activityLocation = param.activityLocation
            if(param.activityTime != null) entity.activityTime = Date(param.activityTime!!)
            if(StringUtils.isNotBlank(param.activityDesc)) entity.activityDesc = param.activityDesc
            super.updateById(entity)
        }
        return true
    }

    override fun info(id: String): BuildingPartyGroupActivityVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(street: String?, community: String?, grid: String?, groupId: String?, title: String?, size: Int, page: Int): PageWrapper<BuildingPartyGroupActivityVO> {
        var pages = Page<BuildingPartyGroupActivityVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, street, community, grid, groupId, title)
        return pages.transfer()
    }

}
