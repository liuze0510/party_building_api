package cc.vv.party.service.impl;

import cc.vv.party.beans.model.OrgAdmin;
import cc.vv.party.mapper.BtOrgAdminMapper;
import cc.vv.party.service.OrgAdminService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class OrgAdminServiceImpl : BaseServiceImpl<BtOrgAdminMapper, OrgAdmin>(), OrgAdminService {

}
