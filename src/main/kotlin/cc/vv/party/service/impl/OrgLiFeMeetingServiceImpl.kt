package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Image
import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.OrgLiFeMeetingVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.ext.uuid
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ImageMapper
import cc.vv.party.mapper.OrgLiFeMeetingMapper
import cc.vv.party.model.OrgLiFeMeeting
import cc.vv.party.param.DemocraticAndOrgLifeMeetingListParam
import cc.vv.party.param.OrgLiFeMeetingEditParam
import cc.vv.party.service.OrgLiFeMeetingService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.streams.toList

/**
 * <p>
 * 组织生活会 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Service
open class OrgLiFeMeetingServiceImpl : BaseServiceImpl<OrgLiFeMeetingMapper, OrgLiFeMeeting>(), OrgLiFeMeetingService {

    @Autowired
    lateinit var imageMapper: ImageMapper

    override fun edit(param: OrgLiFeMeetingEditParam, userId: String, branchInfo: CurrentBranchInfoVO): Boolean {
        var entity: OrgLiFeMeeting
        if (StringUtils.isBlank(param.id)) {
            entity = param.transfer()
            entity.id = uuid()
            entity.createUser = userId
            entity.partyWorkCommitteeId = branchInfo.partyWorkCommitteeId
            entity.partyTotalBranchId = branchInfo.partyTotalBranchId
            entity.partyCommitteeId = branchInfo.partyCommitteeId
            entity.partyBranchId = branchInfo.partyBranchId
            entity.partyWorkCommitteeName = branchInfo.partyWorkCommitteeName
            entity.partyTotalBranchName = branchInfo.partyTotalBranchName
            entity.partyCommitteeName = branchInfo.partyCommitteeName
            entity.partyBranchName = branchInfo.partyBranchName
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.type)) entity.type = param.type
            if (StringUtils.isNotBlank(param.title)) entity.title = param.title
            if (StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if (param.startTime != null) entity.startTime = Date(param.startTime!!)
            if (StringUtils.isNotBlank(param.place)) entity.place = param.place
            if (StringUtils.isNotBlank(param.video)) entity.video = param.video
            if (param.content != null) entity.content = param.content
            if (param.absentee != null) entity.absentee = param.absentee
            if (param.numberPeople != null) entity.numberPeople = param.numberPeople
            if (param.actualNumber != null) entity.actualNumber = param.actualNumber
            super.updateById(entity)

            imageMapper.delete(EntityWrapper<Image>().where("associa_id = {0}", entity.id))
        }

        //保存附件图片信息
        if (CollectionUtils.isNotEmpty(param.imgList)) {
            for (temp in param.imgList!!) {
                var image = Image()
                image.id = uuid()
                image.associaId = entity.id
                image.type = Integer(6)
                image.url = temp
                imageMapper.insert(image)
            }
        }

        return true
    }

    override fun info(id: String): OrgLiFeMeetingVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        var result = entity.transfer<OrgLiFeMeetingVO>()

        var imageList = imageMapper.selectList(EntityWrapper<Image>().where("associa_id = {0}", entity.id))
        if (CollectionUtils.isNotEmpty(imageList)) {
            result.imgList = imageList.stream().filter { (it.url != null && it.url!!.isNotEmpty()) }.map { it.url ?: "" }.toList()
        }
        return result
    }

    override fun appListPage(
        param: DemocraticAndOrgLifeMeetingListParam,
        size: Int,
        page: Int
    ): PageWrapper<OrgLiFeMeetingVO> {
        var wrapper = EntityWrapper<OrgLiFeMeeting>()
        if (StringUtils.isNotBlank(param.partyCommitteeId)) wrapper.and(
            "party_committee_id = {0}",
            param.partyCommitteeId
        )
        if (StringUtils.isNotBlank(param.partyWorkCommitteeId)) wrapper.and(
            "party_work_committee_id = {0}",
            param.partyWorkCommitteeId
        )
        if (StringUtils.isNotBlank(param.partyTotalBranchId)) wrapper.and(
            "party_total_branch_id = {0}",
            param.partyTotalBranchId
        )
        if (StringUtils.isNotBlank(param.partyBranchId)) wrapper.and("party_branch_id = {0}", param.partyBranchId)
        if (param.startTime != null) wrapper.and(
            "UNIX_TIMESTAMP(DATE_FORMAT(start_time, '%Y-%m-%d')) >= {0}",
            param!!.startTime!! / 1000
        )
        if (param.endTime != null) wrapper.and(
            "UNIX_TIMESTAMP(DATE_FORMAT(start_time, '%Y-%m-%d')) <= {0}",
            param!!.endTime!! / 1000
        )
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

    override fun listPage(
        branchType: String?,
        branchId: String?,
        time: Long?,
        type: String?,
        title: String?,
        size: Int,
        page: Int,
        currentBranchInfoVO: CurrentBranchInfoVO
    ): PageWrapper<OrgLiFeMeetingVO> {
        val wrapper = EntityWrapper<OrgLiFeMeeting>()
        if (time != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(start_time, '%Y-%m-%d')) = {0}", time / 1000)
        if (StringUtils.isNotBlank(type)) wrapper.and("type = {0}", type)
        if (StringUtils.isNotBlank(title)) wrapper.like("title", title)
        val pair = getBranchTypeAndId(currentBranchInfoVO)
        val currentBranchType = if (StringUtils.isNotBlank(branchType)) {
            branchType
        } else {
            pair?.first
        }
        val currentBranchId = if (StringUtils.isNotBlank(branchId)) {
            branchId
        } else {
            pair?.second
        }
        if (StringUtils.isNotBlank(currentBranchId) && StringUtils.isNotBlank(currentBranchType)) {
            when (currentBranchType) {
                SysConsts.BranchType.PARTY_BRANCH -> {
                    wrapper.eq("party_branch_id", currentBranchId)
                }
                SysConsts.BranchType.PARTY_TOTAL_BRANCH -> {
                    wrapper.eq("party_total_branch_id", currentBranchId)
                }
                SysConsts.BranchType.PARTY_WORK_COMMITTEE -> {
                    wrapper.eq("party_work_committee_id", currentBranchId)
                }
                SysConsts.BranchType.PARTY_COMMITTEE -> {
                    wrapper.eq("party_committee_id", currentBranchId)
                }
            }
        }
        val pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

    ////partyBranch.partyTotalBranch.partyCommittee.partyWorkCommit.id
    private fun getBranchTypeAndId(currentBranchInfo: CurrentBranchInfoVO): Pair<String, String>? {
        return when {
            StringUtils.isNotBlank(currentBranchInfo.partyBranchId) -> Pair(
                SysConsts.BranchType.PARTY_BRANCH,
                currentBranchInfo.partyBranchId!!
            )
            StringUtils.isNotBlank(currentBranchInfo.partyTotalBranchId) -> Pair(
                SysConsts.BranchType.PARTY_TOTAL_BRANCH,
                currentBranchInfo.partyTotalBranchId!!
            )
            StringUtils.isNotBlank(currentBranchInfo.partyCommitteeId) -> Pair(
                SysConsts.BranchType.PARTY_COMMITTEE,
                currentBranchInfo.partyCommitteeId!!
            )
            StringUtils.isNotBlank(currentBranchInfo.partyWorkCommitteeId) -> Pair(
                SysConsts.BranchType.PARTY_WORK_COMMITTEE,
                currentBranchInfo.partyWorkCommitteeId!!
            )
            else -> Pair(SysConsts.BranchType.REGION_LEVEL_DISTRICT, SysConsts.REGION_ID)
        }
    }


}
