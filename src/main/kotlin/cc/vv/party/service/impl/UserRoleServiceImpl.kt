package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Permission
import cc.vv.party.beans.model.UserRole;
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.mapper.UserRoleMapper;
import cc.vv.party.service.UserRoleService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@Service
open class UserRoleServiceImpl : BaseServiceImpl<UserRoleMapper, UserRole>(), UserRoleService {

    override fun findUserPermission(userAccount: String): List<Permission> {
        return baseMapper.findUserPermission(userAccount)
    }

    override fun findUserRole(userAccount: String): List<RoleVO>? {
        return baseMapper.findUserRole(userAccount)
    }
}
