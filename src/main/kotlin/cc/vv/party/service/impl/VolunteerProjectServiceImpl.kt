package cc.vv.party.service.impl;

import cc.vv.party.beans.model.VolunteerProject
import cc.vv.party.beans.model.VolunteerProjectSignUp
import cc.vv.party.beans.vo.VolunteerProjectSignUpVO
import cc.vv.party.beans.vo.VolunteerProjectVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.VolunteerProjectMapper
import cc.vv.party.mapper.VolunteerProjectSignUpMapper
import cc.vv.party.param.VolunteerProjectEditParam
import cc.vv.party.service.VolunteerProjectService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

/**
 * <p>
 * 志愿项目信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class VolunteerProjectServiceImpl : BaseServiceImpl<VolunteerProjectMapper, VolunteerProject>(), VolunteerProjectService {

    @Autowired
    lateinit var volunteerProjectSignUpMapper: VolunteerProjectSignUpMapper

    override fun edit(param: VolunteerProjectEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<VolunteerProject>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.projectName)) entity.projectName = param.projectName
            if(StringUtils.isNotBlank(param.holdingCompany)) entity.holdingCompany = param.holdingCompany
            if(param.holdingTime != null) entity.holdingTime = Date(param.holdingTime!!)
            if(param.projectQuota != null) entity.projectQuota = param.projectQuota
            if(StringUtils.isNotBlank(param.venue)) entity.venue = param.venue
            if(StringUtils.isNotBlank(param.projectIntroduction)) entity.projectIntroduction = param.projectIntroduction
            return super.updateById(entity)
        }
    }

    override fun appInfo(id: String): VolunteerProjectVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        var result = entity.transfer<VolunteerProjectVO>()
        var signUpList = volunteerProjectSignUpMapper.selectList(EntityWrapper<VolunteerProjectSignUp>().where("volunteer_project_id = {0}", id))
        if(CollectionUtils.isNotEmpty(signUpList)){
            result.volunteerProjectSignUpList = signUpList.transferEntries<VolunteerProjectSignUpVO>() as ArrayList
        }
        return result
    }

    override fun info(id: String): VolunteerProjectVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun userVolunteerProjectList(userId: String): List<VolunteerProjectVO> {
        return baseMapper.selectUserVolunteerProjectList(userId)
    }

    override fun appListPage(projectName: String?, size: Int, page: Int): PageWrapper<VolunteerProjectVO> {
        var wrapper = EntityWrapper<VolunteerProject>()
        if(StringUtils.isNotBlank(projectName)) wrapper.like("project_name", projectName)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

    override fun listPage(holdingTime: Long?, holdingCompany: String?, projectName: String?, size: Int, page: Int): PageWrapper<VolunteerProjectVO> {
        var wrapper = EntityWrapper<VolunteerProject>()
        if(holdingTime != null) wrapper.and("holding_time = {0}", holdingTime)
        if(StringUtils.isNotBlank(holdingCompany)) wrapper.like("holding_company", holdingCompany)
        if(StringUtils.isNotBlank(projectName)) wrapper.like("project_name", projectName)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
