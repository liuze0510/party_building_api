package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ProjectSegmentation;
import cc.vv.party.mapper.ProjectSegmentationMapper;
import cc.vv.party.service.ProjectSegmentationService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目阶段信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ProjectSegmentationServiceImpl : BaseServiceImpl<ProjectSegmentationMapper, ProjectSegmentation>(), ProjectSegmentationService {

}
