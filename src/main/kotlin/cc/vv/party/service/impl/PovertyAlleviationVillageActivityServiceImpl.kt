package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.PovertyAlleviationVillageActivityVO
import cc.vv.party.model.PovertyAlleviationVillageActivity;
import cc.vv.party.mapper.PovertyAlleviationVillageActivityMapper;
import cc.vv.party.service.PovertyAlleviationVillageActivityService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.PovertyAlleviationVillageActivityEditParam
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 脱贫行政村活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Service
open class PovertyAlleviationVillageActivityServiceImpl : BaseServiceImpl<PovertyAlleviationVillageActivityMapper, PovertyAlleviationVillageActivity>(), PovertyAlleviationVillageActivityService {

    override fun edit(param: PovertyAlleviationVillageActivityEditParam, userId: String): Boolean {
        var entity: PovertyAlleviationVillageActivity
        if(StringUtils.isBlank(param.id)){
            entity = param.transfer()
            entity.createUser = userId
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.area)) entity.area = param.area
            if(StringUtils.isNotBlank(param.town)) entity.town = param.town
            if(StringUtils.isNotBlank(param.village)) entity.village = param.village
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.organizer)) entity.organizer = param.organizer
            if(StringUtils.isNotBlank(param.activityLocation)) entity.activityLocation = param.activityLocation
            if(param.activityTime != null) entity.activityTime = Date(param.activityTime!!)
            if(StringUtils.isNotBlank(param.activityDesc)) entity.activityDesc = param.activityDesc
            super.updateById(entity)
        }
        return true
    }

    override fun info(id: String): PovertyAlleviationVillageActivityVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(town: String?, village: String?, title: String?, size: Int, page: Int): PageWrapper<PovertyAlleviationVillageActivityVO> {
        var pages = Page<PovertyAlleviationVillageActivityVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, town, village, title)
        return pages.transfer()
    }

}
