package cc.vv.party.service.impl

import cc.vv.party.beans.model.Org
import cc.vv.party.beans.model.Role
import cc.vv.party.beans.model.RolePermission
import cc.vv.party.beans.model.User
import cc.vv.party.beans.vo.*
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.AdminInfo
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.UserMapper
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UserEditParam
import cc.vv.party.param.UserListParam
import cc.vv.party.param.UserStatisticsParam
import cc.vv.party.processor.http.RemoteService
import cc.vv.party.service.*
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONPath
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.isNotNullOrEmpty
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import commonx.core.date.calculateAge
import commonx.core.json.containsJsonPath
import commonx.core.json.jsonPath
import commonx.core.json.toJSONObject
import org.apache.commons.lang.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.LinkedHashMap

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
@DS("db_local")
class UserServiceImpl : BaseServiceImpl<UserMapper, User>(), UserService {

    @Autowired
    lateinit var remoteService: RemoteService

    @Autowired
    lateinit var userRoleService: UserRoleService

    @Autowired
    lateinit var orgService: OrgService

    @Autowired
    lateinit var rolePermissionService: RolePermissionService

    @Autowired
    lateinit var roleService: RoleService

    @Autowired
    lateinit var pmcPersonService: PmcPersonService

    @Autowired
    lateinit var povertyAlleviationVillageService: PovertyAlleviationVillageService

    private val logger by lazy { LoggerFactory.getLogger(UserServiceImpl::class.java) }

    override fun login(account: String, password: String): MutableMap<String, Any?> {
        if (account == "admin") {
            val admin = AdminInfo.login(account, password)
            val result = mutableMapOf<String, Any?>()
            result["userInfo"] = admin
            result["userId"] = admin.id.toString()
            val roleList =
                userRoleService.findUserRole(admin.name.toString()) ?: throw BizException(StatusCode.FORBIDDEN)
            result["userRole"] = roleList
            result["userName"] = admin.name.toString()
            result["userAccount"] = admin.name.toString()
            result["permissionList"] = userRoleService.findUserPermission(admin.name.toString()).map { it.id }.toList()
            val findOrgByUserAccount = orgService.findOrgByUserAccount(account)
            findOrgByUserAccount.forEach {
                it.introduction = null
            }
            result["adminOrg"] = findOrgByUserAccount
            result["userManageOrg"] = fillUserManageOrgInfo(roleList, findOrgByUserAccount)
            return result
        } else {
            val call = remoteService.login(account, password)
            val response = call.execute()
            if (response.isSuccessful) {
                val result = response.body() ?: throw BizException(StatusCode.LOGIN_PASSWORD_ERROR)
                if (result["level"] != null && result["level"].toString().equals("E", false)) throw BizException(
                    StatusCode.LOGIN_PASSWORD_ERROR.statusCode,
                    result["msgContent"].toString()
                )
                //判断当前账号是否在延安宝塔区
//                val regionId = result.jsonPath<String>("$.curPartyMember.countyRegion.id")
//                if (regionId != SysConsts.REGION_ID) {
//                    throw BizException(StatusCode.FORBIDDEN.statusCode, "只能宝塔区账号登录")
//                }
                val userId = result.jsonPath<String>("$.curPartyMember.id")
                val roleList = userRoleService.findUserRole(account) ?: throw BizException(StatusCode.FORBIDDEN)
                if (roleList.size == 1 && (roleList.first().type == RoleType.PARTY_MEMBER)) {
                    throw BizException(StatusCode.FORBIDDEN)
                }
                var permissionList = userRoleService.findUserPermission(account).map { it.id }.toList()
                val yananRole = result.jsonPath<String>("$.curRoleId")
                result["yananRole"] = yananRole
                //不是延安基础党员的，都认为是支部管理员.如果逻辑有问题，就判断  yananRole == "RPOME"
                if (yananRole != "RMN") {
                    val branchRole = roleService.selectOne(EntityWrapper<Role>().eq("type", RoleType.BRANCH_ADMIN))
                    val list =
                        rolePermissionService.selectList(EntityWrapper<RolePermission>().eq("role_id", branchRole.id))
                    permissionList = permissionList.toMutableList()
                    permissionList.addAll(list.map { it.permissionId }.toList())
                }
                if (permissionList.isEmpty()) {
                    throw BizException(StatusCode.FORBIDDEN.statusCode, "无权限访问，请联系管理员")
                }
                result["permissionList"] = permissionList.distinct()
                result["userId"] = userId
                result["userRole"] = roleList
                result["userName"] = result.jsonPath<String>("$.curPartyMember.name")
                result["userAccount"] = account
                val findOrgByUserAccount = orgService.findOrgByUserAccount(account)
                findOrgByUserAccount.forEach {
                    it.introduction = null
                }
                println(result.toString())
                result["adminOrg"] = findOrgByUserAccount
                result["userManageOrg"] = fillUserManageOrgInfo(roleList, findOrgByUserAccount)
                if (result.containsJsonPath("$.curPartyMember.partyBranch")) {
                    val parseObject = JSON.toJSONString(result)
                    val withoutNullResult = parseObject.toJSONObject<MutableMap<String, Any?>>()
                    val partyCommitteeId: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyCommittee[0].id")
                    val partyCommitteeName: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyCommittee[0].name")
                    val partyWorkCommitteeId: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyWorkCommit[0].id")
                    val partyWorkCommitteeName: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyWorkCommit[0].name")
                    val partyTotalBranchId: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyTotalBranch[0].id")
                    val partyTotalBranchName: String? =
                        withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyTotalBranch[0].name")
                    val partyBranchId: String? = withoutNullResult.jsonPath("$.curPartyMember.partyBranch.id")
                    val partyBranchName: String? = withoutNullResult.jsonPath("$.curPartyMember.partyBranch.name")
                    val orgMinistryId: String? = withoutNullResult.jsonPath("$.curPartyMember.orgMinistry.id")
                    val orgMinistryName: String? = withoutNullResult.jsonPath("$.curPartyMember.orgMinistry.name")
                    result["currentBranchInfo"] = CurrentBranchInfoVO(
                        partyCommitteeId = partyCommitteeId,
                        partyCommitteeName = partyCommitteeName,
                        partyWorkCommitteeId = partyWorkCommitteeId,
                        partyWorkCommitteeName = partyWorkCommitteeName,
                        partyTotalBranchId = partyTotalBranchId,
                        partyTotalBranchName = partyTotalBranchName,
                        partyBranchId = partyBranchId,
                        partyBranchName = partyBranchName,
                        orgMinistryId = orgMinistryId,
                        orgMinistryName = orgMinistryName
                    )

                    //当前用户所管理支部(管理支部即用户所属支部)
                    if(yananRole != null && "RPOME".equals(yananRole, true)){
                        var currentBranch = HashMap<String, Map<String, String?>>()
                        var branchMap: HashMap<String, String?>

                        //党委
                        branchMap = HashMap()
                        branchMap["id"] = partyCommitteeId
                        branchMap["name"] = partyCommitteeName
                        branchMap["type"] = "PARTYCOMMITTEE"
                        currentBranch["PARTYCOMMITTEE"] = branchMap

                        //党工委
                        branchMap = HashMap()
                        branchMap["id"] = partyWorkCommitteeId
                        branchMap["name"] = partyWorkCommitteeName
                        branchMap["type"] = "PARTYWORKCOMMITTEE"
                        currentBranch["PARTYWORKCOMMITTEE"] = branchMap

                        //党总支
                        branchMap = HashMap()
                        branchMap["id"] = partyTotalBranchId
                        branchMap["name"] = partyTotalBranchName
                        branchMap["type"] = "PARTYTOTALBRANCH"
                        currentBranch["PARTYTOTALBRANCH"] = branchMap

                        //党支部
                        branchMap = HashMap()
                        branchMap["id"] = partyBranchId
                        branchMap["name"] = partyBranchName
                        branchMap["type"] = "PARTY"
                        currentBranch["PARTY"] = branchMap

                        result["currentManagerBranch"] = currentBranch
                    }

                }
                return result
            } else {
                throw BizException(StatusCode.LOGIN_PASSWORD_ERROR.statusCode, response.message())
            }
        }
    }

    override fun appUserLogin(account: String, password: String): MutableMap<String, Any?> {
        val call = remoteService.login(account, password)
        val response = call.execute()
        if (response.isSuccessful) {
            val result = response.body() ?: throw BizException(StatusCode.LOGIN_PASSWORD_ERROR)
            if (result["level"] != null && result["level"].toString().equals("E", false)) {
                throw BizException(StatusCode.ERROR.statusCode, result["msgContent"].toString())
            }
            //判断当前账号是否在延安宝塔区
//            val regionId = result.jsonPath<String>("$.curPartyMember.countyRegion.id")
//            if (regionId != SysConsts.REGION_ID) {
//                throw BizException(StatusCode.FORBIDDEN.statusCode, "只能宝塔区账号登录")
//            }
            val userId = result.jsonPath<String>("$.curPartyMember.id")
            var roleList = userRoleService.findUserRole(account) ?: listOf(RoleVO.default())
            roleList = if (roleList.isEmpty()) listOf(RoleVO.default()) else roleList
            result["userId"] = userId
            result["userRole"] = roleList
            result["userName"] = result.jsonPath<String>("$.curPartyMember.name")
            result["userAccount"] = account
            result["yananRole"] = result.jsonPath<String>("$.curRoleId")
            val userInfo = this.baseMapper.selectById(userId) ?: null
            result["partyMemberReported"] = userInfo == null || userInfo.reported == Integer(1)
            result["reportedPartyMemberInfo"] = userInfo?.transfer<UserVO>()
            if (result.containsJsonPath("$.curPartyMember.partyBranch")) {
                val parseObject = JSON.toJSONString(result)
                val withoutNullResult = parseObject.toJSONObject<MutableMap<String, Any?>>()
                val partyCommitteeId: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyCommittee[0].id")
                val partyCommitteeName: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyCommittee[0].name")
                val partyWorkCommitteeId: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyWorkCommit[0].id")
                val partyWorkCommitteeName: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyWorkCommit[0].name")
                val partyTotalBranchId: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyTotalBranch[0].id")
                val partyTotalBranchName: String? =
                    withoutNullResult.jsonPath("$.curPartyMember.partyBranch..partyTotalBranch[0].name")
                val partyBranchId: String? = withoutNullResult.jsonPath("$.curPartyMember.partyBranch.id")
                val partyBranchName: String? = withoutNullResult.jsonPath("$.curPartyMember.partyBranch.name")
                val orgMinistryId: String? = withoutNullResult.jsonPath("$.curPartyMember.orgMinistry.id")
                val orgMinistryName: String? = withoutNullResult.jsonPath("$.curPartyMember.orgMinistry.name")
                result["currentBranchInfo"] = CurrentBranchInfoVO(
                    partyCommitteeId = partyCommitteeId,
                    partyCommitteeName = partyCommitteeName,
                    partyWorkCommitteeId = partyWorkCommitteeId,
                    partyWorkCommitteeName = partyWorkCommitteeName,
                    partyTotalBranchId = partyTotalBranchId,
                    partyTotalBranchName = partyTotalBranchName,
                    partyBranchId = partyBranchId,
                    partyBranchName = partyBranchName,
                    orgMinistryId = orgMinistryId,
                    orgMinistryName = orgMinistryName
                )
            }
            val orgList = mutableListOf<OrgVO>()
            for (role in roleList) {
                if (role.type != RoleType.PARTY_MEMBER) {
                    val findOrgByUserAccount = orgService.findOrgByUserAccount(account)
                    result["userManageOrg"] = fillUserManageOrgInfo(roleList, findOrgByUserAccount)
                    orgList.addAll(findOrgByUserAccount)
                    break
                }
            }
            if (userInfo != null) {
                val orgIds = "${userInfo.area},${userInfo.street},${userInfo.community},${userInfo.grid}"
                val userOrgList = orgService.selectList(EntityWrapper<Org>().`in`("id", orgIds))
                    .map {
                        it.introduction = null
                        it
                    }
                orgList.addAll(userOrgList.transferEntries())
            }
            orgList.forEach {
                it.introduction = null
            }
            result["userOrgList"] = orgList.distinct()
            return result
        } else {
            throw BizException(StatusCode.LOGIN_PASSWORD_ERROR.statusCode, response.message())
        }
    }

    private fun fillUserManageOrgInfo(roleList: List<RoleVO>, orgList: List<OrgVO>): UserManageOrgInfoVO {
        val info = UserManageOrgInfoVO()
        roleList.forEach { role ->
            when (role.type) {
                RoleType.SUPER_ADMIN -> info.areaId =
                        orgList.find { it.type == OrgType.AREA }?.id
                RoleType.AREA_ADMIN -> info.areaId =
                        orgList.find { it.type == OrgType.AREA }?.id
                RoleType.STREET_ADMIN -> {
                    info.areaId = orgList.find { it.type == OrgType.AREA }?.id
                    info.streetId = orgList.find { it.type == OrgType.STREET }?.id
                }
                RoleType.SECTION_ADMIN -> {
                    info.areaId = orgList.find { it.type == OrgType.AREA }?.id
                    info.streetId = orgList.find { it.type == OrgType.STREET }?.id
                    info.communityId = orgList.find { it.type == OrgType.SECTION }?.id
                }
                RoleType.GRID_ADMIN -> {
                    info.areaId = orgList.find { it.type == OrgType.AREA }?.id
                    info.streetId = orgList.find { it.type == OrgType.STREET }?.id
                    info.communityId = orgList.find { it.type == OrgType.SECTION }?.id
                    info.gridId = orgList.find { it.type == OrgType.GRID }?.id
                }
                RoleType.UNIT_ADMIN -> {
                    info.areaId = orgList.find { it.type == OrgType.AREA }?.id
                    info.streetId = orgList.find { it.type == OrgType.STREET }?.id
                    info.communityId = orgList.find { it.type == OrgType.SECTION }?.id
                    info.gridId = orgList.find { it.type == OrgType.GRID }?.id
                    info.unitId = orgList.find { it.type == OrgType.UNIT }?.id
                }
            }
        }
        return info
    }

    override fun save(param: UserEditParam, userId: String): Boolean {
        var entity = param.transfer<User>()
        if (param.dateofbirth != null) {
            entity.age = Integer(Date(param.dateofbirth!!).calculateAge())
        }
        if (param.party!!.toInt() == 0) {
            var entity = userRoleService.selectById(param.id)
            if (entity != null) {
                throw BizException(StatusCode.MESSAGE_EXIST.statusCode, "党员信息已经存在，请勿重复添加")
            }
        }
        entity.createUser = userId
        return this.insert(entity)
    }

    override fun update(param: UserEditParam): Boolean {
        var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)

        if (StringUtils.isNotBlank(param.name)) entity.name = param.name
        if (StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
        if (StringUtils.isNotBlank(param.faceUrl)) entity.faceUrl = param.faceUrl
        if (StringUtils.isNotBlank(param.card)) entity.card = param.card
        if (param.sex != null) entity.sex = param.sex
        if (param.dateofbirth != null) entity.dateofbirth = Date(param.dateofbirth!!)
        if (param.ethnic != null) entity.ethnic = param.ethnic
        if (StringUtils.isNotBlank(param.career)) entity.career = param.career
        if (param.educationalLevel != null) entity.educationalLevel = param.educationalLevel
        if (param.education != null) entity.education = param.education
        if (StringUtils.isNotBlank(param.jobTitle)) entity.jobTitle = param.jobTitle
        if (param.identity != null) entity.identity = param.identity
        if (StringUtils.isNotBlank(param.workCompany)) entity.workCompany = param.workCompany
        if (StringUtils.isNotBlank(param.street)) entity.street = param.street
        if (StringUtils.isNotBlank(param.community)) entity.community = param.community
        if (StringUtils.isNotBlank(param.grid)) entity.grid = param.grid

        return super.updateById(entity)
    }

    override fun info(id: String): UserVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO> {
        var pages = Page<UserVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, param)
        return pages.transfer()
    }

    override fun careerStatistics(param: UserStatisticsParam): Map<String, Int> {
        var list = baseMapper.selectCareerStatistics(param)
        var map = LinkedHashMap<String, Int>()
        for (m in list) {
            var key = m["career"].toString()
            if (map.containsKey(key)) {
                var value = map[key] ?: 0
                map[key] = value.plus(m["num"]!!.toInt())
            } else {
                map[m["career"].toString()] = m["num"]!!.toInt()
            }
        }
        return map
    }

    override fun ethnicStatistics(param: UserStatisticsParam): Map<String, Int> {
        var list = baseMapper.selectEthnicStatistics(param)
        var map = LinkedHashMap<String, Int>()
        for (m in list) {
            var key = m["ethnic"].toString()
            if (map.containsKey(key)) {
                var value = map[key] ?: 0
                map[key] = value.plus(m["num"]!!.toInt())
            } else {
                map[m["ethnic"].toString()] = m["num"]!!.toInt()
            }
        }
        return map
    }

    override fun sexStatistics(param: UserStatisticsParam): Map<String, Int> {
        var list = baseMapper.selectSexStatistics(param)
        var map = LinkedHashMap<String, Int>()
        for (m in list) {
            var key = m["sex"].toString()
            if (map.containsKey(key)) {
                var value = map[key] ?: 0
                map[key] = value.plus(m["num"]!!.toInt())
            } else {
                map[m["sex"].toString()] = m["num"]!!.toInt()
            }
        }
        return map
    }

    override fun educationalLevelStatistics(param: UserStatisticsParam): Map<String, Int> {
        var list = baseMapper.selectEducationalLevelStatistics(param)
        var map = LinkedHashMap<String, Int>()
        for (m in list) {
            var key = m["educationalLevel"].toString()
            if (map.containsKey(key)) {
                var value = map[key] ?: 0
                map[key] = value.plus(m["num"]!!.toInt())
            } else {
                map[m["educationalLevel"].toString()] = m["num"]!!.toInt()
            }
        }
        return map
    }

    override fun ageStatistics(param: UserStatisticsParam): Map<String, Int> {
        var list = baseMapper.selectAgeStatistics(param)
        var map = LinkedHashMap<String, Int>()
        for (m in list) {
            var key = m["age"].toString()
            if (map.containsKey(key)) {
                var value = map[key] ?: 0
                map[key] = value.plus(m["num"]!!.toInt())
            } else {
                map[m["age"].toString()] = m["num"]!!.toInt()
            }
        }
        return map
    }

    override fun reportStatistics(param: ReportStatisticsParam): List<ReportStatisticsVO> {
        return baseMapper.selectReportStatistics(param)
    }

    override fun partyMemberInfo(id: String): UserVO {
        return pmcPersonService.selectPartyMemberById(id) ?: throw BizException(
            StatusCode.MESSAGE_NOT_EXIST.statusCode,
            "党员信息不存在"
        )
    }

    override fun partyMemberListPage(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO> {
        return pmcPersonService.selectPartyMemberList(param, size, page)
    }

    override fun learningStatistics(
        branchType: String, branchId: String, page: Int,
        pageSize: Int
    ): PageWrapper<LearningStatisticsVO> {
        //如果查询的是支部
        if (branchType == SysConsts.BranchType.PARTY_BRANCH) {
            val p = Page<LearningStatisticsVO>(page, pageSize)
            p.records = baseMapper.selectLearningStatistics(listOf(branchId), p)
            p.records.forEach {
                it.totalStr = durationFormat((it.total ?: "0").toLong())
                it.type = RoleType.PARTY_MEMBER.roleName
            }
            return p.wrapper()
        } else {//否则内存分页，并且便利所有支部节点
            val list = orgService.findPartyNode(branchType, branchId)
            val tempPage = if (page == 0) {
                page
            } else {
                page - 1
            }
            val pageWrapper = PageWrapper<LearningStatisticsVO>()
            pageWrapper.pages = ((list.size.minus(1)) / pageSize).plus(1)
            pageWrapper.size = pageSize
            pageWrapper.current = page.plus(1)
            pageWrapper.total = list.size.toLong()

            var records = mutableListOf<LearningStatisticsVO>()
            for (node in list) {
                val nodeList = mutableListOf<String>()
                val vo = LearningStatisticsVO()
                if (node.type == SysConsts.BranchType.PARTY_BRANCH) {
                    nodeList.add(node.id!!)
                } else {
                    val children = orgService.findPartyNode(node.type, node.id)
                    findAllPartyBranchList(children, nodeList)
                }
                vo.id = node.id
                vo.name = node.name
                vo.type = node.type
                vo.total = baseMapper.selectLearningStatistics(nodeList, Page(1, Int.MAX_VALUE))
                    .sumBy { it.total?.toInt() ?: 0 }.toString()
                vo.totalStr = durationFormat((vo.total ?: "0").toLong())
                records.add(vo)
            }
            records.sortByDescending { (it.total ?: "0").toInt() }
            //无法数据库分页，用在内存中分页。
            if (records.size > page * pageSize) {
                records = records.subList(tempPage * pageSize, page * pageSize)
            } else {
                records = records.subList(tempPage * pageSize, list.size)
            }
            pageWrapper.records = records
            return pageWrapper
        }
    }

    override fun learningStatisticsDetail(
        branchType: String,
        branchId: String,
        year: String,
        month: String?,
        startDay: String?, endDay: String?
    ): HashMap<String, Long> {
        val result = hashMapOf<String, Long>()
        if (month.isNullOrEmpty()) {//统计年
            if (branchType == RoleType.PARTY_MEMBER.roleName) {
                val list = baseMapper.selectPartyMemberLearningStatisticsYearDetail(branchId, year)
                list.forEach { key ->
                    key.keys.forEach {
                        if (result.containsKey(it)) {
                            val v = result[it] ?: 0
                            result[it] = v.plus((key[it].toString() ?: "0").toLong())
                        } else {
                            result[it] = (key[it].toString() ?: "0").toLong()
                        }
                    }
                }
            } else {
                val nodeList = mutableListOf<String>()
                if (branchType == SysConsts.BranchType.PARTY_BRANCH) {
                    nodeList.add(branchId)
                } else {
                    val children = orgService.findPartyNode(branchType, branchId)
                    findAllPartyBranchList(children, nodeList)
                }
                val list = baseMapper.selectLearningStatisticsYearDetail(nodeList, year)
                val groupBy = list.associateBy { it["PARTYMEMBERID"] }
                groupBy.values.forEach { value ->
                    value.forEach { entry ->
                        if (entry.key != "PARTYMEMBERID") {
                            if (result.containsKey(entry.key)) {
                                val v = result[entry.key] ?: 0
                                result[entry.key] = v.plus((entry.value.toString() ?: "0").toLong())
                            } else {
                                result[entry.key] = (entry.value.toString() ?: "0").toLong()
                            }
                        }
                    }
                }
            }
        } else {//统计月
            val start = startDay ?: "1"
            if (branchType == RoleType.PARTY_MEMBER.roleName) {
                val list =
                    baseMapper.selectPartyMemberLearningStatisticsDayDetail(branchId, year, month!!, start, endDay)
                list.forEach { value ->
                    if (result.containsKey(value["DAYMARKER"].toString())) {
                        val v = result[value["DAYMARKER"].toString()] ?: 0
                        result[value["DAYMARKER"].toString()] = v.plus((value["HOURS"].toString() ?: "0").toLong())
                    } else {
                        result[value["DAYMARKER"].toString()] = (value["HOURS"].toString() ?: "0").toLong()
                    }
                }
            } else {
                val nodeList = mutableListOf<String>()
                if (branchType == SysConsts.BranchType.PARTY_BRANCH) {
                    nodeList.add(branchId)
                } else {
                    val children = orgService.findPartyNode(branchType, branchId)
                    findAllPartyBranchList(children, nodeList)
                }
                val list = baseMapper.selectLearningStatisticsDayDetail(nodeList, year, month!!, start, endDay)
                val groupBy = list.associateBy { it["ID"] }
                groupBy.values.forEach { value ->
                    if (result.containsKey(value["DAYMARKER"].toString())) {
                        val v = result[value["DAYMARKER"].toString()] ?: 0
                        result[value["DAYMARKER"].toString()] = v.plus((value["HOURS"].toString() ?: "0").toLong())
                    } else {
                        result[value["DAYMARKER"].toString()] = (value["HOURS"].toString() ?: "0").toLong()
                    }
                }
            }
        }
        return result
    }

    override fun learningTotalStatistics(branchType: String, branchId: String): String {
        var totalTime: Long = 0
        //如果查询的是支部
        if (branchType == SysConsts.BranchType.PARTY_BRANCH) {
            val list = baseMapper.selectLearningStatistics(listOf(branchId), null)
            list.forEach {
                totalTime += (it.total ?: "0").toInt()
            }
        } else {//否则内存分页，并且便利所有支部节点
            val list = orgService.findPartyNode(branchType, branchId)
            for (node in list) {
                val nodeList = mutableListOf<String>()
                if (node.type == SysConsts.BranchType.PARTY_BRANCH) {
                    nodeList.add(node.id!!)
                } else {
                    val children = orgService.findPartyNode(node.type, node.id)
                    findAllPartyBranchList(children, nodeList)
                }
                if (nodeList.isNotEmpty()) {
                    totalTime += baseMapper.selectLearningStatistics(nodeList, Page(1, Int.MAX_VALUE))
                        .sumBy { it.total?.toInt() ?: 0 }
                }
            }
        }
        return durationFormat(totalTime)
    }


    private fun durationFormat(time: Long): String {
        logger.error(time.toString())
        val strHour = (time / 3600).toString()
        val strMinute = (time % 3600 / 60).toString()

        var strResult = ""
        if (strHour != "0") {
            strResult += strHour + "小时";
        }

        if (strMinute != "0") {
            strResult += strMinute + "分钟";
        }
        return strResult;
    }

    private fun findAllPartyBranchList(list: List<NodeVO>?, result: MutableList<String>) {
        if (list != null && list.isEmpty()) {
            return
        }
        for (node in list!!) {
            if (node.type == SysConsts.BranchType.PARTY_BRANCH) {
                if (node.id.isNotNullOrEmpty()) {
                    result.add(node.id!!)
                }
            } else {
                val nodeList = orgService.findPartyNode(node.type, node.id)
                findAllPartyBranchList(nodeList, result)
            }
        }
    }
}
