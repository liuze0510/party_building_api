package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.StreetJointMeetingActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.StreetJointMeetingActivityMapper
import cc.vv.party.model.StreetJointMeetingActivity
import cc.vv.party.param.StreetJointMeetingActivityEditParam
import cc.vv.party.service.StreetJointMeetingActivityService
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 街道联席会议活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
@Service
open class StreetJointMeetingActivityServiceImpl : BaseServiceImpl<StreetJointMeetingActivityMapper, StreetJointMeetingActivity>(), StreetJointMeetingActivityService {

    override fun edit(param: StreetJointMeetingActivityEditParam, user: UserVO): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<StreetJointMeetingActivity>()
            entity.createUser = user.id
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.meetingId)) entity.meetingId = param.meetingId
            if(StringUtils.isNotBlank(param.organizer)) entity.organizer = param.organizer
            if(param.activityTime != null) entity.activityTime = Date(param.activityTime!!)
            if(StringUtils.isNotBlank(param.activityLocation)) entity.activityLocation = param.activityLocation
            if(StringUtils.isNotBlank(param.activityDesc)) entity.activityDesc = param.activityDesc
            return super.updateById(entity)
        }
    }

    override fun info(id: String): StreetJointMeetingActivityVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.browseNum = Integer(entity.browseNum!!.toInt().plus(1))
        super.updateById(entity)
        return entity.transfer()
    }

    override fun listPage(street: String?, time: Long?, type: String?, title: String?, size: Int, page: Int): PageWrapper<StreetJointMeetingActivityVO> {
        var date: String? = null
        if(time != null){
            date = DateUtil.format(Date(time!!), DatePattern.NORM_DATE_PATTERN)
        }

        val p = Page<StreetJointMeetingActivityVO>(page, size)
        p.records = this.baseMapper.findManagerPageList(p, street, date, type, title)
        return p.wrapper()
    }

}
