package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Permission;
import cc.vv.party.beans.vo.PermissionVO
import cc.vv.party.mapper.PermissionMapper;
import cc.vv.party.service.PermissionService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@Service
open class PermissionServiceImpl : BaseServiceImpl<PermissionMapper, Permission>(), PermissionService {

    override fun getPermissionListByRoleId(roleId: String): List<PermissionVO> {
        return this.baseMapper.selectPermissionByRoleId(roleId)
    }

}
