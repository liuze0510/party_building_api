package cc.vv.party.service.impl

import cc.vv.party.beans.model.CheckCode
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.mapper.CheckCodeMapper
import cc.vv.party.service.CheckCodeService
import org.springframework.stereotype.Service

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@Service
class CheckCodeServiceImpl : BaseServiceImpl<CheckCodeMapper, CheckCode>(), CheckCodeService