package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Committed;
import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.CommittedVO
import cc.vv.party.mapper.CommittedMapper;
import cc.vv.party.service.CommittedService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.PartyReportMapper
import cc.vv.party.param.CommittedEditParam
import cc.vv.party.service.OrgService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 承诺信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class CommittedServiceImpl : BaseServiceImpl<CommittedMapper, Committed>(), CommittedService {

    @Autowired
    lateinit var orgService: OrgService

    @Autowired
    lateinit var partyReportMapper: PartyReportMapper

    override fun edit(param: CommittedEditParam, userId: String): Boolean {
        return if (StringUtils.isBlank(param.id)) {
            var entity = param.transfer<Committed>()
            entity.createUser = userId
            super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.committedUnitName)) entity.committedUnitName = param.committedUnitName
            if (StringUtils.isNotBlank(param.content)) entity.content = param.content
            if (StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if (param.completeTime != null) entity.completeTime = Date(param.completeTime!!)
            super.updateById(entity)
        }
    }

    override fun info(id: String): CommittedVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        var result = entity.transfer<CommittedVO>()
        if(entity.committedType!!.toInt() == 1){
            val user = partyReportMapper.selectById(entity.targetId) ?: PartyReport()
            result.userName = user.name
        }
        return result
    }

    override fun lastCommitted(userId: String): CommittedVO? {
        var entity = super.selectOne(
            EntityWrapper<Committed>()
                .where("target_id = {0}", userId)
                .orderBy("create_time", false)
        )
        if (entity == null) {
            var partyReport = PartyReport()
            partyReport.id = userId
            partyReport = partyReportMapper.selectOne(partyReport)

            if(StringUtils.isNotBlank(partyReport.personalCommitment)){
                entity = Committed()
                entity.content = partyReport.personalCommitment
            }
        }
        return entity?.transfer()
    }

    override fun listPage(street: String?, community: String?, grid: String?, time: Long?, mobile: String?, type: Int, size: Int, page: Int
    ): PageWrapper<CommittedVO> {
        val p = Page<CommittedVO>(page, size)
        p.records = this.baseMapper.findListPage(p, street, community, grid, time, mobile, type)
        return p.wrapper()
    }

    override fun listByOrg(
        orgId: String,
        committedType: Int,
        page: Int,
        pageSize: Int,
        havingComment: Int
    ): PageWrapper<CommittedVO> {
        val p = Page<CommittedVO>(page, pageSize)
        val org = orgService.selectById(orgId) ?: throw BizException(StatusCode.FIELD_VALUE_ERROR.statusCode, "组织机构不存在")
        //承诺类型0 单位承诺 1 党员承诺
        if (committedType == SysConsts.CONST_0) {//单位承诺
            p.records = this.baseMapper.selectUnitCommittedByOrg(org.id, org.type, p, havingComment)
        } else if (committedType == SysConsts.CONST_1) {//党员承诺
            p.records = this.baseMapper.selectUserCommittedByOrg(org.id, org.type, p, havingComment)
        }
        return p.wrapper()
    }
}
