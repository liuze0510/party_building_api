package cc.vv.party.service.impl;

import cc.vv.party.beans.model.DemandList;
import cc.vv.party.beans.vo.DemandListVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.mapper.DemandListMapper;
import cc.vv.party.service.DemandListService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.DemandListListParam
import cc.vv.party.param.DemandListEditParam
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 需求清单 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class DemandListServiceImpl : BaseServiceImpl<DemandListMapper, DemandList>(), DemandListService {

    override fun edit(param: DemandListEditParam, user: UserVO, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<DemandList>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if(StringUtils.isNotBlank(param.unitId)) entity.unitId = param.unitId
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            if(StringUtils.isNotBlank(param.principal)) entity.principal = param.principal
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(param.reward != null) entity.reward = param.reward
            if(StringUtils.isNotBlank(param.amount)) entity.amount = param.amount
            return super.updateById(entity)
        }
    }

    override fun info(id: String): DemandListVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun appListPage(street: String?, community: String?, grid: String?, key: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<DemandListVO> {
        var pages = Page<DemandListVO>(page, size)
        pages.records = baseMapper.selectAppListPage(pages, street, community, grid, key, startTime, endTime)
        return pages.transfer()
    }

    override fun listPage(param: DemandListListParam, size: Int, page: Int): PageWrapper<DemandListVO> {
        var pages = Page<DemandListVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, param)
        return pages.transfer()
    }

}
