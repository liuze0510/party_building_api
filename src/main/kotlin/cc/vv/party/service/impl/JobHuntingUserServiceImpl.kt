package cc.vv.party.service.impl;

import cc.vv.party.beans.model.JobHuntingUser;
import cc.vv.party.beans.vo.JobHuntingUserVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.mapper.JobHuntingUserMapper;
import cc.vv.party.service.JobHuntingUserService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.JobHuntingUserEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 求职信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class JobHuntingUserServiceImpl : BaseServiceImpl<JobHuntingUserMapper, JobHuntingUser>(), JobHuntingUserService {

    override fun edit(param: JobHuntingUserEditParam, user: UserVO, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<JobHuntingUser>()
            entity.street = user.street
            entity.community = user.community
            entity.grid = user.grid
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.name)) entity.name = param.name
            if(StringUtils.isNotBlank(param.faceUrl)) entity.faceUrl = param.faceUrl
            if(param.sex != null) entity.sex = param.sex
            if(param.dateofbirth != null) entity.dateofbirth = Date(param.dateofbirth!!)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(param.workYear != null ) entity.workYear = param.workYear
            if(StringUtils.isNotBlank(param.education)) entity.education = param.education
            if(StringUtils.isNotBlank(param.careerObjective)) entity.careerObjective = param.careerObjective
            if(StringUtils.isNotBlank(param.expectedSalary)) entity.expectedSalary = param.expectedSalary
            if(StringUtils.isNotBlank(param.skillTag)) entity.skillTag = param.skillTag
            if(StringUtils.isNotBlank(param.workExperience)) entity.workExperience = param.workExperience
            if(StringUtils.isNotBlank(param.visibleRange)) entity.visibleRange = param.visibleRange
            return super.updateById(entity)
        }
    }

    override fun info(id: String): JobHuntingUserVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun appListPage(street: String?, community: String?, grid: String?, key: String?, startTime: Long?, endTime: Long?, size: Int, page: Int, user: UserVO): PageWrapper<JobHuntingUserVO> {
        var p = Page<JobHuntingUserVO>(page, size)
        p.records = baseMapper.findAppListPage(p, street, community,grid,  key, startTime, endTime, user)
        return p.transfer()
    }

    override fun listPage(street: String?, community: String?, grid: String?, sex: Integer?, education: String?, workYear: Integer?, name: String?, mobile: String?, size: Int, page: Int): PageWrapper<JobHuntingUserVO> {
        var p = Page<JobHuntingUserVO>(page, size)
        p.records = baseMapper.findManagerListPage(p, street, community, grid, sex, education, workYear, name, mobile)
        return p.transfer()
    }

}
