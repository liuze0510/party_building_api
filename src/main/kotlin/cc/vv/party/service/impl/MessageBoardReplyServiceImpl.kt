package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.MessageBoardReplyVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.model.MessageBoardReply;
import cc.vv.party.mapper.MessageBoardReplyMapper;
import cc.vv.party.service.MessageBoardReplyService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.OrgMapper
import cc.vv.party.param.MessageBoardReplyParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * <p>
 * 留言回复 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-23
 */
@Service
open class MessageBoardReplyServiceImpl : BaseServiceImpl<MessageBoardReplyMapper, MessageBoardReply>(), MessageBoardReplyService {


    @Transactional(rollbackFor = [(Exception::class)])
    override fun save(param: MessageBoardReplyParam, user: UserVO): Boolean {
        var entity = param.transfer<MessageBoardReply>()
        entity.time = Date(System.currentTimeMillis())
        entity.createUser = user.id
        entity.unit = user.workCompany
        return super.insert(entity)
    }

    override fun listPage(id: String, size: Int, page: Int): PageWrapper<MessageBoardReplyVO> {
        var p = Page<MessageBoardReplyVO>(page, size)
        p.records = baseMapper.findMessageBoardReplyPageList(p, id)
        return p.transfer()
    }

}
