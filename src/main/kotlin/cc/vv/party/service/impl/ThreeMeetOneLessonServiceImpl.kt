package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ThreeMeetOneLessonMapper
import cc.vv.party.model.ThreeMeetOneLesson
import cc.vv.party.processor.http.RemoteService
import cc.vv.party.service.ThreeMeetOneLessonService
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * <p>
 * 三会一课 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Service
open class ThreeMeetOneLessonServiceImpl : BaseServiceImpl<ThreeMeetOneLessonMapper, ThreeMeetOneLesson>(),
    ThreeMeetOneLessonService {

    @Autowired
    lateinit var remoteService: RemoteService

    override fun info(id: String): ThreeMeetOneLessonVO {
        val lessonsDetail = this.baseMapper.selectThreeLessonsDetail(id) ?: throw BizException(
            StatusCode.FIELD_VALUE_ERROR.statusCode,
            "数据不存在"
        )
        var url = "${SysConsts.THREE_LESSON_CONTENT_URL}${lessonsDetail.folder}/$id.txt"
        val call =
            remoteService.getThreeLessonContent(url)
        val response = call.execute()
        if (response.isSuccessful) {
            lessonsDetail.content = response.body()?.string()
        }
        lessonsDetail.imageList = mutableListOf()
        lessonsDetail.videoList = mutableListOf()
        lessonsDetail.imgName?.split(",")?.forEach {
            if (it.isNotEmpty()) {
                lessonsDetail.imageList!!.add("http://dja.yadj.gov.cn/pmc/files/image/threelessons${lessonsDetail.folder}/${lessonsDetail.id}/$it")
            }
        }

        lessonsDetail.videoName?.split(",")?.forEach {
            if (it.isNotEmpty()) {
                lessonsDetail.videoList!!.add("http://dja.yadj.gov.cn/pmc/files/video/threelessons${lessonsDetail.folder}/${lessonsDetail.id}/$it")
            }
        }
        return lessonsDetail
    }

    override fun listPage(
        startDate: String?,
        endDate: String?,
        type: String?,
        title: String?,
        branchId: String?,
        branchType: String?,
        page: Int,
        pageSize: Int
    ): PageWrapper<ThreeMeetOneLessonVO> {
        val p = Page<ThreeMeetOneLessonVO>(page, pageSize)
        p.records = this.baseMapper.selectThreeLessonsList(
            null,
            startDate, endDate, type,
            title, branchId, branchType, p
        ) as MutableList<ThreeMeetOneLessonVO>
        return p.wrapper()
    }

    override fun pcListPage(page: Int, pageSize: Int): PageWrapper<ThreeMeetOneLessonVO> {
        val p = Page<ThreeMeetOneLessonVO>(page, pageSize)
        p.records = this.baseMapper.selectThreeLessonsList(
            "PC",
            null, null, null, null, null, null, p
        ) as MutableList<ThreeMeetOneLessonVO>

        p.records.map {
            val call =
                remoteService.getThreeLessonContent("${SysConsts.THREE_LESSON_CONTENT_URL}${it.folder}/${it.id}.txt")
            val response = call.execute()
            val vo = it.transfer<ThreeMeetOneLessonVO>()
            if (response.isSuccessful) {
                vo.content = response.body()?.string()
            }
            vo
        }
        return p.transfer()
    }
}
