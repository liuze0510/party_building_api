package cc.vv.party.service.impl;

import cc.vv.party.beans.model.StreetJointMeeting;
import cc.vv.party.beans.model.StreetJointMeetingAdmin
import cc.vv.party.beans.model.StreetJointMeetingMember
import cc.vv.party.beans.model.UserRole
import cc.vv.party.beans.vo.StreetJointMeetingAdminVO
import cc.vv.party.beans.vo.StreetJointMeetingVO
import cc.vv.party.service.StreetJointMeetingService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.uuid
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.*
import cc.vv.party.param.StreetjointMeetingEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional
import kotlin.streams.toList

/**
 * <p>
 * 街道联席会议 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class StreetJointMeetingServiceImpl : BaseServiceImpl<StreetJointMeetingMapper, StreetJointMeeting>(), StreetJointMeetingService {

    @Autowired
    lateinit var streetJointMeetingAdminMapper: StreetJointMeetingAdminMapper

    @Autowired
    lateinit var streetJointMeetingMemberMapper: StreetJointMeetingMemberMapper

    @Autowired
    lateinit var roleMapper: RoleMapper

    @Autowired
    lateinit var userRoleMapper: UserRoleMapper

    @Transactional(rollbackFor = [(Exception::class)])
    override fun edit(param: StreetjointMeetingEditParam, userId: String): Boolean {
        var entity: StreetJointMeeting?

        var role = roleMapper.getByType("jdlxhy")

        if(StringUtils.isBlank(param.id)){
            entity = param.transfer<StreetJointMeeting>()
            entity.id = uuid()
            entity.createUser = userId
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.meetingName)) entity.meetingName = param.meetingName
            if(StringUtils.isNotBlank(param.convener)) entity.convener = param.convener
            if(StringUtils.isNotBlank(param.responsibleUnit)) entity.responsibleUnit = param.responsibleUnit
            if(StringUtils.isNotBlank(param.principalName)) entity.principalName = param.principalName
            if(StringUtils.isNotBlank(param.principalAccount)) entity.principalAccount = param.principalAccount
            if(StringUtils.isNotBlank(param.meetingIntroduction)) entity.meetingIntroduction = param.meetingIntroduction


            //删除旧账号管理员权限信息
            var adminList = streetJointMeetingAdminMapper.selectList(EntityWrapper<StreetJointMeetingAdmin>().where("meeting_id = {0}", entity.id))
            if(CollectionUtils.isNotEmpty(adminList)){
                var accountList = adminList.stream().map { it.adminAccount }.toList()
                userRoleMapper.delete(EntityWrapper<UserRole>().eq("role_id", role.id).`in`("user_account", accountList))
            }

            //删除旧的会议管理员信息
            streetJointMeetingAdminMapper.delete(EntityWrapper<StreetJointMeetingAdmin>().where("meeting_id = {0}", entity.id))

            //删除会议成员
            streetJointMeetingMemberMapper.delete(EntityWrapper<StreetJointMeetingMember>().where("meeting_id = {0}", entity.id))

            //更新会议信息
            super.updateById(entity)
        }

        //添加会议管理员
        if(CollectionUtils.isNotEmpty(param.streetJointMeetingAdmin)){
            for(temp in param.streetJointMeetingAdmin!!){
                temp.id = uuid()
                temp.meetingId = entity.id
                streetJointMeetingAdminMapper.insert(temp.transfer())

                //新增管理员权限
                var admin = UserRole()
                admin.roleId = role.id
                admin.userAccount = temp.adminAccount
                admin.createUser = userId
                userRoleMapper.insert(admin)
            }
        }

        //添加会议成员
        if(CollectionUtils.isNotEmpty(param.meetingMemberList)){
            for(temp in param.meetingMemberList!!){
                var member  = StreetJointMeetingMember()
                member.id = uuid()
                member.meetingId = entity.id
                member.memberId = temp
                streetJointMeetingMemberMapper.insert(member)
            }
        }

        return true
    }

    override fun info(id: String): StreetJointMeetingVO {
        var meetingVo = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "会议信息不存在")

        var result = meetingVo.transfer<StreetJointMeetingVO>()

        var meetingAdminList = streetJointMeetingAdminMapper.selectList(EntityWrapper<StreetJointMeetingAdmin>().where("meeting_id = {0}", meetingVo.id))
        if(CollectionUtils.isNotEmpty(meetingAdminList)){
            result.streetJointMeetingAdmin = meetingAdminList.transferEntries<StreetJointMeetingAdminVO>() as ArrayList
        }

        var meetingMember = streetJointMeetingMemberMapper.findByMeetingId(meetingVo.id!!)
        if(CollectionUtils.isNotEmpty(meetingMember)){
            result.meetingMemberList = meetingMember
        }

        return result
    }

    override fun getCurrentUserManagerStreetJointMeeting(account: String): StreetJointMeetingVO {
        return baseMapper.getCurrentUserManagerStreetJointMeeting(account)
    }

    override fun deleteStreetJointMeeting(id: String): Boolean {
        var role = roleMapper.getByType("jdlxhy")

        //删除旧账号管理员权限信息
        var adminList = streetJointMeetingAdminMapper.selectList(EntityWrapper<StreetJointMeetingAdmin>().where("meeting_id = {0}", id))
        if(CollectionUtils.isNotEmpty(adminList)){
            var accountList = adminList.stream().map { it.adminAccount }.toList()
            userRoleMapper.delete(EntityWrapper<UserRole>().eq("role_id", role.id).`in`("user_account", accountList))
        }

        //删除旧的会议管理员信息
        streetJointMeetingAdminMapper.delete(EntityWrapper<StreetJointMeetingAdmin>().where("meeting_id = {0}", id))

        //删除会议成员
        streetJointMeetingMemberMapper.delete(EntityWrapper<StreetJointMeetingMember>().where("meeting_id = {0}", id))

        return super.deleteById(id)
    }


    override fun listPage(street: String?, meetingName: String?, size: Int, page: Int): PageWrapper<StreetJointMeetingVO> {
        var pages = Page<StreetJointMeetingVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, street, meetingName)
        return pages.transfer()
    }

}
