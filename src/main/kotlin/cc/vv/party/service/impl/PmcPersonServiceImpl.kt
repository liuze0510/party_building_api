package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PmcPerson
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.mapper.PmcPersonMapper
import cc.vv.party.param.UserListParam
import cc.vv.party.service.PmcPersonService
import com.baomidou.mybatisplus.plugins.Page
import org.springframework.stereotype.Service

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
@Service
open class PmcPersonServiceImpl : BaseServiceImpl<PmcPersonMapper, PmcPerson>(), PmcPersonService {
    override fun selectPartyMemberList(param: UserListParam, size: Int, page: Int): PageWrapper<UserVO> {
        val p = Page<UserVO>(page, size)
        p.records = this.baseMapper.selectPartyMemberList(param, p)
        return p.wrapper()
    }

    override fun selectPartyMemberById(id: String): UserVO {
        return this.baseMapper.selectPartyMemberById(id)
    }

    override fun selectPartyMemberByIdCard(idCard: String): UserVO {
        return this.baseMapper.selectPartyMemberByIdCard(idCard)
    }

    override fun selectPartyMemberByAccounts(accounts: List<String>): List<UserVO> {
        return this.baseMapper.selectPartyMemberByAccounts(accounts)
    }
}
