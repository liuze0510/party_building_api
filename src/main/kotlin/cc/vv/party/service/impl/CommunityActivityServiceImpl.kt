package cc.vv.party.service.impl;

import cc.vv.party.beans.model.CommunityActivity;
import cc.vv.party.beans.vo.CommunityActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.mapper.CommunityActivityMapper;
import cc.vv.party.service.CommunityActivityService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.CommunityActivityEditParam
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 社区活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class CommunityActivityServiceImpl : BaseServiceImpl<CommunityActivityMapper, CommunityActivity>(), CommunityActivityService {

    override fun edit(param: CommunityActivityEditParam, user: UserVO): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<CommunityActivity>()
            entity.createUser = user.id
            entity.street = user.street
            entity.community = user.community
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.activityType)) entity.activityType = param.activityType
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.organizer)) entity.organizer = param.organizer
            if(param.activityTime != null) entity.activityTime = Date(param.activityTime!!)
            if(StringUtils.isNotBlank(param.activityLocation)) entity.activityLocation = param.activityLocation
            if(StringUtils.isNotBlank(param.activityDesc)) entity.activityDesc = param.activityDesc
            return super.updateById(entity)
        }
    }

    override fun info(id: String): CommunityActivityVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listByCommunityId(communityId: String, size: Int, page: Int): PageWrapper<CommunityActivityVO> {
        return selectPage(
                Page(page, size),
                EntityWrapper<CommunityActivity>().eq("community", communityId)
        ).wrapper()
    }

    override fun listPage(street: String?, community: String?, time: Long?, type: String?, title: String?, size: Int, page: Int): PageWrapper<CommunityActivityVO> {
//        var wrapper = EntityWrapper<CommunityActivity>()
//        if(StringUtils.isNotBlank(street)) wrapper.and("street = {0}", street)
//        if(StringUtils.isNotBlank(community)) wrapper.and("community = {0}", community)
//        if(time != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(activity_time, '%Y-%m-%d')) = {0}", time/1000)
//        if(StringUtils.isNotBlank(type)) wrapper.and("activity_type = {0}", type)
//        if(StringUtils.isNotBlank(title)) wrapper.and("title = {0}", title)
//        var pages = super.selectPage(Page(page, size), wrapper)
//        return pages.wrapper()
        var date: String? = null
        if(time != null){
            date = DateUtil.format(Date(time!!), DatePattern.NORM_DATE_PATTERN)
        }

        val p = Page<CommunityActivityVO>(page, size)
        p.records = this.baseMapper.findManagerPageList(p, street, community, date, type, title)
        return p.wrapper()
    }

}
