package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Wish
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.beans.vo.WishVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.WishMapper
import cc.vv.party.param.WishListParam
import cc.vv.party.param.WishSaveParam
import cc.vv.party.service.WishService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 微心愿 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class WishServiceImpl : BaseServiceImpl<WishMapper, Wish>(), WishService {

    override fun save(param: WishSaveParam, user: UserVO, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<Wish>()
            entity.street = user.street
            entity.community = user.community
            entity.grid = user.grid
            entity.startTime = Date(param.startTime!!)
            entity.endTime = Date(param.endTime!!)
            entity.createUser = userId
            return this.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.name)) entity.name = param.name
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(StringUtils.isNotBlank(param.addr)) entity.addr = param.addr
            if(StringUtils.isNotBlank(param.wishConetnt)) entity.wishConetnt = param.wishConetnt
            if(param.startTime != null) entity.startTime = Date(param.startTime!!)
            if(param.endTime != null) entity.endTime = Date(param.endTime!!)
            if(param.claimState != null) entity.claimState = param.claimState
            if(StringUtils.isNotBlank(param.clainPerson)) entity.clainPerson = param.clainPerson
            if(StringUtils.isNotBlank(param.clainMobile)) entity.clainMobile = param.clainMobile
            if(param.checkState != null && param.checkState!!.toInt() == 1){
                entity.clainTime = Date()
                entity.clainPersonId = userId
            }
            if(StringUtils.isNotBlank(param.remarks)) entity.remarks = param.remarks
            return super.updateById(entity)
        }

    }

    override fun checkWish(id: String, state: Int, reason: String?): Boolean {
        var entity = super.selectById(id)
        if(entity != null) {
            entity.checkState = Integer(state)
            if(StringUtils.isNotBlank(reason)) entity.checkReson = reason
        } else {
            throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        }
        return super.updateById(entity)
    }

    override fun deleteWish(id: String): Boolean {
        return this.deleteById(id)
    }

    override fun info(id: String): WishVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun claimListPage(mobile: String, userId: String, size: Int, page: Int): PageWrapper<WishVO> {
        var pages = Page<WishVO>(page, size)
        pages.records = baseMapper.selectClaimListPage(pages, mobile, userId)
        return pages.transfer()
    }

    override fun listPage(param: WishListParam, size: Int, page: Int): PageWrapper<WishVO> {
        var wrapper = EntityWrapper<Wish>()
        if(StringUtils.isNotBlank(param.street)) wrapper.and("street = {0}", param.street)
        if(StringUtils.isNotBlank(param.community)) wrapper.and("community = {0}", param.community)
        if(StringUtils.isNotBlank(param.grid)) wrapper.and("grid = {0}", param.grid)
        if(StringUtils.isNotBlank(param.name)) wrapper.and("name = {0}", param.name)
        if(StringUtils.isNotBlank(param.mobile)) wrapper.like("mobile", param.mobile)
        if(StringUtils.isNotBlank(param.addr)) wrapper.like("addr", param.addr)
        if(param.claimState != null) wrapper.and("claim_state = {0}", param.claimState)
        if(param.checkState != null) wrapper.and("check_state = {0}", param.checkState)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
