package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PartyNews;
import cc.vv.party.beans.vo.PartyNewsVO
import cc.vv.party.mapper.PartyNewsMapper;
import cc.vv.party.service.PartyNewsService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.PartyNewsEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 党群新闻 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PartyNewsServiceImpl : BaseServiceImpl<PartyNewsMapper, PartyNews>(), PartyNewsService {


    override fun edit(param: PartyNewsEditParam, userId: String): Boolean {
        if (StringUtils.isBlank(param.id)) {
            var entity = param.transfer<PartyNews>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.title)) entity.title = param.title
            if (StringUtils.isNotBlank(param.publisher)) entity.publisher = param.publisher
            if (StringUtils.isNotBlank(param.branch)) entity.branch = param.branch
            if (StringUtils.isNotBlank(param.thumbnail)) entity.thumbnail = param.thumbnail
            if (StringUtils.isNotBlank(param.video)) entity.video = param.video
            if (StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            return super.updateById(entity)
        }
    }

    override fun info(id: String): PartyNewsVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(title: String?, size: Int, page: Int): PageWrapper<PartyNewsVO> {
        var wrapper = EntityWrapper<PartyNews>()
        if (StringUtils.isNotBlank(title)) wrapper.like("title", title)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }


}
