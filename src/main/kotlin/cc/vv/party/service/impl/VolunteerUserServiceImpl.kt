package cc.vv.party.service.impl;

import cc.vv.party.beans.model.VolunteerProject
import cc.vv.party.beans.model.VolunteerUser
import cc.vv.party.beans.vo.VolunteerStatisticsVO
import cc.vv.party.beans.vo.VolunteerUserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.VolunteerProjectMapper
import cc.vv.party.mapper.VolunteerUserMapper
import cc.vv.party.param.VolunteerUserEditParam
import cc.vv.party.param.VolunteerUserListParam
import cc.vv.party.service.VolunteerUserService
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.date.calculateAge
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.LinkedHashMap

/**
 * <p>
 * 志愿者信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class VolunteerUserServiceImpl : BaseServiceImpl<VolunteerUserMapper, VolunteerUser>(), VolunteerUserService {

    @Autowired
    lateinit var volunteerProjectMapper: VolunteerProjectMapper

    override fun edit(param: VolunteerUserEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<VolunteerUser>()
            entity.age = Integer(Date(param.dateofbirth!!).calculateAge())
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.name)) entity.name = param.name
            if(StringUtils.isNotBlank(param.faceUrl)) entity.faceUrl = param.faceUrl
            if(param.sex != null) entity.sex = param.sex
            if(param.dateofbirth != null) entity.dateofbirth = Date(param.dateofbirth!!)
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if(StringUtils.isNotBlank(param.community)) entity.community = param.community
            if(StringUtils.isNotBlank(param.grid)) entity.grid = param.grid
            if(StringUtils.isNotBlank(param.career)) entity.career = param.career
            if(StringUtils.isNotBlank(param.specialty)) entity.specialty = param.specialty
            if(param.volunteerTime != null) entity.volunteerTime = Date(param.volunteerTime!!)
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(StringUtils.isNotBlank(param.volunteerType)) entity.volunteerType = param.volunteerType
            if(StringUtils.isNotBlank(param.remarks)) entity.remarks = param.remarks
            return super.updateById(entity)
        }
    }

    override fun getById(id: String): VolunteerUserVO {
        return baseMapper.getById(id)
    }

    override fun info(id: String): VolunteerUserVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(param: VolunteerUserListParam, size: Int, page: Int): PageWrapper<VolunteerUserVO> {
        var pages = Page<VolunteerUserVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, param)
        return pages.transfer()
    }

    override fun findStatistics(year: Int?, street: String?, community: String?, grid: String?): VolunteerStatisticsVO {
        var proWrapper = EntityWrapper<VolunteerProject>()
        var volunteerWrapper = EntityWrapper<VolunteerUser>()
        var nian = DateUtil.year(Date())
        if(year != null){
            nian = year
        }

        proWrapper.where("DATE_FORMAT(holding_time, '%Y') = {0}",  nian)
        volunteerWrapper.where("DATE_FORMAT(volunteer_time, '%Y') = {0}", nian)

        if(StringUtils.isNotBlank(street)) volunteerWrapper.and(" street = {0} ", street)
        if(StringUtils.isNotBlank(community)) volunteerWrapper.and(" community = {0} ", community)
        if(StringUtils.isNotBlank(grid)) volunteerWrapper.and(" grid = {0} ", grid)

        proWrapper.orderBy("holding_time", true)
        volunteerWrapper.orderBy("volunteer_time", true)

        var proList = volunteerProjectMapper.selectList(proWrapper) ?: emptyList()
        var volunteerList = super.selectList(volunteerWrapper) ?: emptyList()

        //总数
        var result = VolunteerStatisticsVO()
        result.volunteerNum = volunteerList.size
        result.proNum = proList.size

        //项目
        var proMap = LinkedHashMap<String, Int>()
        for(pro in proList){
            val key = DateUtil.format(pro.holdingTime, "MM")
            if(proMap.containsKey(key)){
                proMap[key] = proMap[key]!!.plus(1)
            } else {
                proMap[key] = 1
            }
        }
        result.project = proMap

        //项目
        var maleNum = 0
        var femaleNum = 0
        var volunteerMap = LinkedHashMap<String, Int>()
        for(pro in volunteerList){
            if(pro.sex!!.toInt() == 0){
                maleNum++
            } else {
                femaleNum++
            }

            val key = DateUtil.format(pro.volunteerTime, "MM")
            if(proMap.containsKey(key)){
                volunteerMap[key] = proMap[key]!!.plus(1)
            } else {
                volunteerMap[key] = 1
            }
        }
        result.volunteer = volunteerMap

        //男女数量
        result.maleNum = maleNum
        result.femaleNum = femaleNum

        return result
    }

}
