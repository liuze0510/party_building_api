package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Log;
import cc.vv.party.beans.vo.AppRealTimeStatisticsVO
import cc.vv.party.mapper.LogMapper;
import cc.vv.party.service.LogService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微捐赠 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class LogServiceImpl : BaseServiceImpl<LogMapper, Log>(), LogService {

    override fun findAppRealTimeStatistics(date: String): List<AppRealTimeStatisticsVO> {
        return baseMapper.findAppRealTimeStatistics(date)
    }

}
