package cc.vv.party.service.impl;

import cc.vv.party.beans.model.RolePermission;
import cc.vv.party.mapper.RolePermissionMapper;
import cc.vv.party.service.RolePermissionService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色资源 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@Service
open class RolePermissionServiceImpl : BaseServiceImpl<RolePermissionMapper, RolePermission>(), RolePermissionService {

}
