package cc.vv.party.service.impl;

import cc.vv.party.beans.model.JobHuntingUser
import cc.vv.party.beans.model.Post;
import cc.vv.party.beans.vo.PostJobHuntingUserStatisticsVO
import cc.vv.party.beans.vo.PostVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.mapper.PostMapper;
import cc.vv.party.service.PostService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.JobHuntingUserMapper
import cc.vv.party.param.PostEditParam
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 岗位信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PostServiceImpl : BaseServiceImpl<PostMapper, Post>(), PostService {

    @Autowired
    lateinit var jobHuntingUserMapper: JobHuntingUserMapper

    override fun edit(param: PostEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<Post>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.postName)) entity.postName = param.postName
            if(StringUtils.isNotBlank(param.postSource)) entity.postSource = param.postSource
            if(param.jobNum != null) entity.jobNum = param.jobNum
            if(param.workYear != null) entity.workYear = param.workYear
            if(StringUtils.isNotBlank(param.education)) entity.education = param.education
            if(StringUtils.isNotBlank(param.salary)) entity.salary = param.salary
            if(StringUtils.isNotBlank(param.workAddr)) entity.workAddr = param.workAddr
            if(StringUtils.isNotBlank(param.mobile)) entity.mobile = param.mobile
            if(StringUtils.isNotBlank(param.postDescription)) entity.postDescription = param.postDescription
            if(StringUtils.isNotBlank(param.skillClaim)) entity.skillClaim = param.skillClaim
            if(StringUtils.isNotBlank(param.companyIntroduction)) entity.companyIntroduction = param.companyIntroduction
            if(StringUtils.isNotBlank(param.visibleRange)) entity.visibleRange = param.visibleRange
            return super.updateById(entity)
        }
    }

    override fun info(id: String): PostVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun findAppListPage(street: String?, community: String?, grid: String?, postName: String?, size: Int, page: Int, user: UserVO): PageWrapper<PostVO> {
        var p = Page<PostVO>(page, size)
        p.records = baseMapper.findAppListPage(p, street, community, grid, postName, user)
        return p.transfer()
    }

    override fun listPage(street: String?, community: String?, grid: String?, time: Long?, postSource: String?, salary: String?, postName: String?, size: Int, page: Int): PageWrapper<PostVO> {
        var startSalary: Integer? = null
        var endSalary: Integer? = null
        if(StringUtils.isNotBlank(salary)){
            var salaryArray = salary!!.split('-')
            startSalary = Integer(salaryArray[0].toInt())
            endSalary = Integer(salaryArray[1].toInt())
        }

        var p = Page<PostVO>(page, size)
        p.records = baseMapper.findManagerListPage(p, street, community, grid, time, postSource, startSalary, endSalary, postName)
        return p.transfer()
    }

    override fun findStatistics(time: String?): PostJobHuntingUserStatisticsVO {
        var result = PostJobHuntingUserStatisticsVO()

        var date = if(StringUtils.isBlank(time)) {
            DateUtil.format(Date(), "yyyy")
        } else {
            time!!
        }

        //获取岗位
        var postList = super.selectList(EntityWrapper<Post>().where("DATE_FORMAT(create_time, '%Y') = {0}", date)) ?: emptyList()

        //获取求职者
        var jobHuntingUserList = jobHuntingUserMapper.selectList(EntityWrapper<JobHuntingUser>().where("DATE_FORMAT(create_time, '%Y') = {0}", date)) ?: emptyList()

        result.postNum = postList.size
        result.jobHuntingUserNum = jobHuntingUserList.size

        //岗位
        var postMap = LinkedHashMap<String, Int>()
        for(pro in postList){
            val key = DateUtil.format(pro.createTime, "MM")
            if(postMap.containsKey(key)){
                postMap[key] = postMap[key]!!.plus(1)
            } else {
                postMap[key] = 1
            }
        }
        result.post = postMap

        //求职者
        var jobHuntingUserMap = LinkedHashMap<String, Int>()
        for(pro in jobHuntingUserList){
            val key = DateUtil.format(pro.createTime, "MM")
            if(jobHuntingUserMap.containsKey(key)){
                jobHuntingUserMap[key] = jobHuntingUserMap[key]!!.plus(1)
            } else {
                jobHuntingUserMap[key] = 1
            }
        }
        result.jobHuntingUser = jobHuntingUserMap

        return result
    }

}
