package cc.vv.party.service.impl;

import cc.vv.party.beans.model.StreetJointMeetingAdmin;
import cc.vv.party.mapper.StreetJointMeetingAdminMapper;
import cc.vv.party.service.StreetJointMeetingAdminService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 联席会议管理员 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class StreetJointMeetingAdminServiceImpl : BaseServiceImpl<StreetJointMeetingAdminMapper, StreetJointMeetingAdmin>(), StreetJointMeetingAdminService {

}
