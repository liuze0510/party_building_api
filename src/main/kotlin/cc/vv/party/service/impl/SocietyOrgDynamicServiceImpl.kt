package cc.vv.party.service.impl;

import cc.vv.party.beans.model.SocietyOrg
import cc.vv.party.beans.model.SocietyOrgDynamic;
import cc.vv.party.beans.vo.SocietyOrgDynamicVO
import cc.vv.party.mapper.SocietyOrgDynamicMapper;
import cc.vv.party.service.SocietyOrgDynamicService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.SocietyOrgMapper
import cc.vv.party.param.SocietyOrgDynamicEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 社会组织动态 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class SocietyOrgDynamicServiceImpl : BaseServiceImpl<SocietyOrgDynamicMapper, SocietyOrgDynamic>(),
    SocietyOrgDynamicService {

    @Autowired
    lateinit var societyOrgMapper: SocietyOrgMapper

    override fun edit(param: SocietyOrgDynamicEditParam, userId: String): Boolean {
        if (StringUtils.isBlank(param.id)) {
            var entity = param.transfer<SocietyOrgDynamic>()
            entity.createUser = userId
            entity.communityId = param.community
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (StringUtils.isNotBlank(param.title)) entity.title = param.title
            if (StringUtils.isNotBlank(param.source)) entity.source = param.source
            if(StringUtils.isNotBlank(param.orgId)) entity.orgId = param.orgId
            if (StringUtils.isNotBlank(param.releaseOrg)) entity.releaseOrg = param.releaseOrg
            if (param.releaseTime != null) entity.releaseTime = Date(param.releaseTime!!)
            if(StringUtils.isNotBlank(param.community)) entity.communityId = param.community
            if(StringUtils.isNotBlank(param.street)) entity.street = param.street
            if (StringUtils.isNotBlank(param.releaseContent)) entity.releaseContent = param.releaseContent
            return super.updateById(entity)
        }
    }

    override fun info(id: String): SocietyOrgDynamicVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)

        var result =  entity.transfer<SocietyOrgDynamicVO>()
        var societyOrg = societyOrgMapper.selectById(result.orgId) ?: SocietyOrg()
        result.street = societyOrg.street
        result.community = societyOrg.community
        result.releaseOrg = societyOrg.orgName
        return result
    }

    override fun listPage( time: Long?, releaseOrg: String?, title: String?, size: Int, page: Int ): PageWrapper<SocietyOrgDynamicVO> {
        val wrapper = EntityWrapper<SocietyOrgDynamic>()
        if (time != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(release_time, '%Y-%m-%d')) = {0}", time / 1000)
        if (StringUtils.isNotBlank(releaseOrg)) wrapper.and("mobile = {0}", releaseOrg)
        if (StringUtils.isNotBlank(title)) wrapper.and("title = {0}", title)
        val result = super.selectPage(Page(page, size), wrapper)
        return result.wrapper()
    }

    override fun listPageByCommunityId(communityId: String, size: Int, page: Int): PageWrapper<SocietyOrgDynamicVO> {
        return selectPage(
            Page(page, size),
            EntityWrapper<SocietyOrgDynamic>().eq("community_id", communityId)
        ).wrapper()
    }

}
