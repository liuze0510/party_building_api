package cc.vv.party.service.impl;

import cc.vv.party.beans.model.NotPartyUnit;
import cc.vv.party.beans.vo.NotPartyUnitVO
import cc.vv.party.mapper.NotPartyUnitMapper;
import cc.vv.party.service.NotPartyUnitService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.NotPartyUnitEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 非公党建单位 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class NotPartyUnitServiceImpl : BaseServiceImpl<NotPartyUnitMapper, NotPartyUnit>(), NotPartyUnitService {


    override fun edit(param: NotPartyUnitEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<NotPartyUnit>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.unitName)) entity.unitName = param.unitName
            if(param.type != null) entity.type = param.type
            if(StringUtils.isNotBlank(param.introduction)) entity.introduction = param.introduction
            return super.updateById(entity)
        }
    }

    override fun info(id: String): NotPartyUnitVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(type: Integer?, unitName: String?, size: Int, page: Int): PageWrapper<NotPartyUnitVO> {
        var wrapper = EntityWrapper<NotPartyUnit>()
        if(StringUtils.isNotBlank(unitName)) wrapper.and("unit_name = {0}", unitName)
        if(type != null) wrapper.and("type = {0}", type)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
