package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Image
import cc.vv.party.beans.model.TeamWork;
import cc.vv.party.beans.vo.TeamWorkVO
import cc.vv.party.mapper.TeamWorkMapper;
import cc.vv.party.service.TeamWorkService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.uuid
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ImageMapper
import cc.vv.party.mapper.TeamMapper
import cc.vv.party.param.TeamWorkEditParam
import cn.hutool.core.date.DatePattern
import cn.hutool.core.date.DateUtil
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.date.dateFormat
import commonx.core.date.toDate
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.streams.toList

/**
 * <p>
 * 四支队伍工作信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class TeamWorkServiceImpl : BaseServiceImpl<TeamWorkMapper, TeamWork>(), TeamWorkService {

    @Autowired
    lateinit var teamMapper: TeamMapper

    @Autowired
    lateinit var imageMapper: ImageMapper

    @Transactional(rollbackFor = [(Exception::class)])
    override fun edit(param: TeamWorkEditParam, userId: String): Boolean {
        var entity: TeamWork
        if(StringUtils.isBlank(param.id)){
            entity = param.transfer()
            entity.id = uuid()
            entity.createUser = userId
            super.insert(entity)

        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.content)) entity.content = param.content
            super.updateById(entity)
        }

        //添加图片信息
        if(CollectionUtils.isNotEmpty(param.imgList)){
            for(str in param.imgList!!){
                var img = Image()
                img.type = Integer(1)
                img.associaId = entity.id
                img.url = str
                imageMapper.insert(img)
            }
        }
        return true

    }

    override fun info(id: String): TeamWorkVO {
        var teamWork = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "工作信息不存在")

        var team = teamMapper.selectById(teamWork.teamId) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "队伍信息不存在")

        var teamWorkVO = teamWork.transfer<TeamWorkVO>()
        teamWorkVO.teamVO = team.transfer()

        var list = imageMapper.selectList(EntityWrapper<Image>().where("associa_id = {0}", teamWork.id).and("type = {0}", 1))
        if(CollectionUtils.isNotEmpty(list)) {
            teamWorkVO.imgList = list.stream().map { it.url!! }.toList()
        }

        return teamWorkVO
    }

    override fun listPage(teamId: String, title: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<TeamWorkVO> {
        var wrapper = EntityWrapper<TeamWork>()
        wrapper.and("team_id = {0}", teamId)
        if(StringUtils.isNotBlank(title)) wrapper.and("title = {0}", title)
        if(startTime != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(create_time, '%Y-%m-%d 00:00:00')) >= {0}", startTime!!/1000)
        if(endTime != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(create_time, '%Y-%m-%d 23:59:59')) <= {0}", endTime!!/1000)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

    override fun appListPage(teamId: String, startTime: Long, endTime: Long, size: Int, page: Int): PageWrapper<TeamWorkVO> {
        var wrapper = EntityWrapper<TeamWork>()
        wrapper.and("team_id = {0}", teamId)
        if(startTime != null) wrapper.and("create_time >= {0}", DateUtil.format(DateUtil.beginOfDay(Date(startTime)), DatePattern.NORM_DATETIME_PATTERN))
        if(endTime != null) wrapper.and("create_time <= {0}", DateUtil.format(DateUtil.endOfDay(Date(endTime)), DatePattern.NORM_DATETIME_PATTERN))
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
