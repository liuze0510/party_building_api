package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ConvenienceServiceType
import cc.vv.party.beans.vo.ConvenienceServiceTypeVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ConvenienceServiceTypeMapper
import cc.vv.party.service.ConvenienceServiceTypeService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

/**
 * <p>
 * 便民服务类型 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ConvenienceServiceTypeServiceImpl : BaseServiceImpl<ConvenienceServiceTypeMapper, ConvenienceServiceType>(), ConvenienceServiceTypeService {

    override fun edit(id: String?, name: String, userId: String): Boolean {
        var wrapper = EntityWrapper<ConvenienceServiceType>()
        wrapper.where("name = {0}", name)
        var entity = super.selectOne(wrapper)
        if(StringUtils.isBlank(id)){
            if(entity == null){
                entity = ConvenienceServiceType()
                entity.name = name
                entity.createUser = userId
                return super.insert(entity)
            }
        } else {
            if(entity == null){
                entity = super.selectById(id)
            }

            if(entity.id == id){
                entity.name = name

                //TODO 未添加更改用户用户信息
                return super.updateById(entity)
            }
        }
        throw BizException(StatusCode.MESSAGE_EXIST)
    }

    override fun deleteConvenienceServiceType(id: String): Boolean {
        return super.deleteById(id)
    }

    override fun info(id: String): ConvenienceServiceTypeVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(size: Int, page: Int): PageWrapper<ConvenienceServiceTypeVO> {
        var pages = super.selectPage(Page(page, size), EntityWrapper<ConvenienceServiceType>())
        return pages.wrapper()
    }

    override fun listAll(): List<ConvenienceServiceTypeVO> {
       var list = super.selectList(EntityWrapper<ConvenienceServiceType>())
       if(list == null) list = Collections.emptyList()
       return list.transferEntries<ConvenienceServiceTypeVO>() as ArrayList
    }

}
