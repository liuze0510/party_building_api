package cc.vv.party.service.impl;

import cc.vv.party.beans.model.MicroDonation
import cc.vv.party.beans.vo.MicroDonationClainVO
import cc.vv.party.beans.vo.MicroDonationVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.MicroDonationClainMapper
import cc.vv.party.mapper.MicroDonationMapper
import cc.vv.party.model.MicroDonationClain
import cc.vv.party.param.MicroDonationListParam
import cc.vv.party.param.MicroDonationSaveParam
import cc.vv.party.service.MicroDonationService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

/**
 * <p>
 * 微捐赠 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class MicroDonationServiceImpl : BaseServiceImpl<MicroDonationMapper, MicroDonation>(), MicroDonationService {

    @Autowired
    lateinit var microDonationClainMapper: MicroDonationClainMapper

    override fun save(param: MicroDonationSaveParam, user: UserVO, userId: String): Boolean {
        var entity = param.transfer<MicroDonation>()
        entity.street = user.street
        entity.community = user.community
        entity.grid = user.grid
        entity.claimState = Integer(0)
        entity.startTime = Date(param.startTime!!)
        entity.endTime = Date(param.endTime!!)
        entity.createUser = userId
        return this.insert(entity)
    }

    override fun edit(param: MicroDonationSaveParam): Boolean {
        var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.claimState = Integer(0)
        entity.clainTime = Date(System.currentTimeMillis())
        return super.updateById(entity)

    }

    override fun claimMicroDonation(id: String): Boolean {
        var entity = super.selectById(id)
        if(entity != null) {
            entity.claimState = Integer(1)
        } else {
            throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        }
        return super.updateById(entity)
    }

    override fun checkMicroDonation(id: String, state: Int, reason: String?): Boolean {
        var entity = super.selectById(id)
        if(entity != null) {
            entity.checkState = Integer(state)
            entity.checkReson = reason
        } else {
            throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        }
        return super.updateById(entity)
    }

    override fun deleteMicroDonation(id: String): Boolean {
        return this.deleteById(id)
    }

    override fun info(id: String): MicroDonationVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)

        var clainList = microDonationClainMapper.selectList(EntityWrapper<MicroDonationClain>().where("micro_donation_id = {0}", entity.id))

        var result = entity.transfer<MicroDonationVO>()
        if(CollectionUtils.isNotEmpty(clainList)){
            result.clainList = clainList.transferEntries<MicroDonationClainVO>() as ArrayList
        }
        return result
    }

    override fun claimListPage(mobile: String, userId: String, size: Int, page: Int): PageWrapper<MicroDonationVO> {
        var pages = Page<MicroDonationVO>(page, size)
        pages.records = baseMapper.selectClaimListPage(pages, mobile, userId)
        return pages.transfer()
    }

    override fun listPage(param: MicroDonationListParam, size: Int, page: Int): PageWrapper<MicroDonationVO> {
        var wrapper = EntityWrapper<MicroDonation>()
        if(StringUtils.isNotBlank(param.street)) wrapper.and("street = {0}", param.street)
        if(StringUtils.isNotBlank(param.community)) wrapper.and("community = {0}", param.community)
        if(StringUtils.isNotBlank(param.grid)) wrapper.and("grid = {0}", param.grid)
        if(StringUtils.isNotBlank(param.microDonationName)) wrapper.and("micro_donation_name = {0}", param.microDonationName)
        if(StringUtils.isNotBlank(param.mobile)) wrapper.and("mobile = {0}", param.mobile)
        if(StringUtils.isNotBlank(param.addr)) wrapper.and("addr = {0}", param.addr)
        if(param.claimState != null) wrapper.and("claim_state = {0}", param.claimState)
        if(param.checkState != null) wrapper.and("check_state = {0}", param.checkState)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }


}
