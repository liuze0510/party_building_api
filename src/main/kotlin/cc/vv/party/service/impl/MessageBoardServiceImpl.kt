package cc.vv.party.service.impl;

import cc.vv.party.beans.model.MessageBoard
import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.MessageBoardVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.MessageBoardMapper
import cc.vv.party.param.MessageBoardListParam
import cc.vv.party.param.MessageBoardSaveParam
import cc.vv.party.service.MessageBoardService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service

/**
 * <p>
 * 留言板 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class MessageBoardServiceImpl : BaseServiceImpl<MessageBoardMapper, MessageBoard>(), MessageBoardService {

    override fun save(param: MessageBoardSaveParam, user: UserVO, branchInfo: CurrentBranchInfoVO): Boolean {
        var entity = param.transfer<MessageBoard>()
        entity.street = user.street
        entity.community = user.community
        entity.grid = user.grid
        entity.createUser = user.id
        entity.branch = branchInfo.partyBranchName
        return super.insert(entity)
    }


    override fun info(id: String): MessageBoardVO {
//        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
//        return entity.transfer()
        return baseMapper.getById(id)
    }

    override fun checkMessageBoard(id: String, state: Int): Boolean {
        var entity = super.selectById(id)
        if(entity != null) {
            entity.checkState = Integer(state)
        } else {
            throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        }
        return super.updateById(entity)
    }

    override fun pcListPage(size: Int, page: Int): PageWrapper<MessageBoardVO> {
        var p = Page<MessageBoardVO>(page, size)
        p.records = baseMapper.pcListPage(p)
        return p.transfer()
    }

    override fun appListPage(size: Int, page: Int): PageWrapper<MessageBoardVO> {
        var pages = Page<MessageBoardVO>(page, size)
        pages.records = baseMapper.appListPage(pages)
        return pages.transfer()
    }


    override fun listPage(listParam: MessageBoardListParam, size: Int, page: Int): PageWrapper<MessageBoardVO> {
        var wrapper = EntityWrapper<MessageBoard>()
        if(StringUtils.isNotBlank(listParam.street)) wrapper.and("street = {0}", listParam.street)
        if(StringUtils.isNotBlank(listParam.community)) wrapper.and("community = {0}", listParam.community)
        if(StringUtils.isNotBlank(listParam.grid)) wrapper.and("grid = {0}", listParam.grid)
        if(StringUtils.isNotBlank(listParam.name)) wrapper.and("name = {0}", listParam.name)
        if(StringUtils.isNotBlank(listParam.mobile)) wrapper.and("mobile = {0}", listParam.mobile)
        if(StringUtils.isNotBlank(listParam.branch)) wrapper.like("branch","%" + listParam.branch + "%")
        if(listParam.checkState != null) wrapper.and("check_state = {0}", listParam.checkState)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.transfer()
    }

}
