package cc.vv.party.service.impl

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.CurrentBranchInfoVO
import cc.vv.party.beans.vo.PendingDocVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.PendingDocMapper
import cc.vv.party.service.PendingDocService
import com.baomidou.mybatisplus.plugins.Page
import org.springframework.stereotype.Service

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@Service
class PendingDocServiceImpl : BaseServiceImpl<PendingDocMapper, PartyReport>(), PendingDocService {

    override fun selectPendingDocList(
        page: Int,
        pageSize: Int, yananRole: String, currentUserId: String, branchVo: CurrentBranchInfoVO
    ): PageWrapper<PendingDocVO> {
        val taskIdList = this.baseMapper.selectMemberTaskList(currentUserId)
        val p = Page<PendingDocVO>(page, pageSize)
        val pendingDocList = this.baseMapper.selectDocList(
            p,
            yananRole,
            branchVo.partyBranchId!!,
            branchVo.orgMinistryId,
            currentUserId,
            taskIdList
        )
        val idList = pendingDocList.filter { it.id == null }.map { it.id!! }.toList()
        val readedList = this.baseMapper.selectTaskIdsForMyReaded(currentUserId, idList)
        pendingDocList.forEach {
            it.isReaded = if (readedList.contains(it.id)) PendingDocVO.IS_READED_YES else PendingDocVO.IS_READED_NO
        }
        p.records = pendingDocList
        return p.wrapper()
    }

    override fun details(id: String, currentUserId: String): PendingDocVO {
        val vo = this.baseMapper.selectDocById(id) ?: throw BizException(StatusCode.ERROR.statusCode, "该公文你已经完成了")
        val readedList = this.baseMapper.selectTaskIdsForMyReaded(currentUserId, listOf(vo.id!!))
        vo.isReaded = if (readedList.contains(vo.id!!)) PendingDocVO.IS_READED_YES else PendingDocVO.IS_READED_NO
        return vo
    }
}