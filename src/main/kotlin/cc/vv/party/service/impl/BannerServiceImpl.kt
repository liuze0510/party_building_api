package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Banner;
import cc.vv.party.mapper.BannerMapper;
import cc.vv.party.service.BannerService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * banner管理 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class BannerServiceImpl : BaseServiceImpl<BannerMapper, Banner>(), BannerService {

}
