package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.RedPartyBuildingVO
import cc.vv.party.model.RedPartyBuilding;
import cc.vv.party.mapper.RedPartyBuildingMapper;
import cc.vv.party.service.RedPartyBuildingService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.RedPartyBuildingEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红领党建 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Service
open class RedPartyBuildingServiceImpl : BaseServiceImpl<RedPartyBuildingMapper, RedPartyBuilding>(), RedPartyBuildingService {

    override fun edit(param: RedPartyBuildingEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<RedPartyBuilding>()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            return super.updateById(entity)
        }
    }

    override fun info(id: String): RedPartyBuildingVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(title: String?, type: Integer?, size: Int, page: Int): PageWrapper<RedPartyBuildingVO> {
        var wrapper = EntityWrapper<RedPartyBuilding>()
        if(StringUtils.isNotBlank(title)) wrapper.and("title = {0}", title)
        if(type != null) wrapper.and("type = {0}", type)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
