package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.DemonstrationTypicalVO
import cc.vv.party.model.DemonstrationTypical;
import cc.vv.party.mapper.DemonstrationTypicalMapper;
import cc.vv.party.service.DemonstrationTypicalService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.DemonstrationTypicalEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;

/**
 * <p>
 * 示范典型 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@Service
open class DemonstrationTypicalServiceImpl : BaseServiceImpl<DemonstrationTypicalMapper, DemonstrationTypical>(),
    DemonstrationTypicalService {

    override fun edit(param: DemonstrationTypicalEditParam, userId: String): Boolean {
        val entity: DemonstrationTypical
        if (StringUtils.isBlank(param.id)) {
            entity = param.transfer()
            entity.createUser = userId
            return super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if (param.type != null) entity.type = param.type
            if (StringUtils.isNotBlank(param.title)) entity.title = param.title
            if (StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if (StringUtils.isNotBlank(param.content)) entity.content = param.content
            return super.updateById(entity)
        }
    }

    override fun info(id: String): DemonstrationTypicalVO {
        val entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(type: Integer?, title: String?, size: Int, page: Int): PageWrapper<DemonstrationTypicalVO> {
        val wrapper = EntityWrapper<DemonstrationTypical>()
        if (type != null) wrapper.and("type = {0}", type)
        if (StringUtils.isNotBlank(title)) wrapper.like("title", title)
        wrapper.orderBy("create_time", false)
        val pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
