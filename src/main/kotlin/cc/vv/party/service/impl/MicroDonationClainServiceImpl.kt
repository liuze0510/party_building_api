package cc.vv.party.service.impl;

import cc.vv.party.model.MicroDonationClain;
import cc.vv.party.mapper.MicroDonationClainMapper;
import cc.vv.party.service.MicroDonationClainService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.MicroDonationMapper
import cc.vv.party.param.MicroDonationClainEditParam
import commonx.core.content.transfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional

/**
 * <p>
 * 微捐赠认领 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-05
 */
@Service
open class MicroDonationClainServiceImpl : BaseServiceImpl<MicroDonationClainMapper, MicroDonationClain>(), MicroDonationClainService {

    @Autowired
    lateinit var microDonationMapper: MicroDonationMapper

    override fun save(param: MicroDonationClainEditParam, userId: String): Boolean {
        var entity = param.transfer<MicroDonationClain>()
        entity.createUser = userId
        return super.insert(entity)
    }

    @Transactional(rollbackFor = arrayOf(Exception::class))
    override fun clain(id: String): Boolean {
        var microDonationClain = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "认领信息不存在")

        var microDonation = microDonationMapper.selectById(microDonationClain.microDonationId) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "捐助信息不存在")

        microDonationClain.seelected = Integer(1)
        super.updateById(microDonationClain)

        microDonation.claimState = Integer(1)
        microDonation.clainPerson = microDonationClain.person
        microDonation.clainMobile = microDonationClain.mobile
        microDonation.remarks = microDonationClain.remarks
        microDonation.clainTime = microDonationClain.time
        microDonationMapper.updateById(microDonation)
        return true
    }

}
