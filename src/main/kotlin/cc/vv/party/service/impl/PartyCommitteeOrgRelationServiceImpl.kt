package cc.vv.party.service.impl;

import cc.vv.party.beans.model.PartyCommitteeOrgRelation;
import cc.vv.party.mapper.PartyCommitteeOrgRelationMapper;
import cc.vv.party.service.PartyCommitteeOrgRelationService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 社区大党委与组织成员关系 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class PartyCommitteeOrgRelationServiceImpl : BaseServiceImpl<PartyCommitteeOrgRelationMapper, PartyCommitteeOrgRelation>(), PartyCommitteeOrgRelationService {

}
