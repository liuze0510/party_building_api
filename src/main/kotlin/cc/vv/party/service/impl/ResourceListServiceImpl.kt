package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ResourceList
import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ResourceListMapper
import cc.vv.party.mapper.UnitMapper
import cc.vv.party.param.ResourceListEditParam
import cc.vv.party.param.ResourceListListParam
import cc.vv.party.service.ResourceListService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

/**
 * <p>
 * 资源清单 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ResourceListServiceImpl : BaseServiceImpl<ResourceListMapper, ResourceList>(), ResourceListService {

    @Autowired
    lateinit var unitMapper: UnitMapper

    override fun edit(param: ResourceListEditParam, userId: String): Boolean {
        val count = unitMapper.selectCount(EntityWrapper<Unit>().eq("unit_id", param.unitId))
        if(count <= 0){
            throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "该单位未进行报道，不能添加资源")
        }

        return if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<ResourceList>()
            entity.createUser = userId
            super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.resourceType)) entity.resourceType = param.resourceType
            if(StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            if(param.num != null) entity.num = param.num
            if(param.reward != null) entity.reward = param.reward
            if(param.startTime != null) entity.startTime = Date(param.startTime!!)
            if(param.endTime != null) entity.endTime = Date(param.endTime!!)
            if(StringUtils.isNotBlank(param.amount)) entity.amount = param.amount
            super.updateById(entity)
        }
    }

    override fun info(id: String): ResourceListVO {
        return baseMapper.getById(id)
    }

    override fun getUnitResourceList(unitId: String): List<ResourceListVO> {
        val list = super.selectList(EntityWrapper<ResourceList>().where("unit_id = {0}", unitId)) ?: ArrayList<ResourceList>()
        return list.transferEntries<ResourceListVO>() as ArrayList
    }

    override fun appListPage(street: String?, community: String?, title: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<ResourceListVO> {
        var pages = Page<ResourceListVO>(page, size)
        pages.records = baseMapper.findAppListPage(pages, street, community, title, startTime, endTime)
        return pages.transfer()
    }

    override fun listPage(param: ResourceListListParam, size: Int, page: Int): PageWrapper<ResourceListVO> {
        var pages = Page<ResourceListVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, param)
        return pages.transfer()
    }

}
