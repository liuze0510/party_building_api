package cc.vv.party.service.impl;

import cc.vv.party.beans.model.RatingDetails;
import cc.vv.party.mapper.RatingDetailsMapper;
import cc.vv.party.service.RatingDetailsService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 单位评星晋阶明细 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class RatingDetailsServiceImpl : BaseServiceImpl<RatingDetailsMapper, RatingDetails>(), RatingDetailsService {

}
