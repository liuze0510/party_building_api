package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.SocialistValuesVO
import cc.vv.party.model.SocialistValues;
import cc.vv.party.mapper.SocialistValuesMapper;
import cc.vv.party.service.SocialistValuesService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.SocialistValuesEditParam
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 社会主义核心价值观 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Service
open class SocialistValuesServiceImpl : BaseServiceImpl<SocialistValuesMapper, SocialistValues>(), SocialistValuesService {

    override fun edit(param: SocialistValuesEditParam, userId: String): Boolean {
        if(StringUtils.isBlank(param.id)){
            var entity = param.transfer<SocialistValues>()
            entity.releaseTime = Date()
            entity.createUser = userId
            entity.publisher = entity.createUser
            return super.insert(entity)
        } else {
            var entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.conetnt)) entity.conetnt = param.conetnt
            if(param.type != null) entity.type = param.type
            if(StringUtils.isNotBlank(param.source)) entity.source = param.source
            entity.releaseTime = Date()
            return super.updateById(entity)
        }
    }

    override fun info(id: String): SocialistValuesVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.browseNum = Integer(entity.browseNum!!.toInt().plus(1))
        super.updateById(entity)
        return entity.transfer()
    }

    override fun listPage(time: Long?, type: Integer?, title: String?, size: Int, page: Int): PageWrapper<SocialistValuesVO> {
        var wrapper = EntityWrapper<SocialistValues>()
        if(time != null) wrapper.and("UNIX_TIMESTAMP(DATE_FORMAT(release_time, '%Y-%m-%d')) = {0}", time!!/1000)
        if(type != null) wrapper.and("type = {0}", type)
        if(StringUtils.isNotBlank(title)) wrapper.and("title = {0}", title)
        wrapper.orderBy("create_time", false)
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
