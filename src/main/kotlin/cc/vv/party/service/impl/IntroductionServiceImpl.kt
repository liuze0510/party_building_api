package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Introduction;
import cc.vv.party.mapper.IntroductionMapper;
import cc.vv.party.service.IntroductionService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 党建概况 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class IntroductionServiceImpl : BaseServiceImpl<IntroductionMapper, Introduction>(), IntroductionService {

}
