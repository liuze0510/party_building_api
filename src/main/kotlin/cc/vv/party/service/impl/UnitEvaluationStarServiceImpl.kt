package cc.vv.party.service.impl;

import cc.vv.party.beans.model.RatingDetails
import cc.vv.party.beans.model.UnitEvaluationStar
import cc.vv.party.beans.vo.RatingDetailsVO
import cc.vv.party.beans.vo.UnitEvaluationStarVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.uuid
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.RatingDetailsMapper
import cc.vv.party.mapper.UnitEvaluationStarMapper
import cc.vv.party.param.UnitEvaluationStarEditParam
import cc.vv.party.service.UnitEvaluationStarService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.content.transferEntries
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * <p>
 * 单位评星晋阶 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class UnitEvaluationStarServiceImpl : BaseServiceImpl<UnitEvaluationStarMapper, UnitEvaluationStar>(), UnitEvaluationStarService {

    @Autowired
    lateinit var ratingDetailsMapper: RatingDetailsMapper

    override fun edit(param: UnitEvaluationStarEditParam, userId: String): Boolean {
        var entity : UnitEvaluationStar
        if(StringUtils.isBlank(param.id)){
            entity = param.transfer()
            entity.id = uuid()
            entity.createUser = userId

            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "信息不存在")

            if(StringUtils.isNotBlank(param.companyName)) entity.companyName = param.companyName
            if(StringUtils.isNotBlank(param.orgType)) entity.orgType = param.orgType
            if(param.score != null) entity.score = param.score

            ratingDetailsMapper.delete(EntityWrapper<RatingDetails>().where("star_id = {0}", param.id))
            entity.updateById()
        }

        if(CollectionUtils.isNotEmpty(param.itemList)){
            for(item in param.itemList!!){
                var temp = item.transfer<RatingDetails>()
                temp.starId = entity.id
                temp.createUser = userId
                ratingDetailsMapper.insert(temp)
            }
        }
        return true
    }

    override fun deleteUnitEvaluationStar(id: String): Boolean {
        ratingDetailsMapper.delete(EntityWrapper<RatingDetails>().where("star_id = {0}", id))

        super.deleteById(id)
        return true
    }

    override fun info(id: String): UnitEvaluationStarVO {
        var result: UnitEvaluationStarVO
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "会议信息不存在")
        result = entity.transfer()

        var itemList = ratingDetailsMapper.selectList(EntityWrapper<RatingDetails>().where("star_id = {0}", id)) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "详情信息不存在")

        result.itemList = itemList.transferEntries<RatingDetailsVO>() as ArrayList
        return result
    }

    override fun listPage(name: String?, size: Int, page: Int): PageWrapper<UnitEvaluationStarVO> {
        var wrapper = EntityWrapper<UnitEvaluationStar>()
        if(StringUtils.isNotBlank(name)) wrapper.like("company_name", name)
        wrapper.orderDesc(
                arrayListOf("score", "create_time")
        )
        var pages = super.selectPage(Page(page, size), wrapper)
        return pages.wrapper()
    }

}
