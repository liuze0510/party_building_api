package cc.vv.party.service.impl

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.MsgNotifyVO
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.mapper.MsgNotifyMapper
import cc.vv.party.service.MsgNotifyService
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.date.beginTimeOfDay
import commonx.core.date.dateFormat
import commonx.core.date.endTimeOfDay
import commonx.core.date.yesterday
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
@Service
class MsgNotifyServiceImpl : BaseServiceImpl<MsgNotifyMapper, PartyReport>(), MsgNotifyService {

    @Value("\${yanan.pmc-base-url}")
    lateinit var baseUrl: String

    override fun msgNotifyList(currentUserId: String, type: Int, page: Int, pageSize: Int): PageWrapper<MsgNotifyVO> {
        val userMap = this.baseMapper.selectPartyMemberInfo(currentUserId)
        val p = Page<MsgNotifyVO>(page, pageSize)
        var start: String? = null
        var end: String? = null
        val now = Date()
        if (type == 0) {//今天
            start = now.beginTimeOfDay().time.dateFormat()
            end = now.endTimeOfDay().time.dateFormat()
        } else if (type == 1) {//昨天
            start = now.yesterday().beginTimeOfDay().time.dateFormat()
            end = now.yesterday().endTimeOfDay().time.dateFormat()
        } else {//以前
            end = now.yesterday().endTimeOfDay().time.dateFormat()
        }
        p.records = this.baseMapper.selectMsgNotifyList(
            p,
            start,
            end,
            userMap["TOWNREGIONID"],
            userMap["COUNTYREGIONID"],
            userMap["CITYREGIONID"],
            userMap["ORGMINISTRYID"],
            currentUserId
        )
        return p.wrapper()
    }

    override fun msgNotifyDetail(id: String): MsgNotifyVO {
        val vo = baseMapper.selectMsgNotifyById(id)
        vo.html = "${baseUrl}files/html/notify${vo.timeStampFolder}/${vo.id}.html"
        return vo
    }


}