package cc.vv.party.service.impl;

import cc.vv.party.beans.model.ProjectApply
import cc.vv.party.common.base.BaseServiceImpl
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.exception.BizException
import cc.vv.party.mapper.ProjectApplyMapper
import cc.vv.party.mapper.ProjectMapper
import cc.vv.party.mapper.ProjectSegmentationMapper
import cc.vv.party.param.ProjectApplyEditParam
import cc.vv.party.service.ProjectApplyService
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * <p>
 * 项目延期、取消申请 服务类 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ProjectApplyServiceImpl : BaseServiceImpl<ProjectApplyMapper, ProjectApply>(), ProjectApplyService {

    @Autowired
    lateinit var projectMapper: ProjectMapper

    @Autowired
    lateinit var projectSegmentationMapper: ProjectSegmentationMapper

    override fun save(param: List<ProjectApplyEditParam>, userId: String): Boolean {
        var projectId = ""
        var segmIdList = ArrayList<String>()
        var entityMap = HashMap<String, ProjectApply>()
        for(temp in param){
            var entity = ProjectApply()
            entity.proId = temp.proId
            entity.segId = temp.segId
            entity.type = temp.type
            entity.warningTime = Date(temp.warningTime!!)
            entity.completeTime = Date(temp.completeTime!!)
            entity.reason = temp.reason
            entity.createUser = userId
            if(entity.type!!.toInt() == 0){
                projectId = entity.proId!!
                entityMap[entity.proId!!] = entity
            } else {
                segmIdList.add(entity.segId!!)
                entityMap[entity.segId!!] = entity
            }
        }

        if(StringUtils.isEmpty(projectId)){
            projectId = param[0].proId!!
        }

        //更新项目
        var project = projectMapper.selectById(projectId) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST.statusCode, "项目信息不存在")

        var newProject  = entityMap[projectId]
        if(newProject != null){
            val oldWarningTime = project.warningTime
            var oldCompleteTime = project.completeTime

            project.warningTime = newProject!!.warningTime
            project.completeTime = newProject!!.completeTime

            newProject.warningTime = oldWarningTime
            newProject.completeTime = oldCompleteTime

            project.applyReson = newProject.reason
        } else {
            project.applyReson = param[0].reason
        }
        project.sysState =  Integer(1)
        project.applyType = Integer(2)
        projectMapper.updateById(project)

        //更新阶段信息
        if(CollectionUtils.isNotEmpty(segmIdList) && segmIdList.size > 0){
            var segmList = projectSegmentationMapper.selectBatchIds(segmIdList)
            if(segmList != null){
                for(temp in segmList){
                    if(entityMap.containsKey(temp.id) && entityMap[temp.id] != null){
                        var newSegm  = entityMap[temp.id]
                        val oldWarningTime = temp.warningTime
                        var oldCompleteTime = temp.completeTime

                        temp.warningTime = newSegm!!.warningTime
                        temp.completeTime = newSegm!!.completeTime

                        newSegm.warningTime = oldWarningTime
                        newSegm.completeTime = oldCompleteTime
                        entityMap[temp.id!!] = newSegm
                        projectSegmentationMapper.updateById(temp)
                    }
                }
            }
        }

        return super.insertBatch(entityMap.values.toList())
    }

}
