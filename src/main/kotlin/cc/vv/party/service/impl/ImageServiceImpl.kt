package cc.vv.party.service.impl;

import cc.vv.party.beans.model.Image;
import cc.vv.party.mapper.ImageMapper;
import cc.vv.party.service.ImageService;
import cc.vv.party.common.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 图片信息 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Service
open class ImageServiceImpl : BaseServiceImpl<ImageMapper, Image>(), ImageService {

}
