package cc.vv.party.service.impl;

import cc.vv.party.beans.vo.GridActivityVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.model.GridActivity;
import cc.vv.party.mapper.GridActivityMapper;
import cc.vv.party.service.GridActivityService;
import cc.vv.party.common.base.BaseServiceImpl;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.param.GridActivityEditParam
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service;
import java.util.*

/**
 * <p>
 * 网格活动 服务实现类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Service
open class GridActivityServiceImpl : BaseServiceImpl<GridActivityMapper, GridActivity>(), GridActivityService {

    override fun edit(param: GridActivityEditParam, user: UserVO): Boolean {
        var entity: GridActivity
        if(StringUtils.isBlank(param.id)){
            entity = param.transfer()
            entity.street = user.street
            entity.community = user.community
            entity.grid = user.grid
            entity.createUser = user.id
            super.insert(entity)
        } else {
            entity = super.selectById(param.id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
            if(StringUtils.isNotBlank(param.title)) entity.title = param.title
            if(StringUtils.isNotBlank(param.cover)) entity.cover = param.cover
            if(StringUtils.isNotBlank(param.organizer)) entity.organizer = param.organizer
            if(StringUtils.isNotBlank(param.activityLocation)) entity.activityLocation = param.activityLocation
            if(param.activityTime != null) entity.activityTime = Date(param.activityTime!!)
            if(StringUtils.isNotBlank(param.activityDesc)) entity.activityDesc = param.activityDesc
            super.updateById(entity)
        }
        return true
    }

    override fun info(id: String): GridActivityVO {
        var entity = super.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return entity.transfer()
    }

    override fun listPage(street: String?, community: String?, grid: String?, title: String?, size: Int, page: Int): PageWrapper<GridActivityVO> {
        var pages = Page<GridActivityVO>(page, size)
        pages.records = baseMapper.selectListPage(pages, street, community, grid, title)
        return pages.transfer()
    }

}
