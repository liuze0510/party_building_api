package cc.vv.party.service;

import cc.vv.party.beans.model.Team;
import cc.vv.party.beans.vo.SignInVO
import cc.vv.party.beans.vo.TeamVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.TeamEditParam

/**
 * <p>
 * 四支队伍信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface TeamService : BaseService<Team> {

    fun edit(param: TeamEditParam, userId: String): Boolean

    fun info(id: String): TeamVO

    fun deleteTeam(id: String): Boolean

    fun villageTeamList(village: String): List<TeamVO>

    fun listPage(town: String?, village: String?, teamType: String?, size: Int, page: Int): PageWrapper<TeamVO>

    fun getSecretaryMessage(village: String): TeamVO

    //移动端-获取工作队信息 type 1 村两委班子 2 扶贫工作队 3 驻村干部
    fun findVillageTeamList(village: String, type: String): List<TeamVO>?


}
