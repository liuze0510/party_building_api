package cc.vv.party.service;

import cc.vv.party.beans.model.ResourceList;
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ResourceListListParam
import cc.vv.party.param.ResourceListEditParam

/**
 * <p>
 * 资源清单 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ResourceListService : BaseService<ResourceList> {

    fun edit(param: ResourceListEditParam, userId: String): Boolean

    fun info(id: String): ResourceListVO

    fun getUnitResourceList(unitId: String): List<ResourceListVO>

    fun appListPage(street: String?, community: String?, title: String?, startTime: Long?, endTime: Long?, size: Int, page: Int): PageWrapper<ResourceListVO>

    fun listPage(param: ResourceListListParam, size: Int, page: Int): PageWrapper<ResourceListVO>

}
