package cc.vv.party.service;

import cc.vv.party.beans.model.Committed
import cc.vv.party.beans.vo.CommittedVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.CommittedEditParam
import com.baomidou.mybatisplus.plugins.Page

/**
 * <p>
 * 承诺信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommittedService : BaseService<Committed> {

    fun edit(param: CommittedEditParam, userId: String): Boolean

    fun lastCommitted(userId: String): CommittedVO?

    fun info(id: String): CommittedVO

    fun listPage(street: String?, community: String?, grid: String?, time: Long?,  mobile: String?, type: Int, size: Int, page: Int): PageWrapper<CommittedVO>

    fun listByOrg( orgId: String, committedType: Int,  page: Int, pageSize: Int, havingComment: Int ): PageWrapper<CommittedVO>
}
