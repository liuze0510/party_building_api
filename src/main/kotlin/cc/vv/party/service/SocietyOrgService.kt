package cc.vv.party.service;

import cc.vv.party.beans.model.SocietyOrg;
import cc.vv.party.beans.vo.SocietyOrgVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.SocietyOrgEditParam

/**
 * <p>
 * 社会组织 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface SocietyOrgService : BaseService<SocietyOrg> {

    fun edit(param: SocietyOrgEditParam, userId: String): Boolean

    fun info(id: String): SocietyOrgVO

    fun listPage(community: String?, orgName: String?, size: Int, page: Int): PageWrapper<SocietyOrgVO>

}
