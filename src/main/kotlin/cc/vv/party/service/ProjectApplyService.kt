package cc.vv.party.service;

import cc.vv.party.beans.model.ProjectApply
import cc.vv.party.common.base.BaseService
import cc.vv.party.param.ProjectApplyEditParam

/**
 * <p>
 * 项目延期、取消申请 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectApplyService : BaseService<ProjectApply> {

    fun save(param: List<ProjectApplyEditParam>, userId: String): Boolean

}
