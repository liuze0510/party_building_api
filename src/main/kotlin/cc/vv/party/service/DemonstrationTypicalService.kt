package cc.vv.party.service;

import cc.vv.party.beans.vo.DemonstrationTypicalVO
import cc.vv.party.model.DemonstrationTypical;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.DemonstrationTypicalEditParam

/**
 * <p>
 * 示范典型 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
interface DemonstrationTypicalService : BaseService<DemonstrationTypical> {


    fun edit(param: DemonstrationTypicalEditParam, userId: String): Boolean

    fun info(id: String): DemonstrationTypicalVO

    fun listPage(type: Integer?, title: String?, size: Int, page: Int): PageWrapper<DemonstrationTypicalVO>

}
