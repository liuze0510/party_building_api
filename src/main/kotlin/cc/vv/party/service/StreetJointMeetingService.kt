package cc.vv.party.service;

import cc.vv.party.beans.model.StreetJointMeeting;
import cc.vv.party.beans.vo.StreetJointMeetingVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.StreetjointMeetingEditParam

/**
 * <p>
 * 街道联席会议 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface StreetJointMeetingService : BaseService<StreetJointMeeting> {


    fun edit(param: StreetjointMeetingEditParam, userId: String): Boolean

    fun info(id: String): StreetJointMeetingVO

    /** 获取当前用户管理街道会议信息 */
    fun getCurrentUserManagerStreetJointMeeting(account: String): StreetJointMeetingVO

    fun deleteStreetJointMeeting(id: String): Boolean

    fun listPage(street: String?, meetingName: String?, size: Int, page: Int): PageWrapper<StreetJointMeetingVO>
}
