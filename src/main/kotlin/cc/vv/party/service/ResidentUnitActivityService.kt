package cc.vv.party.service;

import cc.vv.party.beans.vo.ResidentUnitActivityVO
import cc.vv.party.model.ResidentUnitActivity;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.ResidentUnitActivityEditParam

/**
 * <p>
 * 驻区单位活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface ResidentUnitActivityService : BaseService<ResidentUnitActivity> {


    fun edit(param: ResidentUnitActivityEditParam, userId: String): Boolean

    fun info(id: String): ResidentUnitActivityVO

    fun listPage(street: String?, community: String?, unitId: String?, title: String?, size: Int, page: Int): PageWrapper<ResidentUnitActivityVO>

}
