package cc.vv.party.service;

import cc.vv.party.beans.vo.MessageBoardReplyVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.model.MessageBoardReply;
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.MessageBoardReplyParam

/**
 * <p>
 * 留言回复 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-23
 */
interface MessageBoardReplyService : BaseService<MessageBoardReply> {

    fun save(param: MessageBoardReplyParam, user: UserVO): Boolean

    fun listPage(id: String, size: Int, page: Int): PageWrapper<MessageBoardReplyVO>
}
