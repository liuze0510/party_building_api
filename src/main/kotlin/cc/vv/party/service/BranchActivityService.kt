package cc.vv.party.service;

import cc.vv.party.beans.vo.BranchActivityVO
import cc.vv.party.common.base.BaseService
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.model.BranchActivity

/**
 * <p>
 * 支部活动 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
interface BranchActivityService : BaseService<BranchActivity> {

    fun info(id: String): BranchActivityVO

    fun listPage(
        startDate: String?,
        endDate: String?,
        type: String?,
        title: String?,
        branchId: String?,
        branchType: String?,
        page: Int,
        pageSize: Int
    ): PageWrapper<BranchActivityVO>

    fun pcListPage(page: Int, pageSize: Int): PageWrapper<BranchActivityVO>
}
