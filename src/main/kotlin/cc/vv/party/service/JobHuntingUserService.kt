package cc.vv.party.service;

import cc.vv.party.beans.model.JobHuntingUser;
import cc.vv.party.beans.vo.JobHuntingUserVO
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.common.base.BaseService;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.param.JobHuntingUserEditParam

/**
 * <p>
 * 求职信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface JobHuntingUserService : BaseService<JobHuntingUser> {

    fun edit(param: JobHuntingUserEditParam, user: UserVO, userId: String): Boolean

    fun info(id: String): JobHuntingUserVO

    fun appListPage(street: String?, community: String?, grid: String?, key: String?, startTime: Long?, endTime: Long?, size: Int, page: Int, user: UserVO): PageWrapper<JobHuntingUserVO>

    fun listPage(street: String?, community: String?, grid: String?, sex: Integer?, education: String?, workYear: Integer?, name: String?, mobile: String?, size: Int, page: Int): PageWrapper<JobHuntingUserVO>
    
}
