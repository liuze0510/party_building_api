package cc.vv.party.service;

import cc.vv.party.beans.model.Image;
import cc.vv.party.common.base.BaseService;

/**
 * <p>
 * 图片信息 服务类
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ImageService : BaseService<Image>
