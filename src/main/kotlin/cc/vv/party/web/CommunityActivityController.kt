package cc.vv.party.web;


import cc.vv.party.beans.vo.CommunityActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommunityActivityEditParam
import cc.vv.party.service.CommunityActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区活动", tags = ["社区活动务端API"], description = "服务端社区活动相关接口")
@RestController
@RequestMapping("/web/community-activity")
class CommunityActivityController : BaseController() {

    @Autowired
    lateinit var ommunityActivityService: CommunityActivityService

    @OperateLog(content = "编辑社区活动")
    @ApiOperation(value = "编辑社区活动", notes = "编辑社区活动")
    @PostMapping("/edit")
    fun edit(@RequestBody param: CommunityActivityEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = ommunityActivityService.edit(param, getCurrentUserInfo())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社区活动")
    @ApiOperation(value = "单个删除社区活动", notes = "单个删除社区活动")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = ommunityActivityService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社区活动详情")
    @ApiOperation(value = "获取社区活动", notes = "获取社区活动")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityActivityVO> {
        var vo = ommunityActivityService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区活动列表")
    @ApiOperation(value = "获取社区活动列表", notes = "获取社区活动列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "time", value = "活动时间", required = false)),
                (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) type: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommunityActivityVO>> {
        var page = ommunityActivityService.listPage(street, community, time, type, title, size, page)
        return success(page)
    }

}
