package cc.vv.party.api;


import cc.vv.party.beans.vo.PartyActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PartyActivityEditParam
import cc.vv.party.service.PartyActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区大党委活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Api(value = "社区大党委活动", tags = ["社区大党委活动服务端API"], description = "社区大党委活动相关接口")
@RestController
@RequestMapping("/web/party-activity")
class PartyActivityController : BaseController() {

    @Autowired
    lateinit var partyActivityService: PartyActivityService


    @OperateLog(content = "编辑社区大党委活动信息")
    @ApiOperation(value = "编辑信息", notes = "编辑信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PartyActivityEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = partyActivityService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社区大党委活动信息")
    @ApiOperation(value = "单个删除信息", notes = "单个删除信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePost(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = partyActivityService.deleteById(id)
        return success(isDelete)
    }


    @OperateLog(content = "查看社区大党委活动详情")
    @ApiOperation(value = "获取信息", notes = "获取信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PartyActivityVO> {
        var vo = partyActivityService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区大党委活动列表")
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "partyId", value = "社区党委", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) partyId: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PartyActivityVO>> {
        var page = partyActivityService.listPage(street, partyId, title, size, page)
        return success(page)
    }

}
