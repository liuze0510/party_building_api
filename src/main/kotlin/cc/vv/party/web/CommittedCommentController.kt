package cc.vv.party.web;


import cc.vv.party.beans.model.CommittedComment
import cc.vv.party.beans.vo.CommittedCommentVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommittedCommentEditParam
import cc.vv.party.service.CommittedCommentService
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * 承诺评论信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "承诺评论", tags = ["承诺评论务端API"], description = "服务端承诺评论相关接口")
@RestController
@RequestMapping("/web/committed-comment")
class CommittedCommentController : BaseController() {

    @Autowired
    lateinit var committedCommentService: CommittedCommentService

    @OperateLog(content = "添加承诺评论信息")
    @ApiOperation(value = "添加承诺评论信息", notes = "添加承诺评论信息")
    @PostMapping("/save")
    fun save(@RequestBody param: CommittedCommentEditParam): ResponseEntityWrapper<Boolean> {
        var entity = param.transfer<CommittedComment>()
        entity.commentId = getCurrentUserId()
        entity.createUser = getCurrentUserId()
        entity.commentTime = Date()
        var isAdd = committedCommentService.insert(entity)
        return success(isAdd)
    }


    @OperateLog(content = "查看承诺评论列表")
    @ApiOperation(value = "获取承诺评论列表", notes = "获取承诺评论列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "committedId", value = "承诺编号", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam committedId: String,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommittedCommentVO>> {
        var page = committedCommentService.listPage(committedId, size, page)
        return success(page)
    }

}
