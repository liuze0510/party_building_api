package cc.vv.party.web;


import cc.vv.party.beans.vo.TeamWorkVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.TeamWorkEditParam
import cc.vv.party.service.TeamWorkService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 四支队伍工作信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍工作信息", tags = ["四支队伍工作信息服务端API"], description = "服务端四支队伍工作信息相关接口")
@RestController
@RequestMapping("/web/team-work")
class TeamWorkController : BaseController() {

    @Autowired
    lateinit var teamWorkService: TeamWorkService

    @OperateLog(content = "编辑四支队伍工作信息")
    @ApiOperation(value = "编辑四支队伍工作信息", notes = "编辑四支队伍工作信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: TeamWorkEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = teamWorkService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除四支队伍工作信息")
    @ApiOperation(value = "单个删除四支队伍工作信息", notes = "单个删除四支队伍工作信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteTeamWork(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = teamWorkService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看四支队伍工作信息")
    @ApiOperation(value = "获取四支队伍工作信息", notes = "获取四支队伍工作信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<TeamWorkVO> {
        var vo = teamWorkService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看四支队伍工作列表")
    @ApiOperation(value = "获取四支队伍工作列表", notes = "获取四支队伍工作列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "teamId", value = "队伍编号", required = true)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "startTime", value = "开始时间", required = false)),
                (ApiImplicitParam(name = "endTime", value = "结束时间", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam teamId: String,
                 @RequestParam(required = false) title: String?,
                 @RequestParam(required = false) startTime: Long?,
                 @RequestParam(required = false) endTime: Long?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<TeamWorkVO>> {
        var page = teamWorkService.listPage(teamId, title, startTime, endTime, size, page)
        return success(page)
    }
    
}
