package cc.vv.party.web

import cc.vv.party.beans.model.CheckCode
import cc.vv.party.beans.vo.CheckCodeVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.util.*
import javax.imageio.ImageIO


/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
@RestController
@Api(value = "验证码生成", tags = ["验证码生成服务端API"], description = "验证码生成服务端API-服务端验证码生成相关接口")
@RequestMapping("/web/code")
class CheckCodeController : BaseController() {

    companion object {
        const val imageHeight = 40
        const val imageWidth = 100
        const val codeCount = 4
        val codeSequence = arrayOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')
    }

    private val random by lazy { Random() }

    var buffImg: BufferedImage? = null

    @ApiOperation("生成验证码")
    @GetMapping("/generate")
    fun generateCheckCode(): ResponseEntityWrapper<CheckCodeVO> {
        val checkCode = CheckCode()
        checkCode.code = createCode()
        checkCode.insert()
        val vo = CheckCodeVO()
        vo.id = checkCode.id
        vo.code = write()
        return success(vo)
    }

    fun createCode(): String {
        var codeX = 0
        var fontHeight = 0
        fontHeight = imageHeight - 10// 字体的高度
        codeX = imageWidth / (codeCount + 2)// 每个字符的宽度

        // 图像buffer
        buffImg = BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB)
        val g = buffImg!!.createGraphics()

        // 将图像填充为白色
        g.color = Color.WHITE
        g.fillRect(0, 0, imageWidth, imageHeight)

        val font = Font("Arial", Font.PLAIN, fontHeight);
        g.font = font

        val randomCode = StringBuffer()
        // 随机产生验证码字符
        for (i in 0 until codeCount) {
            val strRand = codeSequence[getRandomNumber(codeSequence.size)].toString()
            // 设置字体颜色
            g.color = getRandomColor()
            // 设置字体位置
            g.drawString(strRand, (i + 1) * codeX, imageHeight - 5)
            randomCode.append(strRand)
        }
        return randomCode.toString()
    }

    /** 获取随机颜色 */
    private fun getRandomColor(): Color {
        val r = getRandomNumber(255);
        val g = getRandomNumber(255);
        val b = getRandomNumber(255);
        return Color(r, g, b);
    }

    private fun getRandomNumber(number: Int): Int {
        return random.nextInt(number);
    }

    private fun write(): ByteArray {
        val byteOutputStream = ByteOutputStream()
        ImageIO.write(buffImg, "png", byteOutputStream);
        return byteOutputStream.bytes
    }
}