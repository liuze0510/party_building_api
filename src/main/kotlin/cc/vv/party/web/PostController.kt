package cc.vv.party.web;


import cc.vv.party.beans.vo.PostVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PostEditParam
import cc.vv.party.service.PostService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 岗位信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "岗位信息", tags = ["岗位信息服务端API"], description = "服务端岗位信息相关接口")
@RestController
@RequestMapping("/web/post")
class PostController : BaseController(){

    @Autowired
    lateinit var postService: PostService

    @OperateLog(content = "编辑岗位信息")
    @ApiOperation(value = "编辑岗位信息", notes = "编辑岗位信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PostEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = postService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除岗位信息")
    @ApiOperation(value = "单个删除岗位信息", notes = "单个删除岗位信息")
    @ApiImplicitParam(name = "id", value = "岗位编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePost(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = postService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "批量删除岗位信息")
    @ApiOperation(value = "批量删除岗位信息", notes = "批量删除岗位信息")
    @ApiImplicitParam(name = "ids", value = "岗位编号集合", required = true, dataType = "Array")
    @PostMapping("/delete")
    fun deleteBatchPost(@RequestBody ids: List<String>): ResponseEntityWrapper<Boolean> {
        var isDelete = postService.deleteBatchIds(ids)
        return success(isDelete)
    }

    @OperateLog(content = "查看取岗位详情")
    @ApiOperation(value = "获取岗位信息", notes = "获取岗位信息")
    @ApiImplicitParam(name = "id", value = "岗位编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PostVO> {
        var vo = postService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看取岗位列表")
    @ApiOperation(value = "获取岗位列表", notes = "获取岗位列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "time", value = "时间", required = false)),
                (ApiImplicitParam(name = "postSource", value = "岗位来源", required = false)),
                (ApiImplicitParam(name = "salary", value = "薪资 格式为:2000-3000", required = false)),
                (ApiImplicitParam(name = "postName", value = "岗位名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) postSource: String?,
                 @RequestParam(required = false) salary: String?,
                 @RequestParam(required = false) postName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PostVO>> {
        var page = postService.listPage(street, community, grid, time, postSource, salary, postName, size, page)
        return success(page)
    }


}
