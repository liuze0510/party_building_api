package cc.vv.party.web;


import cc.vv.party.beans.vo.ProjectVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ProjectListParam
import cc.vv.party.param.ProjectSaveParam
import cc.vv.party.service.ProjectService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 项目信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "项目信息", tags = ["项目信息服务端API"], description = "服务端项目信息相关接口")
@RestController
@RequestMapping("/web/project")
class ProjectController : BaseController() {

    @Autowired
    lateinit var projectService: ProjectService

    @OperateLog(content = "编辑项目信息")
    @ApiOperation(value = "编辑项目信息", notes = "编辑项目信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: ProjectSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = projectService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "审核项目信息")
    @ApiOperation(value = "审核项目信息", notes = "审核项目信息")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "项目编号", required = true),
                ApiImplicitParam(name = "state", value = "审核状态 0 通过 1 不通过", required = true),
                ApiImplicitParam(name = "reason", value = "理由", required = true)
            ]
    )
    @PostMapping("/check")
    fun check(@RequestParam id: String, @RequestParam state: Int, @RequestParam reason: String): ResponseEntityWrapper<Boolean> {
        var entity = projectService.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        entity.applyType = Integer(0)
        entity.checkState = Integer(state)
        entity.applyReson = reason
        if(entity.sysState!!.toInt() == 1){
            entity.sysState = Integer(0)
        }
        return success(entity.updateById())
    }

    @OperateLog(content = "单个删除项目信息")
    @ApiOperation(value = "单个删除项目信息", notes = "单个删除项目信息")
    @ApiImplicitParam(name = "id", value = "项目编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteProject(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = projectService.deleteProject(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看项目详情")
    @ApiOperation(value = "获取项目信息", notes = "获取项目信息")
    @ApiImplicitParam(name = "id", value = "项目编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ProjectVO> {
        var vo = projectService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看项目信息列表")
    @ApiOperation(value = "获取项目信息列表", notes = "获取项目信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true)),
                (ApiImplicitParam(name = "type", value = "类型 0 未审核 1 已审核", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}/{type}")
    fun listPage(@RequestBody param: ProjectListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int,
                 @PathVariable type: Int): ResponseEntityWrapper<PageWrapper<ProjectVO>> {
        var page = projectService.listPage(param, size, page, type)
        return success(page)
    }

    @OperateLog(content = "查看预警项目信息列表")
    @ApiOperation(value = "获取预警项目信息列表", notes = "获取预警项目信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/warn-list/{page}/{size}")
    fun findWarnListPage(@RequestBody param: ProjectListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ProjectVO>> {
        var page = projectService.findWarnListPage(param, size, page)
        return success(page)
    }

    @ApiOperation(value = "取消申请", notes = "项目延期、取消申请")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "项目编号", required = true),
                ApiImplicitParam(name = "type", value = "申请类型 2 延期 3 取消", required = true),
                ApiImplicitParam(name = "reason", value = "理由", required = true)
            ]
    )
    @GetMapping("/pro-apply")
    fun projectApply(@RequestParam id: String, @RequestParam type: Int, @RequestParam reason: String): ResponseEntityWrapper<Boolean> {
        var sysState: Int = if(type == 2){ 1 } else { 3 }
        projectService.updateProjectApply(id, type, reason, sysState)
        return success(true)
    }

}
