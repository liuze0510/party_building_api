package cc.vv.party.web;


import cc.vv.party.beans.vo.DemandListVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.DemandListListParam
import cc.vv.party.param.DemandListEditParam
import cc.vv.party.service.DemandListService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 需求清单 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "需求清单", tags = ["需求清单服务端API"], description = "服务端需求清单相关接口")
@RestController
@RequestMapping("/web/demand-list")
class DemandListController : BaseController() {

    @Autowired
    lateinit var demandListService: DemandListService

    @OperateLog(content = "编辑需求清单信息")
    @ApiOperation(value = "编辑需求清单信息", notes = "编辑需求清单信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: DemandListEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = demandListService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除需求清单信息")
    @ApiOperation(value = "删除需求清单信息", notes = "删除需求清单信息")
    @ApiImplicitParam(name = "id", value = "需求清单编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteDemandList(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = demandListService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看需求清单详情")
    @ApiOperation(value = "获取需求清单信息", notes = "获取需求清单信息")
    @ApiImplicitParam(name = "id", value = "需求清单编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemandListVO> {
        var vo = demandListService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看需求清单列表")
    @ApiOperation(value = "获取需求清单列表", notes = "获取需求清单列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: DemandListListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<DemandListVO>> {
        var page = demandListService.listPage(param, size, page)
        return success(page)
    }

}
