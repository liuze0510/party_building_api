package cc.vv.party.web;


import cc.vv.party.beans.vo.CommittedVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommittedEditParam
import cc.vv.party.service.CommittedService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 承诺信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "承诺", tags = ["承诺信息务端API"], description = "服务端承诺信息相关接口")
@RestController
@RequestMapping("/web/committed")
class CommittedController : BaseController() {
    
    @Autowired
    lateinit var committedService: CommittedService

    @OperateLog(content = "编辑承诺信息")
    @ApiOperation(value = "编辑承诺信息", notes = "编辑承诺信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: CommittedEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = committedService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除承诺信息")
    @ApiOperation(value = "单个删除承诺信息", notes = "单个删除承诺信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = committedService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看承诺信息列表详情")
    @ApiOperation(value = "获取承诺信息", notes = "获取承诺信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommittedVO> {
        var vo = committedService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看承诺信息列表")
    @ApiOperation(value = "获取承诺信息列表", notes = "获取承诺信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "time", value = "承诺时间", required = false)),
                (ApiImplicitParam(name = "mobile", value = "联系方式", required = false)),
                (ApiImplicitParam(name = "type", value = "承诺类型 0 单位 1 党员", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) mobile: String?,
                 @RequestParam(required = false) type: Int,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommittedVO>> {
        var page = committedService.listPage(street, community, grid, time, mobile, type, size, page)
        return success(page)
    }
    
}
