package cc.vv.party.web;


import cc.vv.party.beans.vo.SocietyOrgDynamicVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.SocietyOrgDynamicEditParam
import cc.vv.party.service.SocietyOrgDynamicService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社会组织动态 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社会组织动态", tags = ["社会组织动态务端API"], description = "服务端社会组织动态相关接口")
@RestController
@RequestMapping("/web/society-org-dynamic")
class SocietyOrgDynamicController : BaseController() {

    @Autowired
    lateinit var societyOrgDynamicService: SocietyOrgDynamicService


    @OperateLog(content = "编辑社会组织动态")
    @ApiOperation(value = "编辑社会组织动态", notes = "编辑社会组织动态")
    @PostMapping("/edit")
    fun edit(@RequestBody param: SocietyOrgDynamicEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = societyOrgDynamicService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社会组织动态")
    @ApiOperation(value = "单个删除社会组织动态", notes = "单个删除社会组织动态")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = societyOrgDynamicService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社会组织动态详情")
    @ApiOperation(value = "获取社会组织动态", notes = "获取社会组织动态")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<SocietyOrgDynamicVO> {
        var vo = societyOrgDynamicService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社会组织动态列表")
    @ApiOperation(value = "获取社会组织动态列表", notes = "获取社会组织动态列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "time", value = "发布时间", required = false)),
                (ApiImplicitParam(name = "releaseOrg", value = "发布组织", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "type", value = "承诺类型 0 单位 1 党员", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) releaseOrg: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<SocietyOrgDynamicVO>> {
        var page = societyOrgDynamicService.listPage(time, releaseOrg, title, size, page)
        return success(page)
    }


}
