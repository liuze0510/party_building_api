package cc.vv.party.web;


import cc.vv.party.beans.model.VolunteerProjectSignUp
import cc.vv.party.beans.vo.VolunteerProjectSignUpVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.VolunteerProjectSignUpSaveParam
import cc.vv.party.service.VolunteerProjectSignUpService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿项目报名信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿项目报名信息", tags = ["志愿项目报名信息服务端API"], description = "服务端志愿项目报名信息相关接口")
@RestController
@RequestMapping("/web/volunteer-project-sign-up")
class VolunteerProjectSignUpController : BaseController(){

    @Autowired
    lateinit var volunteerProjectSignUpService: VolunteerProjectSignUpService

    @OperateLog(content = "添加志愿项目报名信息")
    @ApiOperation(value = "保存志愿项目报名信息", notes = "保存志愿项目报名信息")
    @PostMapping("/save")
    fun save(@RequestBody param: VolunteerProjectSignUpSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = volunteerProjectSignUpService.save(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "查看志愿项目报名信息")
    @ApiOperation(value = "查看志愿项目报名信息", notes = "查看志愿项目报名信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "projectId", value = "项目名称", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam projectId: String,
             @PathVariable size: Int,
             @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<VolunteerProjectSignUpVO>> {
        var pages = volunteerProjectSignUpService.selectPage(Page(page, size), EntityWrapper<VolunteerProjectSignUp>().eq("volunteer_project_id", projectId))
        return success(pages.wrapper())
    }


}
