package cc.vv.party.web;


import cc.vv.party.beans.model.Log
import cc.vv.party.beans.vo.LogVO
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.LogService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import commonx.core.date.toDate
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

/**
 * <p>
 * 日誌 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/log")
@Api(value = "系统操作日志", tags = ["系统操作日志服务端API"], description = "系统操作日志服务端API-服务端系统操作日志相关接口")
class LogController : BaseController() {

    @Autowired
    lateinit var logService: LogService

    @OperateLog(content = "查看操作日志列表")
    @ApiOperation("获取操作日志列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "page", value = "当前页", type = "int", required = true),
            ApiImplicitParam(name = "pageSize", value = "分页大小", type = "int", required = true),
            ApiImplicitParam(
                name = "searchType",
                value = "搜索类型(1:账号,2:名称，3:操作时间，4:操作内容)",
                type = "int",
                required = false
            ),
            ApiImplicitParam(name = "start", value = "起始时间", type = "string", required = false),
            ApiImplicitParam(name = "end", value = "结束时间", type = "string", required = false),
            ApiImplicitParam(name = "keyword", value = "关键字", type = "string", required = false)
        ]
    )
    @GetMapping("/list")
    fun list(
        @RequestParam page: Int, @RequestParam pageSize: Int,
        @RequestParam(required = false) searchType: Int?,
        @RequestParam(required = false) start: String?,
        @RequestParam(required = false) end: String?,
        @RequestParam(required = false) keyword: String?
    ): ResponseEntityWrapper<PageWrapper<LogVO>> {
        val wrapper = EntityWrapper<Log>()
        if (searchType != null) {
            when (searchType) {
                1 -> {
                    wrapper.like("operate_account", keyword)
                }
                2 -> {
                    wrapper.like("operate_name", keyword)
                }
                3 -> {
                    if (start.isNullOrEmpty()) throw BizException(
                        StatusCode.MISSING_REQUIRE_FIELD.statusCode,
                        "请输入起始时间"
                    )
                    if (!end.isNullOrEmpty()) {
                        if (start!!.toDate("yyyy-MM-dd").time > end!!.toDate("yyyy-MM-dd").time) {
                            throw BizException(StatusCode.FIELD_VALUE_ERROR.statusCode, "起始时间必须小于结束时间 ")
                        }
                    }
                    wrapper.between("create_time", start, end)
                }
                4 -> {
                    wrapper.like("operate_content", keyword)
                }
            }
        }
        wrapper.orderBy("create_time", false)
        return success(logService.selectPage(Page(page, pageSize), wrapper).wrapper())
    }
}
