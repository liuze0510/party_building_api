package cc.vv.party.web;


import cc.vv.party.beans.vo.MicroDonationVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.MicroDonationListParam
import cc.vv.party.param.MicroDonationSaveParam
import cc.vv.party.service.MicroDonationService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 微捐赠 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "微捐助", tags = ["微捐赠服务端API"], description = "微捐赠服务端API-服务端微捐助相关接口")
@RestController
@RequestMapping("/web/micro-donation")
class MicroDonationController : BaseController(){

    @Autowired
    lateinit var microDonationService: MicroDonationService

    @OperateLog(content = "编辑捐赠信息")
    @ApiOperation(value = "保存捐赠信息", notes = "保存捐赠信息")
    @PostMapping("/save")
    fun save(@RequestBody param: MicroDonationSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = microDonationService.save(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "审核捐赠信息")
    @ApiOperation(value = "审核捐赠信息", notes = "审核捐赠信息")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "捐赠编号", required = true),
                ApiImplicitParam(name = "state", value = "审核状态 0 通过 1 不通过", required = true),
                ApiImplicitParam(name = "reason", value = "审核不通过理由")
            ]
    )
    @GetMapping("/check/{id}/{state}")
    fun checkMicroDonation(@PathVariable id: String, @PathVariable state: Int, @RequestParam(required = false) reason: String?): ResponseEntityWrapper<Boolean> {
        var isCheck = microDonationService.checkMicroDonation(id, state, reason)
        return success(isCheck)
    }

    @OperateLog(content = "删除捐赠信息")
    @ApiOperation(value = "删除捐赠信息", notes = "删除捐赠信息")
    @ApiImplicitParam(name = "id", value = "捐赠编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteMicroDonation(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = microDonationService.deleteMicroDonation(id)
        return success(isDelete)
    }


    @OperateLog(content = "查看捐赠详情")
    @ApiOperation(value = "获取捐赠信息", notes = "获取捐赠信息")
    @ApiImplicitParam(name = "id", value = "捐赠编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<MicroDonationVO>{
        var vo = microDonationService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看微捐赠列表")
    @ApiOperation(value = "获取微捐赠列表", notes = "获取微捐赠列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: MicroDonationListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MicroDonationVO>> {
        var page = microDonationService.listPage(param, size, page)
        return success(page)
    }



}



