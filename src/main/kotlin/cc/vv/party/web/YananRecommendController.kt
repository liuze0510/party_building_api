package cc.vv.party.web;


import cc.vv.party.beans.model.AppVersion
import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.model.YananRecommend
import cc.vv.party.beans.vo.BannerVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.BannerParam
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * 延安推荐 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/recommend")
@Api(value = "延安推荐管理", tags = ["延安推荐管理服务端API"], description = "服务端延安推荐管理相关接口")
class YananRecommendController : BaseController() {

    @OperateLog(content = "添加推荐信息")
    @ApiOperation("添加推荐信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "type", value = "类型 0 支部活动 1 三会一课", required = true)),
                (ApiImplicitParam(name = "associaId", value = "关联编号", required = true))
            ]
    )
    @PostMapping("/save")
    fun save(@RequestParam type: Int, @RequestParam associaId: String): ResponseEntityWrapper<Boolean> {
        var entity = YananRecommend()
        entity.type = Integer(type)
        entity.associaId = associaId
        entity.createTime = Date()
        return success(entity.insert())
    }

    @OperateLog(content = "删除推荐信息")
    @ApiOperation("删除推荐信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "type", value = "类型 0 支部活动 1 三会一课", required = true)),
                (ApiImplicitParam(name = "associaId", value = "关联编号", required = true))
            ]
    )
    @PostMapping("/delete")
    fun delete(@RequestParam type: Int, @RequestParam associaId: String): ResponseEntityWrapper<Boolean> {
        return success(YananRecommend().delete(EntityWrapper<YananRecommend>().where("type = {0}", type).and("associa_id = {0}", associaId)))
    }
}
