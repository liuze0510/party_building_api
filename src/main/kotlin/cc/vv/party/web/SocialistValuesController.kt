package cc.vv.party.web;


import cc.vv.party.beans.vo.SocialistValuesVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.SocialistValuesEditParam
import cc.vv.party.service.SocialistValuesService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社会主义核心价值观 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "社会主义核心价值观", tags = ["社会主义核心价值观服务端API"], description = "服务端社会主义核心价值观相关接口")
@RestController
@RequestMapping("/web/socialist-values")
class SocialistValuesController : BaseController() {

    @Autowired
    lateinit var socialistValuesService: SocialistValuesService

    @OperateLog(content = "编辑社会主义核心价值观信息")
    @ApiOperation(value = "编辑社会主义核心价值观信息", notes = "编辑社会主义核心价值观信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: SocialistValuesEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = socialistValuesService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社会主义核心价值观信息")
    @ApiOperation(value = "单个删除社会主义核心价值观信息", notes = "单个删除社会主义核心价值观信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteSocialistValues(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = socialistValuesService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社会主义核心价值观详情")
    @ApiOperation(value = "获取社会主义核心价值观信息", notes = "获取社会主义核心价值观信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<SocialistValuesVO> {
        var vo = socialistValuesService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社会主义核心价值观列表")
    @ApiOperation(value = "获取社会主义核心价值观列表", notes = "获取社会主义核心价值观列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "time", value = "时间", required = false)),
                (ApiImplicitParam(name = "type", value = "类型 0 文明 1 和谐 2 敬业  3 友善 4 诚信", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) type: Integer?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<SocialistValuesVO>> {
        var page = socialistValuesService.listPage(time, type, title, size, page)
        return success(page)
    }


}
