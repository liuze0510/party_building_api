package cc.vv.party.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;

/**
 * <p>
 * 角色资源 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@RestController
@RequestMapping("//role-permission")
class RolePermissionController : BaseController()
