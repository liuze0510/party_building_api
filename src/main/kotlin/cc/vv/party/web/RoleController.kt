package cc.vv.party.web;


import cc.vv.party.beans.model.Role
import cc.vv.party.beans.vo.RoleTypeVO
import cc.vv.party.beans.vo.RoleVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.enums.RoleType
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.RoleParam
import cc.vv.party.service.RoleService
import cc.vv.party.service.UserRoleService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.constraints.NotEmpty
import javax.xml.crypto.KeySelector

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
@Validated
@RestController
@RequestMapping("/web/role")
@Api(value = "角色", tags = ["角色服务端API"], description = "角色服务端API-服务端角色相关接口")
class RoleController : BaseController() {

    @Autowired
    lateinit var roleService: RoleService

    @ApiOperation("获取角色列表")
    @GetMapping("/list")
    fun list(): ResponseEntityWrapper<List<Role>> {
        return success(roleService.selectList(EntityWrapper<Role>()))
    }

    @ApiOperation("添加角色")
    @OperateLog("添加角色")
    @PostMapping("/add")
    fun save(@Validated @RequestBody param: RoleParam): ResponseEntityWrapper<*> {
        roleService.addRole(param, getCurrentUserId())
        return success(null)
    }

    @ApiOperation("删除角色")
    @OperateLog("删除角色")
    @GetMapping("/delete/{id}")
    fun delete(@NotEmpty(message = "ID不能为空") @PathVariable id: String): ResponseEntityWrapper<*> {
        roleService.deleteRole(id)
        return success(null)
    }

    @ApiOperation("角色详情")
    @OperateLog("查看角色详情")
    @GetMapping("/detail/{id}")
    fun detail(@NotEmpty(message = "ID不能为空") @PathVariable id: String): ResponseEntityWrapper<RoleVO> {
        return success(roleService.detail(id))
    }

    @ApiOperation("更新角色")
    @OperateLog("更新角色")
    @PostMapping("/update/{id}")
    fun update(@NotEmpty(message = "ID不能为空") @PathVariable id: String, @Validated @RequestBody param: RoleParam): ResponseEntityWrapper<*> {
        roleService.updateRole(id, param, getCurrentUserId())
        return success(null)
    }

    @ApiOperation("角色类型列表")
    @OperateLog("角色类型列表")
    @GetMapping("/role-type-list")
    fun roleTypeList(): ResponseEntityWrapper<List<RoleTypeVO>> {
        return success(RoleType.values().filter { it.value != "fgdj" && it.value != "org" && it.value != "member" }.map {
            val vo = RoleTypeVO()
            vo.name = it.description
            vo.value = it.roleName
            vo
        }.toList())
    }
}
