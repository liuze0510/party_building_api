package cc.vv.party.web;


import cc.vv.party.beans.vo.TeamVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.TeamEditParam
import cc.vv.party.service.TeamService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 四支队伍信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍信息", tags = ["四支队伍信息服务端API"], description = "服务端四支队伍信息相关接口")
@RestController
@RequestMapping("/web/team")
class TeamController : BaseController(){

    @Autowired
    lateinit var teamService: TeamService

    @OperateLog(content = "编辑四支队伍信息")
    @ApiOperation(value = "编辑四支队伍信息", notes = "编辑四支队伍信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: TeamEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = teamService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除四支队伍信息")
    @ApiOperation(value = "单个删除四支队伍信息", notes = "单个删除四支队伍信息")
    @ApiImplicitParam(name = "id", value = "四支队伍编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteTeam(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = teamService.deleteTeam(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看四支队伍信息")
    @ApiOperation(value = "获取四支队伍信息", notes = "获取四支队伍信息")
    @ApiImplicitParam(name = "id", value = "四支队伍编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<TeamVO> {
        var vo = teamService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取指定村队伍列表", notes = "获取指定村队伍列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "village", value = "所在村", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/village-team-list/{page}/{size}")
    fun villageTeamList(@RequestParam(required = false) village: String): ResponseEntityWrapper<List<TeamVO>> {
        var page = teamService.villageTeamList(village)
        return success(page)
    }

    @OperateLog(content = "查看四支队伍列表")
    @ApiOperation(value = "获取四支队伍列表", notes = "获取四支队伍列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "town", value = "所在镇", required = false)),
                (ApiImplicitParam(name = "village", value = "所在村", required = false)),
                (ApiImplicitParam(name = "teamType", value = "队伍类型 0 第一书记 1 村两委班子 2 扶贫工作队 3 驻村干部", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) town: String?,
                 @RequestParam(required = false) village: String?,
                 @RequestParam(required = false) teamType: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<TeamVO>> {
        var page = teamService.listPage(town, village, teamType, size, page)
        return success(page)
    }

}
