package cc.vv.party.web;


import cc.vv.party.beans.model.StreetJointMeeting
import cc.vv.party.beans.vo.StreetJointMeetingVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.StreetjointMeetingEditParam
import cc.vv.party.service.StreetJointMeetingService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transferEntries
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.collections.CollectionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 街道联席会议 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "街道联席会议信息", tags = ["街道联席会议信息服务端API"], description = "服务端街道联席会议信息相关接口")
@RestController
@RequestMapping("/web/street-joint-meeting")
class StreetJointMeetingController : BaseController() {

    @Autowired
    lateinit var streetJointMeetingService: StreetJointMeetingService

    @OperateLog(content = "编辑街道联席会议信息")
    @ApiOperation(value = "编辑街道联席会议信息", notes = "编辑街道联席会议信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: StreetjointMeetingEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = streetJointMeetingService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除街道联席会议信息")
    @ApiOperation(value = "单个删除街道联席会议信息", notes = "单个删除街道联席会议信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteStreetJointMeeting(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = streetJointMeetingService.deleteStreetJointMeeting(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看街道联席会议信息")
    @ApiOperation(value = "获取街道联席会议信息", notes = "获取街道联席会议信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<StreetJointMeetingVO> {
        var vo = streetJointMeetingService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看街道联席会议信息列表")
    @ApiOperation(value = "获取街道联席会议信息列表", notes = "获取街道联席会议信息列表")
    @ApiImplicitParam(name = "street", value = "所属街道", required = true)
    @PostMapping(value = "/find-list")
    fun findByStreet(@RequestParam street: String): ResponseEntityWrapper<List<StreetJointMeetingVO>> {
        var list: List<StreetJointMeetingVO>? = null
        var entityList = streetJointMeetingService.selectList(EntityWrapper<StreetJointMeeting>().eq("street", street))
        if(CollectionUtils.isNotEmpty(entityList)){
            list = entityList.transferEntries<StreetJointMeetingVO>() as ArrayList
        }
        return success(list)
    }

    @OperateLog(content = "查看街道联席会议信息列表")
    @ApiOperation(value = "获取街道联席会议信息列表", notes = "获取街道联席会议信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "所属街道", required = false)),
                (ApiImplicitParam(name = "meetingName", value = "会议名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) meetingName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<StreetJointMeetingVO>> {
        var page = streetJointMeetingService.listPage(street, meetingName, size, page)
        return success(page)
    }


}
