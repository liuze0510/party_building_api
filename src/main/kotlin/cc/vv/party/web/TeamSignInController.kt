package cc.vv.party.web;


import cc.vv.party.beans.vo.SignInVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.TeamSignInService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 四支队伍签到信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "四支队伍签到信息", tags = ["四支队伍签到信息服务端API"], description = "服务端四支队伍签到信息相关接口")
@RestController
@RequestMapping("/web/team-sign-in")
class TeamSignInController : BaseController(){

    @Autowired
    lateinit var teamSignInService: TeamSignInService

    @OperateLog(content = "查看四支队伍签到信息")
    @ApiOperation(value = "获取四支队伍签到信息", notes = "获取四支队伍签到信息")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "teamId", value = "队伍编号", required = true),
                ApiImplicitParam(name = "time", value = "签到时间 格式yyyy-MM-dd", required = true)
            ]
    )
    @GetMapping("/info/{teamId}")
    fun info(@PathVariable teamId: String, @RequestParam time: String): ResponseEntityWrapper<SignInVO> {
        var vo = teamSignInService.info(teamId, time)
        return success(vo)
    }


}
