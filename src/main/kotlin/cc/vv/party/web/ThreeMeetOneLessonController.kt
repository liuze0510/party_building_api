package cc.vv.party.api;


import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.param.ThreeMeetOneLessonEditParam
import cc.vv.party.service.ThreeMeetOneLessonService
import commonx.core.date.dateFormat
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 三会一课 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "三会一课", tags = ["三会一课信息务端API"], description = "服务端三会一课信息相关接口")
@RestController
@RequestMapping("/web/three-meet-one-lesson")
class ThreeMeetOneLessonController : BaseController() {

    @Autowired
    lateinit var threeMeetOneLessonService: ThreeMeetOneLessonService

    @ApiOperation(value = "获取三会一课信息", notes = "获取三会一课信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ThreeMeetOneLessonVO> {
        val vo = threeMeetOneLessonService.info(id)
        return success(vo)
    }

    @ApiOperation(value = "获取三会一课信息列表", notes = "获取三会一课信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "startDate", value = "起始时间", required = false)),
            (ApiImplicitParam(name = "endDate", value = "结束时间", required = false)),
            (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
            (ApiImplicitParam(name = "title", value = "活动标题", required = false)),
            (ApiImplicitParam(name = "branchId", value = "支部Id", required = false)),
            (ApiImplicitParam(name = "branchType", value = "党支部类型", required = false)),
            (ApiImplicitParam(name = "pageSize", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{pageSize}")
    fun listPage(
        @RequestParam(required = false) startDate: String?,
        @RequestParam(required = false) endDate: String?,
        @RequestParam(required = false) type: String?,
        @RequestParam(required = false) title: String?,
        @RequestParam(required = false, defaultValue = SysConsts.REGION_ID) branchId: String,
        @RequestParam(required = false, defaultValue = SysConsts.BranchType.REGION_LEVEL_DISTRICT) branchType: String,
        @PathVariable pageSize: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<ThreeMeetOneLessonVO>> {
        val start = if (startDate != null && startDate != "") {
            startDate.toLong().dateFormat("yyyy-MM-dd")
        } else {
            startDate
        }
        val end = if (endDate != null && endDate != "") {
            endDate.toLong().dateFormat("yyyy-MM-dd")
        } else {
            endDate
        }
        return success(
            threeMeetOneLessonService.listPage(
                startDate = start,
                endDate = end,
                type = type,
                title = title,
                branchId = branchId,
                branchType = branchType,
                page = page,
                pageSize = pageSize
            )
        )
    }

}
