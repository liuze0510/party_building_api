package cc.vv.party.web;


import cc.vv.party.beans.vo.JobHuntingUserVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.JobHuntingUserEditParam
import cc.vv.party.service.JobHuntingUserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 求职信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "求职信息", tags = ["求职信息服务端API"], description = "服务端求职信息相关接口")
@RestController
@RequestMapping("/web/job-hunting-user")
class JobHuntingUserController : BaseController(){

    @Autowired
    lateinit var jobHuntingUserService: JobHuntingUserService

    @OperateLog(content = "编辑求职信息")
    @ApiOperation(value = "编辑求职信息", notes = "编辑求职信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: JobHuntingUserEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = jobHuntingUserService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除求职信息")
    @ApiOperation(value = "单个删除求职信息", notes = "单个删除求职信息")
    @ApiImplicitParam(name = "id", value = "求职编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteJobHuntingUser(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = jobHuntingUserService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "批量删除求职信息")
    @ApiOperation(value = "批量删除求职信息", notes = "批量删除求职信息")
    @ApiImplicitParam(name = "ids", value = "求职编号集合", required = true, dataType = "Array")
    @PostMapping("/delete")
    fun deleteBatchJobHuntingUser(@RequestBody ids: List<String>): ResponseEntityWrapper<Boolean> {
        var isDelete = jobHuntingUserService.deleteBatchIds(ids)
        return success(isDelete)
    }

    @OperateLog(content = "查看求职详细信息")
    @ApiOperation(value = "获取求职信息", notes = "获取求职信息")
    @ApiImplicitParam(name = "id", value = "求职编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<JobHuntingUserVO> {
        var vo = jobHuntingUserService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看求职列表")
    @ApiOperation(value = "获取求职列表", notes = "获取求职列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "grid", value = "网格", required = false)),
                (ApiImplicitParam(name = "sex", value = "性别", required = false)),
                (ApiImplicitParam(name = "education", value = "学历", required = false)),
                (ApiImplicitParam(name = "workYear", value = "工作年限", required = false)),
                (ApiImplicitParam(name = "name", value = "求职者姓名", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) grid: String?,
                 @RequestParam(required = false) sex: Integer?,
                 @RequestParam(required = false) education: String?,
                 @RequestParam(required = false) workYear: Integer?,
                 @RequestParam(required = false) name: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<JobHuntingUserVO>> {
        var page = jobHuntingUserService.listPage(street, community, grid, sex, education, workYear, name, null, size, page)
        return success(page)
    }


}
