package cc.vv.party.web;


import cc.vv.party.beans.vo.UnitEvaluationStarVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.UnitEvaluationStarEditParam
import cc.vv.party.service.UnitEvaluationStarService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 单位评星晋阶 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "单位评星晋阶", tags = ["单位评星晋阶信息务端API"], description = "服务端单位评星晋阶信息相关接口")
@RestController
@RequestMapping("/web/unit-evaluation-star")
class UnitEvaluationStarController : BaseController() {

    @Autowired
    lateinit var unitEvaluationStarService: UnitEvaluationStarService


    @OperateLog(content = "编辑项目阶段信息")
    @ApiOperation(value = "编辑信息", notes = "编辑信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: UnitEvaluationStarEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = unitEvaluationStarService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除项目阶段信息")
    @ApiOperation(value = "删除信息", notes = "删除信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteUnitEvaluationStar(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = unitEvaluationStarService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看项目阶段详情")
    @ApiOperation(value = "获取详情信息", notes = "获取详情信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<UnitEvaluationStarVO> {
        var vo = unitEvaluationStarService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看项目阶段列表")
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "unitName", value = "单位名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) unitName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<UnitEvaluationStarVO>> {
        var page = unitEvaluationStarService.listPage(unitName, size, page)
        return success(page)
    }



}
