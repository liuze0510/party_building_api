package cc.vv.party.web;


import cc.vv.party.beans.vo.MessageBoardReplyVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.MessageBoardReplyParam
import cc.vv.party.service.MessageBoardReplyService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 留言回复 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-23
 */
@Api(value = "留言回复", tags = ["留言回复服务端API"], description = "留言回复服务端API-服务端留言回复相关接口")
@RestController
@RequestMapping("/web/message-board-reply")
class MessageBoardReplyController : BaseController() {

    @Autowired
    lateinit var messageBoardReplyService: MessageBoardReplyService

    @OperateLog(content = "添加留言回复信息")
    @ApiOperation(value = "保存留言回复信息", notes = "保存留言回复信息")
    @PostMapping("/save")
    fun save(@RequestBody param: MessageBoardReplyParam): ResponseEntityWrapper<Boolean> {
        var isAdd = messageBoardReplyService.save(param, getCurrentUserInfo())
        return success(isAdd)
    }

    @OperateLog(content = "删除留言回复信息")
    @ApiOperation(value = "删除留言回复信息", notes = "删除留言回复信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun delete(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = messageBoardReplyService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看微留言回复列表")
    @ApiOperation(value = "获取微留言回复列表", notes = "获取微留言回复列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "留言编号", required = true),
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam id: String, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<MessageBoardReplyVO>> {
        var page = messageBoardReplyService.listPage(id, size, page)
        return success(page)
    }


}
