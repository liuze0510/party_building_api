package cc.vv.party.api;


import cc.vv.party.beans.vo.RedPartyBuildingVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.RedPartyBuildingEditParam
import cc.vv.party.service.RedPartyBuildingService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 红领党建 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Api(value = "红领党建", tags = ["红领党建信息务端API"], description = "服务端红领党建信息相关接口")
@RestController
@RequestMapping("/web/red-party-building")
class RedPartyBuildingController : BaseController(){

    @Autowired
    lateinit var redPartyBuildingService: RedPartyBuildingService

    @OperateLog(content = "编辑红领党建信息")
    @ApiOperation(value = "编辑红领党建信息", notes = "编辑红领党建信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: RedPartyBuildingEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = redPartyBuildingService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除红领党建信息")
    @ApiOperation(value = "单个删除红领党建信息", notes = "单个删除红领党建信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteRedPartyBuilding(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = redPartyBuildingService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看红领党建信息详情")
    @ApiOperation(value = "获取红领党建信息", notes = "获取红领党建信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<RedPartyBuildingVO> {
        var vo = redPartyBuildingService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看红领党建信息列表")
    @ApiOperation(value = "获取红领党建信息列表", notes = "获取红领党建信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "type", value = "类型: 0 品牌简介 1 六大工程 2 组织引领 3 党员引领 4 文化引领 5 标准引领", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) title: String?,
                 @RequestParam(required = false) type: Integer?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<RedPartyBuildingVO>> {
        var page = redPartyBuildingService.listPage(title, type, size, page)
        return success(page)
    }



}
