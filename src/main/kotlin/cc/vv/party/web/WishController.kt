package cc.vv.party.web;


import cc.vv.party.beans.vo.WishVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.WishListParam
import cc.vv.party.param.WishSaveParam
import cc.vv.party.service.WishService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 微心愿 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "微心愿", tags = ["微心愿服务端API"], description = "服务端微心愿相关接口")
@RestController
@RequestMapping("/web/wish")
class WishController : BaseController(){

    @Autowired
    lateinit var wishService: WishService

    @OperateLog(content = "编辑微心愿信息")
    @ApiOperation(value = "保存微心愿信息", notes = "保存、更新、认领微心愿信息，均用该接口")
    @PostMapping("/save")
    fun save(@RequestBody param: WishSaveParam): ResponseEntityWrapper<Boolean> {
        var isAdd = wishService.save(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "审核微心愿信息")
    @ApiOperation(value = "审核微心愿信息", notes = "审核微心愿信息")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "id", value = "捐赠编号", required = true),
                ApiImplicitParam(name = "state", value = "审核状态 0 通过 1 不通过", required = true),
                ApiImplicitParam(name = "reason", value = "审核不通过理由")
            ]
    )
    @GetMapping("/check/{id}/{state}")
    fun checkWish(@PathVariable id: String, @PathVariable state: Int, @RequestParam(required = false) reason: String?): ResponseEntityWrapper<Boolean> {
        var isCheck = wishService.checkWish(id, state, reason)
        return success(isCheck)
    }

    @OperateLog(content = "单个删除微心愿信息")
    @ApiOperation(value = "删除微心愿信息", notes = "删除微心愿信息")
    @ApiImplicitParam(name = "id", value = "微心愿编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteWish(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = wishService.deleteWish(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看微心愿信息")
    @ApiOperation(value = "获取微心愿信息", notes = "获取微心愿信息")
    @ApiImplicitParam(name = "id", value = "微心愿编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<WishVO>{
        var vo = wishService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看微心愿信息列表")
    @ApiOperation(value = "获取微心愿列表", notes = "获取微心愿列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: WishListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<WishVO>> {
        var page = wishService.listPage(param, size, page)
        return success(page)
    }


}
