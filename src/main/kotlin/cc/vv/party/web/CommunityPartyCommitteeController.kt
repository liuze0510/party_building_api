package cc.vv.party.web;


import cc.vv.party.beans.model.CommunityPartyCommittee
import cc.vv.party.beans.vo.CommunityPartyCommitteeVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommunityPartyCommitteeEditParam
import cc.vv.party.service.CommunityPartyCommitteeService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区大党委  前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区大党委", tags = ["社区大党委服务端API"], description = "服务端社区大党委相关接口")
@RestController
@RequestMapping("/web/community-party-committee")
class CommunityPartyCommitteeController : BaseController() {

    @Autowired
    lateinit var communityPartyCommitteeService: CommunityPartyCommitteeService

    @OperateLog(content = "编辑社区大党委信息")
    @ApiOperation(value = "编辑社区大党委信息", notes = "编辑社区大党委信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: CommunityPartyCommitteeEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = communityPartyCommitteeService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社区大党委信息")
    @ApiOperation(value = "单个删除社区大党委信息", notes = "单个删除社区大党委信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommunityPartyCommittee(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = communityPartyCommitteeService.deleteCommunityPartyCommittee(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社区大党委详情")
    @ApiOperation(value = "获取社区大党委信息", notes = "获取社区大党委信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityPartyCommitteeVO> {
        var vo = communityPartyCommitteeService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区大党委信息列表")
    @ApiOperation(value = "获取社区大党委信息列表", notes = "获取社区大党委信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "street", value = "街道", required = false)),
                (ApiImplicitParam(name = "community", value = "社区", required = false)),
                (ApiImplicitParam(name = "name", value = "党委名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) street: String?,
                 @RequestParam(required = false) community: String?,
                 @RequestParam(required = false) name: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommunityPartyCommitteeVO>> {
        var page = communityPartyCommitteeService.listPage(street, community, name, size, page)
        return success(page)
    }

    @ApiOperation(value = "获取指定社区的社区大党委信息", notes = "获取指定社区的社区大党委信息")
    @ApiImplicitParam(name = "communityId", value = "社区编号", required = true)
    @PostMapping(value = "/community-party")
    fun getCommunityList(@RequestParam communityId: String): ResponseEntityWrapper<CommunityPartyCommitteeVO> {
        var info = communityPartyCommitteeService.selectOne(EntityWrapper<CommunityPartyCommittee>().where("community = {0}",  communityId))
        if(info == null){
            info = CommunityPartyCommittee()
        }
        return success(info.transfer())
    }
    
    
}
