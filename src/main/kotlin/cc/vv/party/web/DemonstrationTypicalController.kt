package cc.vv.party.web;


import cc.vv.party.beans.vo.DemonstrationTypicalVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.DemonstrationTypicalEditParam
import cc.vv.party.service.DemonstrationTypicalService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 示范典型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@Api(value = "示范典型", tags = ["示范典型信息务端API"], description = "服务端示范典型信息相关接口")
@RestController
@RequestMapping("/web/demonstration-typical")
class DemonstrationTypicalController : BaseController() {

    @Autowired
    lateinit var demonstrationTypicalService: DemonstrationTypicalService

    @OperateLog(content = "编辑示范典型信息")
    @ApiOperation(value = "编辑示范典型信息", notes = "编辑示范典型信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: DemonstrationTypicalEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = demonstrationTypicalService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除示范典型信息")
    @ApiOperation(value = "单个删除示范典型信息", notes = "单个删除示范典型信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteDemonstrationTypical(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = demonstrationTypicalService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看示范典型详情")
    @ApiOperation(value = "获取示范典型信息", notes = "获取示范典型信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemonstrationTypicalVO> {
        var vo = demonstrationTypicalService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看示范典型信息列表")
    @ApiOperation(value = "获取示范典型信息列表", notes = "获取示范典型信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "type", value = "类型 0 农村党组织  1 社区党组织  2 学校党组织  3 机关党组织 4 非公党组织  5 社会组织党组织 6 国家企事业单位党组织", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) type: Integer?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<DemonstrationTypicalVO>> {
        var page = demonstrationTypicalService.listPage(type, title, size, page)
        return success(page)
    }
    
}
