package cc.vv.party.web;


import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.vo.BannerVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.BannerParam
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * banner管理 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/banner")
@Api(value = "Banner管理", tags = ["Banner管理服务端API"], description = "Banner管理服务端API-服务端Banner管理相关接口")
class BannerController : BaseController() {

    @Autowired
    lateinit var bannerService: BannerService

    @OperateLog(content = "查看Banner列表")
    @ApiOperation("获取Banner列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "当前页", name = "page"),
            ApiImplicitParam(value = "页大小", name = "pageSize"),
            ApiImplicitParam(value = "分类，0：党群新闻 1 党建概况  2 宝塔头条  3街道头条  4 社区头条 5 网格头条 ", name = "category")
        ]
    )
    @GetMapping("/list")
    fun list(@RequestParam page: Int, @RequestParam pageSize: Int, @RequestParam category: Int): ResponseEntityWrapper<PageWrapper<BannerVO>> {
        return success(
            bannerService.selectPage(
                Page(page, pageSize),
                EntityWrapper<Banner>().eq("category", category).orderBy("create_time", false)
            ).wrapper()
        )
    }

    @OperateLog(content = "删除Banner信息")
    @ApiOperation("删除Banner信息")
    @ApiImplicitParam(value = "banner id")
    @GetMapping("/delete/{id}")
    fun list(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        return success(bannerService.deleteById(id))
    }

    @OperateLog(content = "查看Banner详情")
    @ApiOperation("Banner详情")
    @ApiImplicitParam(value = "banner id")
    @GetMapping("/detail/{id}")
    fun detail(@PathVariable id: String): ResponseEntityWrapper<BannerVO> {
        return success(bannerService.selectById(id).transfer())
    }

    @OperateLog(content = "编辑Banner信息")
    @ApiOperation("添加、保存Banner")
    @PostMapping("/save")
    fun saveOrUpdate(@RequestBody param: BannerParam): ResponseEntityWrapper<Any> {
        var flag = false;
        if (param.id == null) {
            val banner = param.transfer<Banner>()
            banner.editTime = Date()
            banner.createUser = getCurrentUserId()
            flag = bannerService.insert(banner)
        } else {
            val banner = bannerService.selectById(param.id)
            banner.title = param.title
            banner.url = param.url
            banner.editTime = Date()
            flag = bannerService.updateById(banner)
        }
        return if (flag) success(null) else error(StatusCode.ERROR.statusCode, "操作失败")
    }
}
