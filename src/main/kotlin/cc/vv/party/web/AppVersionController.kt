package cc.vv.party.web;


import cc.vv.party.beans.model.AppVersion
import cc.vv.party.beans.model.Banner
import cc.vv.party.beans.vo.BannerVO

import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.BannerParam
import cc.vv.party.service.BannerService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * <p>
 * App 版本 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/app-version")
@Api(value = "App-Version管理", tags = ["App-Version管理服务端API"], description = "服务端App-Version管理相关接口")
class AppVersionController : BaseController() {

    @OperateLog(content = "添加APP版本信息")
    @ApiOperation("添加APP版本信息")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "version", value = "版本信息", required = true)),
            (ApiImplicitParam(name = "content", value = "版本内容", required = true))
        ]
    )
    @PostMapping("/save")
    fun save(@RequestParam version: String, @RequestParam content: String): ResponseEntityWrapper<Boolean> {
        val entity = AppVersion()
        entity.version = version
        entity.content = content
        entity.createTime = Date()
        return success(entity.insert())
    }

    @ApiOperation("获取最新的版本信息")
    @GetMapping("/last")
    fun last(): ResponseEntityWrapper<AppVersion> {
        val entity = AppVersion().selectOne(EntityWrapper<AppVersion>().orderBy("create_time", false).last("limit 0,1"))
        return success(entity)
    }
}
