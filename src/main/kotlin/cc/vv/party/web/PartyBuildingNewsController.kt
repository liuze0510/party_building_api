package cc.vv.party.web;


import cc.vv.party.beans.model.PartyBuildingNews
import cc.vv.party.beans.vo.OrgVO
import cc.vv.party.beans.vo.PartyBuildingNewsVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.constants.enums.OrgType
import cc.vv.party.common.ext.wrapper
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PartyBuildingNewsParam
import cc.vv.party.service.PartyBuildingNewsService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import com.baomidou.mybatisplus.plugins.Page
import commonx.core.content.transfer
import io.swagger.annotations.*
import org.hibernate.validator.constraints.Range
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.constraints.Max
import javax.validation.constraints.NotEmpty

/**
 * <p>
 * 党建要闻 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/party-building-news")
@Api(value = "党建要闻", tags = ["党建要闻服务端API"], description = "党建要闻服务端API-服务端党建要闻相关接口")
class PartyBuildingNewsController : BaseController() {

    @Autowired
    lateinit var partyBuildingNewsService: PartyBuildingNewsService

    @OperateLog(content = "查看党建要闻列表")
    @ApiOperation("获取党建要闻列表")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(value = "当前页", name = "page", required = true),
            ApiImplicitParam(value = "页大小", name = "pageSize", required = true),
            ApiImplicitParam(
                value = "头条类型, 0 宝塔头条 1 街道头条 2 社区头条 3 网格头条",
                name = "type",
                allowableValues = "range[0, 3]",
                required = true
            ),
            ApiImplicitParam(value = "街道", name = "street", required = false),
            ApiImplicitParam(value = "社区", name = "community", required = false),
            ApiImplicitParam(value = "网格", name = "grid", required = false),
            ApiImplicitParam(value = "发布时间", name = "time", required = false),
            ApiImplicitParam(value = "标题", name = "title", required = false)
        ]
    )
    @GetMapping("/list")
    fun list(
        @RequestParam(required = false) street: String?,
        @RequestParam(required = false) community: String?, @RequestParam(required = false) grid: String?,
        @RequestParam(required = false) title: String?, @RequestParam(required = false) time: String?,
        @Range(
            message = "类型不正确",
            max = 3,
            min = 0
        ) @RequestParam type: Int, @RequestParam page: Int,
        @Range(max = 50, min = 10, message = "分页大小范围为[10,50]") @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<PartyBuildingNewsVO>> {
        return success(partyBuildingNewsService.list(type, page, pageSize, title, time, street, community, grid))
    }

    @OperateLog(content = "党建要闻详情")
    @ApiOperation("党建要闻详情")
    @ApiImplicitParam(value = "新闻Id", name = "id")
    @GetMapping("/detail/{id}")
    fun detail(@PathVariable id: String): ResponseEntityWrapper<PartyBuildingNews> {
        var entity = partyBuildingNewsService.selectById(id)
        return success(entity)
    }

    @OperateLog(content = "编辑党建要问")
    @ApiOperation("添加、保存党建要闻")
    @PostMapping("/save")
    fun saveOrUpdate(@RequestBody param: PartyBuildingNewsParam): ResponseEntityWrapper<Any> {
        val flag: Boolean
        if (param.id == null) {
            val news = param.transfer<PartyBuildingNews>()
            val userInfo = getCurrentUserInfo()
            news.createUser = getCurrentUserId()
            news.publisher = getCurrentUserName()
            news.street = userInfo.street
            news.community = userInfo.community
            news.grid = userInfo.grid
            news.releaseTime = Date()
            flag = partyBuildingNewsService.insert(news)
        } else {
            val news = partyBuildingNewsService.selectById(param.id)
            news.title = param.title
            news.cover = param.cover
            news.content = param.content
            news.source = param.source
            flag = partyBuildingNewsService.updateById(news)
        }
        return if (flag) success(null) else error(StatusCode.ERROR.statusCode, "操作失败")
    }

    @OperateLog(content = "删除党建要闻")
    @ApiOperation("删除党建要闻")
    @GetMapping("/delete/{id}")
    fun delete(@NotEmpty(message = "ID不能为空") @PathVariable id: String): ResponseEntityWrapper<Any> {
        return if (partyBuildingNewsService.deleteById(id)) success(null) else error(
            StatusCode.ERROR.statusCode,
            "删除失败"
        )
    }
}
