package cc.vv.party.api;


import cc.vv.party.beans.vo.PovertyAlleviationVillageActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PovertyAlleviationVillageActivityEditParam
import cc.vv.party.service.PovertyAlleviationVillageActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 脱贫行政村活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
@Api(value = "脱贫行政村活动", tags = ["脱贫行政村活动服务端API"], description = "脱贫行政村活动相关接口")
@RestController
@RequestMapping("/web/poverty-alleviation-village-activity")
class PovertyAlleviationVillageActivityController : BaseController(){
    
    @Autowired
    lateinit var povertyAlleviationVillageActivityService: PovertyAlleviationVillageActivityService

    @OperateLog(content = "编辑脱贫行政村活动信息")
    @ApiOperation(value = "编辑信息", notes = "编辑信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PovertyAlleviationVillageActivityEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = povertyAlleviationVillageActivityService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除脱贫行政村活动信息")
    @ApiOperation(value = "单个删除信息", notes = "单个删除信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePost(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = povertyAlleviationVillageActivityService.deleteById(id)
        return success(isDelete)
    }


    @OperateLog(content = "查看脱贫行政村活动详情")
    @ApiOperation(value = "获取信息", notes = "获取信息")
    @ApiImplicitParam(name = "id", value = "岗位编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PovertyAlleviationVillageActivityVO> {
        var vo = povertyAlleviationVillageActivityService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看脱贫行政村活动列表")
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "town", value = "镇", required = false)),
                (ApiImplicitParam(name = "village", value = "村", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) town: String?,
                 @RequestParam(required = false) village: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PovertyAlleviationVillageActivityVO>> {
        var page = povertyAlleviationVillageActivityService.listPage(town, village, title, size, page)
        return success(page)
    }
    
}
