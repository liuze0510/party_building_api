package cc.vv.party.api;


import cc.vv.party.beans.vo.StreetJointMeetingActivityVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.StreetJointMeetingActivityEditParam
import cc.vv.party.service.StreetJointMeetingActivityService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 街道联席会议活动 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
@Api(value = "街道联席会议活动", tags = ["街道联席会议活动服务端API"], description = "服务端街道联席会议活动相关接口")
@RestController
@RequestMapping("/web/street-joint-meeting-activity")
class StreetJointMeetingActivityController : BaseController(){

    @Autowired
    lateinit var streetJointMeetingActivityService: StreetJointMeetingActivityService

    @OperateLog(content = "编辑街道联席会议活动")
    @ApiOperation(value = "编辑街道联席会议活动", notes = "编辑街道联席会议活动")
    @PostMapping("/edit")
    fun edit(@RequestBody param: StreetJointMeetingActivityEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = streetJointMeetingActivityService.edit(param, getCurrentUserInfo())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除街道联席会议活动")
    @ApiOperation(value = "单个删除街道联席会议活动", notes = "单个删除街道联席会议活动")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = streetJointMeetingActivityService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看街街道联席会议活动")
    @ApiOperation(value = "获取街道联席会议活动", notes = "获取街道联席会议活动")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<StreetJointMeetingActivityVO> {
        var vo = streetJointMeetingActivityService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看街街道联席会议活动列表")
    @ApiOperation(value = "获取街道联席会议活动列表", notes = "获取街道联席会议活动列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "time", value = "活动时间", required = false)),
                (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) time: Long?,
                 @RequestParam(required = false) type: String?,
                 @RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<StreetJointMeetingActivityVO>> {
        var page = streetJointMeetingActivityService.listPage( null, time, type, title, size, page)
        return success(page)
    }


}
