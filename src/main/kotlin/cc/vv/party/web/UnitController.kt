package cc.vv.party.web;


import cc.vv.party.beans.model.Unit
import cc.vv.party.beans.vo.UnitVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.UnitEditParam
import cc.vv.party.param.UnitListParam
import cc.vv.party.service.UnitService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transferEntries
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.collections.CollectionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 单位信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "单位", tags = ["单位信息务端API"], description = "服务端单位信息相关接口")
@RestController
@RequestMapping("/web/unit")
class UnitController : BaseController() {

    @Autowired
    lateinit var unitService: UnitService

    @OperateLog(content = "编辑单位信息")
    @ApiOperation(value = "编辑单位信息", notes = "编辑单位信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: UnitEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = unitService.edit(param, getCurrentUserInfo(), getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除单位信息")
    @ApiOperation(value = "单个删除单位信息", notes = "单个删除单位信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteUnit(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = unitService.deleteById(id)
        return success(isDelete)
    }


    @OperateLog(content = "查看单位信息详情")
    @ApiOperation(value = "获取单位信息", notes = "获取单位信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<UnitVO> {
        var vo = unitService.managerInfo(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区单位列表")
    @ApiOperation(value = "查看社区单位列表", notes = "查看社区单位列表")
    @ApiImplicitParam(name = "size", value = "每页显示数量", required = true)
    @PostMapping(value = "/community-unit-list")
    fun communityUnitList(@RequestParam communityId: String): ResponseEntityWrapper<List<UnitVO>> {
        var list = unitService.selectList(EntityWrapper<Unit>().where("community = {0}", communityId)) ?: emptyList()
        return success(list.transferEntries<UnitVO>() as ArrayList)
    }

    @OperateLog(content = "查看单位信息列表")
    @ApiOperation(value = "获取单位信息列表", notes = "获取单位信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: UnitListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<UnitVO>> {
        var page = unitService.listPage(param, size, page)
        return success(page)
    }

    @ApiOperation(value = "获取指定社区下单位信息列表", notes = "获取指定社区下单位信息列表")
    @ApiImplicitParam(name = "communityId", value = "社区编号", required = true)
    @GetMapping(value = "/community-list")
    fun findCommunityList(@RequestParam communityId: String): ResponseEntityWrapper<List<UnitVO>> {
        var list = java.util.ArrayList<UnitVO>()
        var result = unitService.selectList(EntityWrapper<Unit>().where("community = {0}", communityId))
        if(CollectionUtils.isNotEmpty(result)){
            list = result.transferEntries<UnitVO>() as java.util.ArrayList<UnitVO>
        }
        return success(list)
    }



    

}
