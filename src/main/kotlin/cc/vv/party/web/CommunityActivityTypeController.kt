package cc.vv.party.web;


import cc.vv.party.beans.vo.CommunityActivityTypeVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.CommunityActivityTypeEditParam
import cc.vv.party.service.CommunityActivityTypeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 社区活动类型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社区活动类型", tags = ["社区活动类型务端API"], description = "服务端社区活动类型相关接口")
@RestController
@RequestMapping("/web/community-activity-type")
class CommunityActivityTypeController : BaseController() {

    @Autowired
    lateinit var communityActivityTypeService: CommunityActivityTypeService

    @OperateLog(content = "编辑社区活动类型")
    @ApiOperation(value = "编辑社区活动类型", notes = "编辑社区活动类型")
    @PostMapping("/edit")
    fun edit(@RequestBody param: CommunityActivityTypeEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = communityActivityTypeService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社区活动类型")
    @ApiOperation(value = "单个删除社区活动类型", notes = "单个删除社区活动类型")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = communityActivityTypeService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社区活动类型详情")
    @ApiOperation(value = "获取社区活动类型", notes = "获取社区活动类型")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<CommunityActivityTypeVO> {
        var vo = communityActivityTypeService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社区活动类型列表")
    @ApiOperation(value = "获取社区活动类型列表", notes = "获取社区活动类型列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "typeName", value = "类型名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable typeName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<CommunityActivityTypeVO>> {
        var page = communityActivityTypeService.listPage(typeName, size, page)
        return success(page)
    }


}
