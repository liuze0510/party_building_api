package cc.vv.party.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cc.vv.party.common.base.BaseController;
import cc.vv.party.service.OrgService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired

/**
 * <p>
 * 党建概况 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@RestController
@RequestMapping("/web/introduction")
@Api(value = "简介管理", tags = ["简介管理服务端API"], description = "简介管理服务端API-服务端简介管理相关接口")
class IntroductionController : BaseController() {

    @Autowired
    lateinit var orgService: OrgService


}
