package cc.vv.party.web;


import cc.vv.party.beans.vo.OrgLiFeMeetingVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.OrgLiFeMeetingEditParam
import cc.vv.party.service.OrgLiFeMeetingService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 组织生活会 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Api(value = "组织生活会", tags = ["组织生活会信息务端API"], description = "服务端组织生活会信息相关接口")
@RestController
@RequestMapping("/web/org-life-meeting")
class OrgLiFeMeetingController : BaseController() {

    @Autowired
    lateinit var orgLiFeMeetingService: OrgLiFeMeetingService

    @OperateLog(content = "编辑组织生活会信息")
    @ApiOperation(value = "编辑组织生活会信息", notes = "编辑组织生活会信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: OrgLiFeMeetingEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = orgLiFeMeetingService.edit(param, getCurrentUserId(), getCurrentBranch())
        return success(isAdd)
    }

    @OperateLog(content = "删除组织生活会")
    @ApiOperation(value = "删除组织生活会信息", notes = "删除组织生活会信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun delete(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var vo = orgLiFeMeetingService.deleteById(id)
        return success(vo)
    }

    @OperateLog(content = "查看组织生活会详情")
    @ApiOperation(value = "获取组织生活会信息", notes = "获取组织生活会信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<OrgLiFeMeetingVO> {
        var vo = orgLiFeMeetingService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看组织生活会信息列表")
    @ApiOperation(value = "获取组织生活会信息列表", notes = "获取组织生活会信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "branchType", value = "党组织类型", required = false)),
            (ApiImplicitParam(name = "branchId", value = "党组织Id", required = false)),
            (ApiImplicitParam(name = "time", value = "组织生活会时间", required = false)),
            (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
            (ApiImplicitParam(name = "title", value = "活动标题", required = false)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(
        @RequestParam(required = false) branchType: String?,
        @RequestParam(required = false) branchId: String?,
        @RequestParam(required = false) time: Long?,
        @RequestParam(required = false) type: String?,
        @RequestParam(required = false) title: String?,
        @PathVariable size: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<OrgLiFeMeetingVO>> {
        val result = orgLiFeMeetingService.listPage(
            branchType = branchType,
            branchId = branchId,
            title = title,
            time = time,
            type = type,
            size = size,
            page = page,
            currentBranchInfoVO = getCurrentBranch()
        )
        return success(result)
    }

}
