package cc.vv.party.web;


import cc.vv.party.beans.vo.PartyNewsVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PartyNewsEditParam
import cc.vv.party.service.PartyNewsService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 党群新闻 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "党群新闻", tags = ["党群新闻服务端API"], description = "服务端党群新闻相关接口")
@RestController
@RequestMapping("/web/party-news")
class PartyNewsController : BaseController() {

    @Autowired
    lateinit var partyNewsService: PartyNewsService

    @OperateLog(content = "编辑党群新闻")
    @ApiOperation(value = "编辑党群新闻", notes = "编辑党群新闻")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PartyNewsEditParam): ResponseEntityWrapper<Boolean> {
        param.branch = getCurrentBranch().partyBranchName
        var isAdd = partyNewsService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除党群新闻")
    @ApiOperation(value = "单个删除党群新闻", notes = "单个删除党群新闻")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePartyNews(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = partyNewsService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "批量删除党群新闻")
    @ApiOperation(value = "批量删除党群新闻", notes = "批量删除党群新闻")
    @ApiImplicitParam(name = "ids", value = "编号集合", required = true, dataType = "Array")
    @PostMapping("/delete")
    fun deleteBatchPartyNews(@RequestBody ids: List<String>): ResponseEntityWrapper<Boolean> {
        var isDelete = partyNewsService.deleteBatchIds(ids)
        return success(isDelete)
    }

    @OperateLog(content = "查看党群新闻详情")
    @ApiOperation(value = "获取党群新闻", notes = "获取党群新闻")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PartyNewsVO> {
        var vo = partyNewsService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看党群新闻列表")
    @ApiOperation(value = "获取列表", notes = "获取列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) title: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PartyNewsVO>> {
        var page = partyNewsService.listPage(title, size, page)
        return success(page)
    }

}
