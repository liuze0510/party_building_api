package cc.vv.party.web;


import cc.vv.party.beans.vo.DemocraticLifeMeetingVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.DemocraticLifeMeetingEditParam
import cc.vv.party.service.DemocraticLifeMeetingService
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 民主生活会 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
@Api(value = "民主生活会", tags = ["民主生活会信息务端API"], description = "服务端民主生活会信息相关接口")
@RestController
@RequestMapping("/web/democratic-life-meeting")
class DemocraticLifeMeetingController : BaseController() {

    @Autowired
    lateinit var democraticLifeMeetingService: DemocraticLifeMeetingService

    @OperateLog(content = "编辑民主生活会信息")
    @ApiOperation(value = "编辑民主生活会信息", notes = "编辑民主生活会信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: DemocraticLifeMeetingEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = democraticLifeMeetingService.edit(param, getCurrentUserId(), getCurrentBranch())
        return success(isAdd)
    }

    @OperateLog(content = "删除民主生活会")
    @ApiOperation(value = "删除民主生活会信息", notes = "删除民主生活会信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun delete(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var vo = democraticLifeMeetingService.deleteById(id)
        return success(vo)
    }

    @OperateLog(content = "查看民主生活会详情")
    @ApiOperation(value = "获取民主生活会信息", notes = "获取民主生活会信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<DemocraticLifeMeetingVO> {
        var vo = democraticLifeMeetingService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看民主生活会信息列表")
    @ApiOperation(value = "获取民主生活会信息列表", notes = "获取民主生活会信息列表")
    @ApiImplicitParams(
        value = [
            (ApiImplicitParam(name = "branchType", value = "党组织类型", required = false)),
            (ApiImplicitParam(name = "branchId", value = "党组织Id", required = false)),
            (ApiImplicitParam(name = "time", value = "组织生活会时间", required = false)),
            (ApiImplicitParam(name = "type", value = "活动类型", required = false)),
            (ApiImplicitParam(name = "title", value = "活动标题", required = false)),
            (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
            (ApiImplicitParam(name = "page", value = "页码", required = true))
        ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(
        @RequestParam(required = false) branchType: String?,
        @RequestParam(required = false) branchId: String?,
        @RequestParam(required = false) time: Long?,
        @RequestParam(required = false) type: String?,
        @RequestParam(required = false) title: String?,
        @PathVariable size: Int,
        @PathVariable page: Int
    ): ResponseEntityWrapper<PageWrapper<DemocraticLifeMeetingVO>> {
        val result = democraticLifeMeetingService.listPage(
            branchType = branchType,
            branchId = branchId,
            title = title,
            time = time,
            type = type,
            size = size,
            page = page,
            currentBranchInfoVO = getCurrentBranch()
        )
        return success(result)
    }
}
