package cc.vv.party.web;


import cc.vv.party.beans.vo.PovertyAlleviationVillageVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PovertyAlleviationVillageEditParam
import cc.vv.party.service.PovertyAlleviationVillageService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 扶贫行政村信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "扶贫行政村信息", tags = ["扶贫行政村信息服务端API"], description = "服务端扶贫行政村信息相关接口")
@RestController
@RequestMapping("/web/poverty-alleviation-village")
class PovertyAlleviationVillageController : BaseController(){

    @Autowired
    lateinit var povertyAlleviationVillageService: PovertyAlleviationVillageService

    @OperateLog(content = "编辑扶贫行政村信息")
    @ApiOperation(value = "编辑扶贫行政村信息", notes = "编辑扶贫行政村信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PovertyAlleviationVillageEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = povertyAlleviationVillageService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除扶贫行政村信息")
    @ApiOperation(value = "单个删除扶贫行政村信息", notes = "单个删除扶贫行政村信息")
    @ApiImplicitParam(name = "id", value = "扶贫行政村编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePovertyAlleviationVillage(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = povertyAlleviationVillageService.deletePovertyAlleviationVillage(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看扶贫行政村基本信息")
    @ApiOperation(value = "获取扶贫行政村信息", notes = "获取扶贫行政村信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PovertyAlleviationVillageVO> {
        var vo = povertyAlleviationVillageService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看行政村详情")
    @ApiOperation(value = "该方法获取到行政村详情、队伍信息、图片信息", notes = "该方法获取到行政村详情、队伍信息、图片信息")
    @GetMapping("/message/{villageId}")
    fun message(villageId: String): ResponseEntityWrapper<PovertyAlleviationVillageVO> {
        var message = povertyAlleviationVillageService.message(villageId)
        return success(message)
    }

    @ApiOperation(value = "获取扶贫行政子集", notes = "获取扶贫行政子集")
    @ApiImplicitParam(name = "parentId", value = "父编号", required = true)
    @GetMapping("/child")
    fun child(@RequestParam(required = false) parentId: String?): ResponseEntityWrapper<List<PovertyAlleviationVillageVO>> {
        var list = povertyAlleviationVillageService.child(parentId)
        return success(list)
    }

    @OperateLog(content = "查看扶贫行政村树")
    @ApiOperation(value = "获取扶贫行政村树", notes = "获取扶贫行政村树")
    @GetMapping("/tree")
    fun treeList(): ResponseEntityWrapper<List<PovertyAlleviationVillageVO>> {
        var tree = povertyAlleviationVillageService.treeList()
        return success(tree)
    }


    @OperateLog("查看当前用户所在扶贫行政村信息")
    @ApiOperation(value = "获取当前用户所在扶贫行政村信息", notes = "获取当前用户所在扶贫行政村信息")
    @GetMapping("/current-user-village")
    fun getCurrentUserVillage(): ResponseEntityWrapper<PovertyAlleviationVillageVO> {
        var message = povertyAlleviationVillageService.getCurrentUserVillage(getCurrentUserAccount())
        return success(message)
    }
}
