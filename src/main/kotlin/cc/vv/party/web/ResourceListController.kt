package cc.vv.party.web;


import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ResourceListListParam
import cc.vv.party.param.ResourceListEditParam
import cc.vv.party.service.ResourceListService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 资源清单 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "资源清单", tags = ["资源清单服务端API"], description = "服务端资源清单相关接口")
@RestController
@RequestMapping("/web/resource-list")
class ResourceListController : BaseController() {

    @Autowired
    lateinit var resourceListService: ResourceListService

    @OperateLog(content = "编辑资源清单信息")
    @ApiOperation(value = "编辑资源清单信息", notes = "编辑资源清单信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: ResourceListEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = resourceListService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除资源清单信息")
    @ApiOperation(value = "删除资源清单信息", notes = "删除资源清单信息")
    @ApiImplicitParam(name = "id", value = "资源清单编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteResourceList(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = resourceListService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看资源清单详情")
    @ApiOperation(value = "获取资源清单信息", notes = "获取资源清单信息")
    @ApiImplicitParam(name = "id", value = "资源清单编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ResourceListVO> {
        var vo = resourceListService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看单位资源清单详情")
    @ApiOperation(value = "获取单位资源清单详情", notes = "获取单位资源清单详情")
    @ApiImplicitParam(name = "unitId", value = "单位编号", required = true)
    @GetMapping("/unit-resource-list/{unitId}")
    fun unitResourceList(@PathVariable unitId: String): ResponseEntityWrapper<List<ResourceListVO>> {
        var list = resourceListService.getUnitResourceList(unitId)
        return success(list)
    }

    @OperateLog(content = "查看资源清单列表")
    @ApiOperation(value = "获取资源清单列表", notes = "获取资源清单列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: ResourceListListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ResourceListVO>> {
        var page = resourceListService.listPage(param, size, page)
        return success(page)
    }

}
