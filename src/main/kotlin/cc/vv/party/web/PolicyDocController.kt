package cc.vv.party.web;


import cc.vv.party.beans.vo.PolicyDocVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.PolicyDocEditParam
import cc.vv.party.service.PolicyDocService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 政策文件信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "政策文件信息", tags = ["政策文件信息服务端API"], description = "服务端政策文件信息相关接口")
@RestController
@RequestMapping("/web/policy-doc")
class PolicyDocController : BaseController() {

    @Autowired
    lateinit var policyDocService: PolicyDocService

    @OperateLog(content = "编辑政策文件信息")
    @ApiOperation(value = "编辑政策文件信息", notes = "编辑政策文件信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: PolicyDocEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = policyDocService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除政策文件信息")
    @ApiOperation(value = "单个删除政策文件信息", notes = "单个删除政策文件信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deletePolicyDoc(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = policyDocService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看政策文件详情")
    @ApiOperation(value = "获取政策文件信息", notes = "获取政策文件信息")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<PolicyDocVO> {
        var vo = policyDocService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看政策文件信息列表")
    @ApiOperation(value = "获取政策文件信息列表", notes = "获取政策文件信息列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "title", value = "标题", required = false)),
                (ApiImplicitParam(name = "releaseTime", value = "发布时间", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) title: String?,
                 @RequestParam(required = false) releaseTime: Long?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<PolicyDocVO>> {
        var page = policyDocService.listPage(title, releaseTime, size, page)
        return success(page)
    }



}
