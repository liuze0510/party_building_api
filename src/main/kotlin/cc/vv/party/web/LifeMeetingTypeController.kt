package cc.vv.party.web;


import cc.vv.party.beans.vo.LifeMeetingTypeVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.LifeMeetingTypeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 民主、组织生活会类型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
@Api(value = "民主、组织生活会类型", tags = ["民主、组织生活会类型务端API"], description = "服务端民主、组织生活会类型相关接口")
@RestController
@RequestMapping("/web/life-meeting-type")
class LifeMeetingTypeController : BaseController() {

    @Autowired
    lateinit var lifeMeetingTypeService: LifeMeetingTypeService

    @OperateLog(content = "编辑生活会类型详情")
    @ApiOperation(value = "编辑", notes = "编辑")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "id", value = "编号", required = false)),
                (ApiImplicitParam(name = "type", value = "类型 0 民主生活会 1 组织生活会", required = true)),
                (ApiImplicitParam(name = "name", value = "名称", required = true))
            ]
    )
    @PostMapping("/edit")
    fun edit(@RequestParam(value = "id", required = false) id: String?, @RequestParam type: Int, @RequestParam name: String): ResponseEntityWrapper<Boolean> {
        var isAdd = lifeMeetingTypeService.edit(id, type, name, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除生活会类型信息")
    @ApiOperation(value = "删除生活会类型信息", notes = "删除生活会类型信息")
    @ApiImplicitParam(name = "id", value = "服务类型编号", required = true)
    @GetMapping("/delete/{id}")
    fun delete(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = lifeMeetingTypeService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看生活会类型信息")
    @ApiOperation(value = "获取生活会类型信息", notes = "获取生活会类型信息")
    @ApiImplicitParam(name = "id", value = "服务类型编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<LifeMeetingTypeVO> {
        var vo = lifeMeetingTypeService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看生活会类型列表")
    @ApiOperation(value = "获取生活会类型列表", notes = "获取生活会类型列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "type", value = "类型 0 民主生活会 1 组织生活会", required = true)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam type: Int, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<LifeMeetingTypeVO>> {
        var page = lifeMeetingTypeService.listPage(type, size, page)
        return success(page)
    }


    @ApiOperation(value = "获取集合", notes = "获取集合")
    @ApiImplicitParam(name = "type", value = "类型 0 民主生活会 1 组织生活会", required = true)
    @PostMapping(value = "/list-all")
    fun listAll(@RequestParam type: Int): ResponseEntityWrapper<List<LifeMeetingTypeVO>> {
        var page = lifeMeetingTypeService.listAll(type)
        return success(page)
    }

}
