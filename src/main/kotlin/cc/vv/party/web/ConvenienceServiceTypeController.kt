package cc.vv.party.web;


import cc.vv.party.beans.vo.ConvenienceServiceTypeVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.service.ConvenienceServiceTypeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 便民服务类型 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "便民服务类型", tags = ["便民服务类型服务端API"], description = "服务端便民服务类型相关接口")
@RestController
@RequestMapping("/web/convenience-service-type")
class ConvenienceServiceTypeController : BaseController() {

    @Autowired
    lateinit var convenienceServiceTypeService: ConvenienceServiceTypeService

    @OperateLog(content = "编辑服务类型信息")
    @ApiOperation(value = "编辑服务类型信息", notes = "编辑服务类型信息")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "id", value = "服务类型编号", required = false)),
                (ApiImplicitParam(name = "name", value = "服务类型编号", required = true))
            ]
    )
    @PostMapping("/edit")
    fun edit(@RequestParam(value = "id", required = false) id: String?, @RequestParam name: String): ResponseEntityWrapper<Boolean> {
        var isAdd = convenienceServiceTypeService.edit(id, name, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除服务类型信息")
    @ApiOperation(value = "删除服务类型信息", notes = "删除服务类型信息")
    @ApiImplicitParam(name = "id", value = "服务类型编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteConvenienceServiceType(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = convenienceServiceTypeService.deleteConvenienceServiceType(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看便民服务类型详情")
    @ApiOperation(value = "获取服务类型信息", notes = "获取服务类型信息")
    @ApiImplicitParam(name = "id", value = "服务类型编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ConvenienceServiceTypeVO> {
        var vo = convenienceServiceTypeService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看便民服务类型列表")
    @ApiOperation(value = "获取便民服务型列表", notes = "获取便民服务型列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ConvenienceServiceTypeVO>> {
        var page = convenienceServiceTypeService.listPage(size, page)
        return success(page)
    }

    @ApiOperation(value = "获取便民服务型集合", notes = "获取便民服务型集合")
    @PostMapping(value = "/list-all")
    fun listAll(): ResponseEntityWrapper<List<ConvenienceServiceTypeVO>> {
        var page = convenienceServiceTypeService.listAll()
        return success(page)
    }

}
