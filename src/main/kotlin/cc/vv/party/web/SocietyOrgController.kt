package cc.vv.party.web;


import cc.vv.party.beans.model.SocietyOrg
import cc.vv.party.beans.vo.SocietyOrgVO
import cc.vv.party.common.base.BaseController
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.SocietyOrgEditParam
import cc.vv.party.service.SocietyOrgService
import com.baomidou.mybatisplus.mapper.EntityWrapper
import commonx.core.content.transferEntries
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.collections.CollectionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.swing.text.html.parser.Entity

/**
 * <p>
 * 社会组织 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "社会组织", tags = ["社会组织务端API"], description = "服务端社会组织相关接口")
@RestController
@RequestMapping("/web/society-org")
class SocietyOrgController : BaseController() {

    @Autowired
    lateinit var societyOrgService: SocietyOrgService


    @OperateLog(content = "编辑社会组织")
    @ApiOperation(value = "编辑社会组织", notes = "编辑社会组织")
    @PostMapping("/edit")
    fun edit(@RequestBody param: SocietyOrgEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = societyOrgService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除社会组织")
    @ApiOperation(value = "单个删除社会组织", notes = "单个删除社会组织")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteCommitted(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = societyOrgService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看社会组织详情")
    @ApiOperation(value = "获取社会组织", notes = "获取社会组织")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<SocietyOrgVO> {
        var vo = societyOrgService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看社会组织列表")
    @ApiOperation(value = "获取社会组织列表", notes = "获取社会组织列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "community", value = "所在社区", required = false)),
                (ApiImplicitParam(name = "orgName", value = "组织名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) community: String?,
                 @RequestParam(required = false) orgName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<SocietyOrgVO>> {
        var page = societyOrgService.listPage(community, orgName, size, page)
        return success(page)
    }

    @ApiOperation(value = "获取指定社区社会组织列表", notes = "获取指定社区社会组织列表")
    @ApiImplicitParam(name = "communityId", value = "社区编号", required = true)
    @PostMapping(value = "/community-list")
    fun fundCommunityList(@RequestParam communityId: String): ResponseEntityWrapper<List<SocietyOrgVO>> {
        var list = ArrayList<SocietyOrgVO>()
        var result = societyOrgService.selectList(EntityWrapper<SocietyOrg>().where(" community = {0}", communityId))
        if(CollectionUtils.isNotEmpty(result)){
            list = result.transferEntries<SocietyOrgVO>() as ArrayList<SocietyOrgVO>
        }
        return success(list)
    }
    
}
