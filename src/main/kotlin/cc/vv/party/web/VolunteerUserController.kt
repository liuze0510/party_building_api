package cc.vv.party.web;


import cc.vv.party.beans.vo.VolunteerStatisticsVO
import cc.vv.party.beans.vo.VolunteerUserVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.VolunteerUserEditParam
import cc.vv.party.param.VolunteerUserListParam
import cc.vv.party.service.VolunteerUserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿者信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿者信息", tags = ["志愿者信息服务端API"], description = "服务端志愿者信息相关接口")
@RestController
@RequestMapping("/web/volunteer-user")
class VolunteerUserController : BaseController() {

    @Autowired
    lateinit var volunteerUserService: VolunteerUserService

    @OperateLog(content = "编辑志愿者信息")
    @ApiOperation(value = "编辑志愿者信息", notes = "编辑志愿者信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: VolunteerUserEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = volunteerUserService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除志愿者信息")
    @ApiOperation(value = "单个删除志愿者信息", notes = "单个删除志愿者信息")
    @ApiImplicitParam(name = "id", value = "志愿者编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteVolunteerUser(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = volunteerUserService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "批量删除志愿者信息")
    @ApiOperation(value = "批量删除志愿者信息", notes = "批量删除志愿者信息")
    @ApiImplicitParam(name = "ids", value = "志愿者编号集合", required = true, dataType = "Array")
    @PostMapping("/delete")
    fun deleteBatchVolunteerUser(@RequestBody ids: List<String>): ResponseEntityWrapper<Boolean> {
        var isDelete = volunteerUserService.deleteBatchIds(ids)
        return success(isDelete)
    }

    @OperateLog(content = "查看志愿者信息")
    @ApiOperation(value = "获取志愿者信息", notes = "获取志愿者信息")
    @ApiImplicitParam(name = "id", value = "志愿者编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<VolunteerUserVO> {
        var vo = volunteerUserService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看志愿者列表")
    @ApiOperation(value = "获取志愿者列表", notes = "获取志愿者列表")
    @ApiImplicitParams(
            value = [
                ApiImplicitParam(name = "size", value = "每页显示数量", required = true),
                ApiImplicitParam(name = "page", value = "页码", required = true)
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: VolunteerUserListParam,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<VolunteerUserVO>> {
        var page = volunteerUserService.listPage(param, size, page)
        return success(page)
    }


}
