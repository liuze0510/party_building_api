package cc.vv.party.web;


import cc.vv.party.beans.vo.VolunteerProjectVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.VolunteerProjectEditParam
import cc.vv.party.service.VolunteerProjectService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 志愿项目信息 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "志愿项目信息", tags = ["志愿项目信息服务端API"], description = "服务端志愿项目信息相关接口")
@RestController
@RequestMapping("/web/volunteer-project")
class VolunteerProjectController : BaseController() {

    @Autowired
    lateinit var volunteerProjectService: VolunteerProjectService

    @OperateLog(content = "编辑志愿项目信息")
    @ApiOperation(value = "编辑志愿项目信息", notes = "编辑志愿项目信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: VolunteerProjectEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = volunteerProjectService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除志愿项目信息")
    @ApiOperation(value = "单个删除志愿项目信息", notes = "单个删除志愿项目信息")
    @ApiImplicitParam(name = "id", value = "志愿项目编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteVolunteerProject(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = volunteerProjectService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "批量删除志愿项目信息")
    @ApiOperation(value = "批量删除志愿项目信息", notes = "批量删除志愿项目信息")
    @ApiImplicitParam(name = "ids", value = "志愿项目编号集合", required = true, dataType = "Array")
    @PostMapping("/delete")
    fun deleteBatchVolunteerProject(@RequestBody ids: List<String>): ResponseEntityWrapper<Boolean> {
        var isDelete = volunteerProjectService.deleteBatchIds(ids)
        return success(isDelete)
    }

    @OperateLog(content = "查看志愿项目详情")
    @ApiOperation(value = "获取志愿项目信息", notes = "获取志愿项目信息")
    @ApiImplicitParam(name = "id", value = "志愿项目编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<VolunteerProjectVO> {
        var vo = volunteerProjectService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看志愿项目列表")
    @ApiOperation(value = "获取志愿项目列表", notes = "获取志愿项目列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "holdingTime", value = "举办时间", required = false)),
                (ApiImplicitParam(name = "holdingCompany", value = "举办单位", required = false)),
                (ApiImplicitParam(name = "projectName", value = "项目名称", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) holdingTime: Long?,
                 @RequestParam(required = false) holdingCompany: String?,
                 @RequestParam(required = false) projectName: String?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<VolunteerProjectVO>> {
        var page = volunteerProjectService.listPage(holdingTime, holdingCompany, projectName, size, page)
        return success(page)
    }

}
