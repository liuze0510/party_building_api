package cc.vv.party.web;


import cc.vv.party.beans.vo.NotPartyUnitVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.NotPartyUnitEditParam
import cc.vv.party.service.NotPartyUnitService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 非公党建单位 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "非公党建单位", tags = ["非公党建单位服务端API"], description = "服务端非公党建单位相关接口")
@RestController
@RequestMapping("/web/not-party-unit")
class NotPartyUnitController : BaseController() {
    
    @Autowired
    lateinit var notPartyUnitService: NotPartyUnitService


    @OperateLog(content = "编辑非公党建单位")
    @ApiOperation(value = "编辑非公党建单位", notes = "编辑非公党建单位")
    @PostMapping("/edit")
    fun edit(@RequestBody param: NotPartyUnitEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = notPartyUnitService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "单个删除非公党建单位")
    @ApiOperation(value = "单个删除非公党建单位", notes = "单个删除非公党建单位")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteNotPartyUnit(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = notPartyUnitService.deleteById(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看非公党建单位详情")
    @ApiOperation(value = "获取非公党建单位", notes = "获取非公党建单位")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<NotPartyUnitVO> {
        var vo = notPartyUnitService.info(id)
        return success(vo)
    }

    @OperateLog(content = "查看非公党建单位列表")
    @ApiOperation(value = "获取非公党建单位列表", notes = "获取非公党建单位列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "unitName", value = "单位名称", required = false)),
                (ApiImplicitParam(name = "type", value = "组织类型 0 非公企业 1 非公党组织", required = false)),
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestParam(required = false) unitName: String?,
                 @RequestParam(required = false) type: Integer?,
                 @PathVariable size: Int,
                 @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<NotPartyUnitVO>> {
        var page = notPartyUnitService.listPage(type, unitName, size, page)
        return success(page)
    }
    
}
