package cc.vv.party.web;


import cc.vv.party.beans.vo.ConvenienceServiceVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.exception.BizException
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ConvenienceServiceListParam
import cc.vv.party.param.ConvenienceServiceEditParam
import cc.vv.party.service.ConvenienceServicesService
import commonx.core.content.transfer
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 便民服务 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
@Api(value = "便民服务", tags = ["便民服务服务端API"], description = "服务端便民服务相关接口")
@RestController
@RequestMapping("/web/convenience-services")
class ConvenienceServicesController : BaseController() {

    @Autowired
    lateinit var convenienceServicesService: ConvenienceServicesService

    @OperateLog(content = "编辑服务信息")
    @ApiOperation(value = "编辑服务信息", notes = "编辑服务信息")
    @PostMapping("/edit")
    fun edit(@RequestBody param: ConvenienceServiceEditParam): ResponseEntityWrapper<Boolean> {
        var isAdd = convenienceServicesService.edit(param, getCurrentUserId())
        return success(isAdd)
    }

    @OperateLog(content = "删除服务信息")
    @ApiOperation(value = "删除服务信息", notes = "删除服务信息")
    @ApiImplicitParam(name = "id", value = "服务编号", required = true)
    @GetMapping("/delete/{id}")
    fun deleteConvenienceService(@PathVariable id: String): ResponseEntityWrapper<Boolean> {
        var isDelete = convenienceServicesService.deleteConvenienceService(id)
        return success(isDelete)
    }

    @OperateLog(content = "查看便民服务详情")
    @ApiOperation(value = "获取便民服务信息", notes = "获取便民服务信息")
    @ApiImplicitParam(name = "id", value = "便民服务编号", required = true)
    @GetMapping("/info/{id}")
    fun info(@PathVariable id: String): ResponseEntityWrapper<ConvenienceServiceVO> {
        var entity = convenienceServicesService.selectById(id) ?: throw BizException(StatusCode.MESSAGE_NOT_EXIST)
        return success(entity.transfer())
    }

    @OperateLog(content = "查看便民服务列表")
    @ApiOperation(value = "获取便民服务列表", notes = "获取便民服务列表")
    @ApiImplicitParams(
            value = [
                (ApiImplicitParam(name = "size", value = "每页显示数量", required = true)),
                (ApiImplicitParam(name = "page", value = "页码", required = true))
            ]
    )
    @PostMapping(value = "/list/{page}/{size}")
    fun listPage(@RequestBody param: ConvenienceServiceListParam, @PathVariable size: Int, @PathVariable page: Int): ResponseEntityWrapper<PageWrapper<ConvenienceServiceVO>> {
        var page = convenienceServicesService.listPage(param, size, page)
        return success(page)
    }


}
