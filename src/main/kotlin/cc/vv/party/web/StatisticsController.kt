package cc.vv.party.api;


import cc.vv.party.beans.vo.LearningStatisticsVO
import cc.vv.party.beans.vo.PostJobHuntingUserStatisticsVO
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.VolunteerStatisticsVO
import cc.vv.party.common.base.BaseController;
import cc.vv.party.common.constants.SysConsts
import cc.vv.party.common.wrapper.PageWrapper
import cc.vv.party.common.wrapper.ResponseEntityWrapper
import cc.vv.party.logger.annotation.OperateLog
import cc.vv.party.param.ProjectStatisticsParam
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UserStatisticsParam
import cc.vv.party.service.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * <p>
 * 信息统计 前端控制器
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
@Api(value = "信息统计", tags = ["信息统计服务端API"], description = "服务端信息统计相关接口")
@RestController
@RequestMapping("/web/statistics")
class StatisticsController : BaseController() {

    @Autowired
    lateinit var postService: PostService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var unitService: UnitService

    @Autowired
    lateinit var projectService: ProjectService

    @Autowired
    lateinit var volunteerUserService: VolunteerUserService


    @OperateLog(content = "查看岗位、求职统计")
    @ApiOperation(value = "岗位、求职统计", notes = "岗位、求职统计")
    @ApiImplicitParam(name = "time", value = "时间 格式为：yyyy", required = false)
    @PostMapping(value = "/post-job-hunting-statistics")
    fun postJobHuntingStatistics(@RequestParam time: String?): ResponseEntityWrapper<PostJobHuntingUserStatisticsVO> {
        var result = postService.findStatistics(time)
        return success(result)
    }

    @OperateLog(content = "查看职业统计")
    @ApiOperation(
        value = "职业统计",
        notes = "职业统计(key为职业代码，value为职业对应的人数。职业 0 工人 1 农牧渔民 2 专业技术人员 3 学校教职工 4 企事业单位管理人员  5 党政机关工作人员  6 学生  7离退休人员  8其他职业)"
    )
    @PostMapping(value = "/career-statistics")
    fun careerStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        if(StringUtils.isBlank(param.street) && StringUtils.isBlank(param.community) && StringUtils.isBlank(param.grid)){
            var manager = getUserManageOrgInfo()
            if(StringUtils.isBlank(param.street)) param.street = manager.streetId
            if(StringUtils.isBlank(param.community)) param.community = manager.communityId
            if(StringUtils.isBlank(param.grid)) param.grid = manager.gridId
        }
        var result = userService.careerStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看民族统计")
    @ApiOperation(value = "民族统计", notes = "民族统计(key为民族代码，value为该民族数量。民族 0 汉族 1 少数民族)")
    @PostMapping(value = "/ethnic-statistics")
    fun ethnicStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        if(StringUtils.isBlank(param.street) && StringUtils.isBlank(param.community) && StringUtils.isBlank(param.grid)){
            var manager = getUserManageOrgInfo()
            if(StringUtils.isBlank(param.street)) param.street = manager.streetId
            if(StringUtils.isBlank(param.community)) param.community = manager.communityId
            if(StringUtils.isBlank(param.grid)) param.grid = manager.gridId
        }
        var result = userService.ethnicStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看性别统计")
    @ApiOperation(value = "性别统计", notes = "性别统计(key为性别代码，value为该性别数量。性别 0 男 1 女)")
    @PostMapping(value = "/sex-statistics")
    fun sexStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        if(StringUtils.isBlank(param.street) && StringUtils.isBlank(param.community) && StringUtils.isBlank(param.grid)){
            var manager = getUserManageOrgInfo()
            if(StringUtils.isBlank(param.street)) param.street = manager.streetId
            if(StringUtils.isBlank(param.community)) param.community = manager.communityId
            if(StringUtils.isBlank(param.grid)) param.grid = manager.gridId
        }
        var result = userService.sexStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看文化程度统计")
    @ApiOperation(
        value = "文化程度统计",
        notes = "文化程度统计(key为文化程度代码，value为该文化程度数量。文化程度 0 初中及以下 1 高中 2 中专 3 大专 4 本科 5 研究生及以上)"
    )
    @PostMapping(value = "/educational-level-statistics")
    fun educationalLevelStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        if(StringUtils.isBlank(param.street) && StringUtils.isBlank(param.community) && StringUtils.isBlank(param.grid)){
            var manager = getUserManageOrgInfo()
            if(StringUtils.isBlank(param.street)) param.street = manager.streetId
            if(StringUtils.isBlank(param.community)) param.community = manager.communityId
            if(StringUtils.isBlank(param.grid)) param.grid = manager.gridId
        }
        var result = userService.educationalLevelStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看年龄统计")
    @ApiOperation(
        value = "年龄统计",
        notes = "年龄统计(key为年龄段代码，value为该年龄段数量。年龄段 0： 0-6岁， 1:7-17岁，2： 18-35岁，3:36-45岁，4:46-59岁，5:60以上)"
    )
    @PostMapping(value = "/age-statistics")
    fun ageStatistics(@RequestBody param: UserStatisticsParam): ResponseEntityWrapper<Map<String, Int>> {
        if(StringUtils.isBlank(param.street) && StringUtils.isBlank(param.community) && StringUtils.isBlank(param.grid)){
            var manager = getUserManageOrgInfo()
            if(StringUtils.isBlank(param.street)) param.street = manager.streetId
            if(StringUtils.isBlank(param.community)) param.community = manager.communityId
            if(StringUtils.isBlank(param.grid)) param.grid = manager.gridId
        }
        var result = userService.ageStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看报道统计")
    @ApiOperation(value = "报道统计", notes = "报道统计")
    @PostMapping(value = "/report-statistics")
    fun reportStatistics(@RequestBody param: ReportStatisticsParam): ResponseEntityWrapper<List<ReportStatisticsVO>> {
        var result: List<ReportStatisticsVO>
        if (param.type != null && param.type!!.toInt() == 0) {
            result = userService.reportStatistics(param)
        } else {
            result = unitService.reportStatistics(param)
        }
        return success(result)
    }

    @OperateLog(content = "查看项目统计")
    @ApiOperation(value = "项目统计", notes = "项目统计（key 为项目状态类型，value为项目数量。key 取值范围与含义：系统状态 0 正常 1 延期 2 预警  3 取消  4 完结）")
    @PostMapping(value = "/project-statistics")
    fun projectStatistics(@RequestBody param: ProjectStatisticsParam): ResponseEntityWrapper<Map<String, Integer>> {
        var result = projectService.projectSysStateStatistics(param)
        return success(result)
    }

    @OperateLog(content = "查看志愿统计")
    @ApiOperation(value = "志愿统计", notes = "志愿统计")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "street", value = "街道", required = false),
            ApiImplicitParam(name = "community", value = "社区", required = false),
            ApiImplicitParam(name = "grid", value = "网格", required = false)
        ]
    )
    @PostMapping(value = "/volunteer-statistics")
    fun volunteerStatistics(
        @RequestParam street: String?, @RequestParam community: String?,
        @RequestParam grid: String?
    ): ResponseEntityWrapper<VolunteerStatisticsVO> {
        var result = volunteerUserService.findStatistics(null, street, community, grid)
        return success(result)
    }

    @OperateLog(content = "查看学习时长统计列表")
    @ApiOperation(value = "学习时长统计", notes = "学习时长统计")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "branchType", value = "类型"),
            ApiImplicitParam(name = "branchId", value = "组织机构Id")
        ]
    )
    @GetMapping("/learning-statistics")
    fun learningStatistics(
        @RequestParam(required = false, defaultValue = SysConsts.BranchType.REGION_LEVEL_DISTRICT) branchType: String,
        @RequestParam(required = false, defaultValue = SysConsts.REGION_ID) branchId: String,
        @RequestParam page: Int,
        @RequestParam pageSize: Int
    ): ResponseEntityWrapper<PageWrapper<LearningStatisticsVO>> {
        val result = userService.learningStatistics(branchType, branchId, page, pageSize)
        return success(result)
    }

    @OperateLog(content = "查看学习时长统计详情")
    @ApiOperation(value = "学习时长统计详情", notes = "学习时长统计详情")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "branchType", value = "类型"),
            ApiImplicitParam(name = "branchId", value = "Id")
        ]
    )
    @GetMapping("/learning-statistics-details")
    fun learningStatisticsDetails(
        @RequestParam branchType: String,
        @RequestParam branchId: String,
        @RequestParam year: String,
        @RequestParam(required = false) month: String?,
        @RequestParam(required = false) startDay: String?,
        @RequestParam(required = false) endDay: String?
    ): ResponseEntityWrapper<HashMap<String, Long>> {
        val result = userService.learningStatisticsDetail(branchType, branchId, year, month, startDay, endDay)
        return success(result)
    }

    @ApiOperation(value = "组织机构统计学习总时长", notes = "组织机构统计学习总时长")
    @ApiImplicitParams(
        value = [
            ApiImplicitParam(name = "branchType", value = "类型"),
            ApiImplicitParam(name = "branchId", value = "Id")
        ]
    )
    @GetMapping("/total-learning-statistics")
    fun learningStatisticsDetails(
        @RequestParam(required = false, defaultValue = SysConsts.BranchType.REGION_LEVEL_DISTRICT) branchType: String,
        @RequestParam(required = false, defaultValue = SysConsts.REGION_ID) branchId: String
    ): ResponseEntityWrapper<String> {
        val result = userService.learningTotalStatistics(branchType, branchId)
        return success(result)
    }
}
