package cc.vv.party.mapper;

import cc.vv.party.beans.model.AppVersion
import cc.vv.party.beans.model.Banner;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * App 版本管理 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface AppVersionMapper : BaseMapper<AppVersion>
