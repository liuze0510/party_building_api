package cc.vv.party.mapper;

import cc.vv.party.model.DemonstrationTypical;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 示范典型 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
interface DemonstrationTypicalMapper : BaseMapper<DemonstrationTypical>
