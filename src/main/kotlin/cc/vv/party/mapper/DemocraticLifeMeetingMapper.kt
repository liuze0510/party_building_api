package cc.vv.party.mapper;

import cc.vv.party.beans.vo.DemocraticLifeMeetingVO
import cc.vv.party.model.DemocraticLifeMeeting;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 民主生活会 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface DemocraticLifeMeetingMapper : BaseMapper<DemocraticLifeMeeting> {


    fun selectManagerPageList(@Param("page") page: Page<DemocraticLifeMeetingVO>, @Param("branchType")branchType: String?,
                              @Param("branchId") branchId: String?, @Param("time") time: Long?, @Param("type") type: String?,
                              @Param("title") title: String?): List<DemocraticLifeMeetingVO>

}
