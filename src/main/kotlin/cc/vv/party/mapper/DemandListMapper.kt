package cc.vv.party.mapper;

import cc.vv.party.beans.model.DemandList;
import cc.vv.party.beans.vo.DemandListVO
import cc.vv.party.param.DemandListListParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 需求清单 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface DemandListMapper : BaseMapper<DemandList> {

    fun selectAppListPage(@Param("page") page: Page<DemandListVO>,
                          @Param("street")street: String?,
                          @Param("community")community: String?,
                          @Param("grid")grid: String?,
                          @Param("key")key: String?,
                          @Param("startTime")startTime: Long?,
                          @Param("endTime")endTime: Long?) :List<DemandListVO>

    fun selectListPage(@Param("page") page: Page<DemandListVO>, @Param("p") p: DemandListListParam) :List<DemandListVO>

}
