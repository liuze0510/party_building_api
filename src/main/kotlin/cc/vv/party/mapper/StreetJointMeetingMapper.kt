package cc.vv.party.mapper;

import cc.vv.party.beans.model.StreetJointMeeting;
import cc.vv.party.beans.vo.StreetJointMeetingVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 街道联席会议 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface StreetJointMeetingMapper : BaseMapper<StreetJointMeeting> {

    fun selectListPage(@Param("page") page: Page<StreetJointMeetingVO>, @Param("street") street: String?,
                       @Param("meetingName") meetingName: String?): List<StreetJointMeetingVO>

    /** 获取当前用户管理街道会议信息 */
    fun getCurrentUserManagerStreetJointMeeting(account: String): StreetJointMeetingVO

}
