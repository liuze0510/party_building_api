package cc.vv.party.mapper;

import cc.vv.party.beans.model.ConvenienceServices;
import cc.vv.party.beans.vo.ConvenienceServiceVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 便民服务 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ConvenienceServicesMapper : BaseMapper<ConvenienceServices> {


    fun getById(@Param("id") id: String): ConvenienceServiceVO

    fun appListPage(@Param("page") page: Page<ConvenienceServiceVO>,
                    @Param("street") street: String?,
                    @Param("community") community: String?,
                    @Param("grid") grid: String?,
                    @Param("type") type: String?,
                    @Param("title") title: String?): List<ConvenienceServiceVO>

}
