package cc.vv.party.mapper;

import cc.vv.party.beans.model.Org;
import cc.vv.party.beans.vo.NodeVO
import cc.vv.party.beans.vo.OrgVO
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 组织机构 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface OrgMapper : BaseMapper<Org> {

    /**
     * 获取当前账号所管理的组织结构列表
     */
    fun findOrgListByAccount(@Param("user_account") account: String): List<OrgVO>

    /**
     * 获取当前用户所管理的组织结构列表及子集合列表
     */
    fun findUserOrgAndChildren(@Param("user_account") account: String): List<OrgVO>

    /**
     * 获取党支部列表
     */
    @DS("db_yanan")
    fun findPartyBranchList(@Param("type") type: String, @Param("id") id: String): List<NodeVO>

    /**
     * 获取党总支部列表
     */
    @DS("db_yanan")
    fun findPartyTotalBranchList(@Param("type") type: String, @Param("id") id: String): List<NodeVO>

    /**
     * 获取党工委列表
     */
    @DS("db_yanan")
    fun findPartyWorkCommitteeList(): List<NodeVO>

    /**
     * 获取党委列表
     */
    @DS("db_yanan")
    fun findPartyCommitteeList(@Param("type") type: String, @Param("id") id: String): List<NodeVO>

    /**
     * 获取地区列表
     */
    @DS("db_yanan")
    fun findRegionList(@Param("id") id: String?): List<NodeVO>

    fun findAppSatistics(@Param("year") year: Int, @Param("street") street: String?,
                         @Param("community") community: String?, @Param("grid") grid: String?):List<HashMap<String, Int>>

}
