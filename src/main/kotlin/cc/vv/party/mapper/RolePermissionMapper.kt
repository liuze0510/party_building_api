package cc.vv.party.mapper;

import cc.vv.party.beans.model.RolePermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色资源 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface RolePermissionMapper : BaseMapper<RolePermission>
