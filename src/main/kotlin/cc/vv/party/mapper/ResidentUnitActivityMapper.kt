package cc.vv.party.mapper;

import cc.vv.party.beans.vo.ResidentUnitActivityVO
import cc.vv.party.model.ResidentUnitActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 驻区单位活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface ResidentUnitActivityMapper : BaseMapper<ResidentUnitActivity> {

    fun selectListPage(@Param("page") page: Page<ResidentUnitActivityVO>,
                       @Param("street") street: String?, @Param("community") community: String?,
                       @Param("unitId") unitId: String?, @Param("title") title: String?): List<ResidentUnitActivityVO>

}
