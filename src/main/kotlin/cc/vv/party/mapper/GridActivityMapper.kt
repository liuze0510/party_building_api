package cc.vv.party.mapper;

import cc.vv.party.beans.vo.GridActivityVO
import cc.vv.party.model.GridActivity
import com.baomidou.mybatisplus.mapper.BaseMapper
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 网格活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface GridActivityMapper : BaseMapper<GridActivity> {

    fun selectListPage(@Param("page") page: Page<GridActivityVO> , @Param("street")street: String?,
                       @Param("community")community: String?, @Param("grid")grid: String?, @Param("title")title: String?): List<GridActivityVO>

}
