package cc.vv.party.mapper;

import cc.vv.party.beans.model.PovertyAlleviationVillage;
import cc.vv.party.beans.vo.PovertyAlleviationVillageVO
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 扶贫行政村信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PovertyAlleviationVillageMapper : BaseMapper<PovertyAlleviationVillage> {


    fun getCurrentUserVillage(account: String): PovertyAlleviationVillageVO

}
