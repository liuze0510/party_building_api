package cc.vv.party.mapper;

import cc.vv.party.beans.model.ResourceList;
import cc.vv.party.beans.vo.ResourceListVO
import cc.vv.party.param.ResourceListListParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 资源清单 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ResourceListMapper : BaseMapper<ResourceList> {

    fun findAppListPage(@Param("page") page: Page<ResourceListVO>,
                        @Param("street")street: String?,
                        @Param("community")community: String?,
                        @Param("title") title: String?,
                        @Param("startTime") startTime: Long?,
                        @Param("endTime") endTime: Long?): List<ResourceListVO>

    fun selectListPage(@Param("page") page: Page<ResourceListVO>, @Param("p") p: ResourceListListParam): List<ResourceListVO>

    fun getById(@Param("id") id : String): ResourceListVO

}
