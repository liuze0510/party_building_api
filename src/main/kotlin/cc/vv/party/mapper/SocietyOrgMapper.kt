package cc.vv.party.mapper;

import cc.vv.party.beans.model.SocietyOrg;
import cc.vv.party.beans.vo.SocietyOrgVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 社会组织 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface SocietyOrgMapper : BaseMapper<SocietyOrg> {

    fun selectListPage(@Param("page") page: Page<SocietyOrgVO>, @Param("community")community: String?,
                       @Param("orgName")orgName: String?): List<SocietyOrgVO>

}
