package cc.vv.party.mapper

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.MsgNotifyVO
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param
import java.util.*


/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
interface MsgNotifyMapper : BaseMapper<PartyReport> {

    @DS("db_yanan")
    fun selectPartyMemberInfo(@Param("partyMemberId") partyMemberId: String): HashMap<String, String>

    @DS("db_yanan")
    fun selectMsgNotifyList(
        @Param("page") page: Page<MsgNotifyVO>,
        @Param("start") start: String?,
        @Param("end") end: String?,
        @Param("townId") townId: String?,
        @Param("countyRegionId") countyRegionId: String?,
        @Param("cityRegionId") cityRegionId: String?,
        @Param("orgMinistryId") orgMinistryId: String?,
        @Param("partyMemberId") partyMemberId: String
    ): List<MsgNotifyVO>

    @DS("db_yanan")
    fun selectMsgNotifyById(@Param("id") id: String): MsgNotifyVO
}