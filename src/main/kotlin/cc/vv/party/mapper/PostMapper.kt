package cc.vv.party.mapper;

import cc.vv.party.beans.model.Post;
import cc.vv.party.beans.vo.PostVO
import cc.vv.party.beans.vo.UserVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 岗位信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PostMapper : BaseMapper<Post> {


    fun findAppListPage(@Param("page") page: Page<PostVO>,
                        @Param("street") street: String?,
                        @Param("community") community: String?,
                        @Param("grid") grid: String?,
                        @Param("postName") postName: String?,
                        @Param("user") user: UserVO): List<PostVO>

    fun findManagerListPage(@Param("page") page: Page<PostVO>,
                            @Param("street") street: String?,
                            @Param("community") community: String?,
                            @Param("grid") grid: String?,
                            @Param("time") time: Long?,
                            @Param("postSource") postSource: String?,
                            @Param("startSalary") startSalary: Integer?,
                            @Param("endSalary") endSalary: Integer?,
                            @Param("postName") postName: String?): List<PostVO>

}
