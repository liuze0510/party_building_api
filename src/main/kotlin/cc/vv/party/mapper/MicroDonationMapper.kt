package cc.vv.party.mapper;

import cc.vv.party.beans.model.MicroDonation;
import cc.vv.party.beans.vo.MicroDonationVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 微捐赠 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface MicroDonationMapper : BaseMapper<MicroDonation> {

    fun selectClaimListPage(@Param("page") page: Page<MicroDonationVO>, @Param("mobile")mobile: String, @Param("userId")userId: String): List<MicroDonationVO>

}
