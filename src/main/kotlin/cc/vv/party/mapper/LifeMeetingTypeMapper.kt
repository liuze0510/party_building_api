package cc.vv.party.mapper;

import cc.vv.party.model.LifeMeetingType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 民主、组织生活会类型 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-31
 */
interface LifeMeetingTypeMapper : BaseMapper<LifeMeetingType>
