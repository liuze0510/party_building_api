package cc.vv.party.mapper;

import cc.vv.party.beans.model.PartyCommitteeOrgRelation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社区大党委与组织成员关系 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyCommitteeOrgRelationMapper : BaseMapper<PartyCommitteeOrgRelation>
