package cc.vv.party.mapper;

import cc.vv.party.beans.vo.ThreeMeetOneLessonVO
import cc.vv.party.model.ThreeMeetOneLesson;
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 三会一课 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
//@DS("db_yanan")
interface ThreeMeetOneLessonMapper : BaseMapper<ThreeMeetOneLesson> {

//    @DS("db_yanan")
    fun selectThreeLessonsList(
        @Param("pc") pc: String?,
        @Param("startDate") startDate: String?,
        @Param("endDate") endDate: String?,
        @Param("type") type: String?,
        @Param("title") title: String?,
        @Param("branchId") branchId: String?,
        @Param("branchType") branchType: String?,
        @Param("page") page: Page<ThreeMeetOneLessonVO>
    ): List<ThreeMeetOneLessonVO>

//    @DS("db_yanan")
    fun selectThreeLessonsDetail(@Param("id") id: String): ThreeMeetOneLessonVO?
}
