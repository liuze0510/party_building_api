package cc.vv.party.mapper;

import cc.vv.party.beans.model.UnitEvaluationStar;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 单位评星晋阶 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UnitEvaluationStarMapper : BaseMapper<UnitEvaluationStar>
