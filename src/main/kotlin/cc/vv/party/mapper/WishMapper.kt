package cc.vv.party.mapper;

import cc.vv.party.beans.model.Wish;
import cc.vv.party.beans.vo.WishVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 微心愿 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface WishMapper : BaseMapper<Wish> {


    fun selectClaimListPage(@Param("page") page: Page<WishVO>, @Param("mobile")mobile: String, @Param("userId") userId: String): List<WishVO>

}
