package cc.vv.party.mapper;

import cc.vv.party.beans.model.StreetJointMeetingAdmin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 联席会议管理员 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface StreetJointMeetingAdminMapper : BaseMapper<StreetJointMeetingAdmin>
