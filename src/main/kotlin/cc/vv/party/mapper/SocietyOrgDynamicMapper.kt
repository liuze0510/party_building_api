package cc.vv.party.mapper;

import cc.vv.party.beans.model.SocietyOrgDynamic;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社会组织动态 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface SocietyOrgDynamicMapper : BaseMapper<SocietyOrgDynamic>
