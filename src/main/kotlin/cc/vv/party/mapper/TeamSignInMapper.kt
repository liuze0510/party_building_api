package cc.vv.party.mapper;

import cc.vv.party.beans.model.TeamSignIn;
import cc.vv.party.beans.vo.TeamSignInVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 四支队伍信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface TeamSignInMapper : BaseMapper<TeamSignIn> {

    fun findTeamSignInByTeamIdAndCreateTime(@Param("teamId") teamId: String, @Param("time") time: String): List<TeamSignInVO>

}
