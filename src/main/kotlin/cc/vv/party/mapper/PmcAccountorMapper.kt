package cc.vv.party.mapper;

import cc.vv.party.beans.model.PmcAccountor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
interface PmcAccountorMapper : BaseMapper<PmcAccountor>
