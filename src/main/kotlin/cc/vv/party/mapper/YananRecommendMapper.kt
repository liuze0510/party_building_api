package cc.vv.party.mapper;

import cc.vv.party.beans.model.YananRecommend
import com.baomidou.mybatisplus.mapper.BaseMapper

/**
 * <p>
 * 延安推荐 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface YananRecommendMapper : BaseMapper<YananRecommend>
