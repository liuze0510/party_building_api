package cc.vv.party.mapper;

import cc.vv.party.beans.model.CommittedComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 承诺评论信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommittedCommentMapper : BaseMapper<CommittedComment>
