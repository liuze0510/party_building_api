package cc.vv.party.mapper;

import cc.vv.party.beans.model.NotPartyUnit;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 非公党建单位 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface NotPartyUnitMapper : BaseMapper<NotPartyUnit>
