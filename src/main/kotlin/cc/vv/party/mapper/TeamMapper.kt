package cc.vv.party.mapper;

import cc.vv.party.beans.model.Team;
import cc.vv.party.beans.vo.TeamVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 四支队伍信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface TeamMapper : BaseMapper<Team> {


    fun selectListPage(@Param("page") page: Page<TeamVO>, @Param("town") town: String?,
                       @Param("village") village: String?, @Param("teamType") teamType: String?) : List<TeamVO>

}
