package cc.vv.party.mapper

import cc.vv.party.beans.model.CheckCode
import com.baomidou.mybatisplus.mapper.BaseMapper

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-01
 * @description
 **/
interface CheckCodeMapper : BaseMapper<CheckCode>