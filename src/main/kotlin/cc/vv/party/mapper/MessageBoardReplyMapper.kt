package cc.vv.party.mapper;

import cc.vv.party.beans.vo.MessageBoardReplyVO
import cc.vv.party.model.MessageBoardReply;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 留言回复 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-23
 */
interface MessageBoardReplyMapper : BaseMapper<MessageBoardReply> {


    fun findMessageBoardReplyPageList(@Param("page") page: Page<MessageBoardReplyVO>, @Param("messageBoardId") messageBoardId: String): List<MessageBoardReplyVO>

}
