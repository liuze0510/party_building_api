package cc.vv.party.mapper;

import cc.vv.party.beans.vo.*
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param
import java.math.BigDecimal

/**
 * 学习园地
 * @author Gyb
 * @since 2018-10-12
 */
@DS("db_yanan")
interface LearningFieldMapper {

    /** 微感悟 */
    @DS("db_yanan")
    fun findFeelingPageList(@Param("page") page: Page<FeelingVO>, @Param("branchIdList") branchIdList: List<String>): List<FeelingVO>

    /** 党建党章 */
    @DS("db_yanan")
    fun findPartyConstitutionPageList(@Param("page") page: Page<PartyConstitutionVO>): List<PartyConstitutionVO>

    /** 讲话系列 */
    @DS("db_yanan")
    fun findSeriesSpeechPageList(@Param("page") page: Page<SeriesSpeechVO>): List<SeriesSpeechVO>

    /** 微百科科目列表 */
    @DS("db_yanan")
    fun findSubjectList(): List<SubjectVO>

    /** 微百科 */
    @DS("db_yanan")
    fun findOpenClassList(@Param("page") page: Page<OpenClassVO>, @Param("subjectId") subjectId: String): List<OpenClassVO>

    /** 心得体会 */
    @DS("db_yanan")
    fun findExperiencePageList(
        @Param("page") page: Page<ExperienceVO>, @Param("branchIdList") branchIdList: List<String>): List<ExperienceVO>

    /** 在线考试 */
    @DS("db_yanan")
    fun findOnlineExamPageList(@Param("page") page: Page<OnlineExamVO>): List<OnlineExamVO>

    /** 在线考试试卷题目列表 */
    @DS("db_yanan")
    fun findQuestionPageList(@Param("page") page: Page<QuestionVO>, @Param("blankId") blankId: String): List<QuestionVO>

    /** 获取远程教育类型 */
    fun findEducationTypeList(): List<Map<String, String>>

    /** 远程教育列表数量 */
    @DS("db_yanan")
    fun findEducationPageList(@Param("page") page: Page<EducationVO>, @Param("typeId") typeId: String): List<EducationVO>

    /** 宝塔区组织成员统计 */
    @DS("db_yanan")
    fun findPartyOrgMemberStatistics(): List<Map<String, Int>>

    /** 党委成员统计 */
    @DS("db_yanan")
    fun findCompanyPartyOrgUserStatis(@Param("companyId") companyId: String): List<Map<String, Int>>

    /** 党党工委用户统计 */
    @DS("db_yanan")
    fun findPartyWorkCommitteeOrgUserStatis(@Param("partyWorkCommitteeId") partyWorkCommitteeId: String): List<Map<String, Int>>

    /** 党总支用户统计 */
    @DS("db_yanan")
    fun findPartyTotalBranchOrgUserStatis(@Param("partyTotalBranchId") partyTotalBranchId: String): List<Map<String, Int>>

    /** 党支部用户统计 */
    @DS("db_yanan")
    fun findPartyBranchPartyOrgUserStatis(@Param("partyBranchId") partyBranchId: String): List<Map<String, Int>>

    /********  start 微感悟统计  *****************************************************************/

    /** 统计微感悟总数 */
    @DS("db_yanan")
    fun getAllCount(): Integer

    /** 本月新增 */
    @DS("db_yanan")
    fun getMonthAdd(): Integer

    /** 审核通过总数 */
    @DS("db_yanan")
    fun getBySumCount(): Integer

    /** 本月审核通过新增 */
    @DS("db_yanan")
    fun getByMonthAddCount(): Integer

    /** 本周发布总数 */
    @DS("db_yanan")
    fun getWorkReleaseCount(): Integer

    /** 本周审核通过新增 */
    @DS("db_yanan")
    fun getByWorkAddCount(): Integer

    /** 实时统计 */
    @DS("db_yanan")
    fun findRealTimeStatistics(): List<FeelingVO>

    /** 获取发布 */
    @DS("db_yanan")
    fun getFeelingDataTrend(@Param("startTime") startTime: String, @Param("endTime") endTime: String): List<Map<String, Integer>>

    /** 获取通过 */
    @DS("db_yanan")
    fun getFeelingByDataTrend(@Param("startTime") startTime: String, @Param("endTime") endTime: String): List<Map<String, Integer>>

    /** 获取通过 */
    @DS("db_yanan")
    fun getFeelingByDataDetail(
        @Param("page") page: Page<FeelingDataDetailVO>, @Param("time") time: String
        , @Param("branchId") branchId: String?, @Param("sumDesc") sumDesc: Int
        , @Param("byDesc") byDesc: Int
    ): List<FeelingDataDetailVO>

    /********  end   微感悟统计  *****************************************************************/


    /********  start 心得体会统计  *****************************************************************/
    /** 获取心得体会统计数量 */
    @DS("db_yanan")
    fun getExperienceStatistics(): ExperienceStatisticsVO

    /** 获取发布 */
    @DS("db_yanan")
    fun getExperienceDataTrend(@Param("startTime") startTime: String, @Param("endTime") endTime: String): List<Map<String, Integer>>

    /** 获取通过 */
    @DS("db_yanan")
    fun getExperienceByDataTrend(@Param("startTime") startTime: String, @Param("endTime") endTime: String): List<Map<String, Integer>>

    /** 获取通过 */
    @DS("db_yanan")
    fun getExperienceByDataDetail(
        @Param("page") page: Page<ExperienceDataDetailVO>, @Param("time") time: String
        , @Param("branchId") branchId: String?, @Param("branchType") branchType: String?
    ): List<ExperienceDataDetailVO>

    /********  end   心得体会统计  *****************************************************************/

    /********  start   在线考试统计  *****************************************************************/

    @DS("db_yanan")
    fun selectExamStatistics(
        @Param("branchIdList") branchIdList: List<String>
    ): List<LearningStatisticsVO>

    //累计答题用户
    @DS("db_yanan")
    fun selectTotalUserStatistics(@Param("start") start: String?, @Param("end") end: String?): String

    //累计新增题数
    @DS("db_yanan")
    fun selectTotalExamStatistics(@Param("start") start: String?, @Param("end") end: String?): String

    //累计答题总数
    @DS("db_yanan")
    fun selectTotalAnswerQuestionStatistics(@Param("year") year: String): HashMap<String, BigDecimal>

    @DS("db_yanan")
    fun selectTotalAnswerQuestionStatisticsBefore(@Param("year") year: String): HashMap<String, BigDecimal>

    @DS("db_yanan")
    fun selectAverageScore(@Param("start") start: String?, @Param("end") end: String?): BigDecimal?

    @DS("db_yanan")
    fun selectAvgScoreByMonth(@Param("end") end: String?): BigDecimal

    @DS("db_yanan")
    fun selectExamTrend(@Param("scoreLevel") scoreLevel: Int, @Param("start") start: String, @Param("end") end: String): List<ExamTrendVO>

    /** 获取题库列表 */
    @DS("db_yanan")
    fun findQuestionBlankList(): List<OnlineExamVO>

    /** 获取在线考试数据详情 */
    @DS("db_yanan")
    fun findOnlineExamDataDetail(@Param("page") page: Page<OnlineStudyDataDetailVO>,
                                 @Param("blankId") blankId: String,
                                 @Param("date") date: String,
                                 @Param("branchIdList") branchIdList: List<String>?,
                                 @Param("sort") sort: Int): List<OnlineStudyDataDetailVO>
    /********  end   在线考试统计  *****************************************************************/

    /********  start  app端 标准化数据  *****************************************************************/
    @DS("db_weinan")
    fun findAreaTree(): List<AreauserVO>

    @DS("db_weinan")
    fun getStandardData(@Param("areaUserId") areaUserId: Int, @Param("year") year: Int, @Param("month") month: Int): StandardDataVO
    /********  end   app端 标准化数据  *****************************************************************/
}
