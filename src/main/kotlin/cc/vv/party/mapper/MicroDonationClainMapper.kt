package cc.vv.party.mapper;

import cc.vv.party.model.MicroDonationClain;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 微捐赠认领 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-05
 */
interface MicroDonationClainMapper : BaseMapper<MicroDonationClain>
