package cc.vv.party.mapper;

import cc.vv.party.beans.model.VolunteerProjectSignUp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 志愿项目报名信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerProjectSignUpMapper : BaseMapper<VolunteerProjectSignUp>
