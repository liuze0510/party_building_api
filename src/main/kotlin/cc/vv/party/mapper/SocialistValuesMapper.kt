package cc.vv.party.mapper;

import cc.vv.party.model.SocialistValues;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社会主义核心价值观 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
interface SocialistValuesMapper : BaseMapper<SocialistValues>
