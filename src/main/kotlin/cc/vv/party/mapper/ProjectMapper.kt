package cc.vv.party.mapper;

import cc.vv.party.beans.model.Project;
import cc.vv.party.beans.vo.ProjectVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.param.ProjectStatisticsParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 项目信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectMapper : BaseMapper<Project> {

    fun selectProjectSysStateStatistics(@Param("p") p: ProjectStatisticsParam): List<Map<String, Integer>>

    /** 获取预警项目编号集合 */
    fun selectWarningProjectList(): List<String>

    /** 获取完成项目编号集合 */
    fun selectCompleteProjectList(): List<String>

    /** 批量修改项目系统状态 */
    fun batchUpdateProject(@Param("sysState")sysState: Int, @Param("proIdList")proIdList: List<String>)

    /** 更新用户申请状态 */
    fun updateProjectApply(@Param("id") id: String, @Param("applyType") applyType: Int, @Param("applyReson") applyReson: String, @Param("sysState") sysState: Int)

    /** APP查看项目列表 */
    fun findAppListPage(@Param("page") page: Page<ProjectVO>,
                        @Param("street") street: String?,
                        @Param("community") community: String?,
                        @Param("grid") grid: String?,
                        @Param("title") title: String?,
                        @Param("state") state: String?,
                        @Param("currentUser") currentUser: UserVO): List<ProjectVO>


}
