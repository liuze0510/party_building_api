package cc.vv.party.mapper;

import cc.vv.party.beans.vo.StreetJointMeetingActivityVO
import cc.vv.party.model.StreetJointMeetingActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 街道联席会议活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-24
 */
interface StreetJointMeetingActivityMapper : BaseMapper<StreetJointMeetingActivity> {

    fun findManagerPageList(@Param("page") page: Page<StreetJointMeetingActivityVO>,
                            @Param("street")street: String?,
                            @Param("time")time: String?,
                            @Param("type")type: String?,
                            @Param("title")title: String?): List<StreetJointMeetingActivityVO>

}
