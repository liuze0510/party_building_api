package cc.vv.party.mapper;

import cc.vv.party.beans.model.ConvenienceServiceType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 便民服务类型 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ConvenienceServiceTypeMapper : BaseMapper<ConvenienceServiceType>
