package cc.vv.party.mapper;

import cc.vv.party.beans.model.Log;
import cc.vv.party.beans.vo.AppRealTimeStatisticsVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 微捐赠 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface LogMapper : BaseMapper<Log> {


    fun findAppRealTimeStatistics(@Param("date") date: String): List<AppRealTimeStatisticsVO>

}
