package cc.vv.party.mapper;

import cc.vv.party.beans.model.AppVersion
import cc.vv.party.beans.model.Banner;
import cc.vv.party.beans.model.LoginController
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 登陆控制 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface LoginControllerMapper : BaseMapper<LoginController>
