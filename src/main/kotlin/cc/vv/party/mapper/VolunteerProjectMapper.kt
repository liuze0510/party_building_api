package cc.vv.party.mapper;

import cc.vv.party.beans.model.VolunteerProject;
import cc.vv.party.beans.vo.VolunteerProjectVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 志愿项目信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerProjectMapper : BaseMapper<VolunteerProject> {

    fun selectUserVolunteerProjectList(@Param("userId") userId: String): List<VolunteerProjectVO>

}
