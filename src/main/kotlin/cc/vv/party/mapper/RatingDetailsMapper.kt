package cc.vv.party.mapper;

import cc.vv.party.beans.model.RatingDetails;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 单位评星晋阶明细 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface RatingDetailsMapper : BaseMapper<RatingDetails>
