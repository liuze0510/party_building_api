package cc.vv.party.mapper;

import cc.vv.party.beans.model.CommunityPartyCommittee;
import cc.vv.party.beans.vo.CommunityPartyCommitteeVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityPartyCommitteeMapper : BaseMapper<CommunityPartyCommittee> {

    fun selectListPage(@Param("page")page: Page<CommunityPartyCommitteeVO>, @Param("street")street: String?,
                       @Param("community")community: String?, @Param("name")name: String?): List<CommunityPartyCommitteeVO>

    /** 获取当前用户管理社区党委信息 */
    fun getCurrentUserManagerPartyCommittee(account: String): CommunityPartyCommitteeVO

}
