package cc.vv.party.mapper;

import cc.vv.party.beans.model.PartyBuildingNews;
import cc.vv.party.beans.vo.PartyBuildingNewsVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 党建要闻 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyBuildingNewsMapper : BaseMapper<PartyBuildingNews> {

    fun findListByType(
        @Param("page") page: Page<*>, @Param("type") type: Int,
        @Param("street") street: String?, @Param("community") community: String?,
        @Param("grid") grid: String?, @Param("title") title: String?,
        @Param("publishTime") publishTime: String?
    ): List<PartyBuildingNewsVO>
}
