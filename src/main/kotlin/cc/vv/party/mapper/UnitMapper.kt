package cc.vv.party.mapper;

import cc.vv.party.beans.model.Unit;
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.UnitVO
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UnitListParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 单位信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UnitMapper : BaseMapper<Unit> {


    fun selectListPage(@Param("page") page: Page<UnitVO>, @Param("p") p: UnitListParam) :List<UnitVO>

    fun selectReportStatistics(@Param("p") p: ReportStatisticsParam): List<ReportStatisticsVO>

}
