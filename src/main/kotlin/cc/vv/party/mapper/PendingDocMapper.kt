package cc.vv.party.mapper

import cc.vv.party.beans.model.PartyReport
import cc.vv.party.beans.vo.PendingDocVO
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-11-28
 * @description
 **/
interface PendingDocMapper : BaseMapper<PartyReport> {

    @DS("db_yanan")
    fun selectMemberTaskList(@Param("partyMemberId") partyMemberId: String): List<String>

    @DS("db_yanan")
    fun selectDocList(
        @Param("page") page: Page<PendingDocVO>,
        @Param("yananRole") yananRole: String,
        @Param("branchId") branchId: String,
        @Param("orgMinistry") orgMinistry: String?,
        @Param("partyMemberId") partyMemberId: String?,
        @Param("taskIdList") taskIdList: List<String>?
    ): List<PendingDocVO>

    @DS("db_yanan")
    fun selectTaskIdsForMyReaded(@Param("partyMemberId") partyMemberId: String?, @Param("taskIdList") taskIdList: List<String>?): List<String>

    @DS("db_yanan")
    fun selectDocById(@Param("id") id: String): PendingDocVO?
}