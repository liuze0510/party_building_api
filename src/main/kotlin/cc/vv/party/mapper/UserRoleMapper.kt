package cc.vv.party.mapper;

import cc.vv.party.beans.model.Permission
import cc.vv.party.beans.model.Role
import cc.vv.party.beans.model.UserRole;
import cc.vv.party.beans.vo.RoleVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface UserRoleMapper : BaseMapper<UserRole> {

    fun findUserPermission(@Param("user_account") userAccount: String): List<Permission>

    fun findUserRole(@Param("user_account") userAccount: String): List<RoleVO>?
}
