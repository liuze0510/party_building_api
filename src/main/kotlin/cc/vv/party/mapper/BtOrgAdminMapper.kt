package cc.vv.party.mapper;

import cc.vv.party.beans.model.OrgAdmin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface BtOrgAdminMapper : BaseMapper<OrgAdmin>
