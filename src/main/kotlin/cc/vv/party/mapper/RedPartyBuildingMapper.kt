package cc.vv.party.mapper;

import cc.vv.party.model.RedPartyBuilding;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 红领党建 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface RedPartyBuildingMapper : BaseMapper<RedPartyBuilding>
