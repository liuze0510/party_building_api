package cc.vv.party.mapper;

import cc.vv.party.beans.model.Committed;
import cc.vv.party.beans.vo.CommittedVO
import cc.vv.party.common.constants.enums.OrgType
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 承诺信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommittedMapper : BaseMapper<Committed> {

    /**
     * @param orgId 组织机构id
     * @param orgType 组织结构类型
     * @param targetId 单位id
     */
    fun selectUnitCommittedByOrg(
        @Param("orgId") orgId: String?, @Param("orgType") orgType: OrgType?,
        @Param("page") page: Page<*>, @Param("havingComment") havingComment: Int
    ): List<CommittedVO>

    /**
     * @param orgId 组织机构id
     * @param orgType 组织结构类型
     * @param targetId 用户id
     */
    fun selectUserCommittedByOrg(
        @Param("orgId") orgId: String?, @Param("orgType") orgType: OrgType?,
        @Param("page") page: Page<*>, @Param("havingComment") havingComment: Int
    ): List<CommittedVO>


    fun findListPage(@Param("page") page: Page<CommittedVO>, @Param("street") street: String?, @Param("community") community: String?,
                     @Param("grid") grid: String?, @Param("time") time: Long?, @Param("mobile") mobile: String?,
                     @Param("type") type: Int):List<CommittedVO>
}
