package cc.vv.party.mapper;

import cc.vv.party.model.OrgLiFeMeeting
import com.baomidou.mybatisplus.mapper.BaseMapper

/**
 * <p>
 * 组织生活会 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-29
 */
interface OrgLiFeMeetingMapper : BaseMapper<OrgLiFeMeeting>
