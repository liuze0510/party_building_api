package cc.vv.party.mapper;

import cc.vv.party.beans.model.PmcPerson;
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.param.UserListParam
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-07
 */
interface PmcPersonMapper : BaseMapper<PmcPerson> {

    fun selectPartyMemberList(@Param("p") p: UserListParam, @Param("page") page: Page<UserVO>): List<UserVO>

    fun selectPartyMemberById(@Param("id") id: String): UserVO

    fun selectPartyMemberByIdCard(@Param("idCard") idCard: String): UserVO

    @DS("db_yanan")
    fun selectPartyMemberByAccounts(@Param("accounts") accounts: List<String>): List<UserVO>
}
