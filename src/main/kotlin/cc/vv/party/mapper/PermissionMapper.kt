package cc.vv.party.mapper;

import cc.vv.party.beans.model.Permission;
import cc.vv.party.beans.vo.PermissionVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 资源 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface PermissionMapper : BaseMapper<Permission> {

    fun selectPermissionByRoleId(@Param("roleId") roleId: String): List<PermissionVO>
}
