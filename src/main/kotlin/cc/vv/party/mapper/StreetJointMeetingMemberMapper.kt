package cc.vv.party.mapper;

import cc.vv.party.beans.model.StreetJointMeetingMember
import cc.vv.party.beans.vo.StreetJointMeetingMemberVO
import com.baomidou.mybatisplus.mapper.BaseMapper

/**
 * <p>
 * 街道联席会议成员 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface StreetJointMeetingMemberMapper : BaseMapper<StreetJointMeetingMember> {


    fun findByMeetingId(meetingId: String): List<StreetJointMeetingMemberVO>

}
