package cc.vv.party.mapper;

import cc.vv.party.beans.model.MessageBoard;
import cc.vv.party.beans.vo.MessageBoardVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 留言板 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface MessageBoardMapper : BaseMapper<MessageBoard> {

    fun getById(@Param("id") id: String): MessageBoardVO

    fun pcListPage(@Param("page") page: Page<MessageBoardVO>) : List<MessageBoardVO>

    fun appListPage(@Param("page") page: Page<MessageBoardVO>) : List<MessageBoardVO>

}
