package cc.vv.party.mapper;

import cc.vv.party.beans.model.PartyReport;
import cc.vv.party.beans.vo.PartyReportVO
import cc.vv.party.param.PartyReportListParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 党员报道信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyReportMapper : BaseMapper<PartyReport> {

    fun selectInfo(@Param("id") id: String): PartyReportVO

    fun selectPageList(@Param("page") page: Page<PartyReportVO>, @Param("p") param: PartyReportListParam): List<PartyReportVO>

    fun selectReportUserPageList(@Param("page") page: Page<PartyReportVO>, @Param("p") param: PartyReportListParam): List<PartyReportVO>

}
