package cc.vv.party.mapper;

import cc.vv.party.beans.model.CommunityActivity;
import cc.vv.party.beans.vo.CommunityActivityVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 社区活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityActivityMapper : BaseMapper<CommunityActivity> {

    fun findManagerPageList(@Param("page") page: Page<CommunityActivityVO>,
                            @Param("street") street: String?,
                            @Param("community") community: String?,
                            @Param("time") time: String?,
                            @Param("type") type: String?,
                            @Param("title") title: String?): List<CommunityActivityVO>

}
