package cc.vv.party.mapper;

import cc.vv.party.beans.model.JobHuntingUser;
import cc.vv.party.beans.vo.JobHuntingUserVO
import cc.vv.party.beans.vo.UserVO
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 求职信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface JobHuntingUserMapper : BaseMapper<JobHuntingUser> {


    fun findAppListPage(@Param("page") page: Page<JobHuntingUserVO>,
                        @Param("street") street: String?,
                        @Param("community") community: String?,
                        @Param("grid") grid: String?,
                        @Param("key") key: String?,
                        @Param("startTime") startTime: Long?,
                        @Param("endTime") endTime: Long?,
                        @Param("user") user: UserVO): List<JobHuntingUserVO>

    fun findManagerListPage(@Param("page") page: Page<JobHuntingUserVO>,
                            @Param("street") street: String?,
                            @Param("community") community: String?,
                            @Param("grid") grid: String?,
                            @Param("sex") sex: Integer?,
                            @Param("education") education: String?,
                            @Param("workYear") workYear: Integer?,
                            @Param("name") name: String?,
                            @Param("mobile") mobile: String?) : List<JobHuntingUserVO>

}
