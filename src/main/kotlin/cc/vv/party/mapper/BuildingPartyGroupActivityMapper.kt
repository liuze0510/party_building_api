package cc.vv.party.mapper;

import cc.vv.party.beans.vo.BuildingPartyGroupActivityVO
import cc.vv.party.model.BuildingPartyGroupActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 楼栋党小组活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface BuildingPartyGroupActivityMapper : BaseMapper<BuildingPartyGroupActivity> {

    fun selectListPage(@Param("page") page: Page<BuildingPartyGroupActivityVO>, @Param("street")street: String?,
                       @Param("community")community: String?, @Param("grid")grid: String?,
                       @Param("groupId")groupId: String?, @Param("title")title: String?): List<BuildingPartyGroupActivityVO>


}
