package cc.vv.party.mapper;

import cc.vv.party.beans.model.ProjectSegmentation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 项目阶段信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectSegmentationMapper : BaseMapper<ProjectSegmentation>
