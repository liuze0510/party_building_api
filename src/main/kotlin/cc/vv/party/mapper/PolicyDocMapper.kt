package cc.vv.party.mapper;

import cc.vv.party.beans.model.PolicyDoc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 政策文件信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PolicyDocMapper : BaseMapper<PolicyDoc>
