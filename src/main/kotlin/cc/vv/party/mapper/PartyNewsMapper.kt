package cc.vv.party.mapper;

import cc.vv.party.beans.model.PartyNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 党群新闻 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyNewsMapper : BaseMapper<PartyNews>
