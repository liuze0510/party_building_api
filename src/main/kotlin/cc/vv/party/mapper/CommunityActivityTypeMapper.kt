package cc.vv.party.mapper;

import cc.vv.party.beans.model.CommunityActivityType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社区活动类型 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface CommunityActivityTypeMapper : BaseMapper<CommunityActivityType>
