package cc.vv.party.mapper;

import cc.vv.party.beans.model.Introduction;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 党建概况 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface IntroductionMapper : BaseMapper<Introduction>
