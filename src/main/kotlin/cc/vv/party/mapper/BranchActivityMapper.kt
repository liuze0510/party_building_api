package cc.vv.party.mapper;

import cc.vv.party.beans.vo.BranchActivityVO
import cc.vv.party.model.BranchActivity
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 支部活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-18
 */
//@DS("db_yanan")
interface BranchActivityMapper : BaseMapper<BranchActivity> {

//    @DS("db_yanan")
    fun selectBranchActivityList(
        @Param("pc") pc: String?,
        @Param("startDate") startDate: String?,
        @Param("endDate") endDate: String?,
        @Param("type") type: String?,
        @Param("title") title: String?,
        @Param("branchId") branchId: String?,
        @Param("branchType") branchType: String?,
        @Param("page") page: Page<BranchActivityVO>
    ): List<BranchActivityVO>

//    @DS("db_yanan")
    fun selectBranchActivityDetail(@Param("id") id: String): BranchActivityVO
}
