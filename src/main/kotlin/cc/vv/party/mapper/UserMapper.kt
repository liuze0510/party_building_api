package cc.vv.party.mapper;

import cc.vv.party.beans.model.User;
import cc.vv.party.beans.vo.LearningStatisticsVO
import cc.vv.party.beans.vo.ReportStatisticsVO
import cc.vv.party.beans.vo.UserVO
import cc.vv.party.param.ReportStatisticsParam
import cc.vv.party.param.UserListParam
import cc.vv.party.param.UserStatisticsParam
import com.baomidou.dynamic.datasource.annotation.DS
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface UserMapper : BaseMapper<User> {


    fun selectListPage(@Param("page") page: Page<UserVO>, @Param("p") p: UserListParam): List<UserVO>

    /** 获取职业统计 */
    fun selectCareerStatistics(@Param("p") p: UserStatisticsParam): List<LinkedHashMap<String, Integer>>

    /** 获取民族统计 */
    fun selectEthnicStatistics(@Param("p") p: UserStatisticsParam): List<LinkedHashMap<String, Integer>>

    /** 获取性别统计 */
    fun selectSexStatistics(@Param("p") p: UserStatisticsParam): List<LinkedHashMap<String, Integer>>

    /** 获取学历统计 */
    fun selectEducationalLevelStatistics(@Param("p") p: UserStatisticsParam): List<LinkedHashMap<String, Integer>>

    fun selectReportStatistics(@Param("p") p: ReportStatisticsParam): List<ReportStatisticsVO>

    /** 获取年龄统计 */
    fun selectAgeStatistics(@Param("p") p: UserStatisticsParam): List<LinkedHashMap<String, Integer>>

    /** 学习统计 **/
    @DS("db_yanan")
    fun selectLearningStatistics(
        @Param("branchIdList") branchIdList: List<String>,
        @Param("page") page: Page<LearningStatisticsVO>?
    ): List<LearningStatisticsVO>

    @DS("db_yanan")
    fun selectLearningStatisticsYearDetail(
        @Param("branchIdList") branchIdList: List<String>,
        @Param("year") year: String
    ): List<Map<String, String?>>

    @DS("db_yanan")
    fun selectPartyMemberLearningStatisticsYearDetail(
        @Param("id") id: String,
        @Param("year") year: String
    ): List<Map<String, String?>>

    @DS("db_yanan")
    fun selectLearningStatisticsDayDetail(
        @Param("branchIdList") branchIdList: List<String>,
        @Param("year") year: String,
        @Param("month") month: String,
        @Param("startDay") startDay: String,
        @Param("endDay") endDay: String?
    ): List<Map<String, String?>>

    @DS("db_yanan")
    fun selectPartyMemberLearningStatisticsDayDetail(
        @Param("id") id: String,
        @Param("year") year: String,
        @Param("month") month: String,
        @Param("startDay") startDay: String,
        @Param("endDay") endDay: String?
    ): List<Map<String, String?>>
}
