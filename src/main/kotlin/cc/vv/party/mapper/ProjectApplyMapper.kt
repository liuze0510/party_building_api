package cc.vv.party.mapper;

import cc.vv.party.beans.model.ProjectApply
import com.baomidou.mybatisplus.mapper.BaseMapper

/**
 * <p>
 * 项目延期、取消申请 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface ProjectApplyMapper : BaseMapper<ProjectApply>
