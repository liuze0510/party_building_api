package cc.vv.party.mapper;

import cc.vv.party.beans.model.PartyCommitteeAdminRelation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface PartyCommitteeAdminRelationMapper : BaseMapper<PartyCommitteeAdminRelation>
