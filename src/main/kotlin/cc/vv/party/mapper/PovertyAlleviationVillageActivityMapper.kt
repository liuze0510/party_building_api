package cc.vv.party.mapper;

import cc.vv.party.beans.vo.PovertyAlleviationVillageActivityVO
import cc.vv.party.model.PovertyAlleviationVillageActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 脱贫行政村活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface PovertyAlleviationVillageActivityMapper : BaseMapper<PovertyAlleviationVillageActivity> {


    fun selectListPage(@Param("page") page: Page<PovertyAlleviationVillageActivityVO>, @Param("town")town: String?,
                       @Param("village")village: String?, @Param("title")title: String?): List<PovertyAlleviationVillageActivityVO>

}
