package cc.vv.party.mapper;

import cc.vv.party.beans.model.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-13
 */
interface RoleMapper : BaseMapper<Role> {


    fun getByType(@Param("type") type: String): Role

}
