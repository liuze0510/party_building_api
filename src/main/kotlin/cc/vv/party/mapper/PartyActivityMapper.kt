package cc.vv.party.mapper;

import cc.vv.party.beans.vo.PartyActivityVO
import cc.vv.party.model.PartyActivity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 社区大党委活动 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-11-09
 */
interface PartyActivityMapper : BaseMapper<PartyActivity> {

    fun selectListPage(@Param("page") page: Page<PartyActivityVO>, @Param("street") street: String?,
                       @Param("partyId") partyId: String?, @Param("title") title: String?): List<PartyActivityVO>

}
