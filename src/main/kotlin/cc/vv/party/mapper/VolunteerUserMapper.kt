package cc.vv.party.mapper;

import cc.vv.party.beans.model.VolunteerUser;
import cc.vv.party.beans.vo.VolunteerUserVO
import cc.vv.party.param.VolunteerUserListParam
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page
import org.apache.ibatis.annotations.Param

/**
 * <p>
 * 志愿者信息 Mapper 接口
 * </p>
 *
 * @author Gyb
 * @since 2018-10-12
 */
interface VolunteerUserMapper : BaseMapper<VolunteerUser> {

    fun getById(@Param("id") id: String): VolunteerUserVO

    fun selectListPage(@Param("page") page: Page<VolunteerUserVO>, @Param("p") p: VolunteerUserListParam): List<VolunteerUserVO>

}
