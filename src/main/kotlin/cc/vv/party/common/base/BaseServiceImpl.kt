package cc.vv.party.common.base

import com.baomidou.mybatisplus.mapper.BaseMapper
import com.baomidou.mybatisplus.service.impl.ServiceImpl
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description
 **/
open class BaseServiceImpl<MAPPER : BaseMapper<MODEL>, MODEL> : ServiceImpl<MAPPER, MODEL>(), BaseService<MODEL> {


}