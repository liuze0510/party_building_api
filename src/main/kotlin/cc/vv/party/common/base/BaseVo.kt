package cc.vv.party.common.base

import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-10
 * @description
 **/
open class BaseVo : Serializable