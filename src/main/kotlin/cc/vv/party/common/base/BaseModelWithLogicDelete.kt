package cc.vv.party.common.base

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.enums.FieldFill
import com.baomidou.mybatisplus.enums.IdType
import java.io.Serializable
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-15
 * @description
 **/
open class BaseModelWithLogicDelete<T : Model<out Model<*>>> : Model<T>() {

    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    var createTime: Date? = null

    /**
     * 创建用户
     */
    @TableField(value = "create_user")
    var createUser: String? = null

    override fun pkVal(): Serializable? {
        return id
    }
}