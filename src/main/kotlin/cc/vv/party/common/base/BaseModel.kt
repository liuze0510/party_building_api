package cc.vv.party.common.base

import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableLogic
import com.baomidou.mybatisplus.enums.FieldFill
import com.baomidou.mybatisplus.enums.IdType
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-10
 * @description
 **/
open class BaseModel<T : Model<out Model<*>>> : Model<T>() {

    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    var id: String? = null

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    var createTime: Date? = null

    /**
     * 是否逻辑删除，1：删除，0：不删除
     */
    @TableLogic(value = "0", delval = "1")
    @TableField(value = "logic_delete")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var logicDelete: Int? = null

    /**
     * 创建用户
     */
    @TableField(value = "create_user")
    var createUser: String? = null

    override fun pkVal(): Serializable? {
        return id
    }
}