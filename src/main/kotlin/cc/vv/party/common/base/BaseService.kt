package cc.vv.party.common.base

import com.baomidou.mybatisplus.service.IService

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description
 **/
interface BaseService<T> : IService<T>