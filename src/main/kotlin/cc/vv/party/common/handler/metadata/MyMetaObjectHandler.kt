package cc.vv.party.common.handler.metadata

import cn.hutool.core.date.SystemClock
import com.baomidou.mybatisplus.mapper.MetaObjectHandler
import org.apache.ibatis.reflection.MetaObject
import java.util.*

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/9/10
 * @description
 */
class MyMetaObjectHandler : MetaObjectHandler() {

    private val CREATE_TIME_KEY = "createTime"

    override fun insertFill(metaObject: MetaObject) {
        val now = Date()
        if (metaObject.hasSetter(CREATE_TIME_KEY)) {
            setFieldValByName(CREATE_TIME_KEY, now, metaObject)
        }
    }

    override fun updateFill(metaObject: MetaObject) {
     
    }
}
