package cc.vv.party.common.constants

import cc.vv.party.exception.BizException
import commonx.core.json.toJSONObject
import java.io.BufferedInputStream
import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-15
 * @description
 **/
object AdminInfo {

    data class Admin(
        var id: String? = null,
        var name: String? = null,
        var password: String? = null,
        var role: String? = null
    ) : Serializable

    var admin: Admin? = null

    fun login(name: String, password: String): Admin {
        if (admin == null) {
            initAdmin()
        }
        if (name == admin!!.name && admin!!.password == password) {
            return admin!!
        } else {
            throw BizException(StatusCode.LOGIN_PASSWORD_ERROR)
        }
    }

    fun initAdmin() {
        val stream = javaClass.getResourceAsStream("/data/admin.json")
        val buffer = BufferedInputStream(stream).bufferedReader()
        val lines = buffer.readLines()
        val content = lines.joinToString(separator = "")
        admin = content.toJSONObject<Admin>()
    }
}