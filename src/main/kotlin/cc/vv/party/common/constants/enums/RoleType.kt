package cc.vv.party.common.constants.enums

import cc.vv.party.exception.BizException
import com.baomidou.mybatisplus.enums.IEnum
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-13
 * @description
 **/
enum class RoleType(var roleName: String, var description: String) : IEnum, Serializable {
    SUPER_ADMIN("admin", "超级管理员"),
    AREA_ADMIN("area", "区管理员"),
    JDLXHY_ADMIN("jdlxhy", "街道联席会议管理员"),
    TEAM_ADMIN("team", "四支队伍理员"),
    FGDJ_ADMIN("fgdj", "非公党建理员"),

    STREET_ADMIN("street", "街道管理员"),
    SECTION_ADMIN("section", "社区管理员"),
    SECTION_COMMITTEE_ADMIN("committee", "社区大党委管理员"),
    GRID_ADMIN("grid", "网格管理员"),
    UNIT_ADMIN("unit", "单位管理员"),
    ORG_ADMIN("org", "社会组织管理员"),
    BUILDING_TEAM_ADMIN("building", "楼栋党小组管理员"),
    CENTER_ADMIN("center", "单元中心户"),
    BRANCH_ADMIN("branch", "支部管理员"),
    PARTY_MEMBER("member", "党员");

    @JsonValue
    override fun getValue(): Serializable {
        return roleName
    }

    override fun toString(): String {
        return roleName
    }

    companion object {
        @JsonCreator
        fun enumOf(name: String): RoleType {
            return RoleType.values().find { it.roleName == name }
                ?: throw BizException("could not found enum with name ${name}")
        }
    }
}