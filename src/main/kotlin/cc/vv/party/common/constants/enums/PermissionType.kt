package cc.vv.party.common.constants.enums

import com.baomidou.mybatisplus.enums.IEnum
import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-13
 * @description
 **/
enum class PermissionType(var type: String, var description: String) : IEnum, Serializable {
    MENU("menu", "菜单"),
    OPERATE("operate", "操作项");


    override fun getValue(): Serializable {
        return type
    }
}