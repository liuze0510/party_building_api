package cc.vv.party.common.constants

enum class StatusCode constructor(var statusCode: Int, var statusMessage: String) {
    SUCCESS(200, "成功"),
    ERROR(999, "系统异常"),
    FORBIDDEN(403, "权限失败"),
    CREATE_TOKEN_ERROR(999, "Token创建失败"),
    MISSING_REQUIRE_FIELD(1000, "参数缺失"),
    FIELD_VALUE_ERROR(1001, "参数值输入错误"),
    TOKEN_EXPIRED(10002, "Token过期，请重新登录"),
    EXCEPTION_LOGIN_ERROR(10003, "您的账号账号已经在其他地方登陆，请注意账号安全"),
    LOGIN_PASSWORD_ERROR(10100, "密码输入错误！"),

    MESSAGE_NOT_EXIST(100001, "信息不存在！"),
    MESSAGE_EXIST(100002, "信息已经存在！"),

    USER_NOT_HAS_UNIT(100003, "用户没有所属单位"),
    ORG_HAS_MEMBER(100004, "改组织机构下存在人员信息，请先转移或删除人员信息"),
    MAXIMUM_NUMBER_OF_APPLICANTS(200001, "报名人数已达上限！");
}
