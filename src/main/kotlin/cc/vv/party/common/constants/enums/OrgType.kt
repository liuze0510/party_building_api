package cc.vv.party.common.constants.enums

import cc.vv.party.exception.BizException
import com.alibaba.fastjson.annotation.JSONCreator
import com.baomidou.mybatisplus.enums.IEnum
import com.fasterxml.jackson.annotation.JsonValue
import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-16
 * @description 组织机构类型
 * 类型 0 区 1 街道 2 社区 3 网格 4 单位 5 社会组织 6 楼道党小组 7 普通党员 8 单元中心户
 **/
enum class OrgType(var type: Int, desc: String) : IEnum, Serializable {

    AREA(0, "区"),
    STREET(1, "街道"),
    SECTION(2, "社区"),
    GRID(3, "网格"),
    UNIT(4, "单位"),
    ORG(5, "社会组织"),
    BUILDING_TEAM(6, "楼栋党小组"),
    PARTY_MEMBER(7, "党员"),
    CENTER(8, "单元中心户"),
    BRANCH(9, "支部"),
    JDLXHY(10, "街道联席会议"),
    TEAM(11, "四支队伍"),
    FGDJ(12, "非公党建"),
    COMMITTEE(13, "社区大党委");

    @JsonValue
    override fun getValue(): Serializable {
        return type
    }

    override fun toString(): String {
        return type.toString()
    }

    companion object {
        @JSONCreator
        fun enumOf(type: Int): OrgType {
            return OrgType.values().find { it.type == type }
                ?: throw BizException("could not found enum with type $type")
        }
    }
}