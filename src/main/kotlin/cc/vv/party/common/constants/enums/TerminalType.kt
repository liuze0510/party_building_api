package cc.vv.party.common.constants.enums

import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description
 **/
enum class TerminalType(var terminalName: String) : Serializable {

    PC("[PC]"),
    APP("[APP]"),
    WEB("[WEB]");

    override fun toString(): String {
        return terminalName
    }
}