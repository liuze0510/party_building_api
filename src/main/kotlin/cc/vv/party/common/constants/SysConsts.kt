package cc.vv.party.common.constants

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-12
 * @description
 **/
interface SysConsts {

    companion object {
        //session 中延安党建用户信息key
        const val SESSION_KEY_USER_INFO = "session_key_user_info"
        //session 中延安党建用户id key
        const val SESSION_KEY_USER_ID = "session_key_user_id"
        //session 中延安党建用户名称 key
        const val SESSION_KEY_USER_NAME = "session_key_user_name"
        //session 中延安党建用户账号 key
        const val SESSION_KEY_USER_ACCOUNT = "session_key_user_account"
        //session 管理员管理的组织机构key
        const val SESSION_KEY_ADMIN_ORG = "session_key_admin_org"
        //session 根据管理员区分的orgInfo
        const val SESSION_KEY_MANAGE_ORG_INFO = "session_key_manage_org_info"
        //当前用户所在的延安党委组织机构Key
        const val SESSION_KEY_CURRENT_BRANCH: String = "session_key_current_branch"
        //        const val SESSION_KEY_USER_ORG_TREE = "session_key_user_org_tree"
        const val SESSION_KEY_DEVICE = "session_key_device"
        const val SESSION_KEY_USER_ROLE = "session_key_user_role"
        const val SESSION_KEY_APP_USER_ORG = "session_key_app_user_org"
        const val SESSION_KEY_LOCAL_USER_INFO = "session_key_local_user_info"
        const val SESSION_KEY_YANAN_ROLE_ID = "session_key_yanan_role_id"

        const val REGEX_MOBILE = "^[1][3,4,5,7,8][0-9]{9}$"
        const val REGEX_PHONE =
            "(?:(\\(\\+?86\\))(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)|(?:(86-?)?(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)"
        const val REGEX_PHONE_MOBILE =
            "(?:(\\(\\+?86\\))(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)|(?:(86-?)?(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?)|^[1][3,4,5,7,8][0-9]{9}\$"

        val NOT_REPORTED = Integer(1)
        val REPORTED = Integer(0)

        const val CONST_0 = 0
        const val CONST_1 = 1

        //延安宝塔区的编号Id
        const val REGION_ID = "610602"
        //宝塔区的orgId
        const val BAOTA_AREA_ID = "d68ce7986f00482eaad379b09ce4b027"

        /** 三会一课 */
        const val THREE_LESSON_CONTENT_URL = "files/txt/threelessons/"

        /** 党规党章 */
        const val PARTY_CONSTITUTION_CONTENT_URL = "files/txt/partycons/"

        /** 讲话系列 */
        const val SERIES_SPEECH_CONTENT_URL = "files/txt/serspeech/"

        /** 微百科 */
        const val OPEN_CLASS_CONTENT_URL = "files/txt/openclass/"

        const val BRANCH_ACTIVITY_CONTENT_URL = "files/txt/branchactivitie/"

    }

    //党支部类型
    interface BranchType {
        companion object {
            //区县
            const val REGION_LEVEL_DISTRICT = "rld"
            //党委
            const val PARTY_COMMITTEE = "PARTYCOMMITTEE"
            //党工委
            const val PARTY_WORK_COMMITTEE = "PARTYWORKCOMMITTEE"
            //党总支
            const val PARTY_TOTAL_BRANCH = "PARTYTOTALBRANCH"
            //党支部
            const val PARTY_BRANCH = "PARTY"
        }
    }

}