package cc.vv.party.common.wrapper

import java.io.Serializable

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018/8/9
 * @description
 **/
data class PageWrapper<T>(
        var current: Int = 0,
        var total: Long = 0,
        var size: Int = 0,
        var pages: Int = 0,
        var records: MutableList<T>? = null
) : Serializable