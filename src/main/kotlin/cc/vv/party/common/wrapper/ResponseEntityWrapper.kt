package cc.vv.party.common.wrapper

import cc.vv.party.common.constants.StatusCode


@Suppress("UNCHECKED_CAST")
class ResponseEntityWrapper<T> {

    var statusCode: Int? = StatusCode.SUCCESS.statusCode
    var statusMessage: String? = StatusCode.SUCCESS.statusMessage
    var data: T? = null

    constructor() {}

    private constructor(builder: Builder) {
        this.statusCode = builder.c
        this.statusMessage = builder.m
        this.data = builder.o as T?
    }

    private class Builder {

        var c: Int = 0
        var m: String? = null
        var o: Any? = null

        fun statusCode(code: Int): Builder {
            this.c = code
            return this
        }

        fun statusMessage(message: String): Builder {
            this.m = message
            return this
        }

        fun <T> data(data: T): Builder {
            this.o = data
            return this
        }

        fun <T> with(statusCode: StatusCode, data: T): Builder {
            return statusMessage(statusCode.statusMessage)
                    .statusCode(statusCode.statusCode)
                    .data(data)
        }

        fun <T> build(): ResponseEntityWrapper<T> {
            return ResponseEntityWrapper(this)
        }
    }

    companion object {

        fun <T> success(data: T?): ResponseEntityWrapper<T> {
            return Builder().statusCode(StatusCode.SUCCESS.statusCode)
                    .statusMessage(StatusCode.SUCCESS.statusMessage)
                    .data(data)
                    .build()
        }

        fun <T> error(code: Int, message: String): ResponseEntityWrapper<T> {
            return Builder().statusCode(code)
                    .statusMessage(message)
                    .build()
        }

        fun <T> error(statusCode: StatusCode): ResponseEntityWrapper<T> {
            return error(statusCode.statusCode, statusCode.statusMessage)
        }
    }
}
