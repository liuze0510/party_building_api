package cc.vv.party.common.security

import cc.vv.party.beans.model.LoginController
import cc.vv.party.common.constants.StatusCode
import cc.vv.party.exception.BizException
import com.baomidou.mybatisplus.mapper.EntityWrapper
import org.apache.commons.lang.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * @version 1.0.0
 * @author: Gyb
 * @date 2018-10-11
 * @description
 **/
class SessionInvalidSecurityInterceptor : HandlerInterceptor {

    private val logger = LoggerFactory.getLogger(SessionInvalidSecurityInterceptor::class.java)

    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        logger.info(request.requestURI)

//        val platform = request.getHeader("platform")
//        val token = request.getHeader("X-Auth-Token")
//        if(StringUtils.isNotBlank(platform) && "APP".equals(platform, true)){
//            LoginController().selectOne(EntityWrapper<LoginController>().where("token = {0}", token))
//                    ?: throw BizException(StatusCode.EXCEPTION_LOGIN_ERROR)
//        }

        if (request.session.isNew) {
            throw BizException(StatusCode.TOKEN_EXPIRED)
        }
        return true
    }
}